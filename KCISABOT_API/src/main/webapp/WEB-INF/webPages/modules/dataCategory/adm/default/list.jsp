<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />
<style>
.jstree-open > .jstree-anchor > .fa-folder:before { content: "\f07c"; }
.jstree-default .jstree-icon.none { width: 0; }
</style>
						<div class="row">
							<div class="col-3 bg-fusion-400 bg-fusion-gradient" style="padding-left:0px; padding-right:0px; min-height:80vh;">
								<div class="col-lg-12 pd20" id="jstreeWrap">
									<div id="jstree1">
										<ul>
											<c:forEach items="${categoryList1}" var="rootCatModel" varStatus="x">
												<li data-data="${efn:encrypt(rootCatModel.categoryIdx)}" data-lvl="${efn:encrypt(rootCatModel.categoryLevel)}" data-path="${rootCatModel.categoryFullName}" class="jstree-open">${rootCatModel.categoryName}
													<ul>
														<c:forEach items="${categoryList2}" var="catModel" varStatus="i">
															<li data-data="${efn:encrypt(catModel.categoryIdx)}" data-lvl="${efn:encrypt(catModel.categoryLevel)}" data-path="${catModel.categoryFullName}"<c:if test="${i.first}"> class="jstree-open"</c:if>>${catModel.categoryName}
														</c:forEach>
													</ul>
												</li>
											</c:forEach>
										</ul>
									</div>
									<form id="fn_searchFrm" action="dataCategoryListAjax.do?cmnx=${cmnx}">
										<input type="hidden" name="cmnx" value="${cmnx}" />
										<input type="hidden" name="idx" value="${idx }" />
									</form>
									<form id="fn_deleteFrm" action="dataCategoryDelProc.do?cmnx=${cmnx}">
										<input type="hidden" name="cmnx" value="${cmnx}" />
										<input type="hidden" name="idx" value="" />
										<input type="hidden" name="parentCategoryIdx" value="" />
									</form>

								</div>
							</div>
							<div class="col-9" style="padding:0 20px 20px 20px;"  id="sortable-category">
								<div id="catWrap" class="mt-n4">
									<div class="col-lg-12 py-4" id="categoryWrap"></div>
								</div>
								<div class="col-lg-12">
									<div class="mgt30">
										<button type="button" class="btn4openModal btn btn-outline-dark btn-lg btn-block waves-effect waves-themed" data-toggle="modal" data-target="#class_make"><i class="fal fa-plus-circle"></i> &nbsp; 추가</button>
									</div>
								</div>
							</div>
						</div>

						<%-- 데이터카테고리 수정 모달 --%>						
						<%@ include file = "./inc/inc_list_modal.jsp" %>
						<%-- //데이터카테고리 수정 모달 --%>
<script>
const $apiUrl = "<spring:eval expression="@environment.getProperty('univ_tbots.Main.Root.Path')" />/dataCategory/";
const $searchFrm = $('#fn_searchFrm'), $deleteFrm = $('#fn_deleteFrm');
var $jstreeId, $currentIdx;
var $delIdx, $delParent;
$(document).ready(function(){
	$('#jstree1').jstree({ 'core' : { 'check_callback' : true }, 'plugins' : [ 'types' ], 'types' : { 'default' : { 'icon' : 'fal fa-folder' }, } });
	$('a.jstree-anchor').eq(0).click();
})
function fn_setPanel(){
	$('#js-page-content').smartPanel({
		localStorage : false,
		onChange : function(e) {
			var $panel = $('.panel-sortable');
			var $categoryArr = new Array();
			$($panel).each(function(i, e){
  			var category = new Object();
				category.idx = $(e).data("data");
				category.order = i + 1;
				$categoryArr.push(category);
			})
			
			$('#js-page-content').smartPanel("destroy");
			fn_ajax( { url : 'modifyOrder.do?cmnx=${cmnx}', data : { categoryOrder : JSON.stringify($categoryArr) }, type : 'POST' } );
		}
	})
}

function fn_setList(idx){
	$('#js-page-content').smartPanel("destroy");
	$searchFrm.find('input[name=idx]').val(idx);
	fn_ajax( { url : $searchFrm.attr('action'), data : $searchFrm.serialize() } );
}
<%-- 분류 선택 이벤트 --%>
$(document).on('click', 'a.jstree-anchor', function() {
	$jstreeId = $('#' + ($(this).attr('id')).replace('_anchor', ''));
	var $data = $($jstreeId).data('data');
	fn_setList($data);
});
<%-- 모달 오픈 이벤트 > 위치 셀렉트 분류 셋팅 --%>
$('.modal4cat').on('show.bs.modal', function() {
	fn_ajax( { url : 'selectCategoryListAjax.do?cmnx=${cmnx}' } );
});
<%-- 수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.modi-open', function(){
	var $data = $(this).data('data');
	fn_ajax( { url : 'dataCategoryInfoAjax.do?cmnx=${cmnx}', data : { idx : $data } } );
});
<%-- 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4del', function() {
	const $data = $(this).data('data');
	const $success = function() {
		var $action = 'dataCategoryDelProc.do?cmnx=${cmnx}';
		fn_ajax( { url : $action, data : { q : $data } } );
	}
	var $opt = { title : '알림', msg : '${_commonCatDatsetDelMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
});
</script>						
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var $source = '<option value="">선택하세요.</option>';
var $idx = 0;
fn_ajax( { url : '/decrypt.do', data : { source : $jstreeId.data('data') }, dataType : 'text', success : function(data) { $idx = data; } })
<%-- 위치 > 데이터 분류 리스트 --%>
<c:forEach var="category" items="${list }">
	<c:set var="data" value="${category.orderNo }|${category.parentCategoryIdx}|${category.categoryLevel}|${category.categoryIdx}" />
	<c:set var="encData" value="${efn:encrypt(data)}" />
$source += '<option value="${encData}"' + ($idx == '${category.categoryIdx}' ? ' selected' : '') + ' data-role="${category.categoryLevel}">${category.category}</option>';
</c:forEach>

$('.order-select').html($source);

$source = null;
$idx = null;
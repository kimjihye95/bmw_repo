<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '';

$('#jstree').remove();
$('#jstreeWrap').html('');

varSource += '<div id="jstree">';
varSource += '<ul>';

<c:forEach items="${categoryList1}" var="rootCatModel" varStatus="x">
varSource += '<li data-data="${efn:encrypt(rootCatModel.categoryIdx)}" data-role="${rootCatModel.orderNo}" data-lvl="${rootCatModel.categoryLevel}" data-path="${rootCatModel.categoryFullName}" class="jstree-open">${rootCatModel.categoryName}';
varSource += '<ul>';
	<c:forEach items="${categoryList2}" var="catModel" varStatus="i">
varSource += '<li data-data="${efn:encrypt(catModel.categoryIdx)}" data-role="${catModel.orderNo}" data-lvl="${catModel.categoryLevel}" data-path="${catModel.categoryFullName}"<c:if test="${i.first}"> class="jstree-open"</c:if>>${catModel.categoryName}';
	</c:forEach>
varSource += '</ul>';
varSource += '</li>';
</c:forEach>
	
varSource += '</ul>';
varSource += '</div>';

$('#jstreeWrap').append(varSource);
$('#jstree').jstree({ 'core' : { 'check_callback' : true }, 'plugins' : [ 'types' ], 'types' : { 'default' : { 'icon' : 'fal fa-folder' }, } });
$("#jstree").jstree("open_all");
$('a.jstree-anchor:eq(0)').click();

varSource = null;
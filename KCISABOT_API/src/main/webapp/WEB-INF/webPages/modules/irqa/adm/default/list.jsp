<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />

		<div class="row">
			<div class="col-lg-12">
				<c:if test="${empty list}">
					<div class="panel p-5 text-center">${_infoNoDataMsg_}</div>
				</c:if>
				<c:forEach items="${list}" var="model" varStatus="i">
					<div id="panel-${status.count}" class="panel pd20">
						<div class="panel-hdr" style="border-bottom:0px;">
							<h2><i class="fal fa-check text-warning mr-2"></i> ${efn:strContentsFormat(model.botName, '')} &nbsp;ㅣ&nbsp; <small style="font-size:90%;">${efn:strContentsFormat(model.botId, '')}</small>
								<div class="d-flex position-relative ml-auto" style="max-width: 15rem;">
									<c:set var="data" value="${model.botId}_${model.botName}" />
									<button class="btn4submit btn btn-warning btn-lg btn-icon rounded-circle waves-effect waves-themed text-white" data-data="${efn:encrypt(data)}"><i class="fal fa-pen"></i></button>
								</div>
							</h2>
						</div>
					</div>
				</c:forEach>
	
			<form id="gotoIrqaList" action="irqaList.do" method="post" >
				<input type="hidden" name="cmnx" value="${cmnx}">
				<input type="hidden" name="botId" value="">
				<input type="hidden" name="botName" value="">
			</form>
	
			<div class="mgt30" style="text-align:center;">
				<div id="paging"></div>
			</div>
		</div>
	</div>
		
<script>
<%-- 페이지 처리 --%>
var varOpt = {url : '${URL_PAGE_LIST}', obj : 'paging', total : '${pageTotalPage}', visible : '${blockSize}'};
fn_setPaging(varOpt);
$(document).on('click', '.btn4submit', function() {
	const $frm = $('<form/>', { 'id' : 'frm', 'action' : 'irqaList.do?cmnx=${cmnx}', 'method' : 'post'});
	const $data = $(this).data('data');
	$frm.append($('<input/>', {'name' : 'q', 'value' : $data}));
	$('body').append($frm);
	$frm.submit();
	$frm.remove();
});
</script>
<iframe id="targetIfrm" name="targetIfrm" style="width:0px;height:0px;" frameborder="0" title="iframe"></iframe>
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />
	<div class="alert alert-primary alert-dismissible">
		<div class="row">
			<div class="col-lg-12">
				<form id="fn_searchFrm" action="irqaViewAjax.do" method="POST">
					<input type="hidden" name="cmnx" value="${cmnx}" />
					<input type="hidden" name="botId" value="${efn:encrypt(botId)}" />
					<input type="hidden" name="botIdx" value="${efn:encrypt(botIdx)}" />
					<input type="hidden" name="slmt" value="" />
					<input type="hidden" name="schk" value="" />
					<div class="input-group">
						<!-- <div class="custom-control custom-checkbox custom-control-inline" style="padding-top:10px;">
							<input type="checkbox" class="custom-control-input" id="defaultInline1" checked="">
							<label class="custom-control-label" for="defaultInline1">질문</label>
						</div>
						<div class="custom-control custom-checkbox custom-control-inline" style="padding-top:10px;">
							<input type="checkbox" class="custom-control-input" id="defaultInline2" checked="">
							<label class="custom-control-label" for="defaultInline2">답변</label>
						</div> -->
						<input type="text" class="form-control" name="skey" placeholder="검색어를 입력하세요." aria-label="Recipient's username" aria-describedby="button-addon5">
						<div class="input-group-append">
							<button type="button" class="btn btn-warning text-white waves-effect waves-themed" id="button-addon5"><i class="fal fa-search"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6 justify-content-start">
			<p class="mgb0" style="line-height:38px;">Total. <span class="text-warning" id="totalCount">0건</span> <span class="ml-5"><a href="#" class="underline" id="templateDown">엑셀 양식 다운로드</a></span></p>
		</div>
		<div class="col-lg-6 justify-content-end">
			<div class="dt-buttons" style="text-align:right;">
				<button type="button" class="btn btn-warning waves-effect waves-themed text-white direct-add" data-toggle="modal" data-target="#irqa_make">직접등록</button>
				<button type="button" class="btn4addExcel btn btn-success waves-effect waves-themed" data-toggle="modal" data-target="#excel_make">엑셀등록</button>
				<button type="button" class="btn btn-info waves-effect waves-themed" id="export">내보내기</button>
				<button type="button" class="btn4del btn btn-dark waves-effect waves-themed">삭제</button>
				<div class="btn-group mgl10">
					<select class="form-control" name="slmt">
						<c:forEach begin="1" end="3" var="no">
							<option value="${efn:encrypt(no * 10)}"<c:if test="${param.slmt eq no * 10}"> selected</c:if>>${no * 10}건씩 조회</option>
						</c:forEach>
					</select>
				</div> 
				<button type="button" class="btn btn-outline-default waves-effect waves-themed" id="select4count"><span>변경</span></button>
				<button type="button" class="btn btn-danger waves-effect waves-themed" id="retainBtn">재교육</button>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 mgt20" style="padding:0px;">
			<form id="fn_delFrm" action="irqaDelMultiProc.do?cmnx=${cmnx}">
				<div id="viewDiv"></div>
			</form>
			<div class="mgt30" style="text-align:center;">
				<div id="paging-view"></div>
			</div>
		</div>
	</div>
	
	<!-- 등록 모달 -->
	<%@ include file = "./inc/inc_modal.jsp" %>	
	
	<!-- 엑셀 등록 모달 -->
	<%@ include file = "./inc/inc_modal_excel.jsp" %>		
	
<script>
var $frm = $('#fn_searchFrm');
//var $paraphraseList = {};

$(document).ready(function() {
	$('#button-addon5').click();
});

<%-- 검색 버튼 클릭 이벤트 --%>
$(document).on('click', '#button-addon5', function() {
	var $field = { '0' : '', '1' : 'question', '2' : 'answer', '3' : '' };
	var $action = $frm.attr('action');
	$('input[name="slmt"]').val( $('select[name="slmt"] option:selected').val() );
	var checked = 0;
	checked += $('input[id="defaultInline1"]').is(':checked') ? 1 : 0;
	checked += $('input[id="defaultInline2"]').is(':checked') ? 2 : 0;
	
	$('input[name="schk"]').val($field[checked]);
	var $data = $frm.serialize();
	fn_ajax({ url : $action, data : $data});
});

<%-- 검색어 엔터키 이벤트 --%>
$(document).on('keypress', 'input[name=skey]', function(e) {
	if (e.keyCode == 13) {
		$('#button-addon5').click();
		return false;
	}
});

<%-- 조회 건수 변경 이벤트 --%>
$(document).on('click', '#select4count', function() {
	$('#button-addon5').click();
});

<%-- 직접등록 모달 오픈 --%>
$(document).on('click', '.direct-add', function(){
	$('#irqa_make').find('textarea[id="irqa_question"]').val('');
	$('#irqa_make').find('textarea[id="irqa_answer"]').val('');
});

<%-- 수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4mod', function() {
	var $url = 'irqaDetailAjax.do?cmnx=${cmnx}', $data = $(this).data('data');
	fn_ajax( { url : $url, data : { q : $data } } );
});

<%-- 한 건 삭제 --%>
$(document).on('click', '.set-delete', function() {
	var question = $($(this).parent().prev('.position-relative').children('.qa')[0]).data('data');
	var representQuerySeq = $(this).data('data');
	
	var $success = function() {
		var $action = 'irqaDelProc.do?cmnx=${cmnx}';
		var $id = $frm.find('[name=botId]').val(), $idx = $frm.find('[name=botIdx]').val();
		var $data = { representQuerySeq : representQuerySeq, botId : $id, botIdx : $idx };
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
	
	var $opt = { title : '알림', msg : '<b class="text-danger">' + question.toString().trim() + '</b>&nbsp;${_confirmDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
});

<%-- 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4del', function() {
	var $delFrm = $('#fn_delFrm');
	var $id = $frm.find('[name=botId]').val(), $idx = $frm.find('[name=botIdx]').val();
	if (!fn_checkCboxChecked('irqaCboxs')) return false;

	var $success = function() {
		var $action = $delFrm.attr('action');
		var $data = $delFrm.serialize();
		$data += '&botId=' + $id + '&botIdx=' + $idx;
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
	
	var $opt = { title : '알림', msg : '${_confirmDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
	
	return false;
});

<%-- 재교육 --%>
$(document).on('click', '#retainBtn', function() {
	var $action = 'irqaRetrain.do?cmnx=${cmnx}';
	var $id = $frm.find('[name=botId]').val(), $idx = $frm.find('[name=botIdx]').val();
	var $data = { botId : $id, botIdx : $idx };
	fn_ajax({ url : $action, data : $data, type : 'POST' });
});

<%-- 내보내기 --%>
$(document).on('click', '#export', function() {
	var $action = 'irqaExport.do';
	var $id = $frm.find('[name=botId]').val(), $idx = $frm.find('[name=botIdx]').val();
	var $data = { cmnx : '${cmnx}', botId : $id, botIdx : $idx, botName : '${botName}' };
	fn_ajax({ url : $action, data : $data, type : 'POST' });
});

<%-- 엑셀 양식 다운로드 --%>
$(document).on('click', '#templateDown', function() {
	var $action = 'irqaExcelTmplDown.do';
	var $id = $frm.find('[name=botId]').val(), $idx = $frm.find('[name=botIdx]').val();
	var $data = { cmnx : '${cmnx}', botId : $id, botName : '${botName}' };
	fn_ajax({ url : $action, data : $data, type : 'POST' });
});

<%-- 엑셀등록 --%>
$(document).on('click', '.btn4addExcel', function() {
	$('#fileUpload').val('');
	$('#excel_make').css('zIndex', '2060');
	$('.modal-backdrop').css('z-index', '2051');
});

<%-- 엑셀 등록 모달 - 닫기 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4addExcelClose', function() {
	$('.modal-backdrop').css('z-index', '2040');
});

<%-- 리다이렉트 --%>
function redirectList() {
	location.href='list.do?cmnx=${cmnx}';
}

<%-- 코드 -> 특수문자 --%>
function changeCnvrToSpclStr( dataStr ) {
	return dataStr.replace( /&lt;/g, '<' )
			.replace( /&gt;/g, '>' )
			.replace( /&#40;/g, '(' )
			.replace( /&#41;/g, ')' )
			.replace( /&nbsp;/g, ' ' )
			.replace( /&quot;/g, '"' )
			.replace( /&#x27;/g, '\'' )
			.replace( /&#x2F;/g, '/' )
			.replace( /&#x124;/g, '|' )
			.replace( /&#35;/g, '#' )
			.replace( /&amp;/g, '&' );
}

<%-- 특수문자 -> 코드 --%>
function changeSpclStrToCnvr( dataStr ) {
	return dataStr.replace( /&/g, '&amp;' )
			.replace( /</g, '&lt;' )
			.replace( />/g, '&gt;' )
			.replace( /#/g, '&#35;' )
			.replace( /\(/g, '&#40;' )
			.replace( /\)/g, '&#41;' )
			.replace( /\"/g, '&quot;' )
			.replace( /\'/g, '&#x27;' )
			//.replace( / /g, '&nbsp;' )
			.replace( /\//g, '&#x2F;' )
			.replace( /\|/g, '&#x124;' );
}

</script>

<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
<iframe id="targetIfrm" name="targetIfrm" style="width:0px;height:0px;" frameborder="0" title="iframe"></iframe>
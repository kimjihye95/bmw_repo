<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
<%pageContext.setAttribute("replaceChar", "\r\n"); %>
var $source = '', $paraphraseSeq = 0;
var $paraphraseList = ${paraphraseList};

$irqaModFrm.find('textarea[name=irqa_question]').val(changeCnvrToSpclStr('${model.question}').replace( /<br>/g, '\n' ));
$irqaModFrm.find('textarea[name=irqa_answer]').val(changeCnvrToSpclStr('${model.answer}').replace( /<br>/g, '\n' ));

var $list = $($paraphraseList).sort(function(a, b) { return a.paraphraseSeq > b.paraphraseSeq ? -1 : a.paraphraseSeq < b.paraphraseSeq ? 0 : 1});
$.each($list, function( idx, data) {
	if (idx == 0) $paraphraseSeq = data.paraphraseSeq;
	$source += '<div class="panel p-1">';
	$source += '<div class="panel-hdr" style="border-bottom:0px;">';
	$source += '<div class="d-flex position-relative"><span>' +  changeCnvrToSpclStr(data.question) + '</span></div>';
	$source += '<div class="d-flex position-relative ml-auto">';
	$source += '<button type="button" class="btn btn-warning  btn-icon rounded-circle waves-effect waves-themed text-white btn4smod" data-data="' + data.encSeq + '" title="수정"><i class="fal fa-pen"></i></button>';
	$source += '<button type="button" class="btn btn-danger  btn-icon rounded-circle waves-effect waves-themed text-white mgl10 btn4sdel" data-data="' + data.encSeq + '" title="삭제"><i class="fal fa-times"></i></button>';
	//$source += '<button type="button" class="btn btn-warning  btn-icon rounded-circle waves-effect waves-themed text-white" data-data="' + data.encSeq + '" title="수정" onclick="fn_change(this)" ><i class="fal fa-pen"></i></button>';
	//$source += '<button type="button" class="btn btn-danger  btn-icon rounded-circle waves-effect waves-themed text-white mgl10" data-data="' + data.encSeq + '" data-toggle="tooltip" data-offset="0,10" data-original-title="Close" title="삭제" onclick="fn_hidden(' + data.paraphraseSeq + ')"><i class="fal fa-times"></i></button>';
	$source += '</div>';
	$source += '</div>';
	$source += '</div>';
});
$irqaModFrm.find('input[name="representQuerySeq"]').val('${efn:encrypt(model.representQuerySeq)}');
$irqaModFrm.find('div[id="parapViewDiv"]').html($source);
$irqaModFrm.find('#parap-input').val('');

$source = null;
$paraphraseList = null;
$list = null;

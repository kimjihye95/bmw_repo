<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
table.excelScroll { width: 100%; border-spacing: 0; }
table.excelScroll tbody,
table.excelScroll thead { width:100%; display: block; }
thead tr th { height: 30px; line-height: 30px; }
table.excelScroll tbody { overflow-y: auto; overflow-x: hidden; }
</style>

<!-- 엑셀 등록 -->
<div class="modal fade" id="excel_make" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="min-width:95%;">
		<div class="modal-content">
			<div class="modal-header inno">
				<h5 class="modal-title">엑셀등록</h5>
				<button type="button" class="btn4addExcelClose close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<div class="alert alert-primary alert-dismissible">
					<div class="mgb5">
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" class="custom-control-input" id="defaultInline2Radio" name="isDel" value="N" checked="">
							<label class="custom-control-label" for="defaultInline2Radio">추가(기존 데이터 유지)</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
							<input type="radio" class="custom-control-input" id="defaultInline1Radio" name="isDel" value="Y">
							<label class="custom-control-label" for="defaultInline1Radio">갱신(기존데이터 삭제 후 등록)</label>
						</div>
					</div>
					<form name="fn_excelFrm" id="fn_excelFrm" method="post" enctype="multipart/form-data">
						<input type="hidden" name="cmnx" value="${cmnx}" />
						<input type="hidden" name="botId" value="${efn:encrypt(botId)}" />
						<input type="hidden" name="botIdx" value="${efn:encrypt(botIdx)}" />
						<input type="hidden" name="isDelete" value="">
						<div class="row mgt10">
							<div class="input-group">
								<input type="file" class="form-control" accept=".xlsx, .xls" id="fileUpload" name="fileUpload">
								<div class="input-group-append">
									<button class="btn btn-warning text-white waves-effect waves-themed" type="button" id="button-attach">파일첨부</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer pdt0">
					<button type="button" class="btn4excelSubmit btn btn-warning text-white">등록</button>
					<button type="button" class="btn4addExcelClose btn btn-secondary" data-dismiss="modal">취소</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
<%-- 파일첨부 버튼 클릭 이벤트 --%>
$(document).on('click', '#button-attach', function(){
	$('#fileUpload').click();
});

<%-- 등록 --%>
$(document).on('click', '.btn4excelSubmit', function() {
	$('#fn_excelFrm').find('input[name="isDelete"]').val( $('input[name=isDel]:checked').val() );
	var reg = /(.*?)\.(xlsx|xls)$/;
	var formData = new FormData($('#fn_excelFrm')[0]);
	var uploadFile = $("input[name=fileUpload]").val();
	
	if (fn_isNull(uploadFile)) {
		fn_openSweetAlert({ title : '알림', msg : '파일을 선택하여 주십시오.', icon : 'warning' });
		return false;
	}
	
	var reg = /(.*?)\.(xlsx|xls)$/;
	if (!uploadFile.match(reg)) {
		fn_openSweetAlert({ title : '알림', msg : '엑셀 파일 (.xls, .xlsx) 만 등록 가능합니다.', icon : 'warning' });
		$("input[name=fileUpload]").val('');
		return false;
	}
	
	$.ajax({
		type: "POST"
		,url: "irqaImport.do"
		,dataType : "script"
		,enctype: 'multipart/form-data' // 필수
		,data: formData // 필수
		,processData: false // 필수 
		,contentType: false // 필수
		,beforeSend : function(request) {
			request.setRequestHeader('ajax', 'true');
		}
		,success : function(response) {}
		,error : function(e) {
			console.log(e);
		}
	});
});
</script>
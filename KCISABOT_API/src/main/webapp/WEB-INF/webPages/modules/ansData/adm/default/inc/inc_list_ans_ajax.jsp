<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '';
$path = $($jstreeId).data('path')
$('#ansDataNoListDiv').html('');

<c:set var="URL_PAGE_LIST" value="${fn:replace(URL_PAGE_LIST, 'list.do', 'listAns.do')}" />
<c:if test="${empty list}">
$('#ansDataNoListDiv').html('<div class="card w-100 pd20 text-center">${_infoNoDataMsg_}</div>');
</c:if>

varSource += '<c:forEach items="${list}" var="ansModel" varStatus="i">'
<c:set var="data" value="${ansModel.categoryIdx}_${ansModel.datasetIdx}_${ansModel.datasetName}_${path}" />
varSource += '				<div class="col-xl-4<c:if test='${i.count gt 3}'> mt-3</c:if>">';
varSource += '					<div class="card pd20">';
varSource += '						<a href="#" data-data="${efn:encrypt(data)}" data-title="${efn:strContentsFormat(ansModel.datasetName, '')}" class="answer_show">';
varSource += '							<div class="card-body">';
varSource += '								<p class="mgb5 text-dark category-path"><c:if test="${fn:indexOf(ansModel.category, 'ROOT') lt 0}">ROOT > </c:if>${ansModel.category}</p>';
varSource += '								<h5 class="card-title">${efn:strContentsFormat(ansModel.datasetName, '')}</h5>';
varSource += '								<p class="card-text text-dark">${ansModel.description eq null and ansModel.description eq '' ? '&nbsp;' : ''}</p>';
varSource += '							</div>';
varSource += '							<div class="card-footer">';
varSource += '								<p class="text-muted mgb5">등록 : ${ansModel.regiIp} <fmt:formatDate value="${ansModel.regiDate}" pattern="yyyy-MM-dd"/></p>';
varSource += '								<p class="text-muted mgb0">최종 : ${ansModel.modiIp} <fmt:formatDate value="${ansModel.modiDate}" pattern="yyyy-MM-dd"/></p>';
varSource += '							</div>';
varSource += '						</a>';
varSource += '					</div>';
varSource += '				</div>';
varSource += '</c:forEach>'

$('#ansDataListDiv').html(varSource);
$('.totalCount').html(${count}+"건");
//$('.category-path').html($path);

// set paging
$isLoad = true;
var $pagingId = 'paging';
<c:if test="${empty param.page}">
if (!fn_isNull( $('#' + $pagingId).data("twbs-pagination") )) $('#' + $pagingId).twbsPagination('destroy');
var varOpt = {obj : $pagingId, action : '${URL_PAGE_LIST}', param: '&'+$searchFrm.serialize(), idx : '${param.idx}', total : ${pageTotalPage}, visible : ${blockSize}};
fn_setPaging(varOpt);
varOpt = null;
</c:if>
$isLoad = false;
varSource = null;
$pagingId = null;
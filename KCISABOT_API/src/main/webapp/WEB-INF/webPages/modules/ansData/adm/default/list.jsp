<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />
						<div class="row">
							<div class="col-3 bg-fusion-400 bg-fusion-gradient" style="padding-left:0px; padding-right:0px; min-height:80vh;">
								<div class="col-lg-12 pd20"  style="background:#111;">
									<div class="custom-control custom-checkbox">
										<c:set var="modelItem" value="ishide" />
										<input type="checkbox" id="${modelItem}" value="${efn:encrypt('1') }" class="custom-control-input" checked>
										<label class="custom-control-label" for="${modelItem}">하위 분류 내 데이터 조회 안함</label>
									</div>
								</div>
								<div class="col-lg-12 pd20">
									<div id="jstree">
										<ul>
											<c:forEach items="${categoryList1}" var="rootCatModel" varStatus="x">
												<li data-data="${efn:encrypt(rootCatModel.categoryIdx)}" data-lvl="${efn:encrypt(rootCatModel.categoryLevel)}" data-path="${rootCatModel.categoryFullName}" class="jstree-open">${rootCatModel.categoryName}
													<ul>
														<c:forEach items="${categoryList2}" var="catModel" varStatus="i">
															<li data-data="${efn:encrypt(catModel.categoryIdx)}" data-lvl="${efn:encrypt(catModel.categoryLevel)}" data-path="${catModel.categoryFullName}"<c:if test="${i.first}"> class="jstree-open"</c:if>>${catModel.categoryName}
															
															<c:set var="subCatCnt" value="1" />
															<c:forEach items="${categoryList3}" var="subCatModel" varStatus="j">
																<c:if test="${catModel.categoryIdx eq subCatModel.parentCategoryIdx}">
																	<c:if test="${subCatCnt eq 1}">
																	<ul>
																	</c:if>
																		<li data-data="${efn:encrypt(subCatModel.categoryIdx)}" data-lvl="${efn:encrypt(subCatModel.categoryLevel)}" data-path="${subCatModel.categoryFullName}">${subCatModel.categoryName}</li>
																	<c:if test="${subCatModel.categoryCnt eq subCatCnt}">
																	</ul>
																	</c:if>
																	<c:set var="subCatCnt" value="${subCatCnt + 1}" />
																</c:if>
															</c:forEach>
															
														</c:forEach>
													</ul>
												</li>
											</c:forEach>
										</ul>
									</div>

								</div>
							</div>
							<div class="col-9" style="padding:0 20px 20px 20px;">
								<div class="alert alert-primary alert-dismissible">
									<p style="padding-left:0.75rem;" id="category-path"></p>
									<form id="fn_searchFrm" action="listAns.do?cmnx=${cmnx}" target="targetIfrm">
										<input type="hidden" name="cmnx" value="${cmnx}" />
										<input type="hidden" name="idx" value="" />
										<input type="hidden" name="slvl" value="" />
										<input type="hidden" name="slmt" value="" />
										<input type="hidden" name="q" value="" />
										<div class="row">
											<div class="col-3">
												<c:set var="modelItem" value="stype" />
												<select name="${modelItem}" id="${modelItem}" class="form-control">
													<option value="">전체</option>
													<option value="${efn:encrypt('n')}">데이터셋 명</option>
													<option value="${efn:encrypt('r')}">등록일</option>
												</select>
											</div>
											<div class="col-9">
												<div class="input-group">
													<c:set var="modelItem" value="skey" />
													<input type="text" name="${modelItem}" id="list_${modelItem}" class="form-control" placeholder="검색어를 입력하세요.">
													<div class="input-group-append">
														<button type="button" class="btn4search btn btn-warning text-white waves-effect waves-themed"><i class="fal fa-search"></i></button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>

								<div class="row">
									<div class="col-6 justify-content-start">
										<p class="mgb0" style="line-height:38px;">Total. <span class="text-warning totalCount">0건</span></p>
									</div>
									<div class="col-6 justify-content-end">
										<div class="dt-buttons" style="text-align:right;">
											<div class="btn-group">
												<select class="form-control" id="cntSel">
													<option value="${efn:encrypt(6)}">6건씩 조회</option>
													<option value="${efn:encrypt(9)}" selected>9건씩 조회</option>
													<option value="${efn:encrypt(12)}">12건씩 조회</option>
													<option value="${efn:encrypt(15)}">15건씩 조회</option>
												</select>
											</div> 
											<button class="btn btn-outline-default change-cnt" type="button"><span>변경</span></button> 
										</div>
									</div>
								</div>

								<div class="mgt20">
									<div class="row" id="ansDataListDiv"></div>
									<div class="card-deck" id="ansDataNoListDiv">
										<div class="card w-100 pd20 text-center">해당 데이터가 없습니다.</div>
									</div>
								</div>

								<div class="mgt30" style="text-align:center;">
									<div id="paging"></div>
								</div>

							</div>
						</div>
						
						<!-- 등록 모달 -->
						<%@ include file = "./inc/inc_modal.jsp" %>
						
						<!-- 엑셀 등록 모달 -->
						<%@ include file = "./inc/inc_modal_excel.jsp" %>
<script>
var $jstreeId, $searchFrm = $('#fn_searchFrm'), $fade = $('<div/>', { 'class' : 'modal-backdrop fade show' });
var $listCnt, $datasetName;
$(document).ready(function() {
	$jstree = $('#jstree').bind('loaded.jstree', function() {$('a.jstree-anchor:eq(0)').click(); }).jstree({ 'core' : { 'check_callback' : true }, 'plugins' : [ 'types' ], 'types' : { 'default' : { 'icon' : 'fal fa-folder' } } });
});
<%-- 하위 분류 내 데이터조회안함 클릭 이벤트 --%>
$(document).on('click', '#ishide', function() {
	$jstreeId.find('a')[0].click();
});
<%-- 분류 선택 이벤트 --%>
$(document).on('click', 'a.jstree-anchor', function() {
	$jstreeId = $('#' + ($(this).attr('id')).replace('_anchor', ''));
	
	fn_setList();
});

<%-- 검색 버튼 이벤트 --%>
$(document).on('click', '.btn4search', function() {
	fn_setList();
});

<%-- 출력 카운트 변경 이벤트 --%>
$(document).on('click', '.change-cnt', function(){
	$listCnt = $('#cntSel').val();
	fn_setList();
})

function fn_setList(){
	var $data = $($jstreeId).data('data'), $lvl = $($jstreeId).data('lvl'), $path = $($jstreeId).data('path'), $hide = $('#ishide:checked').val();
	$listCnt = $('#cntSel').val();
	$('#category-path').html($path);

	$searchFrm.find('input[name=idx]').val($data);
	$searchFrm.find('input[name=slvl]').val($lvl);
	$searchFrm.find('input[name=q]').val($hide);
	$searchFrm.find('input[name=slmt]').val($listCnt);

	fn_ajax( { url : $searchFrm.attr('action'), data : $searchFrm.serialize() } );
}
$(document).on('click', '.answer_show', function(){
	const $data = $(this).data('data'), $title = $(this).data('title');
	$datasetName = $title;
	fn_ajax( { url : 'answerView.do?cmnx=${cmnx}', data : { q : $data }, dataType : 'json', success : function(data){
		if (data.result) {
			$('#dsetItemDiv').html(''); <%-- 검색 컬럼 항목 초기화 --%>
			var $action = $ansSearchFrm.attr('action');
			var $data = $ansSearchFrm.serialize();
			fn_ajax({ url : $action, data : $data, success : function(data) {
				$('.answer_viewCont .card-title').text($title);
				$('.answer_viewCont').show();
				$('body').append($fade).show();
			}});
		}
		else {
			eval(data.msg);
		}
	}} );
});
$(document).on('click', '.answer_hide', function(){
	$('.answer_viewCont').hide();
	$('body').find($fade).remove();
	$ansSearchFrm[0].reset();
});
<%-- 검색어 엔터키 이벤트 --%>
$(document).on('keypress', '#list_skey', function(e) {
	if (e.keyCode == 13) {
		$('.btn4search').click();
		return false;
	}
});
</script>
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
<iframe id="targetIfrm" name="targetIfrm" style="width:0px;height:0px;" frameborder="0" title="iframe"></iframe>
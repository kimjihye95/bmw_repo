<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
table.excelScroll { width: 100%; border-spacing: 0; }
table.excelScroll tbody,
table.excelScroll thead { width:100%; display: block; }
thead tr th { height: 30px; line-height: 30px; }
table.excelScroll tbody { overflow-y: auto; overflow-x: hidden; }
</style>

<!-- 엑셀 등록 -->
		<div class="modal fade" id="excel_make" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="min-width:95%;">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">엑셀등록 - <span></span></h5>
						<button type="button" class="btn4addExcelClose close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<div class="alert alert-primary alert-dismissible">
							<div class="mgb5">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="defaultInline2Radio" name="isDel" value="N" checked="">
									<label class="custom-control-label" for="defaultInline2Radio">추가(기존 데이터 유지)</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="defaultInline1Radio" name="isDel" value="Y">
									<label class="custom-control-label" for="defaultInline1Radio">갱신(기존데이터 삭제 후 등록)</label>
								</div>
							</div>
							<form name="frm" id="frm" method="post" enctype="multipart/form-data">
								<div class="row mgt10">
									<div class="input-group">
										<input type="file" class="form-control" placeholder="검색어를 입력하세요." accept=".xlsx, .xls" aria-label="Recipient's username" aria-describedby="button-addon5" id="fileUpload" name="fileUpload">
										<div class="input-group-append">
											<button class="btn btn-warning text-white waves-effect waves-themed" type="button" id="button-attach">파일첨부</button>
										</div>
									</div>
									<input type="hidden" name="cmnx" value="${cmnx }">
									<input type="hidden" name="categoryIdx" value="${datasetItemList[0].categoryIdx }">
									<input type="hidden" name="datasetIdx" value="${datasetItemList[0].datasetIdx }">
								</div>
							</form>
							
							<form id="fn_excelFrm" action="ansDataActionProcExcel.do" method="post" target="targetIfrm">
								<input type="hidden" name="cmnx" value="${cmnx }">
								<input type="hidden" name="categoryIdx" value="${datasetItemList[0].categoryIdx }">
								<input type="hidden" name="datasetIdx" value="${datasetItemList[0].datasetIdx }">
								<input type="hidden" name="excelJson" value="">
								<input type="hidden" name="isDelete" value="">
								
							</form>
						</div>

						<div class="div4excelBtn row" style="display:none;">
							<div class="col-lg-6 justify-content-start">
								<p class="mgb0" style="line-height:38px;">Total. <span class="text-warning excel-count">0건</span> &nbsp; 타입오류 <span class="text-danger danger-count">0</span>개 (오류값은 <span class="text-danger">빨간색</span>으로 표시됩니다.)</p>
							</div>
							<div class="col-lg-6 justify-content-end">
								<div class="dt-buttons" style="text-align:right;">
									<button type="button" class="btn btn-warning text-white btn4excelSubmit">등록</button>
									<button type="button" class="btn4addExcelClose btn btn-secondary" data-dismiss="modal">취소</button>
									<button type="button" class="btn btn-dark waves-effect waves-themed btn4excelDel">삭제</button>
							</div>
						</div>

						<div class="row col-lg-12 mgt10" style="padding-right:0; padding-left:0;">
							<div class="demo-window-content h-100" style="width:100%;overflow-x:auto;">
								<div id="slimtest2">
									<table class="table table-striped m-0 tbListA excelScroll" id="excelListTable">
									</table>
								</div>
							</div>
						</div>

					</div>
					<!-- <div class="modal-footer pdt0 mgt20">
						<button type="button" class="btn btn-lg btn-warning text-white btn4excelSubmit">등록</button>
						<button type="button" class="btn btn-lg btn-secondary" data-dismiss="modal">취소</button>
					</div> -->
				</div>
			</div>
		</div>
		
<script>
var $bVal;
var $chkCnt = 0;

function fn_excelDataChk(type, valid, val){
	
	if(val == ''){
		return true;
	}
	
	if(type == 1){
		
		return true;
	}else if(type == 2){
		//이메일
		if(/^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i.test(val )) return true;
		else return false;
	}else if(type == 3){
		//전화번호
		//if(/^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i.test(val )) return true;
		if (/01[0|1|6|7|8|][\.|\-]?[0-9]{3,4}[\.|\-]?[0-9]{3,4}$/g.test(val)) return true;
		else return false;
	}else if(type == 4){
		//URL
		if(/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(val )) return true;
		else return false;
	}else if(type == 5){
		
	}else if(type == 6){
		
	}else if(type == 7){
		//정수
		//if(/^(\-|\+)?([0-9]+)$/.test(val ) && parseInt(val ) > 0) return true;
		if(/^(\-|\+)?([0-9]+)$/.test(val )) return true;
		else return false;
	}else if(type == 8){
		//실수
		if(/^$|^-$|^-?[0-9]([.]\d{0,4})?$|^-?[1-9][0-9]*([.]\d{0,4})?$/.test(val )) return true;
		else return false;
	}else if(type == 9){
		//날짜 : YYYY
		if(/^(19|20)\d{2}$/.test(val )) return true;
		else return false;
	}else if(type == 10){
		//날짜 : YYYYMM
		if(/^(19|20)\d{2}(0[1-9]|1[012])$/.test(val )) return true;
		else return false;
	}else if(type == 11){
		//날짜 : YYYYMMDD
		if(/^(19|20)\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[0-1])$/.test(val )) return true;
		else return false;
	}else if(type == 12){
		//시간 : 시
		if( /^([1-9]|[01][0-9]|2[0-3])$/.test(val )) return true;
		else return false;
	}else if(type == 13){
		//시간 : 시:분
		if(/^([1-9]|[01][0-9]|2[0-3]):([0-5][0-9])$/.test(val )) return true;
		else return false;
	}else if(type == 14){
		//시간 : 시:분:초
		if(/^([1-9]|[01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/.test(val )) return true;
		else return false;
	}else if(type == 99){
		var result;
		
		if(val == ''){
			result = true;
		}else{			
			fn_ajax({ url : 'validationCheck.do', data : { masterIdx : valid, itemName : val}, dataType : 'json', success : function(data){
	
				if(data.count == 1) result = true;
				else result = false;
			}})
		}
		
		return result;
		
	}
	
	return true;
}

function fn_setChkData() {
	var $tr = $('.excel-tbody').children('tr');

	$chkCnt = 0;
	$($tr).each(function(i, e){
		
		var $td = $(e).children('td');
		
		$($td).each(function(j, ee){
			
			var $eeTxt = ($(ee).text()).replaceAll(/\s/g, '');
			if(j < ($td.length - 1)){
				
				var $type = $(ee).data('type');
				var $valid = $(ee).data('valid');
				var $req = $(ee).data('req');
				
				if($req == 1){
					
				}
				
				if(!fn_excelDataChk($type, $valid, $eeTxt)){
					$(ee).addClass('text-danger');
					$chkCnt++;
				}else if($req == 1 && $eeTxt == ''){
					$(ee).addClass('text-danger');
					$(ee).html('필수 입력 항목');
					$chkCnt++;
				}
			}
		})
	})
	$('.danger-count').html($chkCnt);
}

<%-- 삭제 버튼 이벤트 --%>
$(document).on('click', '.btn4excelDel', function(){
	if (!fn_checkCboxChecked('excel_chk')) return false;

	var $success = function() {
		$('input[name=excel_chk]:checked').each(function(i, e){
			$(e).parent().parent().parent().remove();
		})
	};
	var $opt = { title : '알림', msg : '${_confirmDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
})
<%-- 수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4excelModi', function(){
	$('.modify-cancel').click();
	var $td = $(this).parent().siblings('td');
	var $btn = '<button type="button" class="btn btn-info waves-effect waves-themed excel-modify">확인</button><button type="button" class="btn btn-secondary waves-effect waves-themed modify-cancel">취소</button>';

	$bVal = new Array();
	
	$($td).each(function(i, e){
		var $tempVal = $(e).text() == '필수 입력 항목' ? '' : $(e).text();
		$bVal.push($tempVal);
		if (i > 0) $(e).html('<input class="excel-modi-input form-control' + ($(e).hasClass('text-danger') ? ' text-danger' : '') + '" type="text" value="' + $tempVal + '">');
	})
	$(this).parent().html($btn);
})

<%-- 수정 확인 버튼 클릭 이벤트 --%>
$(document).on('click', '.excel-modify', function(){

	var $input = $('.excel-modi-input');
	var $mBtn = '<button type="button" class="btn btn-light waves-effect waves-themed btn4excelModi">수정</button>';
	$($input).each(function(i, e){
		var $type = $(e).parent().data('type');
		var $req = $(e).parent().data('req');
		var $valid = $(e).parent().data('valid');
		if($req == 1 && $(e).val() == ''){
			$(e).parent().addClass('text-danger');
			$(e).val('필수 입력 항목');
		}else if(!fn_excelDataChk($type, $valid, $(e).val())){
			$(e).parent().addClass('text-danger');
		}else {
			$(e).parent().removeClass('text-danger');
		}
		$(e).parent().html(fn_convertHtmlToCode($(e).val()));
	});
	$chkCnt = $('.excel-tbody').find('td.text-danger').length;
	$('.danger-count').html($chkCnt);
	
	$(this).parent().html($mBtn);
})

<%-- 수정 취소 버튼 클릭 이벤트 --%>
$(document).on('click', '.modify-cancel', function(){

	var $td = $(this).parent().siblings('td');
	var $mBtn = '<button type="button" class="btn btn-light waves-effect waves-themed btn4excelModi">수정</button>';
	$($td).each(function(i, e){
		if (i > 0) $(e).html($bVal[i]);
	})
	$(this).parent().html($mBtn);
})

<%-- 엑셀 데이터 등록 --%>
$(document).on('click', '.btn4excelSubmit', function(){
	var $tr = $('.excel-tbody').children('tr');
	var $th = $('.excel-column');
	var $colName = {};
	var $paramArr = new Array();
	var $isDelete = $('input[name=isDel]:checked').val();
	var $excelFrm = $('#fn_excelFrm');
	
	$('.excel-modify').click();
	if( $('.excel-tbody').find('td.text-danger').length > 0 ) {
		var option = new Object();
		option.title = '업로드 실패';
		option.msg = '엑셀 데이터를 확인해주세요.';
		option.icon = 'error';
		option.okMsg = '확인';
		fn_openSweetAlert(option);
		
		return false;
	}

	$($th).each(function(i, e){
		$colName[$(e).index()] = $(e).attr('id');
	});

	$($tr).each(function(i, e){
		var $td = $(e).children('td');
		var $paramObj = new Object();
		$($td).each(function(j, ee){
			if( $colName.hasOwnProperty(j) ) {
				$paramObj[$colName[j]] = $(ee).html();
			}
		});

		$paramArr.push($paramObj);
	});
	
	$excelFrm.children('input[name=excelJson]').val(JSON.stringify($paramArr));
	$excelFrm.children('input[name=isDelete]').val($isDelete);
	$excelFrm.submit();
	//fn_ajax({url : 'ansDataActionProcExcel.do', data : { excelJson : JSON.stringify($paramArr), cmnx : "${cmnx}", categoryIdx : $categoryIdx, datasetIdx : $datasetIdx, isDelete : $isDelete }, method : 'POST'});
})

</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../cresources/jsp/common.jsp" %>
	<!-- 직접 등록 -->
		<div class="modal fade" id="answer_make" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">직접 등록 - <span></span></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_registFrm" action="ansDataActionProc.do?cmnx=${cmnx}" onsubmit="return false;">
						<table class="tbWriteA mgt20" summary="응답데이터 직접 등록입니다.">
							<caption>응답데이터 직접 등록입니다.</caption>
							<colgroup>
								<col width="20%" />
								<col width="80%" />
							</colgroup>

							<tbody class="pd10 alignC">
								<c:forEach items="${datasetItemList}" var="ansModel" varStatus="i">
									<tr>
										<th scope="row"><label for="input0201">${ansModel.itemName} <span>${ansModel.required eq 1 ? '*' : '' }</span></label></th>
										<td class="column-input">
											<c:choose>
												<c:when test="${ansModel.itemType eq 6 }">
												<textarea data-req="${ansModel.required}" data-name="${ansModel.itemName }" data-type="${ansModel.itemType }" data-valid="${ansModel.itemValid }" id="${ansModel.itemColName}" name="${ansModel.itemColName}" class="form-control add-direct-input" placeholder=""></textarea>
												</c:when>
												<c:otherwise>
												<input type="text" data-req="${ansModel.required}" data-name="${ansModel.itemName }" data-type="${ansModel.itemType }" data-valid="${ansModel.itemValid }" id="${ansModel.itemColName}" name="${ansModel.itemColName}" class="form-control add-direct-input" placeholder="">
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</c:forEach>
								<tr style="display:none;">
									<th scope="row"><label for="input0201">비고 </label></th>
									<td>
										<input type="hidden" name="cmnx" value="${cmnx }">
										<input type="hidden" name="categoryIdx" value="${datasetItemList[0].categoryIdx }">
										<input type="hidden" name="datasetIdx" value="${datasetItemList[0].datasetIdx }">
									</td>
								</tr>
							</tbody>
						</table>
						</form>
					</div>
					<div class="modal-footer pdt0">
						<button type="button" class="btn4datasetAdd btn btn-warning text-white">추가</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		
	<!-- 수정 -->
		<div class="modal fade" id="answer_modi" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">수정 - <span></span></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_ansModifyFrm" action="ansDataActionProc.do?cmnx=${cmnx}" onsubmit="return false;">
							<input type="hidden" name="cmnx" value="${cmnx }">
							<input type="hidden" name="idx" value="">			
							<table class="tbWriteA mgt20" summary="응답데이터 직접 등록입니다.">
								<caption>응답데이터 수정입니다.</caption>
								<colgroup>
									<col width="20%" />
									<col width="80%" />
								</colgroup>
	
								<tbody class="pd10 alignC">
									<c:forEach items="${datasetItemList}" var="ansModel" varStatus="i">
										<tr>
											<th scope="row"><label for="input0201">${ansModel.itemName} <span>${ansModel.required eq 1 ? '*' : ''}</span></label></th>
											<td class="column-input">
											<c:choose>
												<c:when test="${ansModel.itemType eq 6 }">
												<textarea data-req="${ansModel.required}" data-name="${ansModel.itemName }" data-type="${ansModel.itemType }" data-valid="${ansModel.itemValid }" id="${ansModel.itemColName}" name="${ansModel.itemColName}" class="form-control modi-direct-input" placeholder=""></textarea>
												</c:when>
												<c:otherwise>
												<input type="text" data-req="${ansModel.required}" data-name="${ansModel.itemName }" data-type="${ansModel.itemType }" data-valid="${ansModel.itemValid }" id="${ansModel.itemColName}" name="${ansModel.itemColName}" class="form-control modi-direct-input" placeholder="">
												</c:otherwise>
											</c:choose>
											<%-- <input type="text" data-req="${ansModel.required }" data-name="${ansModel.itemName }" data-type="${ansModel.itemType }" data-valid="${ansModel.itemValid }" id="${ansModel.itemColName}" name="${ansModel.itemColName}" class="form-control modi-direct-input" placeholder=""> --%>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</form>
					</div>
					<div class="modal-footer pdt0">
						<button type="button" class="btn4datasetModi btn btn-warning text-white">수정</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		
<script>
var $modifyFrm = $('#fn_ansModifyFrm'), $registFrm = $('#fn_registFrm');

<%-- 수정 --%>
$(document).on('click', '.btn4datasetModi', function() {
	fn_submit(1);
});

<%-- 직접등록 --%>
$(document).on('click', '.btn4datasetAdd', function() {
	fn_submit(2);
});

function fn_submit(flag){
	var $frm;
	var $validChk = true;
	if( flag == 1 ) $frm = $modifyFrm;
	else $frm = $registFrm;
	
	var input = $frm.find('[id^="col"]');
	
	$(input).each(function(i, e){
		var $type = $(e).data('type');
		var $valid = $(e).data('valid');
		var $val = $(e).val();
		var $name = $(e).data('name');
		var $req = $(e).data('req');
		
		if($req == 1 && fn_isNull($val)){
			var option = new Object();
			option.title = '실패';
			option.msg = $name + '는 필수 입력 항목입니다.';
			option.icon = 'error';
			option.okMsg = '확인';
			fn_openSweetAlert(option);
			
			$(e).focus();
			
			$validChk = false;
			return false;
			
		}else if(!fn_excelDataChk($type, $valid, $val)){
			var option = new Object();
			option.title = '실패';
			option.msg = $name + '&nbsp;데이터를 확인해주세요.';
			option.icon = 'error';
			option.okMsg = '확인';
			fn_openSweetAlert(option);
			
			$(e).focus();
			
			$validChk = false;
			return false;	
		}
	})
	if($validChk){		
		var $action = $frm.attr('action');
		var $data = $frm.serialize();
	
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
		<%-- 데이터셋 항목 등록 start --%>
		<div class="modal fade" id="dataSetItemModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width:1000px;">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">데이터셋 항목 생성</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_itemInputFrm" method="post" action="datasetActionProc.do?cnmx=${cmnx}">
							<input type="hidden" name="didx" />
							<input type="hidden" name="cidx" />
							<div class="row">
								<div class="col-sm-6 justify-content-start">
									<!-- <button type="button" class="btn btn-dark waves-effect waves-themed"><i class="fal fa-arrow-up"></i> 위로</button>
									<button type="button" class="btn btn-dark waves-effect waves-themed"><i class="fal fa-arrow-down"></i> 아래로</button> -->
								</div>
								<div class="col-sm-6 justify-content-end">
									<c:set var="modelItem" value="itemAddType" />
									<div class="dt-buttons" style="text-align:right;"> 
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="${modelItem}" id="${modelItem}1" value="1" class="custom-control-input">
											<label class="custom-control-label" for="${modelItem}1">최상위</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" name="${modelItem}" id="${modelItem}2" value="2" class="custom-control-input" checked>
											<label class="custom-control-label" for="${modelItem}2">최하위</label>
										</div>
										<button type="button" class="btn4itemAdd btn btn-warning waves-effect waves-themed text-white">추가</button>
										<button type="button" class="btn4itemDel btn btn-dark waves-effect waves-themed">삭제</button>
									</div>
								</div>
							</div>
						</form>
						
						<form id="fn_itemDelFrm" action="datasetItemDelProc.do?cmnx=${cmnx}" class="col-sm-12">
							<input type="hidden" name="didx" />
							<input type="hidden" name="cidx" />
							<input type="hidden" name="order" />
							<table class="table table-striped m-0 tbListA mgt10">
								<colgroup>
									<col style="width:2%;" />
									<col style="width:18%;" />
									<col style="width:15%;" />
									<col style="width:15%;" />
									<col style="width:50px;" />
									<col />
									<col style="width:156px;" />
								</colgroup>
								<thead class="bg-warning-900 text-white">
									<tr>
										<th></th>
										<th>항목명</th>
										<th>데이터유형</th>
										<th>유효값</th>
										<th>필수</th>
										<th>입력 안내글</th>
										<th>관리</th>
									</tr>
								</thead>
								<tbody id="dataSetItemTbody" class="alignC"></tbody>
							</table>
						</form>

					</div>
					<div class="modal-footer inno pdt0">
						<button type="button" class="btn btn-lg btn-secondary" data-dismiss="modal">닫기</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // 데이터셋 항목 등록 end --%>

<script>
var $itemCnt = 0, $datasetItemFrm = $('#fn_itemInputFrm'), $datasetItemDelFrm = $('#fn_itemDelFrm');
<%-- 상단 항목 추가 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4itemAdd', function() {
	$('#itemTr0').remove();
	$itemCnt ++;
	var $tr = $('<tr/>', { id : 'itemTr' + $itemCnt }), $itemAddType = $('input[name=itemAddType]:checked').val();
	$tr.append($('<th/>'));
	$tr.append($('<td/>').append($('<input/>', { type : 'text', name : 'itemName', 'class' : 'form-control', placeholder : '항목명 입력' })));
	$tr.append($('<td/>').append($('<select/>', { name : 'itemType', 'data-rel' : 'itemValid' + $itemCnt, 'class' : 'form-control' }).append($code.options)));
	$tr.append($('<td/>').append($('<span/>', { id : 'itemValid' + $itemCnt, text : $code.itemCode })));
	
	var $div = $('<div/>', { 'class' : 'custom-control custom-checkbox' });
	$div.append($('<input/>', { type : 'checkbox', name : 'required', id : 'required' + $itemCnt, 'class' : 'custom-control-input', value : 1 }));
	$div.append($('<label/>', { 'class' : 'custom-control-label', 'for' : 'required' + $itemCnt}));
	$tr.append($('<td/>').append($div));

	$tr.append($('<td/>').append($('<input/>', { type : 'text', name : 'description', 'class' : 'form-control', placeholder : '미입력시 항목명으로 표시' })));
	
	var $mngTd = $('<td/>');
	$mngTd.append($('<button/>', { type : 'button', 'data-rel' : $itemCnt, 'class' : 'btn4itemSubmit btn btn-warning waves-effect waves-themed text-white mr-2', text : '등록' }));
	$mngTd.append($('<button/>', { type : 'button', 'data-rel' : 'itemTr' + $itemCnt, 'class' : 'btn4itemCancel btn btn-dark waves-effect waves-themed', text : '취소'}));
	$tr.append($mngTd);
	
	if ($itemAddType == 1) $('#dataSetItemTbody').prepend($tr);
	else $('#dataSetItemTbody').append($tr);
});
<%-- 항목 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4itemDel', function() {
	var $frm = $('#fn_itemDelFrm'), $cboxs = $('input[name=datasetItemCboxs]:checked');
	if (!fn_checkCboxChecked('datasetItemCboxs')) return false;

	var $success = function() {
		var $ordr = new Array();
		$($cboxs).each(function(i, e) {
			$ordr.push($(this).data('ordr'));
		});
		$frm.find('input[name=order]').val($ordr.toString());
		var $action = $frm.attr('action');
		var $data = $frm.serialize();
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}

	var $msg = ($itemCnt == $cboxs.length) ? _commonDatsetAllItemDelMsg_ : _commonDatsetItemDelMsg_;
	var $opt = { title : '알림', msg : $msg, icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
	
	return false;
});
<%-- 데이터 유형 선택 이벤트 --%>
$(document).on('change', 'select[name=itemType]', function() {
	var $rel = $(this).data('rel'), $role = $(this).children(':selected').data('role');
	if($(this).val() != 99){		
		$('#' + $rel).text($role);
	}else{
		fn_setMasterList($('#' + $rel));
	}
});

<%-- 마스터 코드 리스트 --%>
function fn_setMasterList(obj){
	fn_ajax({url : 'getMasterList.do', data : {cmnx : 8}, dataType : 'json', success : function(data){
		var masterList = data.masterList;
		var select = '<select name="itemValid" class="form-control">';
		masterList.forEach(function(element, i){
			if(i > 0){				
				var option = '<option value="' + element.masterIdx + '">' + element.masterName + '</option>';
				select += option;
			}
		})
		select += '</select>';
		
		$(obj).html(select);
	}})
}

<%-- 데이터셋 항목 등록 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4itemSubmit', function() {
	var $this = $(this), $tr = $this.closest('tr'), $rel = $this.data('rel'), $itemAddType = $('input[name=itemAddType]:checked').val();
	var $didx = $datasetItemFrm.find('input[name=didx]').val(), $cidx = $datasetItemFrm.find('input[name=cidx]').val(), $idx = $this.data('data');
	var $itemName = $tr.find('[name=itemName]').val();
	const $success = function() { setTimeout(function() { $tr.find('[name=itemName]').focus(); }, 300); };
	if (!fn_checkFill($tr, 'itemName')) return false;
	else if (/[^(0-9가-힣ㄱ-ㅎㅏ-ㅣa-z\&\.\(\))\/\_ ]/gi.test($itemName)) {
		var $opt = { title : '알림', msg : '한글, 영문, 숫자, 특수문자 (.&_/) 만 입력이 가능합니다.', icon : 'warning', success : $success };
		fn_openSweetAlert($opt);
		return false;
	}
	else {
		<%-- 항목명 중복 체크 --%>
		var $isDup = false;
		$.each($itemNameObj, function(k, v) {
			if (v == $itemName && $tr[0].rowIndex != k) $isDup = true;
		});
		
		if ($isDup) {
			fn_openSweetAlert({ title : '알림', msg : '중복된 항목명을 사용하실 수 없습니다.', icon : 'warning', success : $success });
			return false;
		}
	}
	
	var $action = 'datasetItemActionProc.do?cmnx=${cmnx}';
 	var $data = { didx : $didx, cidx : $cidx, itemAddType : $itemAddType, itemName : $tr.find('[name=itemName]').val(), itemType : $tr.find('[name=itemType]').val(), itemValid : $tr.find('[name=itemValid]').val(), required : $tr.find('[name=required]:checked').val(), description : $tr.find('[name=description]').val() };
 	if (!fn_isNull($idx)) $data.idx = $idx;
	fn_ajax({ url : $action, data : $data, type : 'POST' });
});
<%-- 항목 추가 - 취소 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4itemCancel', function() {
	$('#' + $(this).data('rel')).remove();
	if ($('#dataSetItemTbody tr').length == 0) $('#dataSetItemTbody').append($('<tr/>', { id : 'itemTr0' }).append($('<td/>', { 'colspan' : 7, html : _comNoDatasetItemMsg_ })));
});
<%-- 항목 순서 변경 (업, 다운) 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4order', function() {
	var $role = $(this).data('role'), $idx = $(this).data('data'), $order = $(this).data('order');
	var $didx = $datasetItemFrm.find('input[name=didx]').val(), $cidx = $datasetItemFrm.find('input[name=cidx]').val();
	var $action = 'datasetItemOrderActionProc.do?cmnx=${cmnx}';
	var $data = { idx : $idx, didx : $didx, cidx : $cidx, role : $role, order : $order }
	fn_ajax( { url : $action, data : $data, type : 'POST' } );
});
<%-- 데이터셋 항목 모달 닫기 이벤트 --%>
$(document).on('hidden.bs.modal', '#dataSetItemModal', function() {
	$datasetItemFrm[0].reset();
	$('#dataSetItemTbody').html('');
	$itemCnt = 0;
});
</script>
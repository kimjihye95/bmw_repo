<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../cresources/jsp/common.jsp" %>
		<%-- 데이터셋 등록 start --%>
		<div class="modal fade" id="dataSetRegModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width:1000px;">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">데이터셋 생성</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_inputFrm" method="post" action="datasetActionProc.do?cmnx=${cmnx}">
							<table class="tbWriteA mgt20" summary="데이터셋 기본정보 입력입니다.">
								<caption>데이터셋 기본정보 입력입니다.</caption>
								<colgroup>
									<col width="20%" />
									<col width="80%" />
								</colgroup>
	
								<tbody class="pd10 alignC">
									<tr>
										<c:set var="modelItem" value="categoryIdx" />
										<th scope="row"><label for="${modelItem}">분류</label> <span>*</span></th>
										<td>
											<select class="form-control order-select" id="${modelItem}" name="${modelItem}"></select>
											<input type="hidden" name="categoryIdxName" value="">
										</td>
									</tr>
									<tr>
										<c:set var="modelItem" value="datasetName" />
										<th scope="row"><label for="${modelItem}">데이터셋 명</label> <span>*</span></th>
										<td><input type="text" name="${modelItem}" id="${modelItem}" class="form-control" value="" minlength="2" maxlength="20" required></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="refUrl" />
										<th scope="row"><label for="${modelItem}">참조 URL</label></th>
										<td>
											<div class="input-group">
													<input type="text" name="${modelItem}" id="${modelItem}" class="form-control">
													<div class="input-group-append">
														<button class="btn4url btn btn-warning text-white waves-effect waves-themed" type="button"><i class="fal fa-globe"></i> URL 열기</button>
													</div>
												</div>
										</td>
									</tr>
									<tr>
										<c:set var="modelItem" value="description" />
										<th scope="row"><label for="${modelItem}">설명</label></th>
										<td><textarea class="form-control" name="${modelItem}" id="${modelItem}" rows="5"></textarea></td>
									</tr>
								</tbody>
							</table>
						</form>

					</div>
					<div class="modal-footer inno pdt0">
						<button type="button" data-role="r" class="btn4submit btn btn-lg btn-warning text-white">확인</button>
						<button type="button" class="btn btn-lg btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // 데이터셋 등록 end --%>

		<%-- 데이터셋 수정 start --%>
		<div class="modal fade" id="dataSetModiModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width:1000px;">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">데이터셋 수정</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_modFrm" method="post" action="datasetActionProc.do?cmnx=${cmnx}">
							<input type="hidden" name="idx" />
							<input type="hidden" name="cmnx" value="${cmnx}" />
							<table class="tbWriteA mgt20" summary="데이터셋 기본정보 입력입니다.">
								<caption>데이터셋 기본정보 입력입니다.</caption>
								<colgroup>
									<col width="20%" />
									<col width="80%" />
								</colgroup>
	
								<tbody id="datasetModTbody" class="pd10 alignC"></tbody>
							</table>
						</form>

					</div>
					<div class="modal-footer inno pdt0">
						<button type="button" data-role="m" class="btn4submit btn btn-lg btn-warning text-white">수정</button>
						<button type="button" class="btn btn-lg btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // 데이터셋 수정 end --%>

<script>
<%-- 데이터셋 확인 버튼 클릭 이벤트 --%>
var $modal, $datasetFrm = $('#fn_inputFrm'), $datasetModFrm = $('#fn_modFrm');
$(document).on('click', '.btn4submit', function() {
	var $role = $(this).data('role'), $frm = ($role == 'r') ? $datasetFrm : $datasetModFrm;
	if ($frm.valid()) {

		var $datasetNameData = $frm.find('input[name=datasetName]').val(), $refUrl = $frm.find('input[name=refUrl]').val();
		if (/[^(0-9가-힣ㄱ-ㅎㅏ-ㅣa-z\&\.\(\))\/\_ ]/gi.test($datasetNameData)) {
			var $opt = { title : '알림', msg : '한글, 영문, 숫자, 특수문자 (.&_/) 만 입력이 가능합니다.', icon : 'warning' };
			fn_openSweetAlert($opt);
			return false;
		}

		if (!fn_isNull($refUrl)) {
			const $pattern1 = /^(https?|ftp)\:\/\/[ㄱ-힣a-zA-Z0-9-\.]+\.[a-z]{2,4}(:\d{2,5})?(\/)(.*)/i;
			const $pattern2 = /^(https?|ftp)\:\/\/\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}:\d{2,5}(\/)(.*)/i;
			if (!($pattern1.test($refUrl) || $pattern2.test($refUrl))) {
				fn_openSweetAlert({ title : '알림', msg : '잘못된 URL 형식입니다.', icon : 'warning' });
				return false;
			}
		}
		$frm.find('input[name=categoryIdxName]').val($('#categoryIdx option:selected').text());
	 	var $action = $frm.attr('action');
	 	var $data = $frm.serialize();
	 	fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
	
	return false;
});
<%-- URL 열기 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4url', function() {
	var $url = $($modal).find('input[name=refUrl]').val();
	if (fn_checkFill($modal, 'refUrl')) window.open($url);
});
<%-- set validation --%>
var $validatorOpt = { id : ['categoryIdx', 'datasetName'], min : [ 0, 2 ], max : [ 0, 20 ]  };
fn_setValidator($datasetFrm, $validatorOpt);
<%-- 데이터셋 등록 모달 오픈 이벤트 --%>
$(document).on('show.bs.modal', '#dataSetRegModal', function() {
	$(this).find('input[name=datasetName]').trigger('focus');
	$modal = $('#dataSetRegModal');
	fn_ajax( { url : 'selectCategoryListAjax.do?cmnx=${cmnx}' } );
});
<%-- 데이터셋 등록 모달 닫기 이벤트 --%>
$(document).on('hidden.bs.modal', '#dataSetRegModal', function() {
	$datasetFrm[0].reset();
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />

						<div class="row">
							<div class="col-sm-12"  style="padding-left:0px; padding-right:0px;">
								<form id="fn_searchFrm" action="${URL_DEFAULT_LIST}">
									<input type="hidden" name="cmnx" value="${cmnx}" />
									<div class="alert alert-primary alert-dismissible">
										<div class="row">
											<div class="col-1"></div>
											<div class="col-2">
												<c:set var="modelItem" value="stype" />
												<select name="${modelItem}" id="${modelItem}" class="form-control">
													<option value="">전체</option>
													<option value="${efn:encrypt('a')}"<c:if test="${stype eq 'a'}"> selected</c:if>>권한</option>
													<option value="${efn:encrypt('i')}"<c:if test="${stype eq 'i'}"> selected</c:if>>아이디</option>
													<option value="${efn:encrypt('n')}"<c:if test="${stype eq 'n'}"> selected</c:if>>이름</option>
													<option value="${efn:encrypt('d')}"<c:if test="${stype eq 'd'}"> selected</c:if>>소속</option>
												</select>
											</div>
											<div class="col-8">
												<div class="input-group">
													<c:set var="modelItem" value="skey" />
													<input type="text" name="${modelItem}" id="${modelItem}" value="${param[modelItem]}" class="form-control" placeholder="검색어를 입력하세요." required>
													<div class="input-group-append">
														<button type="button" class="btn4search btn btn-warning text-white waves-effect waves-themed"><i class="fal fa-search"></i></button>
														<c:if test="${isSearch}">
														<button type="button" class="btn4allList btn btn-dark text-white waves-effect waves-themed">전체 리스트</button>
														</c:if>
													</div>
												</div>
											</div>
											<div class="col-sm-1"></div>
										</div>
									</div>
								</form>

								<div class="row">
									<div class="col-6 justify-content-start">
										<p class="mgb0" style="line-height:38px;">Total. <span class="text-warning">${count}건</span></p>
									</div>
									<div class="col-6 justify-content-end">
										<div class="dt-buttons" style="text-align:right;"> 
											<button type="button" class="btn4add btn btn-warning waves-effect waves-themed text-white" data-toggle="modal" data-target="#memberRegModal">추가</button>
											<button type="button" class="btn4del btn btn-dark waves-effect waves-themed">삭제</button>
										</div>
									</div>
								</div>

								<div class="row mgt20">
									<form id="fn_memDelFrm" action="memberDelProc.do?cmnx=${cmnx}" class="col-sm-12">
										<table class="table table-striped m-0 tbListA">
											<colgroup>
												<col style="width:50px;"/>
												<col style="width:5%;"/>
												<col style="width:8%;"/>
												<col style="width:8%;"/>
												<col style="width:8%;"/>
												<col style="width:10%;"/>
												<col style="width:10%;"/>
												<col style="width:12%;"/>
												<col />
												<c:if test="${ss.ss_memberIdx eq 1 and ss.ss_memberAuth eq 1}">
												<col style="width:200px;"/>
												</c:if>
												<col style="width:8%;"/>
											</colgroup>
											<thead class="bg-warning-900 text-white">
												<tr>
													<th></th>
													<th>No</th>
													<th>권한</th>
													<th>아이디</th>
													<th>이름</th>
													<th>소속</th>
													<th>연락처</th>
													<th>이메일</th>
													<th>비고</th>
													<c:if test="${ss.ss_memberIdx eq 1 and ss.ss_memberAuth eq 1}">
													<th>로그</th>
													</c:if>
													<th>관리</th>
												</tr>
											</thead>
											<tbody class="alignC">
											
												<c:if test="${empty list}">
													<tr><td colspan="10">${_infoNoDataMsg_}</td></tr>
												</c:if>
												
												<c:set var="listNo" value="${listStartNo}" />
												<c:forEach items="${list}" var="model" varStatus="i">
													<c:set var="encMemIdx" value="${efn:encrypt(model.memberIdx)}" />
													<tr>
														<th scope="row">
															<div class="custom-control custom-checkbox">
																<c:if test="${model.memberIdx ne 1 and model.memberIdx ne ss.ss_memberIdx}">
																<input type="checkbox" name="memCboxs" id="memCboxs${i.count}" value="${encMemIdx}" class="custom-control-input"><label class="custom-control-label" for="memCboxs${i.count}"></label>
																</c:if>
															</div>
														</th>
														<td>${listNo}</td>
														<td>${model.authName}</td>
														<td>${efn:getMaskData(model.memberId, 1)}</td>
														<td>${efn:getMaskData(model.memberName, 1)}</td>
														<td>${efn:strContentsFormat(model.department, '')}</td>
														<td>${efn:getMaskData(efn:decrypt(model.mobile), 2)}</td>
														<td>${efn:getMaskData(efn:decrypt(model.email), 3)}</td>
														<td>${efn:strContentsFormat(model.etc, '')}</td>
														<c:if test="${ss.ss_memberIdx eq 1 and ss.ss_memberAuth eq 1}">
														<td>
															<div class="btn-group btn-sm">
																<button type="button" data-data="${efn:encrypt(model.memberId)}" data-role="${efn:encrypt(1)}" class="btn4log btn btn-sm btn-secondary">로그인</button>
																<button type="button" data-data="${efn:encrypt(model.memberIdx)}" data-role="${efn:encrypt(2)}" class="btn4log btn btn-sm btn-secondary">사용자 정보</button>
															</div>
														</td>
														</c:if>
														<td><c:if test="${model.memberIdx gt 1 or (model.memberIdx eq 1 and model.memberIdx eq ss.ss_memberIdx)}"><button type="button" data-data="${encMemIdx}" data-toggle="modal" data-target="#comMemberModModal" class="bt4memMod btn btn-light waves-effect waves-themed">수정</button></c:if></td>
													</tr>
													<c:set var="listNo" value="${listNo - 1}" />
												</c:forEach>
											</tbody>
										</table>
									</form>
								</div>

								<div class="mgt30" style="text-align:center;">
									<div id="paging"></div>
								</div>
							</div>

						</div>
						
						<%@ include file = "./inc/inc_list_modal.jsp" %>
						<%@ include file = "./inc/inc_log_modal.jsp" %>
<script>
var varOpt = {url : '${URL_PAGE_LIST}', obj : 'paging', total : '${pageTotalPage}', visible : '${blockSize}'};
fn_setPaging(varOpt);
<%-- 검색 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4search', function() {
	var $frm = $('#fn_searchFrm');
	if ($frm[0].checkValidity() === false) {
		event.preventDefault();
		event.stopPropagation();
		$frm.addClass('was-validated');
	}
	else {
		$frm.submit();
	}
});
<%-- 전체 리스트 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4allList', function() {
	location.href = '${URL_DEFAULT_LIST}';
});
<%-- 사용자 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4del', function() {
	var $frm = $('#fn_memDelFrm');
	if (!fn_checkCboxChecked('memCboxs')) return false;

	var $success = function() {
		var $action = $frm.attr('action');
		var $data = $frm.serialize();
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
	
	var $opt = { title : '알림', msg : '${_confirmDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
	
	return false;
});
<%-- 사용자 수정 버튼 클릭 이벤트 --> admresources/jsp/comModal.jsp --%>
<%-- 계정잠금해제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4unlock', function() {
	var $midx = $(this).data('data');
	var $success = function() {
		var $action = 'unlockMemberAccountProc.do?cmnx=${cmnx}';
		var $data = { midx : $midx };
		fn_ajax( { url : $action, data : $data, type : 'POST' } );
	}
	
	var $opt = { title : '알림', msg : _comUnlockMemberAccount, icon : 'info', cancel : true, success : $success };
	fn_openSweetAlert($opt);
	
	return false;
});
<%-- 로그 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4log', function() {
	var $role = $(this).data('role'), $data = $(this).data('data');
	$('.logSearch').find('input[name=idx]').val($data);
	fn_ajax( { url : 'memberLoglistAjax.do?cmnx=${cmnx}', data : { q : $role, idx : $data } } );
});
</script>
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
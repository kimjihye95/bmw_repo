<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
		<%-- 접속 로그 모달 start --%>
		<div class="modal fade memLogModal" id="memberLoginLogModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">로그인 로그</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_loginLogSearchFrm" class="logSearch">
							<input type="hidden" name="cmnx" value="${cmnx}" />
							<input type="hidden" name="q" value="${efn:encrypt(1)}" />
							<input type="hidden" name="idx" value="" />
							<div class="form-group">
								<div id="daterange-picker" class="input-daterange input-group">
									<div class="input-group-append">
										<c:set var="modelItem" value="stype" />
										<select name="${modelItem}" id="${modelItem}" class="form-control">
											<option value="">전체</option>
											<option value="${efn:encrypt('Y')}">로그인</option>
											<option value="${efn:encrypt('N')}">로그인 실패</option>
											<option value="${efn:encrypt('O')}">로그아웃</option>
										</select>
									</div>
									<c:set var="modelItem" value="sdayf" />
									<input type="text" name="${modelItem}" value="${today}" class="form-control" autocomplete="off">
									<div class="input-group-append input-group-prepend">
										<span class="input-group-text fs-xl"><i class="fal fa-ellipsis-h"></i></span>
									</div>
									<c:set var="modelItem" value="sdayt" />
									<input type="text" name="${modelItem}" value="${today}" class="form-control" autocomplete="off">
									<div class="input-group-prepend">
										<button type="button" class="btn4logSearch btn btn-sm btn-dark wave-effect waved-themed form-control">검색</button>
									</div>
								</div>
							</div>
							
							<table class="table table-striped mt-2 tbListA">
								<colgroup>
									<col width="10%" />
									<col width="50%" />
									<col width="40%" />
								</colgroup>
								
								<thead class="bg-warning-900 text-white">
									<tr>
										<th scope="col">번호</th>
										<th scope="col">일시</th>
										<th scope="col">정보</th>
									</tr>
								</thead>
	
								<tbody id="loginLogTbody" class="pd10 alignC"></tbody>
							</table>
						</form>
						<div class="mgt30" style="text-align:center;">
							<div id="loginLogPaging"></div>
						</div>
					</div>
					<div class="modal-footer inno pdt0">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">닫기</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // 접속 로그 모달 end --%>
		<%-- 사용자 메뉴 로그 모달 start --%>
		<div class="modal fade memLogModal" id="memberUserLogModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">사용자 메뉴 로그</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_userLogSearchFrm" class="logSearch">
							<input type="hidden" name="cmnx" value="${cmnx}" />
							<input type="hidden" name="q" value="${efn:encrypt(2)}" />
							<input type="hidden" name="idx" value="" />
							<div class="form-group">
								<c:set var="modelItem" value="sdayf" />
								<div id="daterange-picker" class="input-daterange input-group">
									<input type="text" name="${modelItem}" value="${today}" class="form-control" autocomplete="off">
									<div class="input-group-append input-group-prepend">
										<span class="input-group-text fs-xl"><i class="fal fa-ellipsis-h"></i></span>
									</div>
									<c:set var="modelItem" value="sdayt" />
									<input type="text" name="${modelItem}" value="${today}" class="form-control" autocomplete="off">
									<%-- <div class="input-group-append">
										<c:set var="modelItem" value="stype" />
										<select name="${modelItem}" id="${modelItem}" class="form-control w-50">
											<option value="">전체</option>
											<option value="${efn:encrypt('i')}">아이디</option>
											<option value="${efn:encrypt('n')}">이름</option>
										</select>
										<c:set var="modelItem" value="skey" />
										<label for="${modelItem}" class="sr-only">검색어</label>
										<input type="text" name="${modelItem}" id="${modelItem}2" class="form-control" placeholder="검색어를 입력하세요." />
									</div> --%>
									<div class="input-group-prepend">
										<button type="button" class="btn4logSearch btn btn-sm btn-dark wave-effect waved-themed form-control">검색</button>
									</div>
								</div>
							</div>
							
							<table class="table table-striped m-0 tbListA">
								<colgroup>
									<col style="width:10%" />
									<col />
									<col style="width:30%" />
								</colgroup>
	
								<thead class="bg-warning-900 text-white">
									<tr>
										<th scope="col">번호</th>
										<th scope="col">행위</th>
										<th scope="col">등록일</th>
									</tr>
								</thead>
								
								<tbody id="userLogTbody" class="pd10 alignC"></tbody>
							</table>
						</form>
						
						<div class="mgt30" style="text-align:center;">
							<div id="userLogPaging"></div>
						</div>
					</div>
					<div class="modal-footer inno pdt0">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">닫기</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // 사용자 메뉴 로그 모달 end --%>
<script>
<%-- 검색 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4logSearch', function() {
	var $frm = $(this).closest('form'), $data = $frm.serialize();
	fn_ajax( { url : 'memberLoglistAjax.do?cmnx=${cmnx}', data : $data } );
});
<%-- 로그 모달 클로즈 이벤트 --%>
$(document).on('hidden.bs.modal', '.memLogModal', function() {
	$('input[name=sdayf], input[name=sdayt]').val('${today}');
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '';

varSource += '<tr>';
	<c:set var="modelItem" value="memberId" />
varSource += '<th scope="row"><label for="${modelItem}">아이디</label></th>';
varSource += '<td>${efn:getMaskData(model[modelItem], 1)}<input type="hidden" name="${modelItem}" value="${model[modelItem]}" /></td>';
	<c:set var="modelItem" value="memberName" />
varSource += '<th scope="row"><label for="${modelItem}">성명</label></th>';
varSource += '<td>${efn:getMaskData(model[modelItem], 1)}</td>';
varSource += '</tr>';
varSource += '<tr>';
	<c:set var="modelItem" value="memberPwd" />
varSource += '<th scope="row"><label for="${modelItem}">비밀번호</label></th>';
varSource += '<td><input type="password" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" placeholder="${_comUserRegistFrmPwdMsg_}" class="form-control"/></td>';
	<c:set var="modelItem" value="memberRePwd" />
varSource += '<th scope="row"><label for="${modelItem}">비밀번호 확인</label></th>';
varSource += '<td><input type="password" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control"/></td>';
varSource += '</tr>';

<%-- <c:if test="${model.memberIdx eq ss.ss_memberIdx}"> --%>
varSource += '<tr>';
	<c:set var="modelItem" value="memberNowPwd" />
varSource += '<th scope="row"><label for="${modelItem}">${model.memberIdx eq ss.ss_memberIdx ? '현재' : '관리자'} 비밀번호</label> <span>*</span></th>';
varSource += '<td colspan="3"><input type="password" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" placeholder="현재 로그인한 관리자 비밀번호를 입력해 주십시오." class="form-control" required /></td>';
varSource += '</tr>';
<%-- </c:if> --%>
varSource += '<tr>';
	<c:set var="modelItem" value="authIdx" />
varSource += '<th scope="row"><label for="${modelItem}">권한</label> <span>*</span></th>';
varSource += '<td>';
<c:choose>
	<c:when test="${empty param.q}">
varSource += '<select name="${modelItem}" id="${modelItem}${model.groupIdx}" class="form-control" required>';
	<c:forEach items="${authList}" var="authModel" varStatus="i">
varSource += '<option value="${efn:encrypt(authModel[modelItem])}"<c:if test="${model[modelItem] eq authModel[modelItem]}"> selected</c:if>>${efn:strContentsFormat(authModel.authName, '')}</option>';
	</c:forEach>
varSource += '</select>';
	</c:when>
	<c:otherwise>
varSource += '${model.authName}';
	</c:otherwise>
</c:choose>
varSource += '</td>';
	<c:set var="modelItem" value="department" />
varSource += '<th scope="row"><label for="${modelItem}">소속</label> <span>*</span></th>';
varSource += '<td><input type="text" name="${modelItem}" id="${modelItem}" value="${efn:strContentsFormat(model[modelItem], '')}" class="form-control" required /></td>';
varSource += '</tr>';

varSource += '<tr>';
	<c:set var="modelItem" value="mobile" />
varSource += '<th scope="row"><label for="${modelItem}">연락처</label> <span>*</span></th>';
varSource += '<td>';
varSource += '<input type="tel" name="${modelItem}" id="${modelItem}" value="${efn:decrypt(model[modelItem])}" placeholder="010-1234-5678" pattern="[0-9]{3}-[0-9]{3,4}-[0-9]{4}" class="form-control" required />';
varSource += '</td>';
	<c:set var="modelItem" value="email" />
varSource += '<th scope="row"><label for="${modelItem}">이메일</label> <span>*</span></th>';
varSource += '<td><input type="email" name="${modelItem}" id="${modelItem}" value="${efn:decrypt(model[modelItem])}" class="form-control" required /></td>';
varSource += '</tr>';

	<c:set var="modelItem" value="loginFail" />
varSource += '<tr id="unlockTr">';
	<c:if test="${model[modelItem] gt 4}">
varSource += '<th scope="row"><label for="${modelItem}">계정잠금</label></th>';
varSource += '<td colspan="3"><button type="button" data-data="${efn:encrypt(model.memberIdx)}" class="btn4unlock btn btn-warning waves-effect waves-themed text-white">잠금해제</button></td>';
varSource += '</tr>';
	</c:if>
varSource += '<tr>';
	<c:set var="modelItem" value="etc" />
varSource += '<th scope="row"><label for="${modelItem}">비고</label></th>';
varSource += '<td colspan="3"><textarea name="${modelItem}" id="${modelItem}" rows="5" class="form-control">${efn:strContentsFormat(model[modelItem], crlf)}</textarea></td>';
varSource += '</tr>';

$('#fn_memberModFrm').find('#memberIdx').val('${efn:encrypt(model.memberIdx)}');
$('#memModTableTbody').html(varSource);

varSource = null;
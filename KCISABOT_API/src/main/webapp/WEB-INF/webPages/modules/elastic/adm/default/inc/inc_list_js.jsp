<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
var $_index, $_listPage = 1, $_listPageBlock = 10;
var $_setting, $_mapping, $_conf, $_dict, $_setCont, $_dataDet, $_searchExp;
<%-- 사전 / 불용어 / 동의어 관리 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4dict', function() {
	var $title = $(this).text(), $role = $(this).data('role');
	const $success = function(data) {
		if (data.result) {
			$('#dict-mng-popup').find('.CodeMirror').remove();
			$('#dict').html(data.result);
			$('#dict-mng-popup h4').text($title);
			$('#fn_dictInputFrm input[name=q]').val($role);
			setTimeout(function() {
				$_dict = fn_setCodeEditor({ id : 'dict', height : 500, mode : 'text/html' });
			}, 400);
		}
	};
	fn_apiAjax( { url : '/callEsDictAjax.do', data : { q : $role }, success : $success } );
});
<%-- 엘라스틱 / LOGSTASH 시작 & 종료 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4cmd', function() {
	var $role = $(this).data('role'), $server = $(this).data('server'), $status = $(this).data('status');
	var $msg = '<b class="text-info">'+ $server + '</b> 서버를 <b class="text-danger">' + $status + '</b> 하시겠습니까?';
	fn_openSweetAlert( { icon : 'warning', title : '알림', msg : $msg, cancel : true, successFunc : function() {
		fn_setPageLoading(true);
		const $success = function(data) {
			let $delay, $openFunc;
			if (data.result) {
				$delay = ($role == 'estt') ? 20* 1000 : 2 * 1000;
				$openFunc = function() {
					fn_openNoti( { icon : 'success', title : '알림', msg : '정상적으로 ' + $status + ' 하였습니다.' } );
					$('.btn4refresh').click();
				};
			}
			else {
				$delay = 2000;
				$openFunc = function() {
					fn_openSweetAlert( { icon : 'error', title : '알림', msg : $status + ' 발생하였습니다.<br><br>' + data.msg } );
				};
			}

			setTimeout(function() {
				fn_setPageLoading(false);
				$openFunc();
			}, $delay);
		}
		
		fn_apiAjax( { url : '/callEsCommandProc.do', data : { q : $role }, success : $success } );
	} });
});
<%-- 인덱스 설정 파일 리스트 --%>
function fn_setIndexSettingFileList() {
	const $success = function(data) {
		if (data.result) {
			$.each(data.result, function(k, v) {
				var $html = '', $tagName = '';
				$(v).each(function(i, e) {
					$tagName = k + 'FileName';
					$html += '<div class="col-sm-10 ml-2 mb-2 custom-control custom-radio custom-control-inline">';
					$html += '<input type="radio" name="' + $tagName + '" id="' + $tagName + i + '" value="' + e + '" class="custom-control-input">';
					$html += '<label class="custom-control-label" for="' + $tagName + i + '">' + e + '</label>';
					$html += '<button type="button" data-data="' + e + '" data-role="' + k + '" class="btn4settings btn btn-xs btn-info ml-2">상세</button>';
					$html += '<button type="button" data-data="' + e + '" data-role="' + k + '" class="btn4delSettings btn btn-xs btn-danger ml-2">삭제</button>';
					$html += '</div>';
				});
				$('#' + k + 'FileNameDiv').html($html);
			});
		}
	};
	fn_apiAjax( { url : '/callEsFileListAjax.do', success : $success } );
}
<%-- 인덱스 추가 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4add', function() {
	fn_setIndexSettingFileList();
});
<%-- 인덱스 추가 모달 - 설정 파일 상세 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4settings', function() {
	var $f = $(this).data('data'), $role = $(this).data('role');
	const $success = function(data) {
		if (data.result) {
			$('#es-settings-mng-popup').modal();
			$('#es-settings-mng-popup').find('.CodeMirror').remove();
			$('#settingsCont').html(data.result);
			$('#es-settings-mng-popup h4').text($f);
			$('#fn_esSettingsInputFrm input[name=f]').val($f);
			$('#fn_esSettingsInputFrm input[name=q]').val($role);
			setTimeout(function() {
				$_setCont = fn_setCodeEditor({ id : 'settingsCont', height : 500 });
			}, 400);
		}
	};
	fn_apiAjax( { url : '/callEsSettingsAjax.do', data : { f : $f, q : $role }, success : $success } );
});
<%-- 인덱스 추가 모달 - 설정 파일 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4delSettings', function() {
	var $f = $(this).data('data'), $role = $(this).data('role');
	fn_openSweetAlert( { icon : 'info', title : '알림', msg : '${_confirmDeleteMsg_}', cancel : true, successFunc : function() {
		fn_ajax( { url : '/callEsDelSettingsActionProc.do', data : { f : $f, q : $role }, type : 'POST' } );
	} });
});
<%-- 인덱스 시작 / 중지 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4act', function() {
	var $data = $(this).data('data'), $act = $(this).data('act'), $status = $(this).text();
	var $msg = '<b class="text-primary">' + $data + '</b> 인덱스를 <b class="text-danger">' + $status + '</b> 하시겠습니까?';
	fn_openSweetAlert( { icon : 'warning', title : '알림', msg : $msg, cancel : true, successFunc : function() {
		const $success = function(data) {
			if (data.acknowledged) fn_getIndexList();
		}
		fn_apiAjax( { url : '/callEsAjax.do', data : { url : $data + '/' + $act, method : 'POST' }, success : $success } );
	} });
});
<%-- 인덱스 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4del', function() {
	var $index = $(this).data('data');
	var $msg = '<b class="text-primary">' + $index + '</b> 인덱스를 <b class="text-danger">삭제</b> 하시겠습니까?<br><br><b class="text-danger">삭제후 복구가 불가합니다.</b>';
	fn_openSweetAlert( { icon : 'warning', title : '알림', msg : $msg, cancel : true, successFunc : function() {
		const $action = 'esIndexActionProc.do', $data = { index : $index, q : 'd' };
		const $success = function(data) {
			fn_getIndexList();
		}
		fn_ajax( { url : $action, data : $data, type : 'POST', success : $success } );
	} });
});
<%-- set search parameters --%>
function fn_setParams() {
	var $param = new Object(), $stype = $('#stype').val(), $skey = $('#skey').val(), $scat = $('select[name=scat]').val();
	var $useDate = $('#useDate').is(':checked'), $publishSdate = $('#publishSdate').val(), $publishEdate = $('#publishEdate').val();
	$param.offset = $_listPage
	$param.size = $_listPageBlock;
	//$param.sort = { 'publishDateTime.keyword' : 'desc' };
	$param['_source'] = [ 'id', 'subject', 'category1name', 'category2name', 'dispPublishDate', 'publisher'];

	var $query = new Object(), $range = {'range' : { 'publishDate' : {} } }, $must = new Array();
	$query = { 'bool' : { 'filter' : [], 'must' : [] }};
	if ($useDate) {	<%-- 검색일자 셋팅 --%>
		var $tempPublishDate  = new Object(); 
		if (!fn_isNull($publishSdate)) $tempPublishDate.gte = fn_replaceAll($publishSdate, '-', '');
		if (!fn_isNull($publishEdate)) $tempPublishDate.lte = fn_replaceAll($publishEdate, '-', '');
		$range.range.publishDate = $tempPublishDate;
		$query.bool.filter = $range;
	}
	if (!fn_isNull($skey)) {	<%-- 검색어 셋팅 --%>
		var $_arr = $stype.split(','), $_term = '';
		$($_arr).each(function(i, e) {
			if (!fn_isNull($_term)) $_term += ' ';
			$_term += e + ':(' + $skey + ')';
		});
		$must.push( { 'query_string' : { 'query' : $_term } } );
	}
	if (!fn_isNull($scat)) {	<%-- 분류 셋팅 --%>
		$must.push( { 'match' : { 'category1' : $scat } } );
	}
	
	$query.bool.must = $must;
	$param.query = $query;
	$param.track_total_hits = true;
	
	return JSON.stringify($param);
}
<%-- paging --%>
function fn_paging(page){
	if (!fn_isNull(page)) $_listPage = page;
	fn_apiAjax( { url : '/search.do', data : { url : $_index, method : 'POST', param : fn_setParams() }, success : fn_setDataList } );
}
function fn_setDataList_detail(data, totalCount, totalPage) {
	var $html = '';
	if (!$.isEmptyObject(data)) {
		var $list = data.result.documents;
		if (totalCount == 0) {
			totalPage = 1;
			$html = $('<tr/>').append($('<td/>', { 'colspan' : 5, 'text' : '${_infoNoDataMsg_}' }));
		}
		
		$($list).each(function(i, e) {
			var $item = e;
			var $subject = $item.subject;
			if (!fn_isNull($item.category2name)) $subject = '<b>[' + $item.category1name + ' > ' + $item.category2name + ']</b> ' + $subject;
			$subject = fn_convertHtmlToCode($subject);
			
			$html += '<tr>';
			$html += '<td class="text-left"><span data-toggle="tooltip" data-original-title="' + fn_convertHtmlToCode($item.subject) + '" class="ellipsis500">' + $subject + '</span></td>';
			$html += '<td>' + $item.publisher + '</td>';
			$html += '<td>' + fn_setUTCDateFormat($item.dispPublishDate) + '</td>';
			$html += '<td><button type="button" data-index="' + $_index + '" data-data="' + $item.id + '" data-subj="' + $subject + '" data-toggle="modal" data-target="#data-det-popup" class="btn4dataDet btn btn-sm btn-secondary">상세</button></td>';
			$html += '<td><input type="checkbox" name="listCboxs" data-index="' + e._index + '" value="' + $item.id + '"></td>';
			$html += '</tr>';
		});
	}
	else {
		totalCount = 0;
		$html = $('<tr/>').append($('<td/>', { 'colspan' : 5, 'text' : '${_infoNoDataMsg_}' }));
	}
	$('#index-data-list-modal h5').html($_index + ' - 데이터 리스트 (전체 : ' + $.number(totalCount) + '건) ' + data.result.took);
	$('#dataListTbody').html($html);
	$('#index-data-list-modal').modal();
	$('[data-toggle=tooltip]').tooltip();
}
function fn_setDataList(data) {
	var $html = '', $totalCount, $totalPage = 1;
	if (!$.isEmptyObject(data)) {
		$totalCount = data.result.totalCount;
		$totalPage = Math.ceil($totalCount / $_listPageBlock);
		if ($totalPage == 0) $totalPage = 1;
		<%-- 상세 설정 --%>
		fn_setDataList_detail(data, $totalCount, $totalPage);
	}
	
	<%-- paging --%>
	var pageOpt = {obj : 'paging', fn : 'fn_paging', total : $totalPage, visible : 10};
	$isApiLoad = true;
	fn_setAPIPaging(pageOpt);
	$isApiLoad = false;
}
<%-- 데이터 관리 모달 오픈 이벤트 --%>
$(document).on('shown.bs.modal', '#index-data-list-modal', function() {
	const $indexDate = $_index.replaceAll(/[a-z]|\_/g, '');
	const $minDate = fn_firstDateOfMonth($indexDate.substr(0, 4), $indexDate.substr(4, 2));
	const $maxDate = fn_lastDateOfMonth($indexDate.substr(0, 4), $indexDate.substr(4, 2));
	$(this).find('input[data-role=picker]').each(function() {
		var $period = $(this).attr('data-period');
		$(this).val( ($period == 1) ? $minDate : $maxDate);
	});
	try { $('.input-daterange').datepicker('destroy'); } catch(e) {}
	$('.input-daterange').datepicker({ keyboardNavigation: false, forceParse: false, autoclose: true, format: 'yyyy-mm-dd', language: 'kr', startDate : $minDate, endDate : $maxDate });
});
<%-- 데이터 관리 모달 - 상세 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4dataDet', function() {
	var $index = $(this).data('index'), $data = $(this).data('data'), $subj = $(this).data('subj');
	const $success = function(data) {
		$('#data-det-popup').find('.CodeMirror').remove();
		$('#data-det-popup .modal-title').html($subj);
		$('#data-det-popup').find('#dataDet').val(JSON.stringify(data._source, null, 2));
		setTimeout(function() {
			$_dataDet = fn_setCodeEditor({ id : 'dataDet', height : 500 });
			$('#fn_dataDetFrm').find('input[name=q]').val($index);
		}, 400);
	};
	fn_apiAjax( { url : '/callEsAjax.do', data : { url : $index + '/_doc/' + $data, method : 'GET' }, success : $success } );
});
<%-- 인덱스 관리 리스트 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4list', function() {
	$_index = $(this).data('data');
	$_listPage = 1;
	$_listPageBlock = 10;
	$isApiLoad = true;
	$('#stype').val('subject');
	$('#skey').val('');
	$('#slmt').val($_listPageBlock);
	$('#fn_dataDelFrm input[name=url]').val($_index);
	if (!fn_isNull( $('#paging').data("twbs-pagination") )) $('#paging').twbsPagination('destroy');
	fn_apiAjax( { url : '/search.do', data : { url : $_index, param : fn_setParams(), method : 'POST' }, success : fn_setDataList } );
});
<%-- 엘라스틱 설정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4esCfg', function() {
	const $data = $(this).data('data'), $success = function(data) {
		if (data.result) {
			$('#es-config-mng-popup h4').text($data + ' - 엘라스틱 설정 정보');
			$('#fn_esConfInputFrm input[name=id]').val($data);
			$('#fn_esConfInputFrm textarea[name=setting]').val(JSON.stringify(data.settings, null, 2));
			$('#fn_esConfInputFrm textarea[name=mapping]').val(JSON.stringify(data.mappings , null, 2));
			$('.esConfTab:eq(0)').click();
		}
	};
	fn_apiAjax( { url : '/callEsConfigAjax.do', data : { id : $data, q : 'es' }, success : $success } );
});
<%-- 엘라스틱 / logstash 설정 모달 - 탭 클릭 이벤트 --%>
$(document).on('click', '.esConfTab', function() {
	if ($($(this).attr('href')).find('.CodeMirror').length == 0) {
		var $role = $(this).data('role');
		setTimeout(function() {
			eval('$_' + $role + ' = fn_setCodeEditor({ id : \'' + $role + '\', height : 500 });');
		}, 400);
	}
});
<%-- 엘라스틱 / logstash 설정 모달 클로즈 이벤트 --%>
$(document).on('hidden.bs.modal', '[id*=config-mng-popup]', function() {
	$(this).find('.CodeMirror').remove();
});
<%-- LOGSTASH 설정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4logCfg', function() {
	var $data = $(this).data('data');
	const $success = function(data) {
		if (data.result) {
			$('#log-config-mng-popup h4').text($data + ' - LOGSTASH 설정 정보');
			$('#fn_logConfInputFrm input[name=id]').val($data);
			$('#fn_logConfInputFrm textarea[name=conf]').val(data.result);
			setTimeout(function() {
				$_conf = fn_setCodeEditor({ id : 'conf', height : 500 });
			}, 400);
		}
	};
	fn_apiAjax( { url : '/callEsConfigAjax.do', data : { id : $data, q : 'log' }, success : $success } );
});
<%-- 데이터 리스트 모달 - 검색 / 적용 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4search, .btn4limit', function() {
	$isApiLoad = true;
	$_listPage = 1;
	$_listPageBlock = Number($('#slmt').val());
	if (!fn_isNull( $('#paging').data("twbs-pagination") )) $('#paging').twbsPagination('destroy');
	fn_apiAjax( { url : '/search.do', data : { url : $_index, param : fn_setParams(), method : 'POST' }, success : fn_setDataList } );
});
<%-- 데이터 리스트 모달 - 검색어 엔터키 이벤트 --%>
$(document).on('keypress', '#skey', function(e) {
	if (e.keyCode == 13) $('.btn4search').click();
});
<%-- 엘라스틱 인덱스 리스트 --%>
function fn_getIndexList() {
	const $success = function(data) {
		var $html = '', $idxArr = [];
		if (!$.isEmptyObject(data)) {
			const list = $(data.result).sort(function(a, b) {
				return a.index > b.index ? -1 : a.index < b.index ? 1 : 0;
			});
			$(list).each(function(i, e) {
				if (!e['index'].startsWith('.') && e.index.indexOf('dataset') > -1) {
					var $count = (fn_isNull(e['docs.count'])) ? '' : $.number(e['docs.count']);
					var $size = (fn_isNull(e['store.size'])) ? '' : e['store.size'];
					$html += '<tr>';
					$html += '<td>' + e['index'] + '</td>';
					$html += '<td>' + e['uuid'] + '</td>';
					$html += '<td>' + $count + '</td>';
					$html += '<td>' + $size + '</td>';
					$html += '<td>' + e['status'] + '</td>';
					$html += '<td>';
					if (e['status'] == 'open')
						$html += '<button type="button" data-act="_close" class="btn4act btn btn-sm btn-secondary">중지</button>';
					$html += '</td>';
					$html += '<td>';
					if (e['status'] == 'close')
						$html += '<button type="button" data-act="_open" class="btn4act btn btn-sm btn-primary">시작</button>';
					$html += '</td>';
					$html += '<td><button type="button" class="btn4list btn btn-sm btn-info">리스트</button></td>';
					$html += '<td><button type="button" data-toggle="modal" data-target="#es-config-mng-popup" class="btn4esCfg btn btn-sm btn-warning">설정</button></td>';
					$html += '<td><button type="button" data-toggle="modal" data-target="#log-config-mng-popup" class="btn4logCfg btn btn-sm btn-warning">설정</button></td>';
					$html += '<td><button type="button" class="btn4del btn btn-sm btn-danger">삭제</button></td>';
					$html += '</tr>';
					$idxArr.push(e['index']);
				}
			});
		}
		else {
			$html = $('<tr/>').append($('<td/>', { 'colspan' : 11, 'text' : '엘라스틱서치가 중지되어있습니다.' }));
		}
		$('#listTbody').html($html);
		$($idxArr).each(function(i, e) { $('#listTbody').find('tr:eq(' + i + ') button').data('data', e); });
		fn_setPageLoading(false);
	}
	fn_apiAjax( { url : '/callEsAjax.do', data : { url : '_cat/indices', method : 'GET' }, success : $success } )
}
<%-- SWAGGER 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4swagger', function() {
	window.open('/swagger-ui.html');
});
<%-- REFRESH 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4refresh', function() {
	fn_setPageLoading(true);
	fn_getIndexList();
});
$(document).ready(function() {
	$('.btn4refresh').click();
});
</script>
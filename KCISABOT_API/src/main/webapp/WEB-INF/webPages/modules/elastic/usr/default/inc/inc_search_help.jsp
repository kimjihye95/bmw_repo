<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<aside class="sidebar">
	<div class="sidebar-content">
		<h5>SEARCH API</h5>
		<div class="scroll_side">
			<h6>뉴스 원문 검색 페이지</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/search &nbsp; <a href="/swagger-ui.html#/elastic-search-api-controller/searchUsingPOST" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>query 검색어</li>
					<li>sword 불용어 [ 여러개일 경우 ',' 로 구분 ]</li>
					<li>period 기간 구분 [ D : 일, W : 주, M : 월, Y : 년 ] [ default : D ] (* 필수)</li>
					<li>category1 카테고리 [ 100 : 정치, 101 : 경제, 102 : 사회, 103 : 생활/문화, 104 : 세계, 105 : IT/과학 ]</li>
					<li>category2 서브 카테고리 [ 731 : 모바일, 230 : IT 일반, ... ]</li>
					<li>sdate 검색 시작일 (* 필수)</li>
					<li>edate 검색 종료일 (* 필수)</li>
					<li>sortType 정렬순서 구분 [ ASC, DESC ] [ default : DESC ]</li>
					<li>pageSize 화면에 표시되는 페이징 사이즈 [ default : 10 ]</li>
					<li>offset 검색 결과 중 리턴받을 page offset [ default : 1 ]</li>
					<li>limit 검색 결과 중 리턴받을 개수 [ default : 10 ]</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
					<li style="word-break:break-all;"><pre class="paramCode text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode text-white"></pre></li>
				</ul>
			</div>
			
			<h6>뉴스 원문 상세 페이지</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/searchDetail &nbsp; <a href="/swagger-ui.html#/elastic-search-api-controller/searchDetailUsingGET" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>id 뉴스 원문 ID</li>
					<li>ind 뉴스 인덱스</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
	         <li style="word-break:break-all;"><pre class="paramCode2 text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode2 text-white"></pre></li>
				</ul>
			</div>
		</div>
		
		<button>
			<span class="sidebar-btn"><i class="fas fa-angle-right"></i>
				&nbsp;도움말 열기</span>
		</button>
	</div>
</aside>
<script>
	$('.scroll_side').slimscroll({ height : '850px' });
	var $_param = {"query":"애플","sword":"테스트,테스트2","category2":"731","period":"M","sdate":"20201008","edate":"20210408","limit":50};
	var $_code = {"result":{"took":"36ms","documents":[{"category2":731,"subject":"제목","category1":105,"publishDate":20210305,"link":"뉴스 URL","publishDateTime":202103051650,"category2name":"모바일","type":"itscience","score":null,"contents":"내용","category1name":"IT/과학","publisher":"ZDNet Korea","dispPublishDate":"2021.03.05. 오후 4:50","id":"뉴스 ID"} ],"totalCount":3750}};
	$('.paramCode').text(JSON.stringify($_param, null, 2));
	$('.respCode').text(JSON.stringify($_code, null, 2));

	$_param = {"id":"609961e90b08c6196c16ba41", "ind" : "202105"};
	$_code = {"result":{"category2":263,"subject":"CEO 반체제 고전시 투고로 주가 9.8% 급락","category1":101,"publishDate":20210510,"link":"https://news.naver.com/main/read.nhn","publishDateTime":202105102359,"category2name":"경제 일반","type":"economy","contents":"중국 최대 배달 사이트 ...  ","publisher":"뉴시스","category1name":"경제","dispPublishDate":"2021.05.10. 오후 11:59","id":"609961e90b08c6196c16ba41"}};
	$('.paramCode2').text(JSON.stringify($_param, null, 2));
	$('.respCode2').text(JSON.stringify($_code, null, 2));

</script>
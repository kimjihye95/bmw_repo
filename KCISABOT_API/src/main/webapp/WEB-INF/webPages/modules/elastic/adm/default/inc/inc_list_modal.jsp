<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!--- 모달 팝업 (사전 관리)시작 --->
<div class="modal fade" id="dict-mng-popup" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-dark text-white">
				<h4 class="modal-title"></h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<form id="fn_dictInputFrm" method="post" action="/callEsDictActionProc.do">
					<input type="hidden" name="cmnx" value="${cmnx}" />
					<input type="hidden" name="q" value="" />
					<div class="form-row form-group">
						<div class="col-md-12 mb-3">
							<c:set var="modelItem" value="dict" />
							<label class="form-label" for="${modelItem}">사전</label><span class="text-danger">*</span>
							<textarea name="${modelItem}" id="${modelItem}" rows="20" class="form-control"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
        <a href="#" class="btn4dictSubmit btn btn-primary">저장</a>
        <a href="#menu-popup" id="resetBtnMenuFrm" data-dismiss="modal" rel="#fn_siteInputFrm" class="btn btn-secondary">취소</a>
			</div>
		</div>
	</div>
</div>
<!--- 모달 팝업 (사전 관리) 끝 --->
<script>
<%-- 사전 관리 모달 - 저장 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4dictSubmit', function() {
	var $frm = $('#fn_dictInputFrm');
	fn_openSweetAlert( { icon : 'info', title : '알림', msg : '${_confirmSaveMsg_}', cancel : true, successFunc : function() {
		$_dict.save();
		var $action = $frm.attr('action'), $data = $frm.serialize();
		fn_ajax( { url : $action, data : $data, type : 'POST' } );
	} });
});
</script>
<!--- 모달 팝업 (엘라스틱 설정 관리)시작 --->
<div class="modal fade" id="es-config-mng-popup" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-dark text-white">
				<h4 class="modal-title"></h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item"><a class="esConfTab nav-link active" data-role="setting" data-toggle="tab" href="#tab_direction-1" role="tab" aria-selected="true">SETTING</a></li>
					<li class="nav-item"><a class="esConfTab nav-link" data-role="mapping" data-toggle="tab" href="#tab_direction-2" role="tab" aria-selected="false">MAPPING</a></li>
				</ul>
				
				<form id="fn_esConfInputFrm" method="post" action="/callEsConfigActionProc.do?cmnx=${cmnx}">
					<input type="hidden" name="cmnx" value="${cmnx}" />
					<input type="hidden" name="id" value="" />
					<div class="tab-content p-3">
						<c:set var="tabId" value="setting,mapping" />
						<c:forEach items="${fn:split(tabId, ',')}" var="tabItem" varStatus="i">
							<div class="tab-pane fade esconf<c:if test='${i.first}'> active show</c:if>" data-role="${tabItem}" id="tab_direction-${i.count}" role="tabpanel">
								<div class="form-row form-group">
									<div class="col-md-12 mb-3">
										<label class="form-label" for="${tabItem}">설정</label><span class="text-danger">*</span>
										<textarea name="${tabItem}" id="${tabItem}" rows="20" class="form-control" required></textarea>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</form>
			</div>
			<div class="modal-footer">
        <a href="#" class="btn4esConfSubmit btn btn-primary">저장</a>
        <a href="#menu-popup" id="resetBtnMenuFrm" data-dismiss="modal" rel="#fn_siteInputFrm" class="btn btn-secondary">취소</a>
			</div>
		</div>
	</div>
</div>
<!--- 모달 팝업 (엘라스틱 설정 관리)끝 --->
<script>
<%-- 엘라스틱 설정 관리 모달 - 저장 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4esConfSubmit', function() {
	var $frm = $('#fn_esConfInputFrm'), $role = $('.esconf.show').data('role');
	var $msg = '<b class="text-info">' + $role.toUpperCase() + '</b> 정보를 ${_confirmSaveMsg_}';
	fn_openSweetAlert( { icon : 'info', title : '알림', msg : $msg, cancel : true, successFunc : function() {
		fn_setPageLoading(true);
		eval('$_' + $role + '.save();');<%-- code mirror 데이터 셋팅 --%>
		var $val = $('.esconf.show').find('textarea').val(), $id = $frm.find('input[name=id]').val();
		var $action = $frm.attr('action'), $data = { q : $role, id : $id, data : ($val) };
		
		const $success = function(data) {
			if (data.acknowledged) {
				fn_apiAjax( { url : $action, data : $data, type : 'POST', success : function(data2) {
					if (data2.acknowledged) {
						fn_apiAjax( { url : '/callEsAjax.do', data : { url : $id + '/_open', method : 'POST' }, success : function() {
							$('#es-config-mng-popup').modal('hide');
							fn_openNoti({ icon : 'success', title : '알림', msg : '${_successCommonInsert_}'});
						} } );
					} else {
						fn_openSweetAlert( { icon : 'error', title : '알림', msg : data2.error.reason} );
					}
				} } );
			}
		}
		fn_apiAjax( { url : '/callEsAjax.do', data : { url : $id + '/_close', method : 'POST' }, success : $success } );
		fn_setPageLoading(false);
	} });
});
</script>
<!--- 모달 팝업 (logstash 설정 관리)시작 --->
<div class="modal fade" id="log-config-mng-popup" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-dark text-white">
				<h4 class="modal-title"></h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<form id="fn_logConfInputFrm" method="post" action="/callEsConfigActionProc.do?cmnx=${cmnx}">
					<input type="hidden" name="cmnx" value="${cmnx}" />
					<input type="hidden" name="id" value="" />
					<div class="form-row form-group">
						<div class="col-md-12 mb-3">
							<c:set var="modelItem" value="conf" />
							<label class="form-label" for="${modelItem}">설정</label><span class="text-danger">*</span>
							<textarea name="${modelItem}" id="${modelItem}" rows="20" class="form-control"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
        <a href="#" class="btn4logConfSubmit btn btn-primary">저장</a>
        <a href="#menu-popup" id="resetBtnMenuFrm" data-dismiss="modal" rel="#fn_siteInputFrm" class="btn btn-secondary">취소</a>
			</div>
		</div>
	</div>
</div>
<!--- 모달 팝업 (logstash 설정 관리) 끝 --->
<script>
<%-- logstash 설정 관리 모달 - 저장 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4logConfSubmit', function() {
	var $frm = $('#fn_logConfInputFrm');
	fn_openSweetAlert( { icon : 'info', title : '알림', msg : '${_confirmSaveMsg_}', cancel : true, successFunc : function() {
		$_conf.save();<%-- code mirror 데이터 셋팅 --%>
		var $action = $frm.attr('action'), $data = $frm.serialize();
		const $success = function(data) {
			if (data.acknowledged) {
				$('#log-config-mng-popup').modal('hide');
				fn_openNoti({ icon : 'success', title : '알림', msg : '${_successCommonInsert_}'});
			}
		};
		fn_apiAjax( { url : $action, data : $data, type : 'POST', success : $success } );
	} });
});
</script>
<!--- 모달 팝업 (인덱스 추가)시작 --->
<div class="modal fade" id="index-rgst-popup" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-dark text-white">
				<h4 class="modal-title">인덱스 추가</h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<form id="fn_indexInputFrm" method="post" action="esIndexActionProc.do">
					<input type="hidden" name="cmnx" value="${cmnx}" />
					<div class="form-row form-group">
						<div class="col-md-12 mb-3">
							<c:set var="modelItem" value="index" />
							<label class="form-label" for="${modelItem}">인덱스명</label><span class="text-danger">*</span>
							<input type="text" name="${modelItem}" id="${modelItem}" class="form-control" placeholder="메뉴명을 입력하세요." required>
						</div>
						
						<div class="col-md-12 mb-3">
							<c:set var="modelItem" value="elFileName" />
							<label class="form-label" for="${modelItem}">엘라스틱 설정 파일</label><span class="text-danger">*</span>
							<div id="elFileNameDiv" class="form-group">
							</div>
						</div>
						
						<div class="col-md-12 mb-3">
							<c:set var="modelItem" value="lsFileName" />
							<label class="form-label" for="${modelItem}">logstash 설정 파일</label><span class="text-danger">*</span>
							<div id="lsFileNameDiv" class="form-group">
							</div>
						</div>
						
					</div>
				</form>
			</div>
			<div class="modal-footer">
        <a href="#" class="btn4indexSubmit btn btn-primary">저장</a>
        <a href="#menu-popup" id="resetBtnMenuFrm" data-dismiss="modal" rel="#fn_siteInputFrm" class="btn btn-secondary">취소</a>
			</div>
		</div>
	</div>
</div>
<!--- 모달 팝업 (인덱스 추가) 끝 --->
<script>
<%-- 인덱스 추가 모달 - 저장 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4indexSubmit', function() {
	var $frm = $('#fn_indexInputFrm');
	if (!fn_checkFill($frm, 'index') || !fn_isCheckedRadio($frm, 'elFileName') || !fn_isCheckedRadio($frm, 'lsFileName')) return false;
	else {
		var $action = $frm.attr('action'), $data = $frm.serialize();
		const $success = function(data) {
			fn_getIndexList();
		}
		fn_ajax( { url : $action, data : $data, type : 'POST', success : $success } );
	}
	return false;
});
</script>
<!--- 모달 팝업 (인덱스 추가 모달 - 설정 관리)시작 --->
<div class="modal fade" id="es-settings-mng-popup" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
		<div class="modal-content">
		
			<form id="fn_esSettingsInputFrm" method="post" action="/callEsSettingsActionProc.do?cmnx=${cmnx}">
				<input type="hidden" name="cmnx" value="${cmnx}" />
				<input type="hidden" name="q" value="" />
				<input type="hidden" name="f" value="" />
				<input type="hidden" name="r" value="" />
				<div class="modal-header bg-dark text-white">
					<h4 class="modal-title"></h4>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true"><i class="fal fa-times"></i></span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row form-group">
						<div class="col-md-12 mb-3">
							<c:set var="modelItem" value="settingsCont" />
							<label class="form-label" for="${modelItem}">설정</label><span class="text-danger">*</span>
							<textarea name="${modelItem}" id="${modelItem}" rows="20" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-md-6">
	     			<div class="input-group">
	     				<c:set var="modelItem" value="rename" />
	     				<div class="input-group-prepend">
	     					<span class="input-group-text">저장 파일명</span>
	     				</div>
	     				<label for="${modelItem}" class="sr-only">저장 파일명</label>
		        	<input type="text" name="${modelItem}" id="${modelItem}" class="form-control" placeholder="ex) filename_setting.json">
		        	<div class="input-group-append">
		        		<a href="#" data-role="r" class="btn4settingsSubmit btn btn-success">다른 이름으로 저장</a>
				        <a href="#" data-role="u" class="btn4settingsSubmit btn btn-primary">수정</a>
				        <a href="#" data-dismiss="modal" class="btn btn-secondary">닫기</a>
	        		</div>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>
</div>
<!--- 모달 팝업 (인덱스 추가 모달 - 설정 관리)끝 --->
<script>
<%-- 인덱스 추가 모달 - 설정 관리 모달 - 수정 & 다른 이름으로 저장 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4settingsSubmit', function() {
	var $frm = $('#fn_esSettingsInputFrm'), $role = $(this).data('role');
	<%-- 다른 이름으로 저장일 경우 --%>
	if ($role == 'r') {
		var $rename = $frm.find('#rename'), $renmaeVal = $rename.val(), $msg = '';
		if (!fn_checkFill($frm, 'rename')) return false;
		else if ($renmaeVal.indexOf('_setting.json') < 0) {
			var $success = function() {
				setTimeout(function() {
					$rename.val('').focus();
				}, 500);
			}
			fn_openSweetAlert( { icon : 'info', title : '알림', msg : '파일명은 <b class="text-danger">파일명_setting.json</b> 형식으로 입력해 주십시오.', successFunc : $success } );
			return false;
		}
	}
	fn_openSweetAlert( { icon : 'info', title : '알림', msg : '${_confirmSaveMsg_}', cancel : true, successFunc : function() {
		$_setCont.save();<%-- code mirror 데이터 셋팅 --%>
		$frm.find('input[name=r]').val($role);
		var $action = $frm.attr('action'), $data = $frm.serialize();
		fn_ajax( { url : $action, data : $data, type : 'POST' } );
	} });
});
</script>
<%-- 인덱스 데이터 리스트(시작) --%>
<div class="modal fade" id="index-data-list-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-dark text-white">
				<h5 class="modal-title h4">데이터 관리</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel">
							<div class="panel-hdr">
								<h2></h2>
								<div class="panel-toolbar">
									<div class="form-group">
										<c:set var="modelItem" value="publish" />
										<c:set var="modelItemX" value="${modelItem}Sdate" />
										<c:set var="modelItemY" value="${modelItem}Edate" />
										<div id="daterange-picker" class="input-daterange input-group">
											<div class="input-group-prepend">
												<div class="input-group-text">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" id="useDate" class="custom-control-input">
														<label class="custom-control-label" for="useDate">날짜 검색</label>
													</div>
												</div>
											</div>
											<input type="text" name="${modelItemX}" id="${modelItemX}" value="" class="form-control" style="width:100px;" data-role="picker" data-format="Y-m-d" data-period="1" data-ref="${modelItemY}" autocomplete="off" />
											<div class="input-group-append input-group-prepend">
												<span class="input-group-text fs-xl"><i class="fal fa-ellipsis-h"></i></span>
											</div>
											<input type="text" name="${modelItemY}" id="${modelItemY}" value="" class="form-control" style="width:100px;" data-role="picker" data-format="Y-m-d" data-period="2" data-ref="${modelItemX}" autocomplete="off" />
										</div>
									</div>
								</div>
								<div class="panel-toolbar ml-2">
									<div class="form-group">
										<div class="input-group input-group-sm bg-white">
											<c:set var="modelItem" value="scat" />
											<select name="${modelItem}" id="${modelItem}" class="form-control">
												<option value="">분류 전체</option>
												<option value="100">정치</option>
												<option value="101">경제</option>
												<option value="102">사회</option>
												<option value="103">생활/문화</option>
												<option value="104">세계</option>
												<option value="105">IT/과학</option>
											</select>
										</div>
									</div>
								</div>
								<div class="panel-toolbar ml-2">
									<div class="form-group">
										<div class="input-group input-group-sm bg-white">
											<c:set var="modelItem" value="stype" />
											<select name="${modelItem}" id="${modelItem}" class="form-control">
												<!-- <option value="">전체</option> -->
												<option value="subject">제목</option>
												<option value="contents">내용</option>
												<option value="subject,contents">제목 + 내용</option>
											</select>
											<c:set var="modelItem" value="skey" />
											<label for="${modelItem}" class="sr-only">검색어</label>
											<!-- <div class="input-group-prepend">
												<span class="input-group-text bg-transparent border-right-0">
													<i class="fal fa-search"></i>
												</span>
											</div> -->
											<input type="text" name="${modelItem}" id="${modelItem}" class="form-control bg-transparent w-25" placeholder="검색어를 입력하세요." />
											<div class="input-group-append">
												<button type="button" class="btn4search btn btn-sm btn-dark wave-effect waved-themed">검색</button>
											</div>
										</div>
									</div>
								</div>
								<div class="paenl-tollbar ml-2">
									<div class="form-group">
										<div class="input-group input-group-sm bg-white">
											<c:set var="modelItem" value="slmt" />
											<label for="${modelItem}" class="sr-only">리스트갯수</label>
											<select name="${modelItem}" id="${modelItem}" class="form-control">
												<option value="10">10개씩</option>
												<option value="20">20개씩</option>
												<option value="30">30개씩</option>
												<option value="50">50개씩</option>
												<option value="100">100개씩</option>
											</select>
											<div class="input-group-append">
												<button type="button" class="btn4limit btn btn-sm btn-dark wave-effect waved-themed">적용</button>
											</div>
										</div>
									</div>
								</div>
								<div class="paenl-tollbar ml-2">
									<div class="btn-group btn-group-sm">
										<a href="#" data-rel="listCboxs" class="btn4dataDel btn btn-sm btn-danger">삭제</a>
										<a href="#" data-toggle="modal" data-target="#index-data-list-search-modal" class="btn4searchExp btn btn-sm btn-secondary">검색식</a>
									</div>
								</div>
							</div>
							
							<div class="panel-container px-3 pt-3" role="content" style="height:500px; overflow-y:auto;">
								<form id="fn_dataDelFrm">
									<input type="hidden" name="url" />
									<table class="table table-hover">
										<caption></caption>
										<colgroup>
											<col />
											<col style="width:120px"/>
											<col style="width:150px"/>
											<col style="width:100px"/>
											<col style="width:50px"/>
										</colgroup>
			
										<thead class="thead-dark">
											<tr>
												<th scope="col">제목</th>
												<th scope="col">게시자</th>
												<th scope="col">게시일자</th>
												<th scope="col">상세정보</th>
												<th scope="col">삭제</th>
											</tr>
										</thead>
										
										<tbody id="dataListTbody" class=""></tbody>
									</table>
								</form>
							</div>
						</div>
						<div id="paging"></div>
						
					</div>
				</div>
			</div>
			<%-- <div class="modal-footer">
				<button type="button" class="btn btn-secondary waves-effect waves-themed" data-dismiss="modal">${_buttonCancel_}</button>
			</div> --%>
		</div>
	</div>
</div>
<%-- 인덱스 데이터 리스트(끝) --%>
<script>
<%-- 데이터 관리 모달 - 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4dataDel', function() {
	const $frm = $('#fn_dataDelFrm'), $rel = $(this).data('rel');
	if (!fn_checkCboxChecked($rel)) return false;

	fn_openSweetAlert( { icon : 'warning', title : '알림', msg : '${_confirmDeleteMsg_}', cancel : true, successFunc : function() {
		fn_setPageLoading(true);
		var $index = $frm.find('input[name=url]').val(), $params = new Object(), $query = new Object(), $dataArr = new Array();
		$query.bool = { 'must' : { 'ids' : { 'values' : '' } } };
		$('input[name=listCboxs]:checked').each(function(i, e) {
			$dataArr.push(e.value);
		});
		$query.bool.must.ids.values = $dataArr;
		$params.query = $query;
		const $success = function(data) {
			if (!data.error) {
				setTimeout(function() {
					fn_setPageLoading(false);
					fn_getIndexList();
					fn_openNoti( { icon : 'success', title : '알림', msg : '${_successCommonDelete_}'} );
					$('.btn4search').click();
				}, 1200);
			}
			else {
				fn_openSweetAlert( { icon : 'error', title : '알림', msg : '<b>' + data.status + ' error</b><br><br>' + data.error.reason } );
			}
		};
		fn_apiAjax( { url : '/callEsAjax.do', data : { url : $index + '/_delete_by_query', param : JSON.stringify($params), method : 'POST' }, success : $success } );
	}} );
	
	return false;
});
<%-- 데이터 관리 모달 - 검색식 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4searchExp', function() {
	if ($('#index-data-list-search-modal .CodeMirror').length == 0) {
		setTimeout(function() {
			$_searchExp = fn_setCodeEditor({ id : 'searchExp', height : 500 });
			$('.btn4defaultSearchExp:eq(0)').click();
		}, 400);
	}
});
</script>
<%-- 인덱스 데이터 리스트 - 검색식(시작) --%>
<div class="modal fade" id="index-data-list-search-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-right">
		<div class="modal-content">
			<div class="modal-header bg-dark text-white">
				<h5 class="modal-title h4">엘라스틱서치 검색식</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-row form-group">
					<div class="col-md-12 mb-3">
						<c:set var="modelItem" value="searchExp" />
						<div class="btn-group btn-group-toggle" data-toggle="buttons">
							<label class="btn btn-info waves-effect waves-themed active">
								<input type="radio" name="${modelItem}" data-role="1" class="btn4defaultSearchExp">MATCH 검색 셋팅
							</label>
							<label class="btn btn-info waves-effect waves-themed active">
								<input type="radio" name="${modelItem}" data-role="2" class="btn4defaultSearchExp">QUERYSTRING 검색 셋팅
							</label>
							<label class="btn btn-info waves-effect waves-themed">
								<input type="radio" name="${modelItem}" data-role="3" class="btn4defaultSearchExp">BOOL 검색 셋팅
							</label>
						</div>
					</div>
					
					<div class="col-md-12 mb-3">
						<c:set var="modelItem" value="searchExp" />
						<label class="form-label" for="${modelItem}">검색식</label><span class="text-danger">*</span>
						<textarea name="${modelItem}" id="${modelItem}" rows="20" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
        <button type="button" class="btn4submitSearchExp btn btn-success">검색 적용</a>
				<button type="button" class="btn btn-secondary waves-effect waves-themed" data-dismiss="modal">${_buttonCancel_}</button>
			</div>
		</div>
	</div>
</div>
<%-- 인덱스 데이터 리스트 - 검색식(끝) --%>
<script>
<%-- 검색식 모달 - 검색식 셋팅 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4defaultSearchExp', function() {
	var $role = $(this).data('role'), $exp;
	if ($role == 1) {
		$exp = { 'query' : { 'match' : { 'subject' : '' } }, 'sort' : { 'publishDateTime.keyword' : 'desc' } };
	}
	else if ($role == 2) {
		$exp = { 'query' : { 'query_string' : { 'query' : 'subject:검색어 AND contents:검색어' } }, 'sort' : { 'publishDateTime.keyword' : 'desc' } };
	}
	else if ($role == 3) {
		$exp = {
				'query' : {
					'bool' : {
						'filter' : [{'range' : {'publishDate' : { 'gte' : 0}}}]
						, 'must' : [{ 'query_string' : { 'query' : 'subject:검색어 AND contents:검색어' }}]
					}
				}
				, 'sort' : { 'publishDateTime.keyword' : 'desc' }
		};
	}
	$_searchExp.setValue(JSON.stringify($exp, null, 2));
});
<%-- set search exp params --%>
function fn_setSearchExpParams() {
	try {
		var $param = JSON.parse($_searchExp.getValue());
		$param.from = Number($_listPageBlock * ($_listPage - 1));
		$param.size = $_listPageBlock;
		return JSON.stringify($param);
	} catch (e) {
		fn_openSweetAlert( { icon : 'error', title : '알림', msg : '<b>' + e + '</b>' } );
		return false;
	}
}
<%-- search exp paging --%>
function fn_searchExpPaging(page) {
	$_listPage = page;
	fn_apiAjax( { url : '/search.do', data : { url : $_index, method : 'POST', param : fn_setSearchExpParams() }, success : fn_setSearchExpDataList } );
}
<%-- set data list for search exp --%>
function fn_setSearchExpDataList(data) {
	console.log(1);
	var $html = '', $totalCount, $totalPage;
	if (!$.isEmptyObject(data)) {
		if (!data.error) {
			$totalCount = data.result.totalCount;
			$totalPage = Math.ceil($totalCount / $_listPageBlock);
			if ($totalPage == 0) $totalPage = 1;
			<%-- 상세 설정 --%>
			fn_setDataList_detail(data, $totalCount, $totalPage);
		}
		else {
			fn_openSweetAlert( { icon : 'error', title : '알림', msg : '<b>' + data.status + ' error</b><br><br>' + data.error.reason } );
		}
	}
	
	<%-- paging --%>
	var pageOpt = {obj : 'paging', fn : 'fn_searchExpPaging', total : $totalPage, visible : $_listPageBlock};
	$isApiLoad = true;
	fn_setAPIPaging(pageOpt);
	$isApiLoad = false;
}
<%-- 검색식 모달 - 검색 적용 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4submitSearchExp', function() {
	var $param = fn_setSearchExpParams();
	if (!$param) return false;
	if (!fn_isNull( $('#paging').data("twbs-pagination") )) $('#paging').twbsPagination('destroy');
	fn_apiAjax( { url : '/search.do', data : { url : $_index, param : $param, method : 'POST' }, success : fn_setSearchExpDataList } );
});
<%-- 검색식 모달 - 검색식 에디터창 ctrl+enter 키 이벤트 --%>
$(document).on('keypress', '#index-data-list-search-modal .CodeMirror', function(e) {
	if (e.keyCode == 10) $('.btn4submitSearchExp').click();
});
</script>
<!--- 모달 팝업 (데이터 상세 정보)시작 --->
<div class="modal fade" id="data-det-popup" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-dark text-white">
				<h4 class="modal-title"></h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<form id="fn_dataDetFrm" method="post" action="/callEsDictActionProc.do">
					<input type="hidden" name="cmnx" value="${cmnx}" />
					<input type="hidden" name="q" value="" />
					<div class="form-row form-group">
						<div class="col-md-12 mb-3">
							<c:set var="modelItem" value="dataDet" />
							<label class="form-label" for="${modelItem}">내용</label>
							<textarea name="${modelItem}" id="${modelItem}" rows="20" class="form-control"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
        <a href="#" class="btn4mod btn btn-success">수정</a>
        <a href="#" data-dismiss="modal" class="btn btn-secondary">닫기</a>
			</div>
		</div>
	</div>
</div>
<!--- 모달 팝업 (데이터 상세 정보) 끝 --->
<script>
<%-- 데이터 관리 모달 - 상세보기 > 수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4mod', function() {
	var $frm = $('#fn_dataDetFrm');
	fn_openSweetAlert( { icon : 'warning', title : '알림', msg : '${_confirmUpdateMsg_}', cancel : true, successFunc : function() {
		$_dataDet.save();<%-- code mirror 데이터 셋팅 --%>
		var $data = $frm.find('#dataDet').val(), $index = $frm.find('input[name=q]').val();
		var $jData = JSON.parse($data), $id = $jData.id;
		$jData['@timestamp'] = new Date().toISOString();
		const $success = function(data) {
			if (!data.error) {
				setTimeout(function() {
					fn_paging();
					$('#data-det-popup').modal('hide');
					fn_openNoti({ icon : 'success', title : '알림', msg : '${_successCommonInsert_}'});
				}, 500);
			}
			else {
				fn_openSweetAlert( { icon : 'error', title : '알림', msg : '<b>' + data.status + ' error</b><br><br>' + data.error.reason } );
			}
		};
		fn_apiAjax( { url : '/callEsAjax.do', data : { url : $index + '/_doc/' + $id, param : JSON.stringify($jData), method : 'PUT' }, type : 'POST', success : $success } );
	} });
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
var $speriod = '', $sarea = '', $satm = '', $sdate = '', $edate = '${yesterday}';
var $cookieName = '_nats_issue_filter_cookie_';
var $codeArr = [ 'POLITICS', 'ITSCIENCE', 'SOCIETY', 'ECONOMY', 'WORLD', 'LIVING', 'NONE' ];
var $codeNmArr = [ '정치', 'IT', '사회', '경제', '국제', '생활/문화', '기타' ];

var yearPeriod = [{name:'최근2년', value:'2Y'}, {name:'최근1년', value:'1Y'}, {name:'직접입력', value:'F'}];
var monthPeriod = [{name:'최근6개월', value:'6M'}, {name:'최근3개월', value:'3M'}, {name:'직접입력', value:'F'}];
var weekPeriod = [{name:'최근4주', value:'4W'}, {name:'최근3주', value:'3W'}, {name:'최근2주', value:'2W'}, {name:'최근1주', value:'1W'}, {name:'직접입력', value:'F'}];
var dayPeriod = [{name:'최근7일', value:'7D'}, {name:'최근6일', value:'6D'}, {name:'최근5일', value:'5D'}, {name:'최근4일', value:'4D'}, {name:'최근3일', value:'3D'}, {name:'최근2일', value:'2D'}, {name:'최근1일', value:'1D'}, {name:'직접입력', value:'F'}];
var periodArray = [yearPeriod, monthPeriod, weekPeriod, dayPeriod];
// 상세검색 파라미터 배열
var $paramCategory = new Array(), $paramData = new Array(), $paramNot = new Array();
var $chart2keyword = new Array();

<%-- 기간 단위 선택 이벤트 --%>
$(document).on('click', 'input[name=stype]', function() {
	var $period = $('input[name=stype]');
	var $periodIndex = $($period).index($($period).filter(':checked'));
	var $select = periodArray[$periodIndex];
	$speriod = $(this).val();

	$($select).each(function(i, e) {
		if (i == 0) $('#datePeriod').empty();
		$('#datePeriod').append($('<option value="' + e.value + '">' + e.name + '</option>'));
	});
	$stype = $('input[name=stype]:checked').val();
	$edate = '${yesterday}';
	$('#datePeriod').change();
});
<%-- 기간 선택 이벤트 --%>
$(document).on('change', '#datePeriod', function() {
	$period = $(this).val();
	var $val = Number($period.substr(0, 1));
	var $unit = $period.substr(1);
	if ($period == 'F'){
		$('.input-daterange input').prop('disabled', false);
		$('input[name=sdayf]').focus();

		$('#sdayf, #sdayt').datepicker().on('change', function(e) {
			if (e.currentTarget.id == 'sdayf') $sdate = e.currentTarget.value;
			else $edate = e.currentTarget.value;
		});
	}
	else {
		$('.input-daterange input').prop('disabled', true);
		$('#sdayt').val('${yesterday}');
	}
	if ($unit == 'Y' || $unit == 'M') {
		$sdate = fn_beforeMonth( ($unit == 'Y' ? 12 * $val: $val), fn_dateConvtStr2Date($edate));
	}
	else if ($unit == 'W' || $unit == 'D') {
		$sdate = fn_beforeDate( ($unit == 'W' ? 7 * $val : $val), fn_dateConvtStr2Date($edate));
	}
	$('#sdayf').datepicker('setDate', $sdate);
	$('#sdayt').datepicker('setDate', '${yesterday}');
});
<%-- 상세 분석 열기 버튼 클릭 이벤트 --%>
$('.collapse').on('show.bs.collapse', function (e) {
	$('.btn4searchDet').text('상세 분석 닫기');
});
$('.collapse').on('hide.bs.collapse', function (e) {
	$('.btn4searchDet').text('상세 분석 열기');
});
// 기간 단위 체크
function fn_isCheckDateUnit() {
	var $obj = $('input[name=stype]'), $today = new Date($edate);
	var $pre2year = new Date($today.getFullYear() - 2, $today.getMonth(), $today.getDate());
	var $pre24mon = new Date($today.getFullYear(), $today.getMonth() - 24, $today.getDate());
	var $period = [ fn_dateDiff(fn_dateForm($pre2year), fn_dateForm($today)), fn_dateDiff(fn_dateForm($pre24mon), fn_dateForm($today)), 7 * 10, 10 * 1 ], $periodMsg = [ '2년', '24개월', '10주', '10일' ];
	var $index = $obj.index($obj.filter(':checked'));
	if (fn_dateDiff($sdate, $edate) > $period[$index]) {
		fn_openSweetAlert( { title : '알림', msg : $periodMsg[$index] + ' 이상 기간은 조회가 제한됩니다.' } );
		$('#sdayf').datepicker('setDate', $edate);
		return false;
	}
	return true;
}
function fn_setTagsinput($obj, $data) {
	$($obj).tagsinput('destroy');
	$($obj).val($data).tagsinput({tagClass: 'label label-info'});
}
function fn_setNoDataTag(obj){
	var $tag = '';
	$tag += '<div class="col-lg-12 infoArea" style="padding:100px;">';
	$tag += '<p class="symbol" style="color: #c7d5e2;font-size:100px;line-height:100px;"><i class="fa fa-exclamation-circle"></i></p>'; 
	$tag += '<p class="text">데이터가 없습니다.</p></div>';
	$('#' + obj ).html($tag);	
}
function fn_sortKey(val){
	var $sortArr = new Object();
	var $key = new Array();
	for( temp in val ){
		if(val.hasOwnProperty(temp)) $key.push(temp);
	}
	$key.sort();
	return $key;
}
<%-- 필터 클릭 이벤트 --%>
$(document).on('click', '.filter-content li', function() {
	var $tags = $('input[name=tags]');
	var $data = '', $paramData = '';
	$(this).toggleClass('open');

	$('.filter-content li.open').each(function(i, e) {
		if (i > 0){
			$data += ',';
			$paramData += ',';
		}

		$data += $(this).children('a').text();
		$paramData += $(this).children('a').data('val');
	});
	fn_setDetailParam($paramData);
	fn_setTagsinput($tags, $data);
	return false;
});
function fn_setDetailParam(data){
	var $data = data.split(',');
	
	$paramCategory.splice(0,$paramCategory.length);

	$data.forEach(function( element, i ){
		$paramCategory.push(element);
	});
}
<%-- 필터 유지 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4filter', function() {
	var $filter = $('input[name=tags]').val();
	if (!$(this).is(':checked')) $.removeCookie($cookieName);
	else $.cookie($cookieName, $filter, { expires : 30 });
});
<%-- 필터 'x' 버튼 클릭 이벤트 --%>
$(document).on('click', 'span[data-role=remove]', function() {
	var $tagsTxt = $(this).parent().text();
	$('.filter-content li.open').each(function(i, e) {
		if ($(this).text() == $tagsTxt) $(this).removeClass('open');
	});
});
<%-- 필터 전제지우기 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4filterReset', function() {
	var $tags = $('input[name=tags]');
	$('.filter-content li.open').each(function(i, e) {
		$(this).removeClass('open');
	});
	fn_setTagsinput($tags, '');
	fn_setDetailParam('');
	$('.btn4filter').prop('checked', false);
	$.removeCookie($cookieName);
});
<%-- 불용어 사전 - 추가 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4addsword', function() {
	var $sword = $('#sword').val();
	if(!fn_isNull($sword)) {
		var $isChk = false;
		$('#not li').each(function(i, e) {
			if ($(this).text() == $sword) $isChk = true;
		});
		if (!$isChk) {
			$('#not').prepend('<li><a data-val="' + $sword + '">' + $sword + '</a></li>');
		}
		$('#sword').val('');
	}
});
<%-- 불용어 사전 - 엔터키 이벤트 --%>
$(document).on('keypress', '#sword', function(e) {
	if (e.keyCode == 13) $('.btn4addsword').click();
});
<%-- 이슈 현황 --%>
function fn_setChart1(data) {
	if (data.result) {
		const $category = data.result.category, $documents = data.result.documents;
		//var $total = data.result.total;
		var $chart1data = new Array(), $chart3data = new Array();
		var $chart4data = new Object(), $chart4category = new Array(), $chart4list = new Array();

		// 분야별 데이터 현황
		$category.sort(function(a, b) {
			return a.key < b.key ? -1 : a.key > b.key? 1 : 0;
		});
		$($category).each(function(i, e) {
			$chart1data.push( { name : e.key, value : e.doc_count } );	
		});
		fn_amChart_barChart('chart1', $chart1data, { categoryX : 'name', valueY : 'value' } );
		
		// 이슈 랭킹 추이
		$($documents).each(function(i, e) {
			$chart4category.push(e.dpDate);
			$chart4list.push(e.list);
		});
		$chart4data =  { category : $chart4category, list : $chart4list };
		$('#chart4').slick('unslick').html('');
		fn_setRankChart('chart4', $chart4data, { limit : 10 } );
		$('#chart4').slick({infinite: false, slidesToShow: 4,  slidesToScroll: 1, centerMode: false, responsive: [{breakpoint: 1201,settings: {slidesToShow: 3,slidesToScroll: 1,infinite: false, dots: false	}}]});
	}
}
<%-- 주요 이슈 키워드 추이 --%>
function fn_setChart2(data) {
	var $data = new Array();
	if (data.result) $data = data.result.documents;
	fn_amChart_multiLineChart('chart2', $data, { categoryX : 'date', categoryArr : $chart2keyword, wave : false, bullet : true, share : true});
}
function fn_setChart3(data) {
	if (data.result) {
		var $documents = data.result.documents, $data = new Array();
		$($documents).each(function(i ,e) {
			if (i < 5) {
				$chart2keyword.push(e.word);
				if (i == 4) {
					// 주요 이슈 키워드 추이
					fn_apiAjax( { url : $apiUrl + 'api/facetList', data : fn_setParams(2), success : function(data) { fn_setChart2(data); } });				
				}
			}
			$data.push( { word : e.word, value : e.count } );
		});
	}

	// 키워드 현황
	fn_amChart_treemap('chart3', $data, { word : 'word', value : 'value', child : 'children', limit : 50 });
}
function fn_setFilterParam(obj) {
	var $arr = new Array();
	$('#' + obj + ' li.open a').each(function(i, e) {
		$arr.push($(this).data('val'));
	});
	return $arr.toString();
}
function fn_setParams(idx) {
	var $params = new Object(), $category, $sword;
	
	var $sdayf = fn_replaceAll($('#sdayf').val(), '-', ''), $sdayt = fn_replaceAll($('#sdayt').val(), '-', '');
	var $query = (idx == 2) ? $chart2keyword.toString() : '';
	var $period = $('input[name=stype]:checked').val();
	var $docLimit = ($period == 'D') ? 300 : ($period == 'Y') ? 2000 : 800;

	$category = $paramCategory.toString();
	$sword = $('#sword').val();

	$params = { query : $query, sword : $sword, category2 : $category, period : $period, sdate: $sdayf, edate : $sdayt, limit : 50, docLimit : $docLimit };
	
	return $.param($params);
}
<%-- 분석 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4search', function() {
	$chart2keyword = new Array();
	$('div[id^=chart]').html('<div class="text-center"><img src="${usrSitePath}/images/loading.gif"></div>');
	<%-- 분야별 데이터 현황, 이슈 랭킹 추이 --%>
	fn_apiAjax( { url : $apiUrl + 'api/issue', data : fn_setParams(1), success : function(data) { fn_setChart1(data); } });
	<%-- 키워드 현황 --%>
	fn_apiAjax( { url : $apiUrl + 'api/issueKeyword', data : fn_setParams(3), success : function(data) { fn_setChart3(data); } });
});
$(document).ready(function() {
	$('.input-daterange').datepicker({ keyboardNavigation: false, forceParse: false, autoclose: true, format: 'yyyy-mm-dd', language: 'kr', endDate : '+0d' }).on('change', fn_isCheckDateUnit);

	$('[id^=chart]').attr({'data-chart-fn' : '', 'data-chart-data' : '', 'data-chart-opt' : ''});
	var $cookieFilter = $.cookie($cookieName);
	if (!fn_isNull($cookieFilter)) {
		$('.btn4filter').prop('checked', true);
		$('input[name=tags]').val($cookieFilter);
		$('.filter-content li').each(function(i, e) {
			var $val = $(this).text();
			if ($cookieFilter.indexOf($val) > -1)
				$(this).addClass('open');
		});
	}

	<%-- 기간 단위 일 기본 선택 --%>
	$('input[name=stype]:checked').click();
	$('#chart4').slick();
	$('.filter-content .scroll_content').slimscroll( {height:'150px'} );
	
	$('.btn4search:eq(0)').click();
});
</script>
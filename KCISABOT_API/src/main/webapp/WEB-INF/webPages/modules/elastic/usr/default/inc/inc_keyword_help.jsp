<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<aside class="sidebar">
	<div class="sidebar-content">
		<h5>키워드 분석</h5>
		<div class="scroll_side">
			<h6>키워드 분석</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/keyword &nbsp; <a href="/swagger-ui.html#/elastic-keyword-api-controller/keywordUsingGET" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>query 검색어 (* 필수)</li>
					<li>sword 불용어 [ 여러개일 경우 ',' 로 구분 ]</li>
					<li>period 기간 구분 [ D : 일, W : 주, M : 월, Y : 년 ] [ default : D ] (* 필수)</li>
					<li>category1 카테고리 [ 100 : 정치, 101 : 경제, 102 : 사회, 103 : 생활/문화, 104 : 세계, 105 : IT/과학 ]</li>
					<li>category2 서브 카테고리 [ 731 : 모바일, 230 : IT 일반, ... ]</li>
					<li>sdate 검색 시작일 (* 필수)</li>
					<li>edate 검색 종료일 (* 필수)</li>
					<li>sortType 정렬순서 구분 [ ASC, DESC ] [ default : DESC ]</li>
					<li>limit 검색 결과 중 리턴받을 개수 [ default : 10 ]</li>
					<li>docLimit 검색 문서 개수 [ default : 100 ]</li>
					<li>df df 사용여부 [ default : false ]</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
          <li style="word-break:break-all;"><pre class="paramCode text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode text-white"></pre></li>
				</ul>
			</div>
			
			<h6>연관어 분석</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/topicRank &nbsp; <a href="/swagger-ui.html#/elastic-topicrank-api-controller/topicRankUsingGET" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>query 검색어 (* 필수)</li>
					<li>keyword 키워드 (* 필수)</li>
					<li>sword 불용어 [ 여러개일 경우 ',' 로 구분 ]</li>
					<li>period 기간 구분 [ D : 일, W : 주, M : 월, Y : 년 ] [ default : D ] (* 필수)</li>
					<li>category1 카테고리 [ 100 : 정치, 101 : 경제, 102 : 사회, 103 :
						생활/문화, 104 : 세계, 105 : IT/과학 ]</li>
					<li>category2 서브 카테고리 [ 731 : 모바일, 230 : IT 일반, ... ]</li>
					<li>sdate 검색 시작일 (* 필수)</li>
					<li>edate 검색 종료일 (* 필수)</li>
					<li>sortType 정렬순서 구분 [ ASC, DESC ] [ default : DESC ]</li>
					<li>limit 검색 결과 중 리턴받을 개수 [ default : 10 ]</li>
					<li>docLimit 검색 문서 개수 [ default : 100 ]</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
          <li style="word-break:break-all;"><pre class="paramCode2 text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode2 text-white"></pre></li>
				</ul>
			</div>
			
			<h6>상관관계 순위</h6>
			<div class="contBox">
				<p class="side_title">INFO</p>
				<ul>
					<li>선행 작업 필요</li>
					<li>워드클라우드 상위 10개 키워드를 분석 [ api : api/facetList ]</li>
				</ul>
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/correlationList &nbsp; <a href="/swagger-ui.html#/elastic-correlation-api-controller/correlationListUsingPOST" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>source={ key : value } (* 필수)</li>
					<li>target={ key : value, key2 : value2, ... } (* 필수)</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
          <li style="word-break:break-all;"><pre class="paramCode3 text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode3 text-white"></pre></li>
				</ul>
			</div>
			
			<h6>상관관계 순위 - 키워드 추이</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/facetList &nbsp; <a href="/swagger-ui.html#/elastic-facet-list-api-controller/facetListUsingGET" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>query 검색어</li>
					<li>sword 불용어 [ 여러개일 경우 ',' 로 구분 ]</li>
					<li>period 기간 구분 [ D : 일, W : 주, M : 월, Y : 년 ] [ default : D ] (* 필수)</li>
					<li>category1 카테고리 [ 100 : 정치, 101 : 경제, 102 : 사회, 103 : 생활/문화, 104 : 세계, 105 : IT/과학 ]</li>
					<li>category2 서브 카테고리 [ 731 : 모바일, 230 : IT 일반, ... ]</li>
					<li>sdate 검색 시작일 (* 필수)</li>
					<li>edate 검색 종료일 (* 필수)</li>
					<li>docLimit 검색 문서수</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
          <li style="word-break:break-all;"><pre class="paramCode4 text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode4 text-white"></pre></li>
				</ul>
			</div>

		</div>
		<button>
			<span class="sidebar-btn"><i class="fas fa-angle-right"></i>
				&nbsp;도움말 열기</span>
		</button>
	</div>
</aside>
<script>
	$('.scroll_side').slimscroll({ height : '850px' });
  var $_param = {"query":"애플","sword":"","category2":"","period":"M","sdate":"20201008","edate":"20210408","limit":"50","docLimit":100};
	var $_code = {"result":{"took":"99ms","documents":[{"tf":0.0071,"count":642,"word":"서비스"},{"tf":0.0089,"count":529,"word":"네이버"} ],"keyword":"검색어","jtook":"1385ms"}};
	$('.paramCode').text(JSON.stringify($_param, null, 2));
	$('.respCode').text(JSON.stringify($_code, null, 2));
	
	$_param = {"query":"애플","keyword":"시장","sword":"","category2":"","period":"M","sdate":"20201008","edate":"20210408","limit":30,"docLimit":100};
	$_code = {"result":{"took":"118ms","documents":[{"weight":0.6627,"word":"판매"},{"weight":0.6435,"word":"분야"},{"weight":0.631,"word":"스마트폰"},{"weight":0.6184,"word":"태블릿"}],"keyword":"애플","jtook":"35ms"}};
	$('.paramCode2').text(JSON.stringify($_param, null, 2));
	$('.respCode2').text(JSON.stringify($_code, null, 2));

	$_param = {"source" : {"애플":"0,0,0,548,145,0,92"}, "target" : {"시장":"0,0,0,3770,145,0,2749","스마트폰":"0,0,0,1614,145,0,347","사업":"0,0,0,3637,688,0,3126","삼성전자":"0,0,0,2901,688,0,3126","기업":"0,0,0,3127,466,0,1539","국내":"0,0,0,5338,1201,0,4634","제품":"0,0,0,1840,291,0,900","업체":"0,0,0,1896,291,0,218","기술":"0,0,0,2087,275,0,633","미국":"0,0,0,8307,275,0,834"}};
	$_code = {"기업":0.9342615785738335,"시장":0.8241136766146313,"사업":0.7969687036957435,"스마트폰":0.9811761474717754,"국내":0.7976889761248933,"미국":0.9723019145958142,"제품":0.9366624662826358,"기술":0.9778104863952368,"업체":0.992988978361789,"삼성전자":0.7121717064147453};
	$('.paramCode3').text(JSON.stringify($_param, null, 2));
	$('.respCode3').text(JSON.stringify($_code, null, 2));

	$_param = {"query":"애플,삼성전자","sword":"","category2":"731","period":"M","sdate":"20201008","edate":"20210408"};
	$_code = {"result":{"took":"1ms","documents":[{"date":"2020-10-08","애플":26,"삼성전자":93},{"date":"2020-11-08","애플":41,"삼성전자":158}],"totalCount":10000,"jtook":"0ms"}};
	$('.paramCode4').text(JSON.stringify($_param, null, 2));
	$('.respCode4').text(JSON.stringify($_code, null, 2));
</script>
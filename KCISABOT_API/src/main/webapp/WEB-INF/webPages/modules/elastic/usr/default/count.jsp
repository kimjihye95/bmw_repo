<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../cresources/jsp/commonTop.jsp" %>
<jsp:include page = "${usrSitePath}/jsp/top.jsp" />
<%
String[] crawlingNameArr = { "정치", "경제", "사회", "생활/문화", "세계", "IT/과학" };
int[] crawlingIdArr = { 100, 101, 102, 103, 104, 105 };

request.setAttribute("crawlingNameArr", crawlingNameArr);
request.setAttribute("crawlingIdArr", crawlingIdArr);
%>

			<div class="col-xl-3">
				<div class="card">
					<div class="card-body">
						<p class="subtt">총 데이터 수집 건수</p>
						<p class="dashNum mgt5" id="indexCount">0건</p>
					</div>
				</div>
			</div>
			
			<div class="col-xl-3">
				<div class="card">
					<div class="card-body">
						<p class="subtt">전일 데이터 수집 건수</p>
						<p class="dashNum mgt5" id="dailyCount">0건</p>
					</div>
				</div>
			</div>

			<div class="col-xl-12 mt-3">
				<div class="panel">
					<div class="panel-hdr">
						<h2>대분류 수집현황</h2>
						<div class="paenl-tollbar">
							<div class="btn-group btn-group-sm">
								<button type="button" class="btn4visual btn btn-xs btn-outline-secondary waves-effect waves-themed" data-ref="1" data-toggle="modal" data-target=".bs-chart-modal"><i class="fas fa-chart-bar"></i> 시각화 변경</button> 
								<button type="button" class="btn4dgrid btn btn-xs btn-outline-secondary waves-effect waves-themed modal_dailyCount" data-ref="1" data-toggle="modal" data-title="대분류 수집현황" data-target=".bs-dataView-modal"><i class="fas fa-database"></i> 데이터 보기</button>
							</div>
						</div>
					</div>
					<div class="panel-container">
						<div class="panel-content">
							<div id="chart1" class="mgt10" style="width:100%; height:300px;"></div> 
						</div>
					</div>
				</div>
			</div>
			
			<c:forEach items="${crawlingNameArr}" var="model" varStatus="i">
			<div class="col-xl-6 mt-3">
				<div class="panel">
					<div class="panel-hdr">
						<h2>${model} - 소분류 수집현황</h2>
						<div class="paenl-tollbar">
							<div class="btn-group btn-group-sm">
							<button type="button" class="btn4visual btn btn-xs btn-outline-secondary waves-effect waves-themed" data-ref="2${i.index}" data-toggle="modal" data-target=".bs-chart-modal"><i class="fas fa-chart-bar"></i> 시각화 변경</button> 
							<button type="button" class="btn4dgrid btn btn-xs btn-outline-secondary waves-effect waves-themed modal_dailyCount" data-ref="2${i.index}" data-toggle="modal" data-title="${model} - 소분류 수집현황" data-target=".bs-dataView-modal"><i class="fas fa-database"></i> 데이터 보기</button>
							</div>
						</div>
					</div>
					<div class="panel-container">
						<div class="panel-content">
							<div id="chart2${i.index}" class="mgt10" style="width:100%; height:300px;"></div> 
						</div>
					</div>
				</div>
			</div>
			</c:forEach>
				
<%@ include file = "./inc/inc_count_help.jsp" %>
<%@ include file = "./inc/inc_count_js.jsp" %>
<jsp:include page = "${usrSitePath}/jsp/bottom.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%=java.time.LocalDateTime.now() %>
<script>
var $speriod = '', $sarea = '', $satm = '', $sdate = '', $edate = '${yesterday}';
var $cookieName = '_inno_keyword_filter_cookie_';

var yearPeriod = [{name:'최근2년', value:'2Y'}, {name:'최근1년', value:'1Y'}, {name:'직접입력', value:'F'}];
var monthPeriod = [{name:'최근6개월', value:'6M'}, {name:'최근3개월', value:'3M'}, {name:'직접입력', value:'F'}];
var weekPeriod = [{name:'최근4주', value:'4W'}, {name:'최근3주', value:'3W'}, {name:'최근2주', value:'2W'}, {name:'최근1주', value:'1W'}, {name:'직접입력', value:'F'}];
var dayPeriod = [{name:'최근7일', value:'7D'}, {name:'최근6일', value:'6D'}, {name:'최근5일', value:'5D'}, {name:'최근4일', value:'4D'}, {name:'최근3일', value:'3D'}, {name:'최근2일', value:'2D'}, {name:'최근1일', value:'1D'}, {name:'직접입력', value:'F'}];
var periodArray = [yearPeriod, monthPeriod, weekPeriod, dayPeriod];
// 상세검색 파라미터 배열
var $paramCategory = new Array(), $chartLimit;
var $chart2keyword, $chart2count, $chart2limit = 30, $chart3keyword = new Array(), $chart3limit = 10;
var $chart3data = new Array(), $chart5keyword = new Array();

<%-- 기간 단위 선택 이벤트 --%>
$(document).on('click', 'input[name=stype]', function() {
	var $period = $('input[name=stype]');
	var $periodIndex = $($period).index($($period).filter(':checked'));
	var $select = periodArray[$periodIndex];
	$speriod = $(this).val();

	$($select).each(function(i, e) {
		if (i == 0) $('#datePeriod').empty();
		$('#datePeriod').append($('<option value="' + e.value + '">' + e.name + '</option>'));
	});
	$stype = $('input[name=stype]:checked').val();
	$edate = '${yesterday}';
	$('#datePeriod').change();
});
<%-- 기간 선택 이벤트 --%>
$(document).on('change', '#datePeriod', function() {
	$period = $(this).val();
	var $val = Number($period.substr(0, 1));
	var $unit = $period.substr(1);
	if ($period == 'F'){
		$('.input-daterange input').prop('disabled', false);
		$('input[name=sdayf]').focus();

		$('#sdayf, #sdayt').datepicker().on('change', function(e) {
			if (e.currentTarget.id == 'sdayf') $sdate = e.currentTarget.value;
			else $edate = e.currentTarget.value;
		});
	}
	else {
		$('.input-daterange input').prop('disabled', true);
		$('#sdayt').val('${yesterday}');
	}
	if ($unit == 'Y' || $unit == 'M') {
		$sdate = fn_beforeMonth( ($unit == 'Y' ? 12 * $val: $val), fn_dateConvtStr2Date($edate));
	}
	else if ($unit == 'W' || $unit == 'D') {
		$sdate = fn_beforeDate( ($unit == 'W' ? 7 * $val : $val), fn_dateConvtStr2Date($edate));
	}
	$('#sdayf').datepicker('setDate', $sdate);
	$('#sdayt').datepicker('setDate', '${yesterday}');
});
<%-- 상세 분석 열기 버튼 클릭 이벤트 --%>
$('.collapse').on('show.bs.collapse', function (e) {
	$('.btn4searchDet').text('상세 분석 닫기');
});
$('.collapse').on('hide.bs.collapse', function (e) {
	$('.btn4searchDet').text('상세 분석 열기');
});
// 기간 단위 체크
function fn_isCheckDateUnit() {
	var $obj = $('input[name=stype]'), $today = new Date($edate);
	var $pre2year = new Date($today.getFullYear() - 2, $today.getMonth(), $today.getDate());
	var $pre24mon = new Date($today.getFullYear(), $today.getMonth() - 24, $today.getDate());
	var $period = [ fn_dateDiff(fn_dateForm($pre2year), fn_dateForm($today)), fn_dateDiff(fn_dateForm($pre24mon), fn_dateForm($today)), 7 * 10, 10 * 1 ], $periodMsg = [ '2년', '24개월', '10주', '10일' ];
	var $index = $obj.index($obj.filter(':checked'));
	if (fn_dateDiff($sdate, $edate) > $period[$index]) {
		fn_openSweetAlert( { title : '알림', msg : $periodMsg[$index] + ' 이상 기간은 조회가 제한됩니다.' } );
		$('#sdayf').datepicker('setDate', $edate);
		return false;
	}
	return true;
}
function fn_setTagsinput($obj, $data) {
	$($obj).tagsinput('destroy');
	$($obj).val($data).tagsinput({tagClass: 'label label-info'});
}
function fn_setNoDataTag(obj){
	var $tag = '';
	$tag += '<div class="col-lg-12 infoArea" style="padding:100px;">';
	$tag += '<p class="symbol" style="color: #c7d5e2;font-size:100px;line-height:100px;"><i class="fa fa-exclamation-circle"></i></p>'; 
	$tag += '<p class="text">데이터가 없습니다.</p></div>';
	$('#' + obj ).html($tag);	
}
function fn_sortKey(val){
	var $sortArr = new Object();
	var $key = new Array();
	for( temp in val ){
		if(val.hasOwnProperty(temp)) $key.push(temp);
	}
	$key.sort();
	return $key;
}
<%-- 필터 클릭 이벤트 --%>
$(document).on('click', '.filter-content li', function() {
	var $tags = $('input[name=tags]');
	var $data = '', $paramData = '';
	$(this).toggleClass('open');

	$('.filter-content li.open').each(function(i, e) {
		if (i > 0){
			$data += ',';
			$paramData += ',';
		}

		$data += $(this).children('a').text();
		$paramData += $(this).children('a').data('val');
	});
	fn_setDetailParam($paramData);
	fn_setTagsinput($tags, $data);
	return false;
});
function fn_setDetailParam(data){
	var $data = data.split(',');
	
	$paramCategory.splice(0,$paramCategory.length);

	$data.forEach(function( element, i ){
		$paramCategory.push(element);
	});
}
<%-- 필터 유지 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4filter', function() {
	var $filter = $('input[name=tags]').val();
	if (!$(this).is(':checked')) $.removeCookie($cookieName);
	else $.cookie($cookieName, $filter, { expires : 30 });
});
<%-- 필터 'x' 버튼 클릭 이벤트 --%>
$(document).on('click', 'span[data-role=remove]', function() {
	var $tagsTxt = $(this).parent().text();
	$('.filter-content li.open').each(function(i, e) {
		if ($(this).text() == $tagsTxt) $(this).removeClass('open');
	});
});
<%-- 필터 전제지우기 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4filterReset', function() {
	var $tags = $('input[name=tags]');
	$('.filter-content li.open').each(function(i, e) {
		$(this).removeClass('open');
	});
	fn_setTagsinput($tags, '');
	fn_setDetailParam('');
	$('.btn4filter').prop('checked', false);
	$.removeCookie($cookieName);
});
<%-- 불용어 사전 - 엔터키 이벤트 --%>
$(document).on('keypress', '#sword', function(e) {
	if (e.keyCode == 13) $('.btn4addsword').click();
});
<%-- 키워드 워드 클라우드 --%>
function fn_setChart1(data) {
	if (data.result) {
		var $documents = data.result.documents, $data = new Array();
		$($documents).each(function(i ,e) {
			if (i < 10) {
				if (i == 0) {
					$chart3keyword = new Array();
					$chart3keyword.push($skey);
				}
				$chart3keyword.push(e.word);
			}
			$data.push( { name : e.word, value : e.count } );
		});
		// 워드 클라우드
		fn_amChart_wordCloud('chart1', $data, { word : 'name', value : 'value'});
		// 키워드 순위
		$('#chart21').html('');
		fn_setKeywordRankChart('chart21', $data, {start : 1, end : 10, limit : 10} );
		$('#chart21 li:eq(0)').click();

		// 상관계수 순위
		fn_apiAjax( { url : $apiUrl + 'api/facetList', data : fn_setParams(3), success : function(data) { fn_setChart3(data); } });
	}
}
<%-- 연관 키워드 순위 클릭 이벤트 --%>
$(document).on('click', '#chart21 li', function() {
	$chart2keyword = $(this).find('.rankKey').text();
	$chart2count = 1;//$(this).find('.rankKeyCnt').text();;
	<%-- 유사도 분석 --%>
	fn_apiAjax( { url : $apiUrl + 'api/topicRank', data : fn_setParams(2), success : function(data) { fn_setChart2(data); } });
});
<%-- 유사도 분석 --%>
function fn_setChart2(data) {
	if (data.result) {
		var $documents = data.result.documents;
		var $data = new Array(), $cData = new Object(), $children = new Array();
		$($documents).each(function(i ,e) {
			if (!fn_isNull(e)) {
				var $temp = new Object();
				$temp.name = e.word;
				$temp.value = e.weight;
				$children.push($temp);
			}
		});

		$cData.name = $chart2keyword;
		$cData.value = $chart2count;
		$cData.children = $children;
		$data.push($cData);
		fn_amChart_forceDirected('chart2', $data, { name : 'name', value : 'value', children : 'children', text : 'tool', linkWith : 'linkWith', min : 15, max : 35 });
	}
}
<%-- 상관계수 순위 --%>
function fn_setChart3(data) {
	if (data.result) {
		const $documents = data.result.documents;
		var $source = new Object(), $target = new Object(), $data = new Object();

		$($chart3keyword).each(function(i, e) {
			
			var $tempArr = new Array();
			$($documents).each(function(j, m) {
				$tempArr.push(m[e])
			});
			
			if (i == 0) $source[e] = $tempArr.toString();
			else $target[e] = $tempArr.toString();
		});
		
		$data = { source : JSON.stringify($source), target : JSON.stringify($target) };
		
		fn_apiAjax( { url : $apiUrl + 'api/correlationList', data : $data, type : 'POST', success : function(cdata) {
			$chart3data = new Array();
			$.each(cdata, function(k, v) {
				$chart3data.push( { keyword : k, correlation : v } );
			});
			$chart3data.sort(function(a, b) { return a.correlation > b.correlation ? -1 : a.correlation < b.correlation ? 1 : 0});
			if (fn_isNull($chart3data) || $chart3data.length == 0) $('#chart3, #chart4, #chart5').html('<div style="text-align:center; width:100%; height:50px; line-height:50px;">데이터가 없습니다.</div>');
			else $('.btn4chart3limit').click();
		} });
	}
}
<%-- 상관관계 순위 - 리스트 카운트 적용 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4chart3limit', function() {
	var $cnt = $('#chart3limit').val();
	fn_setCorRelationChart('chart3', $chart3data, { cnt : $cnt });
	$('#chart3').slimscroll( {width:'100%', height:'300px'} );
	$('#chart3 tr:eq(0)').click();
});
<%-- 상관관계 순위 - 리스트 클릭 이벤트 --%>
$(document).on('click', '#chart3 tr', function() {
	var $rel = $(this).attr('rel');
	$('#chart4, #chart5').html('<div class="text-center"><img src="${usrSitePath}/images/loading.gif"></div>');
	$($chart3data).each(function(i, e) {
		var $keyword = e.keyword;
		if ($keyword == $rel)
			fn_setKeywordRelationChart('chart4', { result : e.correlation }, { source : $skey, target : $rel });
	});
	$chart5keyword = new Array();
	$chart5keyword.push($skey);
	$chart5keyword.push($(this).attr('rel'));
	
	$(this).addClass('bg-info-50').siblings().removeClass('bg-info-50');
	fn_apiAjax( { url : $apiUrl + 'api/facetList', data : fn_setParams(5), success : function(data) { fn_setchart5(data); } });
});
<%-- 주요 이슈 키워드 추이 --%>
function fn_setchart5(data) {
	var $data = new Array();
	if (data.result) $data = data.result.documents;
	fn_amChart_multiLineChart('chart5', $data, { categoryX : 'date', categoryArr : $chart5keyword, wave : false, bullet : true, share : true});
}
function fn_setParams(idx) {
	var $params = new Object(), $category, $sword;
	var $sdayf = fn_replaceAll($('#sdayf').val(), '-', ''), $sdayt = fn_replaceAll($('#sdayt').val(), '-', '');
	var $query = (idx == 3) ? $chart3keyword.toString() : (idx == 5) ? $chart5keyword.toString() : $skey;
	var $period = $('input[name=stype]:checked').val();
	var $limit = (idx == 2) ? $chart2limit : (idx == 3) ? $chart3limit : $chartLimit;
	var $docLimit = ($period == 'D') ? 300 : ($period == 'Y') ? 2000 : 800;

	$category = $paramCategory.toString();
	$sword = $('#sword').val();

	$params = { query : $query, sword : $sword, category2 : $category, period : $period, sdate: $sdayf, edate : $sdayt, limit : $limit, docLimit : $docLimit };
	if (idx == 2) $params.keyword = $chart2keyword;

	return $.param($params);
}
<%-- 분석 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4search', function() {
	$skey = $('#skey').val();
	if (!fn_checkFill($('.frame-wrap'), 'skey')) return false;

	$('#searchInfoDiv').hide();
	$chartLimit = $('#slmt').val();
	$('div[id^=chart], ol[id^=chart]').html('<div class="text-center"><img src="${usrSitePath}/images/loading.gif"></div>');
	fn_apiAjax( { url : $apiUrl + 'api/keyword', data : fn_setParams(1), success : function(data) { fn_setChart1(data); } });
	$('#searchResultDiv').show();
});
<%-- 검색어 / 불용어 엔터키 이벤트 --%>
$(document).on('keypress', '#skey, #sword', function(e) {
	if (e.keyCode == 13) {
		$('.btn4search:eq(0)').click();
	}
});
<%-- 워드 클라우드 - 리스트 적용 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4slmt', function() {
	$('.btn4search:eq(0)').click();
});
$(document).ready(function() {
	$('.input-daterange').datepicker({ keyboardNavigation: false, forceParse: false, autoclose: true, format: 'yyyy-mm-dd', language: 'kr', endDate : '+0d' }).on('change', fn_isCheckDateUnit);

	$('[id^=chart]').attr({'data-chart-fn' : '', 'data-chart-data' : '', 'data-chart-opt' : ''});
	var $cookieFilter = $.cookie($cookieName);
	if (!fn_isNull($cookieFilter)) {
		$('.btn4filter').prop('checked', true);
		$('input[name=tags]').val($cookieFilter);
		$('.filter-content li').each(function(i, e) {
			var $val = $(this).text();
			if ($cookieFilter.indexOf($val) > -1)
				$(this).addClass('open');
		});
	}

	<%-- 기간 단위 일 기본 선택 --%>
	$('input[name=stype]:checked').click();
	$('.filter-content .scroll_content').slimscroll( {height:'150px'} );
});
</script>
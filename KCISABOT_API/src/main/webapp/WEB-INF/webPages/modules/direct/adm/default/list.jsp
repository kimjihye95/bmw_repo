<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />

	<div class="row">
		<div class="col-sm-12">
			<h4><span class="text-warning"><i class="fas fa-dot-circle"></i></span> 빌더</h4>
		</div>
		<div class="col-lg-12" style="margin-top: 0.5rem;">
			<c:forEach items="${listBuilder}" var="model" varStatus="i">
				<div class="panel pd20" data-data="${model.builderIdx}" data-panel-collapsed data-panel-fullscreen data-panel-close data-panel-locked data-panel-refresh data-panel-reset data-panel-color data-panel-custombutton>
					<div class="panel-hdr" style="border-bottom:0px;">
						<h2><i class="fal fa-check text-warning mr-2"></i> ${model.builderName}
							<div class="d-flex position-relative ml-auto" style="max-width: 15rem;">
								<a href="${model.builderId}" class="btn btn-warning btn-lg btn-icon rounded-circle waves-effect waves-themed text-white" target="_blank"><i class="fal fa-external-link"></i></a>
							</div>
						</h2>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12"style="margin-top: 0.5rem;">
			<h4><span class="text-warning"><i class="fas fa-dot-circle"></i></span> 대화봇</h4>
		</div>
		<div class="col-lg-12"style="margin-top: 0.5rem;">
			<c:forEach items="${list}" var="model" varStatus="i">
				<div class="panel pd20" data-data="${model.botIdx}" data-panel-collapsed data-panel-fullscreen data-panel-close data-panel-locked data-panel-refresh data-panel-reset data-panel-color data-panel-custombutton>
					<div class="panel-hdr" style="border-bottom:0px;">
						<h2><i class="fal fa-check text-warning mr-2"></i> ${efn:strContentsFormat(model.botName, '')}
							<div class="d-flex position-relative ml-auto" style="max-width: 15rem;">
								<button class="btn btn-warning btn-lg btn-icon rounded-circle waves-effect waves-themed text-white btn4link" data-data="${efn:encrypt(model.botId)}" ><i class="fal fa-external-link"></i></button>
							</div>
						</h2>
					</div>
				</div>
			</c:forEach>
			<div class="mgt30" style="text-align:center;">
				<div id="paging"></div>
			</div>
		</div>
	</div>
		
<script>
<%-- 페이지 처리 --%>
var varOpt = {url : '${URL_PAGE_LIST}', obj : 'paging', total : '${pageTotalPage}', visible : '${blockSize}'};
fn_setPaging(varOpt);
<%-- 대화봇 클릭 이벤트 --%>
$(document).on('click', '.btn4link', function() {
	var $data = $(this).data('data');
	fn_ajax({ url : 'linkPage.do?cmnx=${cmnx}', data : { q : $data } });
});
</script>
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
<iframe id="targetIfrm" name="targetIfrm" style="width:0px;height:0px;" frameborder="0" title="iframe"></iframe>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />

	<div class="row">
		<div class="col-sm-12"  style="padding-left:0px; padding-right: 0px;">
			<div class="alert alert-primary alert-dismissible">
				<div class="row">
					<div class="col-1">
						<c:set var="_builder" value="${efn:encrypt('builder')}" />
						<c:set var="_bot" value="${efn:encrypt('bot')}" />
						<select class="form-control" id="chkItem" onchange="changePlaceholder()">
							<option value="${_builder}">빌더</option>
							<option value="${_bot}">대화봇</option>
						</select>
					</div>
					<div class="col-2">
						<input type="text" class="form-control" id="ins_botName" name="ins_botName" placeholder="빌더 명" minlength="2" maxlength="20">
					</div>
					<div class="col-9">
						<div class="input-group">
							<input type="text" class="form-control" id="ins_botId" name="ins_botId" placeholder="빌더 URL">
							<div class="input-group-append">
								<button class="btn4submit btn btn-warning text-white waves-effect waves-themed" type="button" id="button-addon5" data-role="r">추가</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<form id="fn_searchBuilderFrm" action="botBuilderViewAjax.do" style="display: none;">
				<input type="hidden" name="cmnx" value="${cmnx}" />
			</form>

			<div class="col-sm-12">
				<h4><span class="text-warning"><i class="fas fa-dot-circle"></i></span> 빌더</h4>
			</div>
			<div class="mgt20" style="flex: 0 0 100%; ">
				<table class="table table-striped m-0 tbListA">
					<colgroup>
						<col width="5%" />
						<col width="20%" />
						<col width="" />
						<col width="20%" />
					</colgroup>
					<thead class="bg-warning-900 text-white" style="background-color: #494949;">
					<tr>
						<th>No</th>
						<th>빌더 명</th>
						<th>빌더 URL</th>
						<th>관리</th>
					</tr>
					</thead>
					<tbody class="alignC" id="viewTbodyBuilder"></tbody>
				</table>
			</div>

			<div style="margin-top: 3rem !important; ">
				<div class="col-sm-12">
					<h4><span class="text-warning"><i class="fas fa-dot-circle"></i></span> 대화봇</h4>
				</div>
				<div class="col-sm-6 justify-content-start" style="float: left;">
					<p class="mgb0" style="line-height:38px;">Total. <span class="text-warning" id="totalCount"></span></p>
				</div>
				<div class="col-sm-6 justify-content-end" style="float: left;">
					<form id="fn_searchFrm" action="botIdViewAjax.do">
						<input type="hidden" name="cmnx" value="${cmnx}" />
						<div class="dt-buttons" style="text-align:right;">
							<div class="btn-group mgl10">
								<select class="form-control" name="slmt">
									<c:forEach begin="1" end="3" var="no">
										<option value="${efn:encrypt(no * 4)}"<c:if test="${param.slmt eq no * 4}"> selected</c:if>>${no * 4}건씩 조회</option>
									</c:forEach>
								</select>
							</div>
							<button class="btn btn-outline-default" type="button" onclick="fn_setList()"><span>변경</span></button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="mgt20"  style="width: 100%;">
			<table class="table table-striped m-0 tbListA">
				<colgroup>
					<col width="5%" />
					<col width="20%" />
					<col width="" />
					<col width="20%" />
				</colgroup>
				<thead class="bg-warning-900 text-white">
					<tr>
						<th>No</th>
						<th>대화봇 명</th>
						<th>대화봇 ID</th>
						<th>관리</th>
					</tr>
				</thead>
				<tbody class="alignC" id="viewTbody"></tbody>
			</table>
			<form id="fn_botIdFrm">
				<input type="hidden" id = "cmnx" name="cmnx" value="${cmnx}" />
				<input type="hidden" id = "botIdx" name="botIdx" value="" />
				<input type="hidden" id = "botId" name="botId" value="" />
				<input type="hidden" id = "botName" name="botName" value="" />
			</form>
		</div>

		<div class="mgt30" style="text-align:center; width: 100%;">
			<div id="paging-view"></div>
		</div>
	</div>
<script>
const $frm_cudFrm = $('#fn_botIdFrm');
$(document).ready(function() {
	fn_setBuilderList();
	fn_setList();
});

<%-- 추가/수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4submit', function() {
	var $role = $(this).data('role');
	var $action = '';
	var botIdx = '0';
	var inputId = { 'botName' : { 'val' : '', 'title' : $('#ins_botName').prop('placeholder') }, 'botId' : { 'val' : '', 'title' : $('#ins_botId').prop('placeholder') } };
	var selectedVal = $('#chkItem').val();
	var result = true;

	// 등록일 떄
	if($role == 'r') {
		inputId.botId.val = '#ins_botId';
		inputId.botName.val = '#ins_botName';
		$action = ((selectedVal == 'builder') ? 'botBuilderActionProc' : 'botIdActionProc' ) +'.do?cmnx=${cmnx}';
		if( !changePlaceholder($role) ) {
			return;
		}
	}
	// 수정일 때
	else {
		botIdx = $(this).data('data');
		inputId.botId.val = '#' + $(this).parents('tr').find('input[id*="Id"]').prop('id');
		inputId.botName.val = '#' + $(this).parents('tr').find('input[id*="Name"]').prop('id');

		const $_builder = inputId.botId.val.includes('builder');
		inputId.botId.title = ($_builder ? '빌더 URL' : '대화봇 ID');
		inputId.botName.title = ($_builder ? '빌더 명' : '대화봇 명');
		$action = ((inputId.botId.val.includes('builder')) ? 'botBuilderActionProc' : 'botIdActionProc' ) +'.do?cmnx=${cmnx}';
	}
	
	$frm_cudFrm.find('input[name=botIdx]').val( botIdx );
	$frm_cudFrm.find('input[name=botId]').val( $( inputId.botId.val ).val() );
	$frm_cudFrm.find('input[name=botName]').val( $( inputId.botName.val ).val() );

	// 유효성 검증
 	$.each( inputId, function( key, value ) {
 		var $keyValue = $frm_cudFrm.find('[name=' + key + ']').val();
		if(fn_isNull( $keyValue ) ) {
			var $opt = { title : '알림', msg : value.title + _comInputTextReqMsg, icon : 'warning', success : function() { $( value.val ).trigger('focus'); } };
			fn_openSweetAlert($opt);
			result = false;
		}
		else if (key.includes('Name')) {
			if (!/^[\d가-힣ㄱ-ㅎㅏ-ㅣa-z]+$/gi.test($keyValue)) {
				fn_openSweetAlert({ title : '알림', msg : value.title + '은 한글, 영문, 숫자, 공백 만 입력이 가능합니다.', icon : 'warning' });
				result = false;
			}
			else if ($keyValue.length < 2 || $keyValue.length > 20) {
				fn_openSweetAlert({ title : '알림', msg : value.title + '은 2자 이상 20자 이하로 입력이 가능합니다.', icon : 'warning' });
				result = false;
			}
		}
		else if (key.includes('Id')) {
			if (value.val.includes('builder')) {
				const $pattern1 = /^(https?|ftp)\:\/\/[ㄱ-힣a-zA-Z0-9-\.]+\.[a-z]{2,4}(:\d{2,5})?(\/)(.*)/i;
				const $pattern2 = /^(https?|ftp)\:\/\/\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}:\d{2,5}(\/)(.*)/i;
				if (!($pattern1.test($keyValue) || $pattern2.test($keyValue))) {
					fn_openSweetAlert({ title : '알림', msg : '잘못된 URL 형식입니다.', icon : 'warning' });
					result = false;
				}
			}
			else {
				if (!/^[a-z0-9]{8}[-][a-z0-9]{4}[-][a-z0-9]{4}[-][a-z0-9]{4}[-][a-z0-9]{12}$/.test($keyValue)) {
					fn_openSweetAlert({ title : '알림', msg : value.title + '는 영문, 숫자만 입력가능하며<br>00000000-0000-0000-0000-000000000000<br>형식으로 입력하셔야 합니다.', icon : 'warning' });
					result = false;
				}
			}
		}
		return result;
	});

	// 유효성 검증 통과 시 서비스 호출
	if(result) {
	 	var $data = $frm_cudFrm.serialize();
	 	fn_ajax({ url : $action, data : $data, type : 'POST' });
	 	$('input[name^=ins_]').val('');
	}
	
});

<%-- 셀렉트 박스 변경 이벤트 --%>
function changePlaceholder(r) {
	var result = false;
	var $placeholderData = {
		'${_builder}': {'ins_botName': '빌더 명', 'ins_botId': '빌더 URL', 'alert' : '빌더는 1개만 추가 가능합니다.'},
		'${_bot}': {'ins_botName': '대화봇 명', 'ins_botId': '대화봇 ID', 'alert' : '먼저 빌더를 추가해주세요.'}
	};
	var $selBox = $('#chkItem');
	var selectedVal = $('#chkItem').val();

	if(checkBeforeSelectChange(selectedVal)) {
		$('#ins_botName').prop('placeholder', $placeholderData[selectedVal].ins_botName);
		$('#ins_botId').prop('placeholder', $placeholderData[selectedVal].ins_botId);
		result = true;
	} else {
		if (!fn_isNull(r)) {
			var $opt = { title : '알림', msg : $placeholderData[selectedVal].alert, icon : 'warning' };
			fn_openSweetAlert($opt);
		}
	}
	return result;
}

<%-- 셀렉트 박스 변경 전 체크사항 --%>
function checkBeforeSelectChange(selectVal) {
	var result = false;
	switch (selectVal) {
		case '${_builder}':
				result = (($('#viewTbodyBuilder').children('tr').length == 1 && $('#viewTbodyBuilder').children('tr').children('td').text() == '${_infoNoDataMsg_}')) ? true : false;
			break;
		case '${_bot}':
				result = (($('#viewTbodyBuilder').children('tr').length == 1 && $('#viewTbodyBuilder').children('tr').children('td').text() != '${_infoNoDataMsg_}')) ? true : false;
			break;
		default:
			break;
	}
	return result;
}

<%-- 빌더 조회 이벤트 --%>
function fn_setBuilderList() {
	var $frm = $('#fn_searchBuilderFrm');
	var $action = $frm.attr('action');
	var $data = $frm.serialize();
	fn_ajax({ url : $action, data : $data});
}

<%-- 조회 카운트 변경 버튼 클릭 이벤트 --%>
function fn_setList() {
	var $frm = $('#fn_searchFrm');
	var $action = $frm.attr('action');
	var $data = $frm.serialize();
	fn_ajax({ url : $action, data : $data});
}

<%-- 봇빌더 수정가능하게 변경 --%>
function fn_bchange($this, builderId, builderName) {
	var prevChanged = $(document).find( '.btn-dark' )[0];
	// 수정가능 입력창 한 개만 나타나도록 함
	if(prevChanged != undefined) {
		$(prevChanged).trigger('click');
	}

	var $tr = $($this).parents('tr');
	var builderIdx = $($this).data('data');
	$tr.children("td").remove();

	$tr.append( $('<td/>').append( $('<div/>', {'class' : 'input-group'}).append( $('<input/>', {type : 'text', id : builderIdx + '_builderName', 'class' : 'form-control', value : builderName, minlength : 2, maxlength : 20 }) ) ) );
	$tr.append( $('<td/>').append( $('<div/>', {'class' : 'input-group'}).append( $('<input/>', {type : 'text', id : builderIdx + '_builderId', 'class' : 'form-control', value : builderId}) ) ) );
	$tr.append( $('<td/>').append( $('<button/>', {type : 'button', 'class' : 'btn4submit btn btn-info waves-effect waves-themed', text : '확인', 'data-data' : builderIdx}) )
			.append( " " )
			.append( $('<button/>', {type : 'button', 'class' : 'btn btn-dark waves-effect waves-themed', text : '취소', 'data-data' : builderIdx, click : function() {
					fn_brollback(this, builderId, builderName);
				}}) ) );
}

<%-- 봇빌더 수정불가능하게 변경 --%>
function fn_brollback($this, builderId, builderName) {
	var $tr = $($this).parents('tr');
	var builderIdx = $($this).data('data');
	$tr.children("td").remove();

	$tr.append( $('<td/>', {text : builderName}) );
	$tr.append( $('<td/>', {text : builderId}) );
	$tr.append( $('<td/>').append( $('<button/>', {type : 'button', 'class' : 'btn btn-light waves-effect waves-themed', 'data-role' : "m", text : '수정', 'data-data' : builderIdx, click : function() {
			fn_bchange(this, builderId, builderName)
		}}) )
			.append( " " )
			.append( $('<button/>', {type : 'button', 'class' : 'btn btn-danger waves-effect waves-themed', 'data-data' : builderIdx, text : '삭제', click : function() {
					fn_bdelete(this, builderId, builderName);
				}}) ) );
}

<%-- 봇빌더 삭제 --%>
function fn_bdelete($this, builderId, builderName) {
	$frm_cudFrm.find('input[name=botIdx]').val($($this).data('data'));
	$frm_cudFrm.find('input[name=botId]').val(builderId);

	var $success = function() {
		var $action = 'botBuilderDelProc.do?cmnx=${cmnx}';
		var $data = $frm_cudFrm.serialize();
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}

	var $opt = { title : '알림', msg : builderName.trim() + '&nbsp;${_confirmDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
}

<%-- 봇아이디 수정가능하게 변경 --%>
function fn_change($this, botId, botName) {
	var prevChanged = $(document).find( '.btn-dark' )[0];
	// 수정가능 입력창 한 개만 나타나도록 함
	if(prevChanged != undefined) {
		$(prevChanged).trigger('click');
	}
	
	var $tr = $($this).parents('tr');
	var botIdx = $($this).data('data');
	$tr.children("td").remove();
	
	$tr.append( $('<td/>').append( $('<div/>', {'class' : 'input-group'}).append( $('<input/>', {type : 'text', id : botIdx + '_botName', 'class' : 'form-control', value : botName, minlength : 2, maxlength : 20}) ) ) );
	$tr.append( $('<td/>').append( $('<div/>', {'class' : 'input-group'}).append( $('<input/>', {type : 'text', id : botIdx + '_botId', 'class' : 'form-control', value : botId}) ) ) );
	$tr.append( $('<td/>').append( $('<button/>', {type : 'button', 'class' : 'btn4submit btn btn-info waves-effect waves-themed', text : '확인', 'data-data' : botIdx}) )
						  .append( " " )
						  .append( $('<button/>', {type : 'button', 'class' : 'btn btn-dark waves-effect waves-themed', text : '취소', 'data-data' : botIdx, click : function() {
							  fn_rollback(this, botId, botName);
						  }}) ) );
}

<%-- 봇아이디 수정불가능하게 변경 --%>
function fn_rollback($this, botId, botName) {
	var $tr = $($this).parents('tr');
	var botIdx = $($this).data('data');
	$tr.children("td").remove();
	
	$tr.append( $('<td/>', {text : botName}) );
	$tr.append( $('<td/>', {text : botId}) );
	$tr.append( $('<td/>').append( $('<button/>', {type : 'button', 'class' : 'btn btn-light waves-effect waves-themed', 'data-role' : "m", text : '수정', 'data-data' : botIdx, click : function() {
								fn_change(this, botId, botName)
						  }}) )
						  .append( " " )
						  .append( $('<button/>', {type : 'button', 'class' : 'btn btn-danger waves-effect waves-themed', 'data-data' : botIdx, text : '삭제', click : function() {
							  fn_delete(this, botId, botName);
						  }}) ) );
}

<%-- 봇아이디 삭제 --%>
function fn_delete($this, botId, botName) {
	$frm_cudFrm.find('input[name=botIdx]').val($($this).data('data'));
	$frm_cudFrm.find('input[name=botId]').val(botId);
	
	var $success = function() {
		var $action = 'botIdDelProc.do?cmnx=${cmnx}';
		var $data = $frm_cudFrm.serialize();
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
	
	var $opt = { title : '알림', msg : botName.trim() + '&nbsp;${_confirmDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
}
</script>			
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
<iframe id="targetIfrm" name="targetIfrm" style="width:0px;height:0px;" frameborder="0" title="iframe"></iframe>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
<%pageContext.setAttribute("replaceChar", "\r\n"); %>

<c:set var="URL_PAGE_LIST" value="${fn:replace(URL_PAGE_LIST, 'list.do', 'botIdViewAjax.do') }"/>

var varSource = '';
<c:if test="${empty listBuilder}">
varSource += '	<tr><td colspan="10">${_infoNoDataMsg_}</td></tr>';
</c:if>

<c:set var="listNo" value="${listStartNo}" />
<c:forEach items="${listBuilder}" var="model" varStatus="i">
	varSource += '	<tr>';
	varSource += '		<th scope="row">${listNo}</th>';
	varSource += '			<td>${model.builderName}</td>';
	varSource += '		<td>${model.builderId}</td>';
	varSource += '		<td>';
	varSource += '			<button type="button" data-data="${model.builderIdx}" class="btn btn-light waves-effect waves-themed" onclick="fn_bchange(this, \'${model.builderId}\', \'${model.builderName}\')">수정</button>';
	varSource += '			<button type="button" data-data="${model.builderIdx}" class="btn btn-danger waves-effect waves-themed" onclick="fn_bdelete(this, \'${model.builderId}\', \'${model.builderName}\')">삭제</button>';
	varSource += '		</td>';
	varSource += '	</tr>';
	<c:set var="listNo" value="${listNo - 1}" />
</c:forEach>

$('tbody[id="viewTbodyBuilder"]').html(varSource);

varSource = null;
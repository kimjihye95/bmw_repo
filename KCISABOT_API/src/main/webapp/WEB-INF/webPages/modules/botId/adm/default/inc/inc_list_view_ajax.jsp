<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
<%pageContext.setAttribute("replaceChar", "\r\n"); %>

<c:set var="URL_PAGE_LIST" value="${fn:replace(URL_PAGE_LIST, 'list.do', 'botIdViewAjax.do') }"/>

var varSource = '';
<c:if test="${empty list}">
varSource += '	<tr><td colspan="10">${_infoNoDataMsg_}</td></tr>';
</c:if>			

<c:set var="listNo" value="${listStartNo}" />
<c:forEach items="${list}" var="model" varStatus="i">
	<c:set var="botID" value="${efn:strContentsFormat(model.botId, '')}" />
	<c:set var="botName" value="${efn:strContentsFormat(model.botName, '')}" />
	varSource += '	<tr>';
	varSource += '		<th scope="row">${listNo}</th>';
	varSource += '			<td>${botName}</td>';
	varSource += '		<td style="word-break: break-word">${botID}</td>';
	varSource += '		<td>';
	varSource += '			<button type="button" data-data="${model.botIdx}" class="btn btn-light waves-effect waves-themed" onclick="fn_change(this, \'${botID}\', \'${botName}\')">수정</button>';
	varSource += '			<button type="button" data-data="${model.botIdx}" class="btn btn-danger waves-effect waves-themed" onclick="fn_delete(this, \'${botID}\', \'${botName}\')">삭제</button>';
	varSource += '		</td>';
	varSource += '	</tr>';
	<c:set var="listNo" value="${listNo - 1}" />
</c:forEach>

$('tbody[id="viewTbody"]').html(varSource);
$('#totalCount').html(${count}+"건");

// set paging
$isLoad = true;
<c:if test="${empty param.page}">
	var $pagingId = 'paging-view';
	if (!fn_isNull( $('#' + $pagingId).data("twbs-pagination") )) $('#' + $pagingId).twbsPagination('destroy');
	<c:if test="${not empty list}">
		var varOpt = {action : '${URL_PAGE_LIST}', obj : $pagingId, total : '${pageTotalPage}', visible : '${blockSize}'};
		fn_setPaging(varOpt);
		varOpt = null;
	</c:if>
	$pagingId = null;
</c:if>
$isLoad = false;

varSource = null;
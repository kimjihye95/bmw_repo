<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />

						<div class="row">
							<div class="col-sm-12">
								<h4><span class="text-warning"><i class="fas fa-dot-circle"></i></span> 마스터 코드</h4>
							</div>
							<div class="col-sm-6 justify-content-start">
								<p class="mgb0" style="line-height:38px;">Total. <span class="text-warning">${count}건</span></p>
							</div>
							<div class="col-sm-6 justify-content-end">
								<form id="fn_searchFrm" action="${URL_DEFAULT_LIST}">
									<input type="hidden" name="cmnx" value="${cmnx}" />
									<div class="dt-buttons" style="text-align:right;">
										<button type="button" class="btn btn-warning waves-effect waves-themed text-white" data-toggle="modal" data-target="#codeMasterRegModal">추가</button>
										<button type="button" class="btn4del btn btn-dark waves-effect waves-themed">삭제</button>
										<div class="btn-group mgl10">
											<select name="slmt" class="form-control">
												<c:forEach begin="1" end="3" var="no">
													<option value="${no * 10}"<c:if test="${param.slmt eq no * 10}"> selected</c:if>>${no * 10}건씩 조회</option>
												</c:forEach>
											</select>
										</div> 
										<button type="button" class="btn4search btn btn-outline-default"><span>변경</span></button> 
									</div>
								</form>
							</div>
						</div>

						<c:set var="URL_VIEW" value="${fn:replace(URL_DEFAULT_LIST, 'list.do', 'itemList.do')}" />
						<div class="mgt20">
							<form id="fn_delFrm" action="masterDelProc.do?cmnx=${cmnx}" class="col-sm-12">
								<table class="table table-striped m-0 tbListA">
									<colgroup>
										<col width="5%" />
										<col width="" />
										<col width="10%" />
									</colgroup>
									<thead class="bg-warning-900 text-white">
										<tr>
											<th></th>
											<th>마스터명</th>
											<th>관리</th>
										</tr>
									</thead>
									<tbody class="alignC">
									
										<c:if test="${empty list}">
											<tr><td colspan="3">${_infoNoDataMsg_}</td></tr>
										</c:if>
										
										<c:forEach items="${list}" var="model" varStatus="i">
											<c:set var="encMasterIdx" value="${efn:encrypt(model.masterIdx)}" />
											<tr>
												<th scope="row">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" name="codeCboxs" id="codeCboxs${i.count}" value="${encMasterIdx}" class="custom-control-input">
														<label class="custom-control-label" for="codeCboxs${i.count}"></label>
													</div>
												</th>
												<td><a href="${URL_VIEW}&idx=${efn:encrypt(model.masterIdx)}" class="btn4view">${model.masterName}</a></td>
												<td><button type="button" data-data="${encMasterIdx}" class="btn4mod btn btn-light waves-effect waves-themed" data-toggle="modal" data-target="#codeMasterModModal">수정</button></td>
											</tr>
										
										</c:forEach>
									</tbody>
								</table>
							</form>
						</div>
						
						<div class="mgt30" style="text-align:center;">
							<div id="paging"></div>
						</div>
						
						<%@ include file = "./inc/inc_list_modal.jsp" %>
<script>
var varOpt = {url : '${URL_PAGE_LIST}', obj : 'paging', total : '${pageTotalPage}', visible : '${blockSize}'};
fn_setPaging(varOpt);
<%-- 조회 카운트 변경 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4search', function() {
	var $frm = $('#fn_searchFrm');
	$frm.submit();
});
<%-- 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4del', function() {
	var $frm = $('#fn_delFrm');
	if (!fn_checkCboxChecked('codeCboxs')) return false;

	var $success = function() {
		var $action = $frm.attr('action');
		var $data = $frm.serialize();
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
	
	var $opt = { title : '알림', msg : '${_confirmDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
	
	return false;
});
<%-- 마스터 리스트 클릭 이벤트 --%>
$(document).on('click', '.btn4view', function() {
	$.cookie(_comMasterPageCookieName, '${param.page}', {expries : 1});
});
<%-- 코드 마스터 수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4mod', function() {
	var $midx = $(this).data('data');
	var $action = 'masterInfoAjax.do?cmnx=${cmnx}';
	fn_ajax({ url : $action, data : { idx : $midx } });
	$masterModFrm.find('input[name=idx]').val($midx);
});
</script>
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
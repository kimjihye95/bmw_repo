<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

		<%-- 권한 등록 start --%>
		<div class="modal fade" id="authRegModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content" >
					<div class="modal-header inno">
						<h5 class="modal-title">권한 추가</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_inputFrm" action="authRegistActionProc.do?cmnx=${cmnx}">
							<table class="tbWriteA mgt20">
								<caption>권한관리 추가 생성입니다.</caption>
								<colgroup>
									<col width="20%" />
									<col width="80%" />
								</colgroup>
	
								<tbody class="pd10 alignC">
									<tr>
										<c:set var="modelItem" value="authName" />
										<th scope="row"><label for="${modelItem}">권한명 <span>*</span></label></th>
										<td><input type="text" name="${modelItem}" id="${modelItem}" class="form-control" placeholder="권한명을 입력하세요." minlength="2" maxlength="20" required></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="menuIdx" />
										<th scope="row"><label for="${modelItem}">메뉴접근 권한 </label></th>
										<td>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" id="allMenu" data-frm="fn_inputFrm" class="cbox cbox4all cbox4reg custom-control-input">
												<label class="custom-control-label" for="allMenu">메뉴 전체</label>
											</div>
											
											<c:forEach items="${menuList}" var="menuModel" varStatus="i">
												<div class="custom-control custom-checkbox mgt10">
													<input type="checkbox" name="${modelItem}" id="${modelItem}${i.count}" class="cbox cbox4reg custom-control-input" value="${efn:encrypt(menuModel.menuIdx)}">
													<label class="custom-control-label" for="${modelItem}${i.count}">${menuModel.menuName}</label>
												</div>
											</c:forEach>
										</td>
									</tr>
									<tr>
										<c:set var="modelItem" value="irqaIsOpen" />
										<th scope="row"><label for="${modelItem}1">IRQA 접근 권한 </label></th>
										<td>
											<c:forEach items="${CODE_IRQA_STATUS_LIST}" var="item" varStatus="i">
												<div class="custom-control custom-radio custom-control-inline">
													<input type="radio" name="${modelItem}" id="${modelItem}${i.count}" value="${efn:encrypt(i.count)}"<c:if test="${i.first}"> checked</c:if> class="custom-control-input">
													<label class="custom-control-label" for="${modelItem}${i.count}">${item}</label>
												</div>
											</c:forEach>
											
											<c:set var="modelItem" value="botIdx" />
											<div class="form-group mgt10">
												<select name="${modelItem}" id="${modelItem}" class="select2 form-control" multiple="multiple">
													<c:forEach items="${botIdList}" var="botIdModel" varStatus="i">
														<option value="${efn:encrypt(botIdModel.botIdx)}">${efn:strContentsFormat(botIdModel.botName, '')}</option>
													</c:forEach>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<c:set var="modelItem" value="categoryIsOpen" />
										<th scope="row"><label for="${modelItem}1">분류접근 권한 </label></th>
										<td>
											<c:forEach items="${CODE_CATEGORY_STATUS_LIST}" var="item" varStatus="i">
												<div class="custom-control custom-radio custom-control-inline">
													<input type="radio" name="${modelItem}" id="${modelItem}${i.count}" value="${efn:encrypt(i.count)}"<c:if test="${i.first}"> checked</c:if> class="custom-control-input">
													<label class="custom-control-label" for="${modelItem}${i.count}">${item}</label>
												</div>
											</c:forEach>
											
											<c:set var="modelItem" value="categoryIdx" />
											<div id="regCatDiv" class="form-group mgt10"></div>
										</td>
									</tr>
									<tr>
										<c:set var="modelItem" value="datasetIsOpen" />
										<th scope="row"><label for="${modelItem}1">데이터셋 접근 권한 </label></th>
										<td>
											<c:forEach items="${CODE_DATASET_STATUS_LIST}" var="item" varStatus="i">
												<div class="custom-control custom-radio custom-control-inline">
													<input type="radio" name="${modelItem}" id="${modelItem}${i.count}" value="${efn:encrypt(i.count)}"<c:if test="${i.first}"> checked</c:if> class="custom-control-input">
													<label class="custom-control-label" for="${modelItem}${i.count}">${item}</label>
												</div>
											</c:forEach>
											<span class="text-danger mgl20">* 접근가능 분류 내 데이터셋만 접근 가능</span>
											
											<div class="mgt10">
												<div class="custom-control-inline dataset-list">
													<span class="dset-item dset-fst"><input type="hidden" name="datasetIdx" value="${efn:encrypt(0)}" />모든 데이터셋</span>
												</div>
												<div class="custom-control-inline" style="float:right;">
													<button type="button" data-role="r" class="btn4dsAdd btn btn-dark" data-toggle="modal" data-target="#auth_dataset">추가</button>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="modal-footer pdt0">
						<button type="button" data-role="r" class="btn4submit btn btn-warning text-white">확인</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // 권한 등록 end --%>
		
		<%-- 권한 수정 start --%>
		<div class="modal fade" id="authModModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content" >
					<div class="modal-header inno">
						<h5 class="modal-title">권한 수정 - <span id="authModTitle"></span></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_modFrm" action="authRegistActionProc.do?cmnx=${cmnx}">
							<input type="hidden" name="idx" value="" />
							<table class="tbWriteA mgt20">
								<caption>권한관리 수정 입니다.</caption>
								<colgroup>
									<col width="20%" />
									<col width="80%" />
								</colgroup>
	
								<tbody class="pd10 alignC" id="authModTableTbody"></tbody>
							</table>
						</form>
					</div>
					<div class="modal-footer pdt0">
						<button type="button" data-role="m" class="btn4submit btn btn-warning text-white">확인</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // 권한 수정 end --%>
<script>
var $authRegFrm = $('#fn_inputFrm'), $authModFrm = $('#fn_modFrm'), $datasetListDiv;
<%-- 메뉴 접근 권한 - 체크박스 클릭 이벤트 --%>
$(document).on('click', '.cbox', function() {
	var $cls = ($(this).hasClass('cbox4mod') ? '.cbox4mod' : '.cbox4reg');
	var $allLen = $('.cbox' + $cls).not('.cbox4all').length, $chkLen = $('.cbox' + $cls + ':checked').not('.cbox4all').length;
	if ($(this).hasClass('cbox4all')) $('.cbox' + $cls).not('.cbox4all').prop('checked', $(this).is(':checked'));
	else $('.cbox.cbox4all' + $cls).prop('checked', ($allLen == $chkLen));
});
<%-- 데이터셋 접근 권한 - 라디오 버튼 클릭 이벤트 --%>
$(document).on('click', 'input[name=datasetIsOpen]', function() {
	$('.dataset-list b').text($(this).next().text());
});
<%-- 권한 추가 모달 - 데이터셋 찾기 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4dsAdd', function() {
	var $role = $(this).data('role');
	$datasetListDiv = ($role == 'r') ? $authRegFrm.find('.dataset-list') : $authModFrm.find('.dataset-list');
	fn_ajax( { url : 'authTreeAjax.do?cmnx=${cmnx}', data : {}, type : 'POST' } );
});
<%-- 권한 추가 모달 닫기 이벤트 --%>
$(document).on('hidden.bs.modal', '#authRegModal', function() {
	$authRegFrm[0].reset();
	$($authRegFrm).find('.dataset-list').html('<span class="dset-item dset-fst"><input type="hidden" name="datasetIdx" value="${efn:encrypt(0)}" />모든 데이터셋</span>');
});
<%-- 권한 추가 모달 - 확인 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4submit', function() {
	var $role = $(this).data('role');
	var $frm = ($role == 'r') ? $authRegFrm : $authModFrm;

	if ($frm.valid()) {

		var $authNameData = $frm.find('input[name=authName]').val();
		if (/[^(0-9가-힣ㄱ-ㅎㅏ-ㅣa-zA-Z ]/gi.test($authNameData)) {
			var $opt = { title : '알림', msg : '한글, 영문, 숫자, 공백 만 입력이 가능합니다.', icon : 'warning' };
			fn_openSweetAlert($opt);
			return false;
		}
		
		var $action = $frm.attr('action');
		var $data = $frm.serialize();
		fn_ajax( { url : $action, data : $data, type : 'POST' } );
	}
});
</script>
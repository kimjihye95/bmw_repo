<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '';
<c:set var="authMenu" value="" />
<c:forEach items="${authMenuList}" var="menuModel" varStatus="i">
	<c:set var="authMenu" value="${authMenu}|${menuModel.menuIdx}|" />
</c:forEach>

<c:set var="authCategory" value="" />
<c:forEach items="${authCategoryList}" var="categoryModel" varStatus="i">
	<c:set var="authCategory" value="${authCategory}|${categoryModel.categoryIdx}|" />
</c:forEach>

<c:set var="authIrqa" value="" />
<c:forEach items="${authIrqaList}" var="irqaModel" varStatus="i">
	<c:set var="authIrqa" value="${authIrqa}|${irqaModel.botIdx}|" />
</c:forEach>

varSource += '<tr>';
varSource += '<c:set var="modelItem" value="authName" />';
varSource += '<th scope="row"><label for="${modelItem}">권한명 <span>*</span></label></th>';
varSource += '<td><input type="text" name="${modelItem}" id="${modelItem}" value="${efn:strContentsFormat(model[modelItem], '')}" class="form-control" placeholder="권한명을 입력하세요." minlength="2" maxlength="20" required></td>';
varSource += '</tr>';

varSource += '<tr>';
varSource += '<c:set var="modelItem" value="menuIdx" />';
varSource += '<th scope="row"><label for="${modelItem}">메뉴접근 권한 </label></th>';
varSource += '<td>';
varSource += '<div class="custom-control custom-checkbox">';
varSource += '<input type="checkbox" id="allMenu2" data-frm="fn_modFrm" class="cbox cbox4all cbox4mod custom-control-input"<c:if test="${fn:length(authMenuList) eq fn:length(menuList)}"> checked</c:if>>';
varSource += '<label class="custom-control-label" for="allMenu2">메뉴 전체</label>';
varSource += '</div>';
	<c:forEach items="${menuList}" var="menuModel" varStatus="i">
	<c:set var="menuTarget" value="|${menuModel.menuIdx}|" />
varSource += '<div class="custom-control custom-checkbox mgt10">';
varSource += '<input type="checkbox" name="${modelItem}" id="${modelItem}_${i.count}"<c:if test="${fn:indexOf(authMenu, menuTarget) ge 0}"> checked</c:if> value="${efn:encrypt(menuModel.menuIdx)}" class="cbox cbox4mod custom-control-input">';
varSource += '<label class="custom-control-label" for="${modelItem}_${i.count}">${menuModel.menuName}</label>';
varSource += '</div>';
	</c:forEach>
varSource += '</td>';
varSource += '</tr>';

varSource += '<tr>';
	<c:set var="modelItem" value="irqaIsOpen" />
varSource += '<th scope="row"><label for="${modelItem}m1">IRQA 접근 권한 </label></th>';
varSource += '<td>';
	<c:forEach items="${CODE_IRQA_STATUS_LIST}" var="item" varStatus="i">
varSource += '<div class="custom-control custom-radio custom-control-inline">';
varSource += '<input type="radio" name="${modelItem}" id="${modelItem}m${i.count}" value="${efn:encrypt(i.count)}"<c:if test="${i.first or i.count eq model.irqaStatus}"> checked</c:if> class="custom-control-input">';
varSource += '<label class="custom-control-label" for="${modelItem}m${i.count}">${item}</label>';
varSource += '</div>';
	</c:forEach>
varSource += '<div class="form-group mgt10">';
varSource += '<select class="select2 form-control" multiple="multiple" id="multiple-irqa" name="botIdx">';
	<c:forEach items="${botIdList}" var="botIdModel" varStatus="i">
	<c:set var="botIdTarget" value="|${botIdModel.botIdx}|" />
varSource += '<option value="${efn:encrypt(botIdModel.botIdx)}"<c:if test="${fn:indexOf(authIrqa, botIdTarget) ge 0}"> selected</c:if>>${efn:strContentsFormat(botIdModel.botName, '')}</option>';
	</c:forEach>
varSource += '</select>';
varSource += '</div>';
varSource += '</td>';
varSource += '</tr>';

varSource += '<tr>';
	<c:set var="modelItem" value="categoryIsOpen" />
varSource += '<th scope="row"><label for="${modelItem}m1">분류접근 권한 </label></th>';
varSource += '<td>';
	<c:forEach items="${CODE_CATEGORY_STATUS_LIST}" var="item" varStatus="i">
varSource += '<div class="custom-control custom-radio custom-control-inline">';
varSource += '<input type="radio" name="${modelItem}" id="${modelItem}m${i.count}" value="${efn:encrypt(i.count)}"<c:if test="${i.first or i.count eq model.categoryStatus}"> checked</c:if> class="custom-control-input">';
varSource += '<label class="custom-control-label" for="${modelItem}m${i.count}">${item}</label>';
varSource += '</div>';
	</c:forEach>
varSource += '<div class="form-group mgt10">';
varSource += '<select class="select2 form-control" multiple="multiple" id="multiple-basic" name="categoryIdx">';
	<c:forEach items="${categoryList}" var="categoryModel" varStatus="i">
		<c:if test="${!i.first}">
	<c:set var="categoryTarget" value="|${categoryModel.categoryIdx}|" />
varSource += '<option value="${efn:encrypt(categoryModel.categoryIdx)}"<c:if test="${fn:indexOf(authCategory, categoryTarget) ge 0}"> selected</c:if>>${categoryModel.categoryName}</option>';
		</c:if>
	</c:forEach>
varSource += '</select>';
varSource += '</div>';
varSource += '</td>';
varSource += '</tr>';

varSource += '<tr>';
	<c:set var="modelItem" value="datasetIsOpen" />
varSource += '<th scope="row"><label for="${modelItem}m1">데이터셋 접근 권한 </label></th>';
varSource += '<td>';

<c:forEach items="${CODE_DATASET_STATUS_LIST}" var="item" varStatus="i">
varSource += '<div class="custom-control custom-radio custom-control-inline">';
varSource += '<input type="radio" name="${modelItem}" id="${modelItem}m${i.count}" value="${efn:encrypt(i.count)}"<c:if test="${i.first or i.count eq model.datasetStatus}"> checked</c:if> class="custom-control-input">';
varSource += '<label class="custom-control-label" for="${modelItem}m${i.count}">${item}</label>';
varSource += '</div>';
</c:forEach>
varSource += '<span class="text-danger mgl20">* 접근가능 분류 내 데이터셋만 접근 가능</span>';
varSource += '<div class="mgt10">';
varSource += '<div class="custom-control-inline dataset-list">';
	<c:if test="${empty authDatasetList}">
varSource += '<span class="dset-item dset-fst"><input type="hidden" name="datasetIdx" value="${efn:encrypt(0)}" />모든 데이터셋</span>';
	</c:if>
	<c:forEach items="${authDatasetList}" var="datasetModel" varStatus="i">
varSource += '<p><span class="dset-item">${datasetModel.datasetName}<input type="hidden" name="datasetIdx" value="${efn:encrypt(datasetModel.datasetIdx)}" /><span class="btn4datasetRemove"> ×</span></span></p>';
	</c:forEach>
varSource += '</div>';
varSource += '<div class="custom-control-inline" style="float:right;">';
varSource += '<button type="button" data-role="m" class="btn4dsAdd btn btn-dark" data-toggle="modal" data-target="#auth_dataset">추가</button>';
varSource += '</div>';
varSource += '</div>';
varSource += '</td>';
varSource += '</tr>';

$authModFrm.find('input[name=idx]').val('${efn:encrypt(model.authIdx)}');
$('#authModTableTbody').html(varSource);
$('.select2').select2();

varSource = null;
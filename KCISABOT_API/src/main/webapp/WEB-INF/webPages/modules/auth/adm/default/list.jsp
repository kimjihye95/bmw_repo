<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />
<style>
.dataset-list {display:block !important;}
.dset-item {background:#efebf5; border:1px solid #967bbd; color:#563d7c; text-align:center; padding:5px 10px; border-radius:3px; font-weight:400;}
.btn4datasetRemove {cursor:pointer;}
</style>

						<div class="row">
							<div class="col-lg-12" style="padding:0 20px 20px 20px;"  id="sortable-category">
								<div id="catWrap" class="mt-n4">
									<div class="col-lg-12 py-4" id="categoryWrap">
									
										<c:forEach items="${list}" var="model" varStatus="i">
											<c:set var="encAuthIdx" value="${efn:encrypt(model.authIdx)}" />
											<div id="panel${i.count}" data-data="${encAuthIdx}" data-ordr="${model.orderNo}" class="panel<c:if test='${model.authIdx eq 1}'> panel-locked</c:if> pd20"<c:if test="${model.authIdx eq 1}"> data-panel-sortable</c:if> data-panel-collapsed data-panel-fullscreen data-panel-close data-panel-locked data-panel-refresh data-panel-reset data-panel-color data-panel-custombutton>
												<div class="panel-hdr" style="border-bottom:0px;">
													<h2><i class="fal fa-check text-warning mr-2"></i> ${efn:strContentsFormat(model.authName, '')}
														<div class="d-flex position-relative ml-auto" style="max-width: 20rem;">
															<c:if test="${model.authIdx eq 1}">
															<span class="btn btn-secondary btn-lg btn-icon rounded-circle waves-effect waves-themed text-white"><i class="fal fa-lock-alt"></i></span>
															</c:if>
															<c:if test="${model.authIdx ne 1}">
															<button data-data="${encAuthIdx}" data-title="${model.authName}" class="btn4mod btn btn-warning btn-lg btn-icon rounded-circle waves-effect waves-themed text-white mgl10" data-toggle="modal" data-target="#authModModal"><i class="fal fa-pen"></i></button>
															<%-- <button data-idx="${encAuthIdx}" data-url="authDeleteActionProc.do?cmnx=${cmnx}" class="btn4del btn btn-danger btn-lg btn-icon rounded-circle waves-effect waves-themed text-white mgl10" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"><i class="fal fa-times"></i></button> --%>
															<button data-data="${encAuthIdx}" class="btn4del btn btn-danger btn-lg btn-icon rounded-circle waves-effect waves-themed text-white mgl10"><i class="fal fa-times"></i></button>
															<button data-data="${encAuthIdx}" class="panel-drag btn4drag btn btn-dark btn-lg btn-icon rounded-circle waves-effect waves-themed text-white mgl10"><i class="fal fa-arrows"></i></button>
															</c:if>
														</div>
													</h2>
												</div>
											</div>
										</c:forEach>
											
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="mgt30">
									<button type="button" class="btn4reg btn btn-outline-dark btn-lg btn-block waves-effect waves-themed" data-toggle="modal" data-target="#authRegModal"><i class="fal fa-plus-circle"></i> &nbsp; 추가</button>
								</div>
							</div>

						</div>
						
						<%@ include file = "./inc/inc_modal.jsp" %>
						<%@ include file = "./inc/inc_dataset_modal.jsp" %>
<script>
<%-- 리스트 수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4mod', function () {
	var $idx = $(this).data('data'), $title = $(this).data('title');
	fn_ajax( { url : "authInfo.do?cmnx=${cmnx}", data : { idx : $idx } } );
	$('#authModTitle').text($title);
	$('#authModModal').find('.select2').select2();
	$datasetListDiv = $authModFrm.find('.dataset-list');
});
<%-- 추가 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4reg', function(){
	$('#authRegModal').find('.select2').select2();
	fn_ajax( { url : 'authCategoryAjax.do?cmnx=${cmnx}', data : {}, type : 'POST' } );
})
<%-- 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4del', function() {
	var $this = $(this);
	const $success = function() {
		var $action = 'authDeleteActionProc.do?cmnx=${cmnx}';
		var $data = { idx : $this.data('data') };
		fn_ajax( { url : $action, data : $data, type : 'POST' } );
	}
	var $opt = { title : '알림', msg : '${_confirmDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
});
$(document).ready(function() {
	$('#js-page-content').smartPanel({
		localStorage : false,
		onChange : function(e) {
			var $prev = $(e).prev(), $this = $(e), $next = $(e).next(), $thisData = $this.data('data');
			var $prevOrdr = $prev.data('ordr'), $thisOrdr = $this.data('ordr'), $nextOrdr = $next.data('ordr');
			var $type = ($prevOrdr < $thisOrdr ? 'u' : 'd'), $refOrder = ($prevOrdr < $thisOrdr ? $nextOrdr : $prevOrdr);
			var $data = { idx : $thisData, refOrder : $refOrder, nowOrder : $thisOrdr, type : $type };
			fn_ajax( { url : 'modifyAuthOrderActionProc.do?cmnx=${cmnx}', data : $data, type : 'POST' } );
		}
	});
	$('.select2').select2();
	$('#categoryWrap').sortable({ containment: '#catWrap', scroll: false });
});
$(document).on('hidden.bs.modal', function (event) {
  if ($('.modal:visible').length) {
    $('body').addClass('modal-open');
  }
});
</script>
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
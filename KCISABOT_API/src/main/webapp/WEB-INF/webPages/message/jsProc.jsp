<%@ include file="../../../../admresources/jsp/commonTop.jsp"%>
<c:if test="${!empty header[Ajax] and header[Ajax] eq 'true'}">
	<c:set var="isAjax" value="${true}" />
</c:if>
<c:if test="${!isAjax && (empty message || fn:indexOf(message, '<script') == 0)}">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	
	<link id="vendorsbundle" rel="stylesheet" media="screen, print" href="../../../../admresources/css/vendors.bundle.css">
	<link id="appbundle" rel="stylesheet" media="screen, print" href="../../../../admresources/css/app.bundle.css">
	<link id="mytheme" rel="stylesheet" media="screen, print" href="#">
	<link id="myskin" rel="stylesheet" media="screen, print" href="../../../../admresources/css/skins/skin-master.css">
	<link rel="stylesheet" media="screen, print" href="../../../../admresources/css/themes/cust-theme-15.css">
	<link rel="stylesheet" href="../../../../admresources/css/notifications/sweetalert2/sweetalert2.bundle.css">
	<link rel="stylesheet" media="screen, print" href="../../../../admresources/css/toastr.css">
	<link rel="stylesheet" media="screen, print" href="../../../../admresources/css/inno-custom.css">
	<!-- Place favicon.ico in the root directory -->
	<link rel="apple-touch-icon" sizes="180x180" href="../../../../admresources/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../../admresources/img/favicon/favicon-32x32.png">
	<link rel="mask-icon" href="../../../../admresources/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	
	<script src="../../../../admresources/js/jquery-3.5.1.min.js"></script>
	<script src="../../../../admresources/js/common.js"></script>
	<script src="../../../../admresources/js/notifications/sweetalert2/sweetalert2.bundle.js"></script>

	<noscript>스크립트를 지원하지 않는 브라우저 입니다</noscript>
</c:if>
	${message}
<c:if test="${!isAjax && (empty message || fn:indexOf(message, '<script') == 0)}">
</head>
<body>
</body>
</html>
</c:if>
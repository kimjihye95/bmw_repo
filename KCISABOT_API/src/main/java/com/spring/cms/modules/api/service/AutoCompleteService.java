package com.spring.cms.modules.api.service;

import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cms.modules.api.mapper.AutoCompleteMapper;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Service
public class AutoCompleteService {
	
	private final SqlSessionFactory sqlSessionFactory;
	
	public AutoCompleteService(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	@Autowired
	private AutoCompleteMapper automapper;
	
	public List<EgovMap> getFAQList(EgovMap map){
		return automapper.getFAQList(map);
	}

}

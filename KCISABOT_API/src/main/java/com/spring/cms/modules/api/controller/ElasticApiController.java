package com.spring.cms.modules.api.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.spring.cms.framework.Vars;
import com.spring.cms.framework.util.CommonUtil;
import com.spring.cms.framework.util.StringUtil;
import com.spring.cms.modules.api.common.TableSet;

import egovframework.rte.psl.dataaccess.util.EgovMap;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

//Serial

@Controller
public class ElasticApiController extends CommonUtil{
	
	/**
	 * 엘라스틱서치 INDEX NAME
	 */
	@Value("${spring.es.index}")
	public String ES_INDEX;

	
	/**
	 * 엘라스틱서치 USER ID
	 */
	@Value("${spring.es.auth.id}")
	public String ES_AUTH_ID;

	/**
	 * 엘라스틱서치 USER PWD
	 */
	@Value("${spring.es.auth.pw}")
	public String ES_AUTH_PW;

	/**
	 * 엘라스틱서치 api url
	 */
	@Value("${spring.es.api.url}")
	public String ES_API_URL;
  
	@Autowired
	private TraditionalPatternController patten;

	@ResponseBody
	@PostMapping(value = "/api/elastic/search.do")
	@ApiOperation(value = "원문 검색 API")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = "no", value = "색인 번호", required = false, dataType = "int", defaultValue = "17"),
	    @ApiImplicitParam(name = "query", value = "검색어", required = false, dataType = "strsing", defaultValue = "현대"),
	    @ApiImplicitParam(name = "type", value = "검색어", required = false, dataType = "string", defaultValue = "디자인문양"),
	    @ApiImplicitParam(name = "feature", value = "검색어", required = false, dataType = "string", defaultValue = "특증 들"),
	    @ApiImplicitParam(name = "category", value = "검색어", required = false, dataType = "string", defaultValue = "식물문"),
	    @ApiImplicitParam(name = "bigcategory", value = "검색어", required = false, dataType = "string", defaultValue = "원시문양"),
	    @ApiImplicitParam(name = "offset", value = "검색 결과 중 리턴받을 page offset", required = false, dataType = "int", defaultValue = "1"),
	    @ApiImplicitParam(name = "limit", value = "검색 결과 중 리턴받을 개수", required = false, dataType = "int", defaultValue = "10"),
	})
	public EgovMap search1(
			@RequestParam(value="query", required=false) String query
			, @RequestParam(value="type", required=false) String type
			, @RequestParam(value="category", required=false) String category
			, @RequestParam(value="feature", required=false) String feature
			, @RequestParam(value="bigcategory", required=false) String bigcategory
			, @RequestParam(value="offset", required=false, defaultValue="1") int offset
			, @RequestParam(value="limit", required=false, defaultValue="10") int limit
			, @ApiIgnore HttpSession session) throws Exception {

		JSONObject json = new JSONObject();
		EgovMap returnJson = new EgovMap();
		
		JSONParser parser = new JSONParser();

		LinkedHashMap<String, String> paramSort = new LinkedHashMap<String, String>();
		
		JSONArray paramMust = new JSONArray();
		JSONArray paramMustNot = new JSONArray();
		JSONArray paramShould = new JSONArray();
		JSONArray paramFilter = new JSONArray();
				
		//int typenum = 1;
		//int no = patten.getNum(type, category, typenum, typenum);

		// 인덱스 설정
		//String index = "dataset_" + no + "/_search";
		//System.out.println("----------------no ::" + no);
		
		EgovMap tableMap = getTableInfo(type, category,0);
		int no = (int) tableMap.get("tableNo");
		String index = (String) tableMap.get("index");
		
		String[] paramArr = {query,category,feature};
		//paramMust.add(setQuery(paramArr));
		System.out.println("----------------paramMust :::" + paramMust);
		

		paramSort.put("_score", "desc");

		// search bool setting
		Map<String, Object> boolMap = new HashMap<String, Object>();
		boolMap.put("must", paramMust);
		boolMap.put("must_not", paramMustNot);
		boolMap.put("should", paramShould);
		boolMap.put("filter", paramFilter);

		// search query setting
		Map<String, Object> boolQueryMap = new HashMap<String, Object>();
		boolQueryMap.put("bool", new JSONObject(boolMap));

		// search param body setting
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("query", new JSONObject(boolQueryMap));
		paramMap.put("size", limit);
		paramMap.put("sort", paramSort);
		paramMap.put("track_total_hits", true);

		// elastic search parameters
		JSONObject param = new JSONObject(paramMap);
		json = callElasticSearch(index+"/_search", Vars.CODE_GET, param.toJSONString());
		
		JSONArray docsList = new JSONArray();

		if (json.get("error") == null) {
			// set custom data
			JSONObject hits = (JSONObject) json.get("hits");
			JSONArray hitsList = (JSONArray) hits.get("hits");
			//JSONArray docsList = new JSONArray();
			for (Object map : hitsList) {
				JSONObject temp = (JSONObject) map;
				JSONObject _source = (JSONObject) temp.get("_source");
				_source.put("score", temp.get("_score"));
				_source.remove("terms");
				_source.remove("@timestamp");
				docsList.add(_source);
			}
			
			EgovMap docs = new EgovMap();
			docs.put("took", json.get("took") + "ms");
			docs.put("totalCount", ((JSONObject) hits.get("total")).get("value"));
			docs.put("documents", docsList);
			returnJson.put("result", docs);
		}
		else {
			returnJson.putAll(json);
		}
		EgovMap newList = patten.getHtml(returnJson, no);
			
		return newList;
	}
	
	@ResponseBody
	@PostMapping(value = "/api/elastic/search2.do")
	@ApiOperation(value = "원문 검색 API")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "no", value = "색인 번호", required = false, dataType = "int", defaultValue = "17"),
			@ApiImplicitParam(name = "query", value = "검색어", required = false, dataType = "strsing", defaultValue = "현대"),
			@ApiImplicitParam(name = "type", value = "검색어", required = false, dataType = "string", defaultValue = "디자인문양"),
			@ApiImplicitParam(name = "feature", value = "검색어", required = false, dataType = "string", defaultValue = "특증 들"),
			@ApiImplicitParam(name = "category", value = "검색어", required = false, dataType = "string", defaultValue = "식물문"),
			@ApiImplicitParam(name = "bigcategory", value = "검색어", required = false, dataType = "string", defaultValue = "원시문양"),
			@ApiImplicitParam(name = "offset", value = "검색 결과 중 리턴받을 page offset", required = false, dataType = "int", defaultValue = "1"),
			@ApiImplicitParam(name = "limit", value = "검색 결과 중 리턴받을 개수", required = false, dataType = "int", defaultValue = "10"), })
	public JSONObject search2(@RequestParam(value = "query", required = false) String query,
			@RequestParam(value = "type", required = false) String type,
			@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "feature", required = false) String feature,
			@RequestParam(value = "bigcategory", required = false, defaultValue = "") String bigcategory,
			@RequestParam(value = "offset", required = false, defaultValue = "1") int offset,
			@RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
			@ApiIgnore HttpSession session) throws Exception {

		JSONObject json = new JSONObject();

		JSONObject paramSort = new JSONObject(); // 정렬 기준
		JSONArray paramMust = new JSONArray(); // 반드시 포함
		JSONArray paramMustNot = new JSONArray(); // 포함안함
		JSONArray paramShould = new JSONArray(); // ...
		JSONArray paramFilter = new JSONArray(); // 스코어에 반영 안되는 포함
		
		

		EgovMap tableMap = getTableInfo(type, category, 0);
		int code = (int) tableMap.get("code");
		String index = (String) tableMap.get("index");

		if (feature != null && !StringUtil.isEmpty(feature)) {
			limit = 10;
			paramMust.add(getQueryString(feature, tableMap.get("search")));
		}
		
		// 필더 데이터
		paramFilter = getParamFilter(getFilterMap(code, query, category));
		
		if(query != null && !StringUtil.isEmpty(query) && query.equals("기타")) {
			String[] ageArr = {"백제","신라","고려","조선","근대","현대","통일신라","고구려"};
			for(String item : ageArr) {
				paramMustNot.add(getMatchObj("xage", item));
			}
		}

		// 정렬 기준
		paramSort.put("_score", "desc");

		// elastic search parameters
		JSONObject param = setParamQuery(paramMust, paramMustNot, paramShould, paramFilter, paramSort, limit);
		System.out.println(param);
		json = callElasticSearch(index+"/_search", Vars.CODE_GET, param.toJSONString());

		System.out.println(json);
		EgovMap newList = getSearchResult(json);
			
		EgovMap result = getResultList(newList, code, 0);
		
		if(bigcategory.equals("원시문양")) {
			result = getOrgByResult(paramMust, paramMustNot, paramShould, paramFilter, paramSort, limit, result, true);
		}
		
		//JSONObject resultJson = new JSONObject(result);
		JSONObject resultJson = new JSONObject(result);
		
		return resultJson;
	}
	
	@ResponseBody
	@PostMapping(value = "/api/elastic/detail.do")
	public EgovMap detailSearch(
			@RequestParam(value="query", required=false) String query
			, @RequestParam(value="type", required=false) String type
			, @RequestParam(value="category", required=false) String category
			, @RequestParam(value="feature", required=false) String feature
			, @RequestParam(value="did", required=false) int did
			, @RequestParam(value="data", required=false) String data
			, @RequestParam(value="offset", required=false, defaultValue="1") int offset
			, @RequestParam(value="limit", required=false, defaultValue="10") int limit
			, @ApiIgnore HttpSession session) throws Exception {

		JSONObject json = new JSONObject();
		
		JSONObject paramSort = new JSONObject(); // 정렬 기준
		JSONArray paramMust = new JSONArray(); // 반드시 포함
		JSONArray paramMustNot = new JSONArray(); // 포함안함
		JSONArray paramShould = new JSONArray(); // ...
		JSONArray paramFilter = new JSONArray(); // 스코어에 반영 안되는 포함 
		
		
		int code = (int) getCode(data, did).get("code"); // code를 반환 받는다
		String original = getCode(data, did).get("original").toString();
		
		System.out.println(did + " : " + code + " : " + original);
		
		EgovMap tableMap = getTableInfo(type, category, code);
		String index = (String) tableMap.get("index");
		String didCol = (String) tableMap.get("didCol");
		
		if(code == TableSet.PC_DESIGN.getCode()) {
			paramMust.add(getQueryString("xoriginal:\""+original+"\"", null));
		}else {
			paramMust.add(getQueryString(didCol+":"+did, null));
		}
		
		// 정렬 기준
		paramSort.put("_score", "desc");
		
		JSONObject param = setParamQuery(paramMust, paramMustNot, paramShould, paramFilter, paramSort, limit);
		System.out.println(param);
		json = callElasticSearch(index+"/_search", Vars.CODE_GET, param.toJSONString());
		
		EgovMap newList =  getSearchResult(json);
		System.out.println(newList);
		
		EgovMap result = getResultList(newList, code, did);
		System.out.println(result);
		
		if(code != TableSet.PP_GALLERY.getCode() && code != TableSet.PM_TEMPLETE.getCode()) {
			EgovMap org = getOrgByResult(paramMust, paramMustNot, paramShould, paramFilter, paramSort, limit, result, false); 
			
			result.put("org", org.get("result"));
			result.put("orgText", org.get("resultText"));
		}
		
		return result;
	}
	
	// 엘라스틱에 전송할 쿼리로 가공
	private JSONObject setParamQuery(JSONArray paramMust, JSONArray paramMustNot, JSONArray paramShould,
			JSONArray paramFilter, JSONObject paramSort, int limit) {

		// search bool setting
		Map<String, Object> boolMap = new HashMap<String, Object>();
		boolMap.put("must", paramMust);
		boolMap.put("must_not", paramMustNot);
		boolMap.put("should", paramShould);
		boolMap.put("filter", paramFilter);

		// search query setting
		Map<String, Object> boolQueryMap = new HashMap<String, Object>();
		boolQueryMap.put("bool", new JSONObject(boolMap));

		// search param body setting
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("query", new JSONObject(boolQueryMap));
		paramMap.put("size", limit);
		paramMap.put("sort", paramSort);
		paramMap.put("track_total_hits", true);
		
		return new JSONObject(paramMap);
	}
		
	// 필터 데이터 가공
	private JSONArray getParamFilter(Map<String, Object> filterMap) {
		JSONArray paramFilter = new JSONArray(); // 스코어에 반영 안되는 포함
		
		for(Entry<String, Object> item : filterMap.entrySet()) {			
			paramFilter.add(getMatchObj(item.getKey(), item.getValue().toString()));
		}
		
		return paramFilter;
	}
	
	// 필터 데이터 가공
	private Map<String, Object> getQueryString(String query,Object fields) {

		// 문양의 특징을 입력 받았을 때
		Map<String, Object> queryMap = new HashMap<String, Object>();
		Map<String, Object> queryStringMap = new HashMap<String, Object>();

		queryMap.put("query", query);
		if(fields != null) queryMap.put("fields", fields);
		queryStringMap.put("query_string", queryMap);
		System.out.println(queryStringMap);

		return queryStringMap;
	}
	
	// 검색 결과를 가공
	private EgovMap getSearchResult(JSONObject json) {
		
		EgovMap returnJson = new EgovMap();
		JSONArray docsList = new JSONArray();
		
		if (json.get("error") == null) {
			// set custom data
			JSONObject hits = (JSONObject) json.get("hits");
			JSONArray hitsList = (JSONArray) hits.get("hits");
			//JSONArray docsList = new JSONArray();
			for (Object map : hitsList) {
				JSONObject temp = (JSONObject) map;
				JSONObject _source = (JSONObject) temp.get("_source");
				_source.put("score", temp.get("_score"));
				_source.remove("terms");
				_source.remove("@timestamp");
				docsList.add(_source);
			}
			
			EgovMap docs = new EgovMap();
			docs.put("took", json.get("took") + "ms");
			docs.put("totalCount", ((JSONObject) hits.get("total")).get("value"));
			docs.put("documents", docsList);
			returnJson.put("result", docs);
		}
		else {
			returnJson.putAll(json);
		}
		return returnJson;
	}
	
	private EgovMap getTableInfo(String type, String category, int code) {
		EgovMap map = new EgovMap();
		if("템플릿조합".equals(category) || code == TableSet.PM_TEMPLETE.getCode()) {
			map = TableSet.PM_TEMPLETE.getTableInfo();
		}else {
			if("형태별문양".equals(type) || code == TableSet.DOCMETA_FORM.getCode()) {
				map = TableSet.DOCMETA_FORM.getTableInfo();
			}else if("용도별문양".equals(type) || code == TableSet.DOCMETA_USE.getCode()) {
				map = TableSet.DOCMETA_USE.getTableInfo();
			}else if("디자인문양".equals(type) || code == TableSet.PC_DESIGN.getCode()) {
				map = TableSet.PC_DESIGN.getTableInfo();
			}else if("활용디자인".equals(type) || code == TableSet.PM_PATTERN.getCode()) {
				map = TableSet.PM_PATTERN.getTableInfo();
				if("패턴응용".equals(category)) {
					map.put("index", map.get("index")+","+TableSet.PM_TEMPLETE.getIndex());
				}
			}else if("제품디자인".equals(type) || code == TableSet.PP_DESIGN.getCode()) {
				map = TableSet.PP_DESIGN.getTableInfo();
			}else if("산업제품홍보관".equals(type) || code == TableSet.PP_GALLERY.getCode()) {
				map = TableSet.PP_GALLERY.getTableInfo();
			}
		}
		return map;
	}
	
	private Map<String, Object> getFilterMap(int code, String query, String category) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		System.out.println(code + " : " + query + " : " + category);
		
		if(category != null && !StringUtil.isEmpty(category)) {
			if(TableSet.DOCMETA_FORM.getCode() == code) {
				map = TableSet.DOCMETA_FORM.setCategoryMap(category);
				map.put("xhidden", "거짓");
			}else if(TableSet.DOCMETA_USE.getCode() == code) {
				map = TableSet.DOCMETA_USE.setCategoryMap(category);
				map.put("xhidden", "거짓");
			}else if(TableSet.PC_DESIGN.getCode() == code) {
				map = TableSet.PC_DESIGN.setCategoryMap(category);
				map.put("del_yn", "N");
			}else if(TableSet.PM_PATTERN.getCode() == code) {
				if(category.equalsIgnoreCase("Mobile 배경화면")) {
					map.put("depth1", "MOBILE 배경화면");
				}else {
					map = TableSet.PM_PATTERN.setCategoryMap(category);
				}
				map.put("del_yn", "N");
			}else if(TableSet.PP_DESIGN.getCode() == code) {
				map = TableSet.PP_DESIGN.setCategoryMap(category);
			}else if(TableSet.PP_GALLERY.getCode() == code) {
				map = TableSet.PP_GALLERY.setCategoryMap(category);
			}
		}
		
		if(query != null && !StringUtil.isEmpty(query) && !query.equals("기타")) {
			map.put("xage", query);
		} 
		
		return map;
	}

	// 엘라스틱 검색후 반환 받은 데이터 가공 
	private EgovMap getResultList(EgovMap returnJson, int code, int did) {

		// 엘라스틱 결과에서 result 분리
		EgovMap elasticResultObj = (EgovMap) returnJson.get("result");
		
		JSONParser parser = new JSONParser();

		// 결과값 중 필요한 값인 documents만 추출
		@SuppressWarnings("unchecked")
		List<Map<String, String>> documents = (List<Map<String, String>>) elasticResultObj.get("documents");

		// 담을 변수 선언
		JSONArray extractObjList = new JSONArray();
		EgovMap extractResult = new EgovMap();

		// 검색 결과가 없을 경우
		if (documents.size() == 0)
			extractResult.put("result", "매칭되는 결과가 없습니다.");
		
		if(code == TableSet.PC_DESIGN.getCode() && documents.size() > 0 && did > 0) {
			System.out.println("===================================================");
			System.out.println(documents);
			
			Map<String, String> tmpElement = new HashMap<String, String>();
			
			for (Map<String, String> element : documents) {
				if(Integer.parseInt(((Object) element.get("seq")).toString()) == did) {
					tmpElement.putAll(element);
				}else if(element.get("ucategory").contains("기본")) {
					tmpElement.put("xmain_abstract", element.get("uabstract"));
				}else if(element.get("ucategory").contains("확장")){
					tmpElement.put("xsub_abstract", element.get("uabstract"));
				}else {
					tmpElement.put("xmain_abstract", element.get("uabstract"));		
				}
			}
			documents = new ArrayList<Map<String,String>>();
			documents.add(tmpElement);
		}

		Object[] keyArr = {"did","target","original","code"};
		JSONArray resultText = new JSONArray();
		// documents 안의 값 추출
		for (Map<String, String> element : documents) {
			Map<String, Object> map = getReturnObj(code, element);
			extractObjList.add(map);

			Map<String, Object> tmp = new HashMap<String, Object>();
			for(Entry<String, Object> item : map.entrySet()) {
				for(Object obj : keyArr) {
					if(obj.equals(item.getKey())) {
						tmp.put(item.getKey(), item.getValue());
					}
				}
			}
			
			if(tmp != null) {
				resultText.add(tmp);
			}
			
		}
		
		System.out.println(resultText);
		
		extractResult.put("result", extractObjList);
		
		if(extractObjList.size() > 0) {
			extractResult.put("detail", extractObjList.get(0));
		}
		
		extractResult.put("resultText",resultText.toJSONString());
		extractResult.put("resultCount", documents.size());
		return extractResult;
	}

	// 반환받은 데이터중 필요한 데이터를 정의
	private EgovMap getReturnObj(int code, Map<String, String> element) {
		
		EgovMap displayObj = new EgovMap(); // 디스플레이용 오브젝트
		EgovMap commandObj = new EgovMap(); // 값 보관용 오브젝트

		displayObj.put("code", code); // 코드
		displayObj.put("img", element.get("thumb_800")); // 이미지 경로

		if (TableSet.DOCMETA_FORM.getCode() == code) {
			displayObj.put("original", element.get("xoriginal"));
			displayObj.put("target", element.get("did"));
			displayObj.put("file", element.get("xfile"));
			displayObj.put("did", element.get("did"));
			displayObj.put("url", "https://www.culture.go.kr/tradition/useView.do?did="+(Object)element.get("did"));
			displayObj.put("title", element.get("xtitle"));
			
			displayObj.put("문양설명", element.get("xabstract"));
			displayObj.put("문양분류", element.get("xtaxonomy")); //문양분류
			displayObj.put("국적", element.get("xage")); // 국적 시대
			displayObj.put("원천유물명", element.get("xmain")); // 원천유물명

		} else if (TableSet.DOCMETA_USE.getCode() == code) {
			displayObj.put("original", element.get("xoriginal"));
			displayObj.put("target", element.get("did"));
			displayObj.put("file", element.get("xfile"));
			displayObj.put("did", element.get("did"));
			displayObj.put("title", element.get("xtitle"));
			displayObj.put("url", "https://www.culture.go.kr/tradition/useView.do?did="+(Object)element.get("did"));
			
			
			displayObj.put("문양분류", element.get("xtaxonomy")); //문양분류XTAXONOMY
			displayObj.put("국적", element.get("xage")); // 국적 시대
			displayObj.put("원천유물명", element.get("xmain")); // 원천유물명
			displayObj.put("문양설명", element.get("xabstract"));

		} else if (TableSet.PC_DESIGN.getCode() == code) {
			displayObj.put("original", element.get("xoriginal"));
			displayObj.put("target", element.get("did"));
			displayObj.put("file", element.get("xfile"));
			displayObj.put("did", element.get("seq"));
			displayObj.put("title", element.get("xtitle"));
			
			displayObj.put("문양분류", element.get("xtype")); //문양분류
			displayObj.put("국적", element.get("xage")); // 국적 시대
			displayObj.put("소장기관", element.get("xlocation")); // 원천유물명
			displayObj.put("디자인방향성", element.get("xsub_abstract")); // 서브설명 디자인방향성
			displayObj.put("디자인요소", element.get("xmain_abstract")); // 메인설명 디자인요소
			displayObj.put("적용제품군", element.get("uitem")); // 문양활용 적용제품군
			displayObj.put("medium1", getMedium(element.get("medium1"), 1)); // 문양활용
			displayObj.put("medium2", getMedium(element.get("medium2"), 2)); // 문양활용
			displayObj.put("url", "https://www.culture.go.kr/tradition/designPatternView.do?seq="+(Object)element.get("seq"));
			

		} else if (TableSet.PM_PATTERN.getCode() == code) {
			if(element.get("xoriginal") == null) {
				displayObj.put("code", TableSet.PM_TEMPLETE.getCode());
			}
			displayObj.put("original", element.get("xoriginal"));
			displayObj.put("target", element.get("did"));
			displayObj.put("did", element.get("seq"));
			displayObj.put("title", element.get("xtitle"));
			displayObj.put("패턴조합정보", element.get("uabstract"));
			
			displayObj.put("문양분류", element.get("xtaxonomy")); //문양분류
			displayObj.put("국적", element.get("xage")); // 국적 시대
			displayObj.put("디자인문양", element.get("uformat")); // 국적 시대
			displayObj.put("url", "https://www.culture.go.kr/tradition/usePatternView.do?seq="+(Object)element.get("seq"));

		} else if (TableSet.PP_DESIGN.getCode() == code) {
			displayObj.put("original", element.get("xoriginal"));
			displayObj.put("target", element.get("usec_upid"));
			displayObj.put("did", element.get("seq"));
			displayObj.put("title", element.get("title"));
			displayObj.put("활용문양정보", element.get("content"));
			displayObj.put("활용문양", element.get("xtitle")); // 활용문양명
			displayObj.put("문양분류", element.get("xtaxonomy")); //문양분류
			displayObj.put("국적", element.get("xage")); // 국적 시대

		} else if (TableSet.PP_GALLERY.getCode() == code) {
			displayObj.put("did", element.get("seq"));
			displayObj.put("title", element.get("title"));
			displayObj.put("디자인설명", element.get("content"));
			
			displayObj.put("디자인컨셉", element.get("concept")); // 디자인컨셉
			displayObj.put("용도", element.get("use")); // 용도
			displayObj.put("활용문양", element.get("pattern_name")); // 활용문양명

		} else if (TableSet.PM_TEMPLETE.getCode() == code) {
			displayObj.put("did", element.get("seq"));
			displayObj.put("title", element.get("xtitle"));
			displayObj.put("url", "https://www.culture.go.kr/tradition/usePatternView.do?seq="+(Object)element.get("seq")+"&type=B");
			
		}

		return displayObj;
	}
	
	private EgovMap getOrgByResult(JSONArray paramMust, JSONArray paramMustNot, JSONArray paramShould,
			JSONArray paramFilter, JSONObject paramSort, int limit, EgovMap result, boolean isOrg) {
		JSONObject json = new JSONObject();
		paramMust = new JSONArray();
		
		List<EgovMap> resultList = (List<EgovMap>) result.get("result");
		List<String> targetList = new ArrayList<String>();
		
		String did = "";
		
		for(EgovMap item : resultList) {
			targetList.add(item.get("original").toString());
			did = item.get("did").toString();
		}
		
		JSONObject resultObj = new JSONObject();
		String queryStr = "";
		Map<String, String> queryMap = new HashMap<String, String>();
		for(String item : targetList) {
			queryStr += (queryStr == "" ? "(xoriginal:\""+item : "\" OR xoriginal:\"" + item);
		}
		queryStr += isOrg ? "\") AND xfile : *ORG" : "\")";
		queryMap.put("query", queryStr);
		resultObj.put("query_string", queryMap);
		
		
		paramFilter = new JSONArray();
		
		paramMust.add(resultObj);
		
		if(!isOrg) {
			paramMustNot.add(getMatchObj("did",did));
		}else {
			paramFilter.add(getMatchObj("xhidden", "거짓"));
		}
		
		JSONObject param = setParamQuery(paramMust, paramMustNot, paramShould, paramFilter, paramSort, limit);
		json = callElasticSearch(TableSet.DOCMETA_USE.getIndex()+"/_search", Vars.CODE_GET, param.toJSONString());
		EgovMap listMap = getSearchResult(json);
		EgovMap resultMap = getResultList(listMap, TableSet.DOCMETA_USE.getCode(), 0);
		
		return resultMap;
	}

	//파라미터로 받은 오브젝트 배열에서 필요한 code값을 반환
	private Map<String, Object> getCode(String data,int did) throws ParseException {
		
		
		System.out.println(data);
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONArray jsonArr = (JSONArray) obj;
		int code = TableSet.DOCMETA_USE.getCode();
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		for(Object jObj : jsonArr) {
			JSONObject target = (JSONObject)jObj;
			int targetDid = Integer.parseInt(target.get("did").toString());
			if(targetDid == did && target.get("code") != null) {
				code = Integer.parseInt(target.get("code").toString());
				map.put("original", target.get("original") != null ? target.get("original").toString() : " ");
				break;
			}
		}
		
		map.put("code", code);
		return map;
	}
	
	private JSONObject getMatchObj(String field, String param) {
		JSONObject obj = new JSONObject();
		obj.put(field, param);
		
		JSONObject matchObj = new JSONObject();
		matchObj.put("match", obj);
		return matchObj;
	}
	
	private String getMedium(String str, int num) {
		String result = "";
		
		if(StringUtil.isEmpty(str)) {
			return result;
		}
		
		if(str.equals("레")) {
			result = "추천 재질 " + num + " : " + "레진 (SLA/DLP)";
		}else if(str.equals("파")) {
			result = "추천 재질 " + num + " : " + "파우더 (SLS/DMLS)";
		}else if(str.equals("플")) {
			result = "추천 재질 " + num + " : " + "플라스틱 (FDM/FFF)";
		}
		
		return result;
	}
}



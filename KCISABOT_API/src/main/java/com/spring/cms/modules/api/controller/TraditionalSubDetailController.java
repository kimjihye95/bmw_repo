package com.spring.cms.modules.api.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.cms.modules.api.common.ApiDatasetNumber;
import com.spring.cms.modules.api.service.TraditionalPatternDetailService;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Controller
public class TraditionalSubDetailController {
	
	@Autowired
	private TraditionalPatternDetailService detailService;
	 
	@Autowired
	private ApiDatasetNumber apiDatasetNumber;
	
	/*
	 * KSM 2021-11-03 add
	 * ------------------------------------------------------*/
	@ResponseBody
	@PostMapping("/api/kcisa/detailTest.do")
	public JSONObject getDetailData(@RequestParam HashMap<String, String> paramMap
			, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
				
		JSONObject resultJson = new JSONObject();  
		
	    // 문양 고유번호 , 테이블 번호 
		int did = Integer.parseInt(paramMap.get("did"));
		int tableNo = getTableNo(paramMap.get("data"),did);
		
		EgovMap commandMap = new EgovMap(); // 파라미터 전달용 맵
		commandMap.put("did",did);
		commandMap.put("tableNo",tableNo);
		commandMap.put("colNum",1);
			
		// 상세보기 상단 데이터
		EgovMap detailMainMap = detailService.getDetailMain(commandMap);
		System.out.println("MAIN ===============" + detailMainMap);
		
		// 상세보기 하단 썸네일 데이터
		List<EgovMap> detailSubMap = detailService.getDetailSub(commandMap);
		System.out.println("SUB ===============" + detailSubMap);
		
		// 반환값
		resultJson.put("html", changeHtml(getDetailMainColName(tableNo), detailMainMap));
		resultJson.put("org", getDetailSubList(detailSubMap));
		resultJson.put("url", getDownloadUrl(tableNo, did));		
		
		return resultJson;
	}
	
	private List<EgovMap> getDetailSubList(List<EgovMap> list){
		List<EgovMap> commandList = new ArrayList<EgovMap>();
		String imgBaseUrl = "https://www.culture.go.kr/ptn/images/";
		EgovMap map = getDetailSubColName();
		
		for(EgovMap item : list) {
			EgovMap tmp = new EgovMap();
			tmp.put("did",item.get(map.get("did")));
			tmp.put("orgimg", imgBaseUrl + item.get("img"));
			tmp.put("orgtitle",item.get(map.get("orgtitle")));
			commandList.add(tmp);
		}
		return commandList;
	}
	
	private EgovMap getDetailSubColName() {
		EgovMap map = new EgovMap();
		map.put("did", "col1");
		map.put("orgimg", "col21");
		map.put("orgtitle", "col7");
		return map;
	}
	
	private StringBuffer changeHtml(EgovMap colName, EgovMap map) {
		StringBuffer html = new StringBuffer();
		EgovMap temp = map;
		String title = colName.get("bigtitle") + "";
		String content = "";
		
		 html.append("<div>");
		 html.append("<div style='border:1px solid red; width:300px; padding:10px;'>");
		 html.append("<span style = 'color:green'>" + temp.get(title) + "</span><br>");
		 html.append("<span style='width:75px; height:90px; border:1px solid red; display:block; margin:auto; font-size:5px;'><img src='https://www.culture.go.kr/ptn/images/'"+temp.get("img")+" /></span> <br>");
         html.append("<span>");
         for (int i = 0; i < colName.size(); i++) {
        	 title = colName.get("title" + i) + "";
        	 content = colName.get("num" + i) + "";
        	
        	 if (!"null".equals(title) && !"null".equals(content)) html.append("<span>" + title + " : " + temp.get(content) + "</span><br/>");
		}
         html.append("<hr>");
         title = colName.get("contenttitle") + "";
    	 content = colName.get("content") + "";
    	 html.append("<span>"+ title + " : " + temp.get(content) +"</span>");
         html.append("</div>");
		
		return html;
	}
	
	private EgovMap getDetailMainColName(int tableNo) {
		EgovMap map = new EgovMap();
		
		switch (tableNo) {
		case 17:
			map.put("bigtitle", "col2");
			map.put("contenttitle", "문양 설명");
			map.put("content", "col33");
			break;
		case 18:
			map.put("bigtitle", "col2");
			map.put("contenttitle", "문양 설명");
			map.put("content", "col17");
			break;
		case 19:
			map.put("bigtitle", "col2");
			map.put("contenttitle", "문양 설명");
			map.put("content", "col20");
			break;
		case 21:
			map.put("bigtitle", "co15");
			map.put("contenttitle", "문양 설명");
			map.put("content", "col15");
			break;
		case 24:
			map.put("bigtitle", "col2");
			map.put("contenttitle", "문양 설명");
			map.put("content", "col7");
			break;
		default:
			map.put("bigtitle", "col2");
			map.put("contenttitle", "문양 설명");
			map.put("content", "col33");
			break;
		}
		
		return map;
	}
	
	private String getDownloadUrl(int tableNo, int did) {
		String url = "";
		switch (tableNo) {
		case 17:
			url = "https://www.culture.go.kr/tradition/useView.do?did="+did;
			break;
		case 18:
			url = "https://www.culture.go.kr/tradition/designPatternView.do?seq="+did;
			break;
		case 19:
			url = "https://www.culture.go.kr/tradition/usePatternView.do?seq="+did;
			break;
		case 21:
			url = "https://www.culture.go.kr/tradition/usePatternView.do?seq="+did+"&type=B";
			break;
		case 24:
			url = "none";
			break;
		default:
			url = "https://www.culture.go.kr/tradition/useView.do?did="+did;
			break;
		}
		return url;
	}
	
	private int getTableNo(String data,int did) throws ParseException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(data);
		JSONArray jsonArr = (JSONArray) obj;
		int tableNum = apiDatasetNumber.getDocmetaNumber();
		
		for(Object jObj : jsonArr) {
			JSONObject target = (JSONObject)jObj;
			int targetDid = Integer.parseInt(target.get("did").toString());
			if(targetDid == did && target.get("tableNum") != null) {
				tableNum = Integer.parseInt(target.get("tableNum").toString());
				break;
			}
		}
		return tableNum;
	}
}

package com.spring.cms.modules.api.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Mapper
@Repository
public interface TraditionalPatternDetailMapper {
	// List 가져오는 쪽 (미사용)
	public List<EgovMap> getList(EgovMap map);
	
	// 상세보기 / 디자인별, 용도별, 형태별
	public EgovMap getDetailManin(EgovMap map);
	
	// 상세보기 / 위에 외 제외한 다른 table
	public List<EgovMap> getDetailSub(EgovMap map);

}

package com.spring.cms.modules.api.service;

import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cms.modules.api.mapper.TraditionalPatternDetailMapper;

import egovframework.rte.psl.dataaccess.util.EgovMap;


@Service
public class TraditionalPatternDetailService {
	
	private final SqlSessionFactory sqlSessionFactory;
	  
	  public TraditionalPatternDetailService(SqlSessionFactory sqlSessionFactory) {
	      this.sqlSessionFactory = sqlSessionFactory;
	  }

		@Autowired
		private TraditionalPatternDetailMapper mapper;
		
		// getList (미사용)
		public List<EgovMap> getList(EgovMap map) throws Exception {

			return mapper.getList(map);
		}
		
		/*
		 * KSM 2021-11-03 add
		 * ------------------------------------------------------*/
		public EgovMap getDetailMain(EgovMap map) throws Exception {
			return mapper.getDetailManin(map);
		}
		
		public List<EgovMap> getDetailSub(EgovMap map) throws Exception {
			
			return mapper.getDetailSub(map);
		}

}

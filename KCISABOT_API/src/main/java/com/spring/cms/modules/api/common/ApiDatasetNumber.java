package com.spring.cms.modules.api.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * API 데이터셋 번호 세팅 클래스
 * <p><b>NOTE:</b>
 * BAMS API에서 사용하는 데이터셋 번호를 조회한다.
 * 각 API 클래스는 필요 시 ApiDatasetNumber을 주입받아 사용한다.<br/>
 * --------------------------------------------------------------<br/>
 * </p>
 * <pre>
 *
 *  == 개정이력(Modification Information) ==<br/>
 *
 *   수정일         수정자       수정내용
 *  --------------------------------------------------------------------------------
 *   2021.02.17     고혜민       최초생성
 *  2021.08.13   KW          Spring Boot 개편
 *
 * </pre>
 * @author HM
 * @since 2021.02.17
 * @version 1.0
 */

@Component
public class ApiDatasetNumber {
	
	/** DOCMETA 테이블 번호*/
	@Value("${KCISA.DOCMETA.NUMBER}")
	private int docmetaNumber;

	/** PCN_CULTURE_DESIGN 테이블 번호 */
	@Value("${KCISA.PCDESIGN.NUMBER}")
	private int pcdesignNumber;
	
	/** PCN_MCH_PATTERN 테이블 번호 */
	@Value("${KCISA.PMPATTERN.NUMBER}")
	private int pmpatternNumber;
	
	/** PCN_MCH_TEMPLETE 테이블 번호 */
	@Value("${KCISA.PMTEMPLETE.NUMBER}")
	private int pmtempleateNumber;
	
	/** PCN_PATTERN_DESIGN 테이블 번호 */
	@Value("${KCISA.PPDESIGN.NUMBER}")
	private int ppdesignNumber;
	
	/** 구분 테이블 번호 */
	@Value("${KCISA.CATEGORY.NUMBER}")
	private int categoryNumber;

	/** ############################################################# **/
	/**
	 * 2021-02-24 추가사항 API.Gdsc.Acheive : 동의어1
	 * 
	 * @return int
	 */
	
	public int getDocmetaNumber() {
		return docmetaNumber;
	}

	public int getPcdesignNumber() {
		return pcdesignNumber;
	}

	public int getPmpatternNumber() {
		return pmpatternNumber;
	}

	public int getPmtempleateNumber() {
		return pmtempleateNumber;
	}

	public int getPpdesignNumber() {
		return ppdesignNumber;
	}

	public int getCategoryNumber() {
		return categoryNumber;
	}

	
}

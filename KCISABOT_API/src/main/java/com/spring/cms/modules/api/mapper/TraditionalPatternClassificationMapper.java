package com.spring.cms.modules.api.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Mapper
@Repository
public interface TraditionalPatternClassificationMapper {
	
	public List<EgovMap> selectCategory(EgovMap map);

	public String selectMenuContent(EgovMap menuContent);
}

package com.spring.cms.modules.api.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.mapper.Mapper;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Mapper
@Repository
public interface TraditionalChatUIAPIMapper {

	public List<EgovMap> faq_all(String position);


}

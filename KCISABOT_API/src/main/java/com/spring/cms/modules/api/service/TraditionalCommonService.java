package com.spring.cms.modules.api.service;

import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cms.modules.api.mapper.TraditionalCommonMapper;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Service
public class TraditionalCommonService {
	
	private final SqlSessionFactory sqlSessionFactory;
	  
	public TraditionalCommonService(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	@Autowired
	private TraditionalCommonMapper mapper;


	public EgovMap getStep1Text(EgovMap map) throws Exception{
		return mapper.getStep1Text(map);
	}
	
	public List<EgovMap> getCategory(EgovMap map){
		return mapper.selectCategory(map);
	}
	
	public String getMenuContent(EgovMap map){
		return mapper.selectMenuContent(map);
	}
}

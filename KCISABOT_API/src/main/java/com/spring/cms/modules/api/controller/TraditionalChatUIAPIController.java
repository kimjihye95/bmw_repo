package com.spring.cms.modules.api.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.cms.modules.api.service.TraditionalChatUIAPIService;

import egovframework.rte.psl.dataaccess.util.EgovMap;


@Controller
public class TraditionalChatUIAPIController {
	
	@Autowired
	private TraditionalChatUIAPIService chatUIAPIService;
	 
	/**
	 * 2021.11.15 YHS
	 * FAQ list 가져오기
	 * URI = /api/kcisa/faq.do
	 * */
	@ResponseBody
	@PostMapping("/api/kcisa/faq/{position}")
	public EgovMap faq(@PathVariable("position") String position) {
		
		EgovMap faq_result = new EgovMap();
		
		// 자주묻는 질문 가져오기.
		List<EgovMap> faq_all = chatUIAPIService.faq_all(position);

		TreeSet<Object> get_category = new TreeSet<Object>();
		List<Object> get_content = new ArrayList<Object>();
		
		// 자주묻는 질문 카테고리 중복 제거 및 추츌.
		for (EgovMap temp_gov : faq_all) {
			get_category.add(temp_gov.get("category"));
		}

		faq_result.put("title", get_category);
		
		// 카테고리에 해당하는 값 추출.
		for (Object temp_obj : get_category) {
			
			List<Object> get_value = new ArrayList<Object>();
			
			for (EgovMap temp_egov : faq_all) {
				
				if (temp_egov.get("category").equals(temp_obj)) {
					get_value.add(temp_egov.get("value"));
				}
				
			}
			get_content.add(get_value);
		}
		faq_result.put("content", get_content);
		
		System.out.println(faq_result);
		
		return faq_result;
	}
}

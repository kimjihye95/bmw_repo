package com.spring.cms.modules.api.service;

import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cms.modules.api.mapper.TalkbotUiMapper;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Service
public class TalkbotUiService {
	
	private final SqlSessionFactory sqlSessionFactory;
	
	public TalkbotUiService(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	@Autowired
	private TalkbotUiMapper uimapper;
	
	public List<EgovMap> getFAQList(int num){
		return uimapper.getFAQList(num);
	}

}

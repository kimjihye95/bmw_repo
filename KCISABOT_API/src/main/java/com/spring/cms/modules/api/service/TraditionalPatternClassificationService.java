package com.spring.cms.modules.api.service;

import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cms.modules.api.mapper.TraditionalPatternClassificationMapper;

import egovframework.rte.psl.dataaccess.util.EgovMap;


@Service
public class TraditionalPatternClassificationService {
	
	private final SqlSessionFactory sqlSessionFactory;
	  
	public TraditionalPatternClassificationService(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	@Autowired
	private TraditionalPatternClassificationMapper mapper;
	
	public List<EgovMap> getCategory(EgovMap map){
		return mapper.selectCategory(map);
	}
	
	public String getMenuContent(EgovMap map){
		return mapper.selectMenuContent(map);
	}

}

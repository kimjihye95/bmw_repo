package com.spring.cms.modules.api.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.cms.modules.api.service.TalkbotUiService;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Controller
public class TalkbotUiController {
	
	@Autowired
	private TalkbotUiService autoservice;
	
	// client 실행 시 가지고 있을 data
	@ResponseBody
	@PostMapping(value = "/api/ui/data.do")
	public JSONObject uidata(HttpServletRequest request, HttpServletResponse response) {
		JSONObject json = new JSONObject();
		List<EgovMap> autolist = new ArrayList<EgovMap>();
		EgovMap map = new EgovMap();
		// 금칙어
		map.put("wordtableNum", 41);
		// auto complete 결과 list
		autolist = autoservice.getFAQList(40);
		
		
		// auto complete setting
		json.put("autocomplete", autolist);
		
		// 금칙어 setting
		return json;
	}

}

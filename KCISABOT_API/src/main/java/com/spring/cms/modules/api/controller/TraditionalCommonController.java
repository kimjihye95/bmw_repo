package com.spring.cms.modules.api.controller;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.cms.framework.util.StringUtil;
import com.spring.cms.modules.api.common.ApiDatasetNumber;
import com.spring.cms.modules.api.service.TraditionalCommonService;
import com.spring.cms.modules.api.service.TraditionalPatternClassificationService;
import com.spring.cms.modules.api.service.TraditionalPatternDetailService;

import egovframework.rte.psl.dataaccess.util.EgovMap;


@Controller
public class TraditionalCommonController {
private static final Logger LOGGER = LoggerFactory.getLogger(TraditionalPatternController.class);
	

	@Autowired
	private TraditionalPatternClassificationService classService;
	
	@Autowired
	private TraditionalPatternDetailService patternService;
	
	@Autowired
	private TraditionalCommonService commonControllerService;
	
	private final ApiDatasetNumber apiDatasetNumber;
	
	private String typename1 = "형태별문양";
	private String typename2 = "용도별문양";
	private String typename3 = "디자인문양";
	private String typename4 = "활용디자인";
	private String typename5 = "패턴응용";
	private String typename6 = "제품디자인";
	
	private TraditionalCommonController(ApiDatasetNumber apiDatasetNumber) {
		this.apiDatasetNumber = apiDatasetNumber;
	}
	
	public EgovMap getStep(String type, String category, String era, String feature) {
		int num = 0;
		int categoryNum = 0;
		int eraNum = 0;
		EgovMap map = new EgovMap();
		
		
		while(true) {
			if(typename1.equals(type) || typename2.equals(type)) {
				num = 17;
				categoryNum = 5;
				if(!era.isEmpty()) {
					eraNum = 10;
					map.put("eraNum", eraNum);
				}
				break;
			}else if(typename3.equals(type)) {
				num = 18;
				categoryNum = 27;
				if(!era.isEmpty()) {
					eraNum = 6;
					map.put("eraNum", eraNum);
				}
				break;
			}else if(typename4.equals(type) && !typename5.equals(category)){
				num = 19;
				categoryNum = 5;
				break;
			}else if(typename4.equals(type) && typename5.equals(category)){
				num = 21;
				categoryNum = 6;
				break;
			}else if(typename6.equals(type)){
				num = 24;
				categoryNum = 16;
				break;
			}else {
				num = 17;
				categoryNum = 5;
				break;
			}
		}
		if(era.isEmpty()) {map.put("era", ""); map.put("eraNum", 0);}else {map.put("era", era);};
		if(feature.isEmpty()) {map.put("feature", "");}else {map.put("feature", feature);};
		
		map.put("tableNum", num);
		map.put("categoryNum", categoryNum);
		map.put("type", type);
		map.put("category", category);
		
		return map;
	}
	
	// 문양안내 정보 불러오기
	@ResponseBody
	@RequestMapping(value = "/api/kcisa/gethtml.do", method = RequestMethod.POST)
	public JSONObject getStep1Text(String col1, String col2, String col3) {
		EgovMap map = new EgovMap();
		JSONObject jsonO = new JSONObject();
		
		map.put("col1",col1);
		map.put("col2",col2);
		map.put("col3",col3);
		
		System.out.println("map" + map);
		
		try {
			jsonO.put("result", commonControllerService.getStep1Text(map).get("content"));
		}catch(Exception e) {
			e.printStackTrace();
		}
			
		return jsonO;
	}
	
	// 구분 불러오는 메소드
	@ResponseBody
	@PostMapping("/api/kcisa/categories.do")
	public JSONObject commoncategories(
			@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "hidden", required = false, defaultValue = "") String hidden)
			throws Exception {
		int datasetIdx = apiDatasetNumber.getCategoryNumber();
		JSONObject json = new JSONObject();
		EgovMap map = new EgovMap();
		map.put("category", category);
		map.put("datasetIdx", datasetIdx);
		map.put("hidden", hidden.length()>0 ? hidden.split(",") : null);
		
		json.put("result", commonControllerService.getCategory(map));
		return json;
	}

	// 구분 불러오는 메소드
	@ResponseBody
	@PostMapping("/api/kcisa/menu.do")
	public JSONObject menuset(
			@RequestParam(value = "col1") String bigCategory,
			@RequestParam(value = "col2") String middleCategory,
			@RequestParam(value = "col3") String smallCategory,
			@RequestParam(value = "param", defaultValue = "") String param,
			@RequestParam(value = "isBold", defaultValue = "") String isBold
			) throws Exception {

		int datasetIdx = 30;
		JSONObject json = new JSONObject();
		EgovMap map = new EgovMap();
		map.put("bigCategory", bigCategory);
		map.put("middleCategory", middleCategory);
		map.put("smallCategory", smallCategory);
		map.put("datasetIdx", datasetIdx);

		String result = commonControllerService.getMenuContent(map);
		
		if(!StringUtil.isEmpty(isBold) && isBold.equals("true")) {
			result = result.replace("${param}", "<b>${param}</b>");
		}
		
		if(param != null && !StringUtil.isEmpty(param)) {
			result = result.replace("${param}", param);
		}

		json.put("result", result);
		System.out.println(result);
		return json;
	}

}


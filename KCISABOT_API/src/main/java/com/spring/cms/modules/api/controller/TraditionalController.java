package com.spring.cms.modules.api.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.cms.modules.api.common.ApiDatasetNumber;
import com.spring.cms.modules.api.service.TraditionalPatternClassificationService;
import com.spring.cms.modules.api.service.TraditionalPatternDetailService;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Controller
public class TraditionalController {
	
private static final Logger LOGGER = LoggerFactory.getLogger(TraditionalPatternController.class);
	
	@Autowired
	private TraditionalPatternClassificationService classService;
	
	@Autowired
	private TraditionalPatternDetailService patternService;
	@Autowired
	private TraditionalPatternDetailService testService;
	@Autowired
	private ApiDatasetNumber apiDatasetNumber;
	
	@ResponseBody
	@RequestMapping(value = "/api/kcisa/test1.do", method = RequestMethod.POST)
	public JSONObject test(@RequestParam HashMap<String, String> paramMap, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		
		JSONObject json = new JSONObject();
		json.put("status", "fail");
		json.put("result", "request method failed !!");
		
		return json;
	}
	
	// common 컨트롤러로
//	// 구분 불러오는 메소드
//	@ResponseBody
//	@PostMapping("/api/kcisa/categories1.do")
//	public JSONObject commoncategories(@RequestParam(value="category", required=false) String category) throws Exception {
//		int datasetIdx = apiDatasetNumber.getCategoryNumber();
//		JSONObject json = new JSONObject();
//		EgovMap map = new EgovMap();
//		map.put("category", category);
//		map.put("datasetIdx", datasetIdx);
//		
//		json.put("result", classService.getCategory(map));
//		return json;
//	}
//	
//	// 구분 불러오는 메소드
//	@ResponseBody
//	@PostMapping("/api/kcisa/menu1.do")
//	public JSONObject menuset(@RequestParam(value="col1", required=false) String bigCategory,
//							  @RequestParam(value="col2", required=false) String middleCategory,
//							  @RequestParam(value="col3", required=false) String smallCategory
//							  ) throws Exception {
//		
//		int datasetIdx = 30;
//		JSONObject json = new JSONObject();
//		EgovMap map = new EgovMap();
//		map.put("bigCategory", bigCategory);
//		map.put("middleCategory", middleCategory);
//		map.put("smallCategory", smallCategory);
//		map.put("datasetIdx", datasetIdx);
//		
//		System.out.println(map);
//		
//		json.put("result", classService.getMenuContent(map));
//		System.out.println(json);
//		return json;
//	}

}

package com.spring.cms.modules.api.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Mapper
@Repository
public interface TraditionalCommonMapper {
	

	//활용 가이드 데이터
	public EgovMap getStep1Text(EgovMap e);
	
	// 구분 데이터
	public List<EgovMap> selectCategory(EgovMap map);
	
	// 메뉴 데이터
	public String selectMenuContent(EgovMap menuContent);
	
}

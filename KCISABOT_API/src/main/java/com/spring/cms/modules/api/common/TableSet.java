package com.spring.cms.modules.api.common;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import egovframework.rte.psl.dataaccess.util.EgovMap;

public enum TableSet {
	/* 시대 : XAGE
	 * 용도 : XSUBJECT
	 * 특징 : XABSTRACT,XTITLE
	 * */
	DOCMETA_USE(1,17,"did","docmeta","xsubject","xabstract,xtitle"), // 용도별
	
	/* 시대 : XAGE
	 * 분류 : XTAXONOMY
	 * 특징 : XABSTRACT,XTITLE
	 * */
	DOCMETA_FORM(2,17,"did","docmeta","xtaxonomy","xabstract,xtitle"), // 형태별
	
	/* 시대 : XAGE
	 * 분류 : XTAXONOMY2
	 * 특징 : XABSTRACT,XTITLE,UITEM,UABSTRACT
	 * */
	PC_DESIGN(3,18,"seq","pcn_culture_design","xtaxonomy2","xabstract,xtitle,uitem,uabstract"), // 디자인문양
	
	/* 분류 : CATEGORY
	 * 특징 : XTITLE
	 * */
	PM_TEMPLETE(4,21,"seq","pcn_mch_templete","category","xtitle"), // 활용디자인->템플릿조합
	
	/* 시대 : XAGE
	 * 분류 : XTAXONOMY
	 * 특징 : UCATEGORY,XABSTRACT,XTITLE,UITEM,UABSTRACT,UABSTRACT2
	 * */
	PM_PATTERN(5,19,"seq","pcn_mch_pattern","ucategory","ucategory,xabstract,xtitle,uitem,uabstract,uabstract2"), // 활용디자인
	
	/* 분류 : CATEGORY
	 * 특징 : TITLE
	 * */
	PP_GALLERY(6,24,"seq","pcn_pattern_gallery","category","title,content"), // 산업제품홍보관
	
	/* 분류 : UITEM
	 * 특징 : XTAXONOMY,XABSTRACT,CONTENT,TITLE
	 * */
	PP_DESIGN(7,17,"seq","pcn_pattern_design","uitem","xtaxonomy,xabstract,content,xtitle"); // 제품디자인
	
	int code;
	int tableNo;
	String didCol;
	String index;
	String category;	
	String searchCol;
	
	TableSet(int code,int tableNo, String didCol, String index, String category, String searchCol) {
		// TODO Auto-generated constructor stub
		this.code = code;
		this.tableNo = tableNo;
		this.didCol = didCol;
		this.index = index;
		this.category = category;
		this.searchCol = searchCol;
	}

	public String getIndex() {
		return this.index;
	}
	
	public int getCode() {
		return this.code;
	}
	
	public int getTableNo() {
		return this.tableNo;
	}
	
	public EgovMap getTableInfo() {
		EgovMap map = new EgovMap();
		map.put("code", code);
		map.put("tableNo", tableNo);
		map.put("didCol", didCol);
		map.put("index", index);
		map.put("search", Arrays.asList(searchCol.split(",")));
		return map;
	}
	
	public Map<String, Object> setCategoryMap(String category) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(this.category, category);
		return map;
	}
}

package com.spring.cms.modules.api.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.cms.modules.api.service.AutoCompleteService;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Controller
public class AutoCompleteController {
	
	@Autowired
	private AutoCompleteService autoservice;
	
	@ResponseBody
	@PostMapping(value = "/api/autocomplete/auto.do")
	public JSONObject autoComplete(HttpServletRequest request, HttpServletResponse response) {
		JSONObject json = new JSONObject();
		List<EgovMap> list = new ArrayList<EgovMap>();
		EgovMap map = new EgovMap();
		map.put("tableNum", 40);
		list = autoservice.getFAQList(map);
		json.put("result", list);
		return json;
	}

}

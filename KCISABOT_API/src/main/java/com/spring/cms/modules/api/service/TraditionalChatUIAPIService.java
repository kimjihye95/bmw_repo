package com.spring.cms.modules.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.cms.modules.api.mapper.TraditionalChatUIAPIMapper;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Service
public class TraditionalChatUIAPIService {
	
	@Autowired
	private TraditionalChatUIAPIMapper mapper;

	public List<EgovMap> faq_all(String position) {
		return mapper.faq_all(position);
	}
	
}
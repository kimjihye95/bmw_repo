package com.spring.cms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.PropertySource;

@MapperScan(basePackages = "com.spring.cms")
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@PropertySource(value = { "${spring.prop.api.conf}" })
public class SourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SourceApplication.class, args);
	}

}

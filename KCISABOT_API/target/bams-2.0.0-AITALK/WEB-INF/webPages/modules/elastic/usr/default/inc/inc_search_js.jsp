<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
var $speriod = '', $sarea = '', $satm = '', $sdate = '', $edate = '${yesterday}';
var $cookieName = '_inno_search_filter_cookie_';

var yearPeriod = [{name:'최근2년', value:'2Y'}, {name:'최근1년', value:'1Y'}, {name:'직접입력', value:'F'}];
var monthPeriod = [{name:'최근6개월', value:'6M'}, {name:'최근3개월', value:'3M'}, {name:'직접입력', value:'F'}];
var weekPeriod = [{name:'최근4주', value:'4W'}, {name:'최근3주', value:'3W'}, {name:'최근2주', value:'2W'}, {name:'최근1주', value:'1W'}, {name:'직접입력', value:'F'}];
var dayPeriod = [{name:'최근7일', value:'7D'}, {name:'최근6일', value:'6D'}, {name:'최근5일', value:'5D'}, {name:'최근4일', value:'4D'}, {name:'최근3일', value:'3D'}, {name:'최근2일', value:'2D'}, {name:'최근1일', value:'1D'}, {name:'직접입력', value:'F'}];
var periodArray = [yearPeriod, monthPeriod, weekPeriod, dayPeriod];
// 상세검색 파라미터 배열
var $paramCategory = new Array();
var $chart1page = 1, $chart1limit = 10;

<%-- 기간 단위 선택 이벤트 --%>
$(document).on('click', 'input[name=stype]', function() {
	var $period = $('input[name=stype]');
	var $periodIndex = $($period).index($($period).filter(':checked'));
	var $select = periodArray[$periodIndex];
	$speriod = $(this).val();

	$($select).each(function(i, e) {
		if (i == 0) $('#datePeriod').empty();
		$('#datePeriod').append($('<option value="' + e.value + '">' + e.name + '</option>'));
	});
	$stype = $('input[name=stype]:checked').val();
	$edate = '${yesterday}';
	$('#datePeriod').change();
});
<%-- 기간 선택 이벤트 --%>
$(document).on('change', '#datePeriod', function() {
	$period = $(this).val();
	var $val = Number($period.substr(0, 1));
	var $unit = $period.substr(1);
	if ($period == 'F'){
		$('.input-daterange input').prop('disabled', false);
		$('input[name=sdayf]').focus();

		$('#sdayf, #sdayt').datepicker().on('change', function(e) {
			if (e.currentTarget.id == 'sdayf') $sdate = e.currentTarget.value;
			else $edate = e.currentTarget.value;
		});
	}
	else {
		$('.input-daterange input').prop('disabled', true);
		$('#sdayt').val('${yesterday}');
	}
	if ($unit == 'Y' || $unit == 'M') {
		$sdate = fn_beforeMonth( ($unit == 'Y' ? 12 * $val: $val), fn_dateConvtStr2Date($edate));
	}
	else if ($unit == 'W' || $unit == 'D') {
		$sdate = fn_beforeDate( ($unit == 'W' ? 7 * $val : $val), fn_dateConvtStr2Date($edate));
	}
	$('#sdayf').datepicker('setDate', $sdate);
	$('#sdayt').datepicker('setDate', '${yesterday}');
});
<%-- 상세 분석 열기 버튼 클릭 이벤트 --%>
$('.collapse').on('show.bs.collapse', function (e) {
	$('.btn4searchDet').text('상세 분석 닫기');
});
$('.collapse').on('hide.bs.collapse', function (e) {
	$('.btn4searchDet').text('상세 분석 열기');
});
// 기간 단위 체크
function fn_isCheckDateUnit() {
	var $obj = $('input[name=stype]'), $today = new Date($edate);
	var $pre2year = new Date($today.getFullYear() - 2, $today.getMonth(), $today.getDate());
	var $pre24mon = new Date($today.getFullYear(), $today.getMonth() - 24, $today.getDate());
	var $period = [ fn_dateDiff(fn_dateForm($pre2year), fn_dateForm($today)), fn_dateDiff(fn_dateForm($pre24mon), fn_dateForm($today)), 7 * 10, 10 * 1 ], $periodMsg = [ '2년', '24개월', '10주', '10일' ];
	var $index = $obj.index($obj.filter(':checked'));
	if (fn_dateDiff($sdate, $edate) > $period[$index]) {
		fn_openSweetAlert( { title : '알림', msg : $periodMsg[$index] + ' 이상 기간은 조회가 제한됩니다.' } );
		$('#sdayf').datepicker('setDate', $edate);
		return false;
	}
	return true;
}
function fn_setTagsinput($obj, $data) {
	$($obj).tagsinput('destroy');
	$($obj).val($data).tagsinput({tagClass: 'label label-info'});
}
<%-- 필터 클릭 이벤트 --%>
$(document).on('click', '.filter-content li', function() {
	var $tags = $('input[name=tags]');
	var $data = '', $paramData = '';
	$(this).toggleClass('open');

	$('.filter-content li.open').each(function(i, e) {
		if (i > 0){
			$data += ',';
			$paramData += ',';
		}

		$data += $(this).children('a').text();
		$paramData += $(this).children('a').data('val');
	});
	fn_setDetailParam($paramData);
	fn_setTagsinput($tags, $data);
	return false;
});
function fn_setDetailParam(data){
	var $data = data.split(',');
	
	$paramCategory.splice(0,$paramCategory.length);

	$data.forEach(function( element, i ){
		$paramCategory.push(element);
	});
}
<%-- 필터 유지 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4filter', function() {
	var $filter = $('input[name=tags]').val();
	if (!$(this).is(':checked')) $.removeCookie($cookieName);
	else $.cookie($cookieName, $filter, { expires : 30 });
});
<%-- 필터 'x' 버튼 클릭 이벤트 --%>
$(document).on('click', 'span[data-role=remove]', function() {
	var $tagsTxt = $(this).parent().text();
	$('.filter-content li.open').each(function(i, e) {
		if ($(this).text() == $tagsTxt) $(this).removeClass('open');
	});
});
<%-- 필터 전제지우기 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4filterReset', function() {
	var $tags = $('input[name=tags]');
	$('.filter-content li.open').each(function(i, e) {
		$(this).removeClass('open');
	});
	fn_setTagsinput($tags, '');
	fn_setDetailParam('');
	$('.btn4filter').prop('checked', false);
	$.removeCookie($cookieName);
});
<%-- paging --%>
function fn_paging(page){
	$chart1page = page;
	fn_apiAjax({ url : $apiUrl + 'api/search', data : fn_setParams(), type : 'POST', success : function(data) { fn_setChart1(data); } });
}
<%-- 원문 검색 --%>
function fn_setChart1(data) {
	var $result = data.result, $tag = '';
	var $doc = $result.documents;
	var $totalCnt = Math.ceil($result.totalCount / $chart1limit);
	if ($totalCnt == 0) $totalCnt = 1;
	var $offset = ($chart1page - 1) * $chart1limit;
	var $listNo = $result.totalCount - $offset;
	
	if (fn_isNull($doc)) {
		$tag = '<tr><td colspan="4" class="text-center">${_infoNoDataMsg_}</td></tr>';
	}
	$($doc).each(function(i, e) {
		if ($listNo > 0) {
			var $ind = (e.dispPublishDate).substring(0, 7).replace('.', '');
			$tag += '<tr>';
			$tag += '<td>' + $.number($listNo) + '</td>';
			$tag += '<td class="text-left">' + e.subject + '</td>';
			$tag += '<td>' + e.dispPublishDate + '</td>';
			$tag += '<td><button type="button" data-data="' + e.id + '" data-ind="' + $ind + '" data-toggle="modal" data-target=".bs-news-detail-modal" class="btn4detail btn btn-sm btn-outline-secondary waves-effect waves-themed">상세</button></td>';
			$tag += '</tr>';
		}
		$listNo --;
	});
	
	if ($('#skey').val()) $('#topKeyword').html('<strong>\'' + $('#skey').val() + '\'</strong></span>에 대한 ');
	else $('#topKeyword').html('');
	$('#topCount').text($.number($result.totalCount));
	$('#chart1').html($tag);

	var pageOpt = {obj : 'paging', fn : 'fn_paging', total : $totalCnt, visible : 10};
	fn_setAPIPaging(pageOpt);
	$isApiLoad = false;
}
<%-- 원문 상세보기 --%>
function fn_setChart1detail(data) {
	var $tag = '';
	var $fields = [ 'category', 'link', 'subject', 'dispPublishDate', 'publisher', 'contents' ];
	var $fieldNames = [ '분류', 'URL', '제목' , '게시일', '게시자', '내용' ];
	if (data.result) {
		const $result = data.result;
		$($fields).each(function(i, e) {
			var $cont = $result[e];
			if (e == 'category') {
				$cont = $result['category1name'] + ' > ' + $result['category2name'];
			}else if (e == 'contents') {
				$cont = fn_replaceAll($cont, '다.', '다.<br>');
			}
			$tag += '<div class="form-group">';
			$tag += '<label class="form-label">' + $fieldNames[i] + '</span>';
			if (e == 'link')
				$tag += '<div><a href="' + $cont + '" target="_blank">' + $cont + '</a></div>';
			else
				$tag += '<div>' + $cont + '</div>';
			$tag += '</div>';
		});
	}
	
	$('#newsDetailModalDiv').html($tag);
}
function fn_setParams() {
	var $params = new Object(), $category, $sword;
	var $sdayf = fn_replaceAll($('#sdayf').val(), '-', ''), $sdayt = fn_replaceAll($('#sdayt').val(), '-', '');
	var $query = $('#skey').val();
	var $period = $('input[name=stype]:checked').val();

	$category = $paramCategory.toString();
	$sword = $('#sword').val();

	$params = { query : $query, sword : $sword, category2 : $category, period : $period, sdate: $sdayf, edate : $sdayt, offset : $chart1page, limit : $chart1limit };
	
	return $.param($params);
}
<%-- 분석 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4search', function() {
	$chart1page = 1;
	$chart1limit = $('#slmt').val();
	$isApiLoad = true;
	$('#chart1').html('<tr><td colspan="4" class="text-center bg-white"><div class="text-center"><img src="${usrSitePath}/images/loading.gif"></div></td></tr>');
	if (!fn_isNull( $('#paging').data("twbs-pagination") )) $('#paging').twbsPagination('destroy');
	fn_apiAjax( { url : $apiUrl + 'api/search', data : fn_setParams(), type : 'POST', success : function(data) { fn_setChart1(data); } });
});
<%-- 검색어 / 불용어 엔터키 이벤트 --%>
$(document).on('keypress', '#skey, #sword', function(e) {
	if (e.keyCode == 13) {
		$('.btn4search:eq(0)').click();
	}
});
<%-- 원문 검색 - 리스트 적용 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4slmt', function() {
	$('.btn4search:eq(0)').click();
});
<%-- 리스트 상세 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4detail', function() {
	const $id = $(this).data('data'), $ind = $(this).data('ind');
	fn_apiAjax( { url : $apiUrl + 'api/searchDetail', data : { id : $id, ind : $ind }, success : function(data) { fn_setChart1detail(data); } });
});

$(document).ready(function() {
	$('.input-daterange').datepicker({ keyboardNavigation: false, forceParse: false, autoclose: true, format: 'yyyy-mm-dd', language: 'kr', endDate : '+0d' }).on('change', fn_isCheckDateUnit);
	var $cookieFilter = $.cookie($cookieName);
	if (!fn_isNull($cookieFilter)) {
		$('.btn4filter').prop('checked', true);
		$('input[name=tags]').val($cookieFilter);
		$('.filter-content li').each(function(i, e) {
			var $val = $(this).text();
			if ($cookieFilter.indexOf($val) > -1)
				$(this).addClass('open');
		});
	}

	<%-- 기간 단위 일 기본 선택 --%>
	$('input[name=stype]:checked').click();
	$('.btn4search:eq(0)').click();
});
</script>
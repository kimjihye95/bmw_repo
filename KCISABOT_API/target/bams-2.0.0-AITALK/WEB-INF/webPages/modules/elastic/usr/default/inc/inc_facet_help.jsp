<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<aside class="sidebar">
	<div class="sidebar-content">
		<h5>FACET API</h5>
		<div class="scroll_side">
			<h6>키워드 추이 검색 페이지</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/facet &nbsp; <a href="/swagger-ui.html#/elastic-facet-api-controller/facetUsingGET" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>query 검색어</li>
					<li>sword 불용어 [ 여러개일 경우 ',' 로 구분 ]</li>
					<li>period 기간 구분 [ D : 일, W : 주, M : 월, Y : 년 ] [ default : D ] (* 필수)</li>
					<li>category1 카테고리 [ 100 : 정치, 101 : 경제, 102 : 사회, 103 : 생활/문화, 104 : 세계, 105 : IT/과학 ]</li>
					<li>category2 서브 카테고리 [ 731 : 모바일, 230 : IT 일반, ... ]</li>
					<li>facetField 패싯 대상 컬럼 [ publishDate, category1, category2 ] [ default : publishDate ]</li>
					<li>sdate 검색 시작일 (* 필수)</li>
					<li>edate 검색 종료일 (* 필수)</li>
					<li>sort 정렬 항목 [ word, count, key ] [ default : key ]</li>
					<li>sortType 정렬순서 구분 [ ASC, DESC ] [ default : DESC ]</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
					<li style="word-break:break-all;"><pre class="paramCode text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode text-white"></pre></li>
				</ul>
			</div>
		</div>
		<button>
			<span class="sidebar-btn"><i class="fas fa-angle-right"></i>
				&nbsp;도움말 열기</span>
		</button>
	</div>
</aside>
<script>
	$('.scroll_side').slimscroll({ height : '850px' });
	var $_param = {"query":"애플","category2":"","facetField":"publishDate","period":"M","sdate":"20201008","edate":"20210408","sort":"key","sortType":"ASC"};
	var $_code = {"result":{"took":"3ms","documents":[{"count":650,"word":"2021Y 2M","key":"202102"}, {"count":3100,"word":"2021Y 3M","key":"202103"} ],"totalCount":3750,"jtook":"1ms"}};
	$('.paramCode').text(JSON.stringify($_code, null, 2));
	$('.respCode').text(JSON.stringify($_code, null, 2));
</script>
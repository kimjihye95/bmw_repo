<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<aside class="sidebar">
	<div class="sidebar-content">
		<h5>전체 데이터 수집현황 페이지</h5>
		<div class="scroll_side">
			<h6>전체 데이터 분석</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/count &nbsp; <a href="/swagger-ui.html#/elastic-count-api-controller/countUsingGET" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>NONE</li>
				</ul>
				<p class="side_title">Response</p>
				<ul>
					<li><pre class="respCode text-white"></pre></li>
				</ul>
			</div>
		</div>
		<button>
			<span class="sidebar-btn"><i class="fas fa-angle-right"></i>
				&nbsp;도움말 열기</span>
		</button>
	</div>
</aside>
<script>
	$('.scroll_side').slimscroll({ height : '850px' });
	var $_code = {"result":{"category2_100":[{"doc_count":2362,"key":"노동"},{"doc_count":569,"key":"사전 사고"},{"doc_count":300467,"key":"사회 일반"},{"doc_count":352,"key":"언론"},{"doc_count":271352,"key":"지역"}],"took":"4ms","jtook":"54ms","category1":[{"doc_count":3750,"key":"IT/과학"}],"totalCount":1263750,"dailyCount":10225}};
	$('.respCode').text(JSON.stringify($_code, null, 2));
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<aside class="sidebar">
	<div class="sidebar-content">
		<h5>키워드 분석</h5>
		<div class="scroll_side">
			<h6>분야별 데이터 현황, 이슈 랭킹 추이</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/issue &nbsp; <a href="/swagger-ui.html#/elastic-issue-api-controller/issueUsingGET" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>sword 불용어 [ 여러개일 경우 ',' 로 구분 ]</li>
					<li>period 기간 구분 [ D : 일, W : 주, M : 월, Y : 년 ] [ default : D ] (* 필수)</li>
					<li>category1 카테고리 [ 100 : 정치, 101 : 경제, 102 : 사회, 103 : 생활/문화, 104 : 세계, 105 : IT/과학 ]</li>
					<li>category2 서브 카테고리 [ 731 : 모바일, 230 : IT 일반, ... ]</li>
					<li>sdate 검색 시작일 (* 필수)</li>
					<li>edate 검색 종료일 (* 필수)</li>
					<li>sortType 정렬순서 구분 [ ASC, DESC ] [ default : DESC ]</li>
					<li>limit 검색 결과 중 리턴받을 개수 [ default : 10 ]</li>
					<li>docLimit 검색 문서 개수 [ default : 100 ]</li>
					<li>df df 사용여부 [ default : false ]</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
          <li style="word-break:break-all;"><pre class="paramCode text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode text-white"></pre></li>
				</ul>
			</div>
			
			<h6>주요 이슈 키워드 추이 페이지</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/facetList &nbsp; <a href="/swagger-ui.html#/elastic-facet-list-api-controller/facetListUsingGET" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>query 검색어</li>
					<li>sword 불용어 [ 여러개일 경우 ',' 로 구분 ]</li>
					<li>period 기간 구분 [ D : 일, W : 주, M : 월, Y : 년 ] [ default : D ] (* 필수)</li>
					<li>category1 카테고리 [ 100 : 정치, 101 : 경제, 102 : 사회, 103 : 생활/문화, 104 : 세계, 105 : IT/과학 ]</li>
					<li>category2 서브 카테고리 [ 731 : 모바일, 230 : IT 일반, ... ]</li>
					<li>sdate 검색 시작일 (* 필수)</li>
					<li>edate 검색 종료일 (* 필수)</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
          <li style="word-break:break-all;"><pre class="paramCode2 text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode2 text-white"></pre></li>
				</ul>
			</div>
			
			<h6>키워드 현황</h6>
			<div class="contBox">
				<p class="side_title">API URL</p>
				<ul>
					<li>/api/issueKeyword &nbsp; <a href="/swagger-ui.html#/elastic-issue-keyword-api-controller/issueKeywordUsingGET" class="btn btn-xs btn-success" target="_blank">OPEN SWAGGER</a></li>
				</ul>
				<p class="side_title">Parameters</p>
				<ul>
					<li>sword 불용어 [ 여러개일 경우 ',' 로 구분 ]</li>
					<li>period 기간 구분 [ D : 일, W : 주, M : 월, Y : 년 ] [ default : D ] (* 필수)</li>
					<li>category1 카테고리 [ 100 : 정치, 101 : 경제, 102 : 사회, 103 : 생활/문화, 104 : 세계, 105 : IT/과학 ]</li>
					<li>category2 서브 카테고리 [ 731 : 모바일, 230 : IT 일반, ... ]</li>
					<li>sdate 검색 시작일 (* 필수)</li>
					<li>edate 검색 종료일 (* 필수)</li>
					<li>sortType 정렬순서 구분 [ ASC, DESC ] [ default : DESC ]</li>
					<li>limit 검색 결과 중 리턴받을 개수 [ default : 10 ]</li>
					<li>docLimit 검색 문서 개수 [ default : 100 ]</li>
					<li>df df 사용여부 [ default : false ]</li>
				</ul>
				<p class="side_title">Parameters Sample</p>
				<ul>
          <li style="word-break:break-all;"><pre class="paramCode3 text-white"></pre></li>
				</ul>
				<p class="side_title">Response Sample</p>
				<ul>
					<li><pre class="respCode3 text-white"></pre></li>
				</ul>
			</div>
		</div>
		<button>
			<span class="sidebar-btn"><i class="fas fa-angle-right"></i>
				&nbsp;도움말 열기</span>
		</button>
	</div>
</aside>
<script>
	$('.scroll_side').slimscroll({ height : '850px' });
  var $_param = {"sword":"테스트,테스트2","category2":"731","period":"M","sdate":"20201008","edate":"20210408","limit":50,"docLimit":100};
	var $_code = {"result":{"took":"89ms","documents":[{"date":"202102","dpDate":"2021Y 2M","list":[{"tf":0.0068,"count":95,"word":"서비스"},{"tf":0.013,"count":89,"word":"콘텐츠"},{"tf":0.0098,"count":77,"word":"뉴스"},{"tf":0.0207,"count":75,"word":"애플"} ],"listCount":10} ],"category":[{"doc_count":3750,"key":"IT/과학"},{"doc_count":3515,"key":"정치"},{"doc_count":1981,"key":"경제"},{"doc_count":832,"key":"사회"},{"doc_count":536,"key":"생활/문화"}],"jtook":"674ms"}};
	$('.paramCode').text(JSON.stringify($_param, null, 2));
	$('.respCode').text(JSON.stringify($_code, null, 2));

	$_param = {"query":"애플","sword":"테스트,테스트2","category2":"731","period":"M","sdate":"20201008","edate":"20210408"};
	$_code = {"result":{"took":"6ms","documents":[{"date":"2021Y 2M","경찰":1,"국민":27,"후보":1,"수사":1,"대통령":1},{"date":"2021Y 3M","경찰":672,"국민":398,"후보":496,"수사":509,"대통령":777}],"totalCount":10000,"jtook":"1ms"}};
	$('.paramCode2').text(JSON.stringify($_param, null, 2));
	$('.respCode2').text(JSON.stringify($_code, null, 2));

	$_param = {"sword":"테스트,테스트2","category2":"731","period":"M","sdate":"20201008","edate":"20210408","limit":50,"docLimit":100};
	$_code = {"result":{"took":"99ms","documents":[{"tf":0.0071,"count":642,"word":"서비스"},{"tf":0.0089,"count":529,"word":"네이버"} ],"keyword":"검색어","jtook":"1385ms"}};
	$('.paramCode3').text(JSON.stringify($_param, null, 2));
	$('.respCode3').text(JSON.stringify($_code, null, 2));
</script>
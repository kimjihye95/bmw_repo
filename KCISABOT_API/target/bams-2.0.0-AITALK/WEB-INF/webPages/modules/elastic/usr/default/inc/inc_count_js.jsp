<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
function fn_setChart(data) {
	if (data.result) {
		var $category1data = new Array();
		const $result = data.result;
		const $category1 = $result.category1;
		<%-- 총 데이터 수집 건수 --%>
		$('#indexCount').html($.number($result.totalCount) + ' 건');
		<%-- 전일 데이터 수집 건수 --%>
		$('#dailyCount').html($.number($result.dailyCount) + ' 건');
		<%-- 대분류 수집 현황 --%>
		$category1.sort(function(a, b) {
			return a.key < b.key ? -1 : a.key > b.key? 1 : 0;
		});
		$($category1).each(function(i, e) {
			$category1data.push( { name : e.key, value : e.doc_count } );	
		});
		fn_amChart_barChart('chart1', $category1data, { categoryX : 'name', valueY : 'value' } );
		<%-- 소분류 수집 현황 --%>
		for (var i = 0; i < 6; i ++) {
			var $code = Number(100 + i), $data = new Array();
			var $category2 = $result['category2_' + $code];

			$category2.sort(function(a, b) {
				return a.key < b.key ? -1 : a.key > b.key? 1 : 0;
			});
			$($category2).each(function(i, e) {
				$data.push( { name : e.key, value : e.doc_count } );	
			});
			fn_amChart_barChart('chart2' + i, $data, { categoryX : 'name', valueY : 'value' } );
		}
	}
}
$(document).ready(function() {
	$('div[id^=chart]').html('<div class="text-center"><img src="${usrSitePath}/images/loading.gif"></div>');
	fn_apiAjax( { url : $apiUrl + 'api/count', success : function(data) { fn_setChart(data); } });
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../cresources/jsp/commonTop.jsp" %>
<jsp:include page = "${usrSitePath}/jsp/top.jsp" />
<%
String[] politicsArr = { "청와대", "국회/정당", "행정", "정치일반" };
int[] politicsCodeArr = { 264, 265, 266, 269 };

String[] economyArr = { "글로벌 경제", "생활경제", "경제 일반"};
int[] economyCodeArr = { 262, 310, 263};

String[] societyArr = { "사건사고", "노동", "언론", "지역", "사회 일반"};
int[] societyCodeArr = { 249, 251, 254, 256, 257 };

String[] livingArr = { "건강정보", "도로/교통", "생활문화 일반" };
int[] livingCodeArr = { 241, 240, 245 };

String[] worldArr = { "세계 일반" };
int[] worldCodeArr = { 322 };

String[] itscienceArr = { "모바일", "통신/뉴미디어", "IT 일반", "과학 일반" };
int[] itscienceCodeArr = { 731, 227, 230, 228 };

String[] crawlingNameArr = { "정치", "경제", "사회", "생활/문화", "세계", "IT/과학" };
int[] crawlingIdArr = { 100, 101, 102, 103, 104, 105 };
String[][] crawlingArr = { politicsArr, economyArr, societyArr, livingArr, worldArr, itscienceArr };
int[][] crawlingCodeArr = { politicsCodeArr, economyCodeArr, societyCodeArr, livingCodeArr, worldCodeArr, itscienceCodeArr };

request.setAttribute("crawlingNameArr", crawlingNameArr);
request.setAttribute("crawlingIdArr", crawlingIdArr);
request.setAttribute("crawlingArr", crawlingArr);
request.setAttribute("crawlingCodeArr", crawlingCodeArr);
%>

			<div class="col-xl-12">
				<div class="card">
					<div class="card-body">
						<%-- 검색 영역 start --%>
						<div class="frame-wrap">
							
							<c:set var="modelItem" value="stype" />
							<label class="mt-3 ml-2 mr-3" for="${modelItem}1"><strong>기간단위</strong></label>
							<div class="custom-control custom-radio custom-control-inline pdt10">
								<input type="radio" name="${modelItem}" id="${modelItem}1" class="custom-control-input" value="Y">
								<label class="custom-control-label" for="${modelItem}1">년</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline pdt10">
								<input type="radio" name="${modelItem}" id="${modelItem}2" class="custom-control-input" value="M">
								<label class="custom-control-label" for="${modelItem}2">월</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline pdt10">
								<input type="radio" name="${modelItem}" id="${modelItem}3" class="custom-control-input" value="W">
								<label class="custom-control-label" for="${modelItem}3">주</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline pdt10">
								<input type="radio" name="${modelItem}" id="${modelItem}4" class="custom-control-input" value="D" checked>
								<label class="custom-control-label" for="${modelItem}4">일</label>
							</div>

							<label class="ml-3 mr-1 mgt10" for=""><strong>기간</strong></label>
							<div class="custom-control custom-control-inline pdl0 mgr0">
								<c:set var="modelItem" value="datePeriod" />
								<select name="${modelItem}" id="${modelItem}" class="form-control"></select>
							</div>
							
							<div class="mr-3" style="display:inline-block;">
								<div class="input-daterange input-group" data-provide="datepicker">
									<c:set var="modelItem" value="sdayf" />
									<input type="text" name="${modelItem}" id="${modelItem}" value="${yesterday}" class="form-control" style="width:130px;" readOnly />
									<div class="input-group-append">
										<span class="input-group-text bg-primary text-white b-0">to</span>
									</div>
									<c:set var="modelItem" value="sdayt" />
									<input type="text" name="${modelItem}" id="${modelItem}" value="${yesterday}" class="form-control" style="width:130px;" readOnly />
								</div>
							</div>

							<c:set var="modelItem" value="skey" />
							<label class="ml-3 mr-1 mgt10" for="${modelItem}"><strong>검색어</strong></label>
							<div class=" custom-control custom-control-inline pdl0">
								<input type="text" name="${modelItem}" id="${modelItem}" class="form-control" value="${param.skey}" placeholder="키워드를 입력하세요." />
								<button type="button" class="btn4search btn btn-primary my-2 ml-1 mr-1 my-sm-0" style="width:100px;">분석</button>
								<button type="button" class="btn4searchDet btn btn-danger my-2 my-sm-0" style="width:200px;" data-toggle="collapse" data-target="#search">상세 분석 열기</button>
							</div>
						</div>
						<%-- //검색 영역 end --%>
					</div>
				</div>
				
				<%-- 상세 분석 영역 start --%>
				<div class="row">
					<div class="col-xl-12">
						<div class="card collapse mgb5" id="search" style="z-index:999;">
							<div class="card-header bg-dark">
								<h4 class="card-title text-white mb-0"><strong>상세분석</strong></h4>
								<div class="card-tools">
									<button type="button" class="close float-right text-white" data-toggle="collapse" data-target="#search">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-xl-12">
										<div class="form-group">
											<c:set var="modelItem" value="sword" />
											<label class="ml-2 mt-3" for="${modelItem}"><strong>불용어</strong></label>
											<div class="control-box control-box-full m-t-sm">
													<div class="col6">
												<input type="text" name="${modelItem}" id="${modelItem}" class="form-control" value="${param.skey}" placeholder="불용어 키워드를 입력하세요. 여러개일경우 ','로 구분하여 주십시오." />
												</div>
											</div>
											
											<label class="ml-2 mt-3" for=""><strong>필터</strong></label>
											<div class="control-box control-box-full m-t-sm">

												<c:forEach items="${crawlingNameArr}" var="crawlItem" varStatus="x">
													<c:set var="modelItem" value="${crawlingIdArr[x.index]}" />
													<div class="col6">
														<div class="filter-box-wrap">
															<div class="filter-box">
																<div class="filter-title">
																	<h5>${crawlItem}</h5>
																</div>
																<div class="filter-content">
																	<div class="scroll_content" style="min-height:150px;">
																		<ol id="cat">
																			<c:forEach items="${crawlingArr[x.index]}" var="item" varStatus="i">
																				<li><a data-val="${crawlingCodeArr[x.index][i.index]}">${item}</a></li>
																			</c:forEach>
																		</ol>
																	</div>
																</div>
															</div>
														</div>
													</div>
												
												</c:forEach>
													
											</div>
										</div>
									</div>
									
									<div class="col-xl-9">&nbsp;</div>
									<div class="col-xl-3 mgt20">
										<button type="button" class="btn4search btn btn-primary waves-effect waves-light btn-lg" style="width:100px">분석</button>
										<button type="button" data-toggle="collapse" data-target="#search" class="btn btn-danger waves-effect waves-light btn-lg" style="width:200px">상세 분석 닫기</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<%-- // 상세 분석 영역 end --%>

			</div>

			<%-- 적용필터 start --%>
			<div class="col-xl-12 mgt5">
				<div class="card">
					<div class="card-body" style="padding:10px;">
						<label class="mgl20 mr-2 mgt10" for=""><strong>적용필터</strong></label>
						<div style="display:inline-block">
							<c:set var="modelItem" value="tags" />
							<input type="text" name="${modelItem}" id="${modelItem}" value="" data-role="tagsinput" readOnly/>
						</div>
						<div class="custom-control custom-checkbox custom-control-inline float-right">
							<div class="mgt10">
							<input type="checkbox" class="btn4filter custom-control-input" id="customCheckinline2">
							<label class="custom-control-label" for="customCheckinline2">필터유지</label>
							</div>
							<button class="btn4filterReset btn btn-secondary waves-effect mgl20"><i class="mdi mdi-close"></i> 전체 지우기</button>
						</div>
					</div>
				</div>
			</div>
			<%-- // 적용필터 end --%>
			
			<%-- 최초 접속 시, 검색어 입력 전 메세지 출력 영역 --%>
			<div id="searchInfoDiv" class="col-xl-12 infoArea">
				<p class="symbol"><i class="mdi mdi-alert-circle"></i></p>
				<p class="text"><strong>키워드를 입력</strong>하고 분석하세요.</p>
			</div>
			<%-- //최초 접속 시, 검색어 입력 전 메세지 출력 영역 --%>
			
			<%-- 검색 영역 start --%>
			<div id="searchResultDiv" class="col-xl-12 mt-2" style="display:none;">
				<%-- 키워드 워드 클라우드 start --%>
				<div class="panel">
					<div class="panel-hdr">
						<h2>워드 클라우드</h2>
						<div class="paenl-tollbar">
							<div class="btn-group btn-group-sm">
								<button type="button" class="btn4visual btn btn-outline-secondary waves-effect waves-themed" data-ref="1" data-toggle="modal" data-target=".bs-chart-modal"><i class="fas fa-chart-bar"></i> 시각화 변경</button> 
								<button type="button" class="btn4dgrid btn btn-outline-secondary waves-effect waves-themed modal_dailyCount" data-ref="1" data-toggle="modal" data-title="워드 클라우드" data-target=".bs-dataView-modal"><i class="fas fa-database"></i> 데이터 보기</button>
							</div>
						</div>
						<div class="paenl-tollbar ml-1">
							<div class="input-group input-group-sm">
								<c:set var="modelItem" value="slmt" />
								<select name="${modelItem}" id="${modelItem}" class="form-control">
									<option value="30">30건씩</option>
									<option value="50" selected>50건씩</option>
									<option value="100">100건씩</option>
								</select>
								<div class="input-group-append">
									<button type="button" class="btn4slmt btn btn-dark btn-nora my-2 my-sm-0">적용</button>
								</div>
							</div>
						</div>
					</div>
					
					<div class="panel-container">
						<div class="panel-content">
							<div id="chart1" class="mgt10" style="min-height:350px;"></div>
						</div>
					</div>
				</div>
				<%-- //키워드 워드 클라우드 end --%>
				
				<%-- 연관어 분석 start --%>
				<div class="panel">
					<div class="panel-hdr">
						<h2>연관어 분석</h2>
						<div class="paenl-tollbar">
							<div class="btn-group btn-group-sm">
								<button type="button" class="btn4dgrid btn btn-outline-secondary waves-effect waves-themed modal_dailyCount" data-ref="2" data-toggle="modal" data-title="유사도 분석" data-target=".bs-dataView-modal"><i class="fas fa-database"></i> 데이터 보기</button>
							</div>
						</div>
					</div>
					
					<div class="panel-container">
						<div class="panel-content">
							<div class="row">
								<div class="col-lg-9">
									<div id="chart2" class="mgt10" style="min-height:350px;"></div>
								</div>
								<div class="col-lg-3">
									<div class="rank-box mr-3">
										<div class="rank-box-title green on">
											<h5>키워드 순위</h5>
										</div>
										<div class="rank-box-content">
											<ol id="chart21" class="num-st02"></ol>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<%-- // 연관어 분석 end --%>
				
				<%-- 상관관계 start --%>
				<div class="panel">
					<div class="panel-hdr">
						<h2>상관관계</h2>
					</div>
					
					<div class="panel-container">
						<div class="panel-content">
						
							<div class="row">
								<div class="col-xl-6 pdl0">
								
									<%-- 상관관계 순위 start --%>
									<div class="panel">
										<div class="panel-hdr">
											<h2>상관관계 순위</h2>
											<div class="panel-tollbar">
												<div class="input-group">
													<div class="btn-group btn-group-sm">
														<button type="button" class="btn4dgrid btn btn-outline-secondary waves-effect waves-themed modal_dailyCount" data-ref="3" data-toggle="modal" data-title="상관관계 순위" data-target=".bs-dataView-modal"><i class="fas fa-database"></i> 데이터 보기</button>
													</div>
													<select id="chart3limit" class="form-control ml-2">
														<option value="3">3건씩</option>
														<option value="5" selected>5건씩</option>
														<option value="10">10건씩</option>
													</select>
													<div class="input-append">
														<button type="button" class="btn4chart3limit btn btn-secondary waves-effect waves-light">적용</button>
													</div>
												</div>
											</div>
										</div>
										<div class="panel-container">
											<div class="panel-content">
												<div id="chart3" style="width:100%; height:300px;"></div> 
											</div>
										</div>
									</div>
									<%-- // 상관관계 순위 end --%>
								</div>
								
								<div class="col-xl-6 pdr0">
									<%-- 상관관계 분석 start --%>
									<div class="panel">
										<div class="panel-hdr">
											<h2>상관관계 분석</h2>
											<div class="panel-tollbar">
											</div>
										</div>
										<div class="panel-container">
											<div class="panel-content">
												<div id="chart4" style="width:100%; height:300px;"></div> 
											</div>
										</div>
									</div>
									<%-- // 상관관계 분석 end --%>
								</div>
								
								<div class="col-xl-12 pdr0">
								
									<%-- 상관관계 순위 - 키워드 추이 start --%>
									<div class="panel">
										<div class="panel-hdr">
											<h2>상관관계 순위 - 키워드 추이</h2>
											<div class="paenl-tollbar">
												<div class="btn-group btn-group-sm">
													<button type="button" class="btn4visual btn btn-outline-secondary waves-effect waves-themed" data-ref="5" data-toggle="modal" data-target=".bs-chart-modal"><i class="fas fa-chart-bar"></i> 시각화 변경</button> 
													<button type="button" class="btn4dgrid btn btn-outline-secondary waves-effect waves-themed modal_dailyCount" data-ref="5" data-toggle="modal" data-title="상관관계 순위 - 키워드 추이" data-target=".bs-dataView-modal"><i class="fas fa-database"></i> 데이터 보기</button>
												</div>
											</div>
										</div>
										<div class="panel-container">
											<div class="panel-content">
												<div id="chart5" style="width:100%; height:300px;"></div> 
											</div>
										</div>
									</div>
									<%-- // 상관관계 순위 - 키워드 추이 end --%>
								</div>
								
							</div>
							
						</div>
					</div>
					
				</div>
				<%-- //상관관계 end --%>
			</div>
			<%-- //검색 영역 end --%>
				
<%@ include file = "./inc/inc_keyword_help.jsp" %>
<%@ include file = "./inc/inc_keyword_js.jsp" %>
<jsp:include page = "${usrSitePath}/jsp/bottom.jsp" />
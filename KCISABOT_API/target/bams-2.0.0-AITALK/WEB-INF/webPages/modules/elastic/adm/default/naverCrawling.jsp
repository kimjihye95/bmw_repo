<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${thisModelInfo.includeTop}" />
<%
String[] politicsArr = { "청와대", "국회/정당", "행정", "정치일반" };
int[] politicsCodeArr = { 264, 265, 266, 269 };

String[] economyArr = { "글로벌 경제", "생활경제", "경제 일반"};
int[] economyCodeArr = { 262, 310, 263};

String[] societyArr = { "사건사고", "노동", "언론", "지역", "사회 일반"};
int[] societyCodeArr = { 249, 251, 254, 256, 257 };

String[] livingArr = { "건강정보", "도로/교통", "생활문화 일반" };
int[] livingCodeArr = { 241, 240, 245 };

String[] worldArr = { "세계 일반" };
int[] worldCodeArr = { 322 };

String[] itscienceArr = { "모바일", "통신/뉴미디어", "IT 일반", "과학 일반" };
int[] itscienceCodeArr = { 731, 227, 230, 228 };

String[] crawlingNameArr = { "정치", "경제", "사회", "생활/문화", "세계", "IT/과학" };
String[] crawlingIdArr = { "politics", "economy", "society", "living", "world", "itscience" };
String[][] crawlingArr = { politicsArr, economyArr, societyArr, livingArr, worldArr, itscienceArr };
int[][] crawlingCodeArr = { politicsCodeArr, economyCodeArr, societyCodeArr, livingCodeArr, worldCodeArr, itscienceCodeArr };
//int[] crawlingCodeArr = { 100, 101, 102, 103, 104, 105 };

request.setAttribute("crawlingNameArr", crawlingNameArr);
request.setAttribute("crawlingIdArr", crawlingIdArr);
request.setAttribute("crawlingArr", crawlingArr);
request.setAttribute("crawlingCodeArr", crawlingCodeArr);
%>
					<div class="row">
						<div class="col-lg-12">
						
							<form id="fn_inputFrm" method="post" action="naverCrawlingInfoActionProc.do?cmnx=${cmnx}">
							
								<div class="panel">
									<div class="panel-hdr">
										<h2>네이버 크롤링 설정</h2>
										<div class="paenl-tollbar">
											<div id="date-picker" class="input-date input-group">
												<c:set var="modelItem" value="sday" />
												<label for="${modelItem}" class="sr-only">색인일자</label>
												<input type="text" name="${modelItem}" id="${modelItem}" value="${yesterday}" class="form-control" style="width:100px;" autocomplete="off" />
												<div class="input-group-append">
													<div class="btn-group">
														<a href="#" class="btn4publish btn btn-primary">뉴스 가져오기</a>
														<a href="#" class="btn4submit btn btn-success">설정 ${_buttonSave_}</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="panel-container" role="content">
										<div class="paenl-content">
											<div class="row px-3 pt-3">
												<div class="col-lg-12">
												
													<div id="formDiv" class="form-row form-group">
													
														<c:forEach items="${crawlingNameArr}" var="crawlItem" varStatus="x">
														
															<div class="col-md-12 mb-3">
																<c:set var="modelItem" value="${crawlingIdArr[x.index]}" />
																<label class="form-label" for="${modelItem}">${crawlItem}</label>
																<button type="button" data-data="${modelItem}" class="btn4catall btn btn-xs btn-secondary">전체 선택/해제</button>
																<div class="form-group form-row">
																	<c:forEach items="${crawlingArr[x.index]}" var="item" varStatus="i">
																		
																		<c:set var="isCheck" value="${false}" />
																		<c:forEach items="${model[modelItem]['category']}" var="catItem" varStatus="j">
																			<c:if test="${catItem eq crawlingCodeArr[x.index][i.index]}">
																				<c:set var="isCheck" value="${true}" />
																			</c:if>
																		</c:forEach>
																		<div class="custom-control custom-checkbox ml-3">
																			<input type="checkbox" name="${modelItem}" id="${modelItem}${i.count}" value="${crawlingCodeArr[x.index][i.index]}" class="custom-control-input"<c:if test="${isCheck}"> checked</c:if>>
																			<label class="custom-control-label" for="${modelItem}${i.count}">${item}</label>
																		</div>
																	</c:forEach>
																</div>
															</div>
														
														</c:forEach>
														
														<hr class="w-100">
														
														<div class="col-md-12 my-3">
															<c:set var="modelItem" value="newsBody" />
															<label class="form-label" for="${modelItem}">뉴스 본문 영역</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 본문 영역 항목 정보를 입력하세요. ex) #main_content" required>
														</div>
														
														<div class="col-md-12 my-3">
															<c:set var="modelItem" value="newsBodyList" />
															<label class="form-label" for="${modelItem}">뉴스 본문 목록 영역</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 본문 목록 영역 항목 정보를 입력하세요. ex) .newsflash_body li dl" required>
														</div>
														
														<div class="col-md-12 my-3">
															<c:set var="modelItem" value="newsSubjectArea" />
															<label class="form-label" for="${modelItem}">뉴스 목록 제목 영역</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 본문 제목 영역 항목 정보를 입력하세요. ex) .newsflash_body li dl" required>
														</div>
														
														<div class="col-md-12 mb-3">
															<c:set var="modelItem" value="summary" />
															<label class="form-label" for="${modelItem}">뉴스 목록 내용 요약</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 내용 요약 항목 정보를 입력하세요. ex) .lede" required>
														</div>
														
														<%-- <div class="col-md-12 mb-3">
															<c:set var="modelItem" value="url" />
															<label class="form-label" for="${modelItem}">URL</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 URL 항목 정보를 입력하세요. ex) href" required>
														</div> --%>
	
														<div class="col-md-12 mb-3">
															<c:set var="modelItem" value="subject" />
															<label class="form-label" for="${modelItem}">뉴스 상세 - 제목</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 제목 항목 정보를 입력하세요. ex) .article_info #articleTitle" required>
														</div>
														
														<div class="col-md-12 mb-3">
															<c:set var="modelItem" value="publisher" />
															<label class="form-label" for="${modelItem}">뉴스 상세 - 언론사</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 언론사 항목 정보를 입력하세요. ex) .writing" required>
														</div>
	
														<div class="col-md-12 mb-3">
															<c:set var="modelItem" value="publishDate" />
															<label class="form-label" for="${modelItem}">뉴스 상세 - 게시일</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 게시일 항목 정보를 입력하세요. ex) .article_info .t11" required>
														</div>
	
														<div class="col-md-12 mb-3">
															<c:set var="modelItem" value="contents" />
															<label class="form-label" for="${modelItem}">뉴스 상세 - 내용</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 내용 항목 정보를 입력하세요. ex) #articleBodyContents" required>
														</div>
	
														<div class="col-md-12 mb-3">
															<c:set var="modelItem" value="paging" />
															<label class="form-label" for="${modelItem}">뉴스 목록 페이징</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 페이징 항목 정보를 입력하세요. ex) .paging" required>
														</div>
	
														<div class="col-md-12 mb-3">
															<c:set var="modelItem" value="except" />
															<label class="form-label" for="${modelItem}">크롤링 제외 항목 영역</label>
															<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" placeholder="뉴스 본문 크롤링 제외 영역 항목 정보를 입력하세요. ex) script, a, span em, ..." required>
														</div>
														
													</div>
												
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
							
						</div>
					</div>
<script>
<%-- 뉴스 카테고리 > 전체 선택/해제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4catall', function() {
	var $data = $(this).data('data');
	var $catItems = $('input[name=' + $data + ']'), $catItemsChkd = $('input[name=' + $data + ']:checked');
	if ($catItems.length == $catItemsChkd.length) $catItems.prop('checked', false);
	else $catItems.prop('checked', true);
});
<%-- 설정 저장 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4submit', function() {
	var $frm = $('#fn_inputFrm');
	$('#formDiv [type=checkbox]:checked').each(function(i, e) {
		var $inp = $('<input />', { type : 'hidden', name : e.name + 'Name', value : $(this).next().text(), 'class' : 'addCls'});
		$(this).parent().append($inp);
	});
	var $action= $frm.attr('action'), $data = $frm.serialize();
	fn_ajax( { url : $action, data : $data } );
	$('#formDiv .addCls').remove();
});
<%-- 뉴스 가져오기 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4publish', function() {
	if (!fn_checkFill($('#date-picker'), 'sday')) return false;
	else {
		var $sday = $('#sday').val();
		var $msg = '설정이 수정되었을 경우 먼저 설정 저장을 해주십시오.<br><br><b class="text-danger">' + $sday + '</b> 뉴스를 가져오시겠습니까?';
		fn_openSweetAlert( { icon : 'warning', title : '알림', msg : $msg, cancel : true, successFunc : function() {
			fn_setPageLoading(true);
			setTimeout(function() {
				fn_ajax( { url : 'naverNewsCrawlingProc.do?cmnx=${cmnx}', data : { sday : $sday }, type : 'POST' });
			}, 200);
		} });
	}
});
$(document).ready(function() {
	fn_setDateTimePicker( { obj : 'sday', format : 'Y-m-d', time : false, maxDate : 0 } );
});
</script>
<iframe id="targetIfrm" name="targetIfrm" style="width:0px;height:0px;" frameborder="0" title="iframe"></iframe>
<jsp:include page="${thisModelInfo.includeBottom}" />
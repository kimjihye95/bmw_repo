<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />
<link rel="stylesheet" media="screen, print" href="${resourcesPath}/js/codeMirror/css/codemirror.css">
<link rel="stylesheet" media="screen, print" href="${resourcesPath}/js/codeMirror/css/moxer.css">
<script src="${resourcesPath}/js/codeMirror/js/codemirror.js"></script>
<script src="${resourcesPath}/js/codeMirror/mode/javascript/javascript.js"></script>
<script src="${resourcesPath}/js/codeMirror/addon/edit/closebrackets.js"></script>
<script src="${resourcesPath}/js/codeMirror/addon/edit/matchbrackets.js"></script>
<script src="${cresourcesPath}/js/jquery.number.min.js"></script>

							<div class="panel">
								<div class="panel-hdr">
									<h2>인덱스 관리</h2>
									<div class="paenl-tollbar ml-2">
										<div class="btn-group btn-group-sm">
											<button type="button" data-toggle="modal" data-target="#dict-mng-popup" data-role="u" class="btn4dict btn btn-sm btn-dark">사전 관리</button>
											<button type="button" data-toggle="modal" data-target="#dict-mng-popup" data-role="s" class="btn4dict btn btn-sm btn-dark">불용어 관리</button>
											<button type="button" data-toggle="modal" data-target="#dict-mng-popup" data-role="n" class="btn4dict btn btn-sm btn-dark">동의어 관리</button>
										</div>
									</div>
									<div class="paenl-tollbar ml-2">
										<div class="btn-group btn-group-sm">
											<button type="button" data-role="estt" data-server="엘라스틱" data-status="시작" class="btn4cmd btn btn-sm btn-secondary">엘라스틱 시작</button>
											<button type="button" data-role="estp" data-server="엘라스틱" data-status="종료" class="btn4cmd btn btn-sm btn-secondary">엘라스틱 종료</button>
											<c:if test="${os eq 'Linux'}">
											<button type="button" data-role="lstt" data-server="LOGSTASH" data-status="시작" class="btn4cmd btn btn-sm btn-secondary">LOGSTASH 시작</button>
											<button type="button" data-role="lstp" data-server="LOGSTASH" data-status="종료" class="btn4cmd btn btn-sm btn-secondary">LOGSTASH 종료</button>
											</c:if>
										</div>
									</div>
									<div class="paenl-tollbar ml-2">
										<div class="btn-group btn-group-sm">
											<button type="button" class="btn4swagger btn btn-sm btn-dark">SWAGGER</button>
											<button type="button" data-toggle="modal" data-target="#index-rgst-popup" class="btn4add btn btn-sm btn-success">인덱스 추가</button>
											<button type="button" class="btn4refresh btn btn-sm btn-info">새로고침</button>
										</div>
									</div>
								</div>
								
								<div class="panel-container" role="content">
									<div class="panel-content">
										<form id="fn_delFrm" method="post" action="popupDelProc.do?cmnx=${cmnx}">
											<input type="hidden" name="idx" id="idx" />
											<table class="table table-hover">
												<caption></caption>
										
												<colgroup>
													<col />
													<col style="width:14%;" />
													<col style="width:10%;" />
													<col style="width:10%;" />
													<col style="width:8%;" />
													<col style="width:8%;" />
													<col style="width:8%;" />
													<col style="width:8%;" />
													<col style="width:8%;" />
													<col style="width:8%;" />
													<col style="width:8%;" />
												</colgroup>
	
												<thead class="thead-dark text-center">
													<tr>
														<th>인덱스명</th>
														<th>UUID</th>
														<th>데이터건수</th>
														<th>용량</th>
														<th>상태</th>
														<th>중지</th>
														<th>시작</th>
														<th>데이터</th>
														<th>엘라스틱</th>
														<th>LOGSTASH</th>
														<th>삭제</th>
													</tr>
												</thead>
	
												<tbody id="listTbody" class="text-center"></tbody>
											</table>
										</form>
									</div>
								</div>	
							</div>
					
							<%@ include file = "./inc/inc_list_modal.jsp" %>
<%@ include file = "./inc/inc_list_js.jsp" %>
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../../../../admresources/jsp/commonTop.jsp"%>
<jsp:include page="${resourcesPath}/jsp/logIn_top.jsp" />

					<div class="flex-1" style="background: url(${resourcesPath}/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover;">
					
						<div class="d-flex flex-column align-items-center justify-content-center" style="margin-top:120px;">
							<div class="alert alert-warning bg-white pt-6 pr-6 pb-6 pl-6" id="demo-alert">
								<h1 class="fs-xxl fw-700 color-fusion-500 d-flex align-items-center justify-content-center m-0">
									<img src="${resourcesPath}/img/common/logo.png" alt="BAMS A.I. Talk"><br />
								</h1>
								<h3 class="fw-500 mb-0 mt-3  text-right">
									지능형 응답 챗봇 v1.0<small>ADMINISTRATOR</small>
								</h3>				
												
								<div style="margin-top:30px;">
									<form id="fn_logInForm" method="post" action="${contextPath}/logIn/actionSecurityLogin.do">
										<input type="hidden" name="rx" value="${param.rx}" />
										<div class="form-group">
											<c:set var="modelItem" value="id" />
											<input type="text" name="${modelItem}" id="${modelItem}" class="form-control form-control-lg" placeholder="ID" value="" required>
											<div class="invalid-feedback">아이디를 입력하세요.</div>
										</div>
										<div class="form-group">
											<c:set var="modelItem" value="pwd" />
											<input type="password" name="${modelItem}" id="${modelItem}" class="form-control form-control-lg" placeholder="password" value="" required>
											<div class="invalid-feedback">비밀번호를 입력하세요.</div>
										</div>
										<div class="form-group text-right">
											<c:set var="modelItem" value="idSave" />
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="${modelItem}">
												<label class="custom-control-label" for="${modelItem}"> 아이디 저장</label>
											</div>
										</div>
										<div class="row no-gutters">
											<div class="col-lg-12 pl-lg-1 my-2">
												<button type="submit" id="submit" class="btn btn-warning btn-block btn-lg text-white"><i class="fal fa-lock-alt"></i> &nbsp; 로그인</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

<script>
<%-- 아이디 / 비밀번호 엔터키 이벤트 --%>
$(document).on('keypress', 'input[name=mid], input[name=mpwd]', function(e) {
	if (e.keyCode == 13) {
		$('#submitBtn').click();
		$(this).blur();
	}
});
$(document).on('click', '#submit', function(event) {
	var $frm = $('#fn_logInForm'), $id = $('#id').val(), $idSave = $('#idSave');
	<%-- Fetch form to apply custom Bootstrap validation --%>
	if ($frm[0].checkValidity() === false) {
		event.preventDefault();
		event.stopPropagation();

		$frm.addClass('was-validated');
	}
	else {
		if ($idSave.is(':checked')) {
			$.cookie(_comIdCookieName, $id, {expries : 7});
			$id = '';
		}
		else $.removeCookie(_comIdCookieName);

		var source = [ 'id', 'pwd' ];
		fn_encrypt($frm, source);
	}
});
$(document).ready(function() {
	 var $cookieValue = $.cookie(_comIdCookieName);
	  if (fn_isNull($cookieValue)) {
	  	$('#id').focus();
	  } else {
	    $('#id').val($cookieValue);
	    $('#pwd').focus();
	    $('#idSave').prop('checked', true);
	  }
	});
</script>
<jsp:include page="${resourcesPath}/jsp/logIn_bottom.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 데이터셋 -->
		<div class="modal fade" id="auth_dataset" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width:1000px;">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">데이터셋 찾기</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-3 bg-fusion-400 bg-fusion-gradient" style="padding-left:0px; padding-right:0px; min-height:70vh;">
								<div class="col-lg-12 pd20"  style="background:#111;">
									<div class="custom-control custom-checkbox">
										<c:set var="modelItem" value="ishide" />
										<input type="checkbox" id="${modelItem}" value="1" class="custom-control-input" checked>
										<label class="custom-control-label" for="${modelItem}">하위 분류 내 데이터 조회 안함</label>
									</div>
								</div>
								<div id="jstreeWrap" class="col-lg-12 pd20">
									<div id="jstree">
										<ul>
											<c:forEach items="${categoryList1}" var="rootCatModel" varStatus="x">
												<li data-data="${rootCatModel.categoryIdx}" data-role="${rootCatModel.orderNo}" data-lvl="${rootCatModel.categoryLevel}" data-path="${rootCatModel.categoryFullName}" class="jstree-open">${rootCatModel.categoryName}
													<ul>
														<c:forEach items="${categoryList2}" var="catModel" varStatus="i">
															<li data-data="${catModel.categoryIdx}" data-role="${catModel.orderNo}" data-lvl="${catModel.categoryLevel}" data-path="${catModel.categoryFullName}"<c:if test="${i.first}"> class="jstree-open"</c:if>>${catModel.categoryName}
															
															<c:set var="subCatCnt" value="1" />
															<c:forEach items="${categoryList3}" var="subCatModel" varStatus="j">
																<c:if test="${catModel.categoryIdx eq subCatModel.parentCategoryIdx}">
																	<c:if test="${subCatCnt eq 1}">
																	<ul>
																	</c:if>
																		<li data-data="${subCatModel.categoryIdx}" data-role="${subCatModel.orderNo}" data-lvl="${subCatModel.categoryLevel}" data-path="${subCatModel.categoryFullName}">${subCatModel.categoryName}</li>
																	<c:if test="${subCatModel.categoryCnt eq subCatCnt}">
																	</ul>
																	</c:if>
																	<c:set var="subCatCnt" value="${subCatCnt + 1}" />
																</c:if>
															</c:forEach>
															
														</c:forEach>
													</ul>
												</li>
											</c:forEach>
										</ul>
									</div>

								</div>
							</div>
							<div class="col-lg-9" style="padding:0 20px 20px 20px;">
								<div class="alert alert-primary alert-dismissible">
									<form id="fn_searchFrm" action="datasetListAjax.do?cmnx=${cmnx}" target="targetIfrm">
										<input type="hidden" name="cmnx" value="${cmnx}" />
										<input type="hidden" name="idx" value="" />
										<input type="hidden" name="slvl" value="" />
										<input type="hidden" name="slmt" value="" />
										<input type="hidden" name="q" value="" />
										<div class="row">
											<div class="col-sm-3">
												<c:set var="modelItem" value="stype" />
												<select name="${modelItem}" id="${modelItem}" class="form-control">
													<option value="">전체</option>
													<option value="${efn:encrypt('n')}">데이터셋 명</option>
													<option value="${efn:encrypt('r')}">등록일</option>
												</select>
											</div>
											<div class="col-sm-9">
												<div class="input-group">
													<c:set var="modelItem" value="skey" />
													<input type="text" name="${modelItem}" id="${modelItem}" class="form-control" placeholder="검색어를 입력하세요.">
													<div class="input-group-append">
														<button type="button" class="btn4search btn btn-warning text-white waves-effect waves-themed"><i class="fal fa-search"></i></button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>

								<div class="row">
									<div class="col-sm-6 justify-content-start">
										<p class="mgb0" style="line-height:38px;">Total. <span id="datasetCount" class="text-warning"></span></p>
									</div>
									<div class="col-sm-6 justify-content-end">
										<div class="dt-buttons" style="text-align:right;">											
											<div class="btn-group mgl10">
												<c:set var="modelItem" value="slmt" />
												<select name="${modelItem}" id="${modelItem}" class="form-control">
													<option value="${efn:encrypt(10)}" selected>10건씩 조회</option>
													<option value="${efn:encrypt(20)}">20건씩 조회</option>
													<option value="${efn:encrypt(30)}">30건씩 조회</option>
												</select>
											</div> 
											<button class="btn4list btn btn-outline-default" type="button"><span>변경</span></button> 
										</div>
									</div>
								</div>

								<div class="mgt20">
									<form id="fn_delFrm" action="datasetDelProc.do?cmnx=${cmnx}" class="col-sm-12">
										<table class="table table-striped m-0 tbListA">
											<colgroup>
												<col style="width:5%" />
												<col style="width:6%" />
												<col style="width:12%" />
												<col />
												<col style="width:14%" />
												<col style="width:14%" />
											</colgroup>
											<thead class="bg-warning-900 text-white">
												<tr>
													<th></th>
													<th>No</th>
													<th>분류</th>
													<th>데이터셋 명</th>
													<th>등록</th>
													<th>최종수정</th>
												</tr>
											</thead>
											<tbody id="datasetTbody" class="alignC"></tbody>
										</table>
									</form>
								</div>
								
								<div class="mgt30" style="text-align:center;">
									<div id="pagingDataset"></div>
								</div>

							</div>
						</div>
						<div class="modal-footer inno pdt0">
							<button type="button" class="btn4datasetAdd btn btn-lg btn-warning text-white waves-effect waves-themed">선택</button>
							<button type="button" class="btn btn-lg btn-secondary waves-effect waves-themed" data-dismiss="modal">취소</button>
						</div>
					</div>
					<div class="modal-footer pdt0">
					</div>
				</div>
			</div>
		</div>
<script>
var $jstree, $jstreeId, $searchFrm = $('#fn_searchFrm'), $code = new Object();
<%-- 하위 분류 내 데이터조회안함 클릭 이벤트 --%>
$(document).on('click', '#ishide', function() {
	$jstreeId.find('a')[0].click();
});
<%-- 검색 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4search', function() {
	if ($searchFrm[0].checkValidity() === false) {
		event.preventDefault();
		event.stopPropagation();
		$searchFrm.addClass('was-validated');
	}
	else {
		fn_ajax( { url : $searchFrm.attr('action'), data : $searchFrm.serialize() } );
	}
});
<%-- 검색어 엔터키 이벤트 --%>
$(document).on('keypress', '#skey', function(e) {
	if (e.keyCode == 13) {
		$('.btn4search').click();
		return false;
	}
});
<%-- 리스트 카운트 적용 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4list', function() {
	$searchFrm.find('[name=slmt]').val($('select[name=slmt]').val());
	$('.btn4search').click();
	return false;
});
<%-- 분류 선택 이벤트 --%>
$(document).on('click', 'a.jstree-anchor', function() {
	$jstreeId = $('#' + ($(this).attr('id')).replace('_anchor', ''));
	var $data = $($jstreeId).data('data'), $role = $($jstreeId).data('role'), $lvl = $($jstreeId).data('lvl'), $path = $($jstreeId).data('path'), $hide = $('#ishide:checked').val();
	$searchFrm.find('input[name=idx]').val($data);
	$searchFrm.find('input[name=slvl]').val($lvl);
	$searchFrm.find('input[name=q]').val($hide);

	fn_ajax( { url : $searchFrm.attr('action'), data : $searchFrm.serialize() } );
});
<%-- 데이터셋 찾기 모달 - 선택 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4datasetAdd', function() {
	var $datasetCboxs = $('input[name=datasetCboxs]:checked').not(':disabled');
	if ($datasetCboxs.length == 0) {
		fn_openSweetAlert( { title : '알림', msg : '데이터셋 항목을 선택해 주십시오.', icon : 'warning' } );
		return false;
	}
	$datasetCboxs.each(function(i, e) {
		var $_idx = $(e).val(), $_nav = $(e).data('nav'), $isSkip = false;
		var $tag = '<p><span class="dset-item">' + $_nav + '<input type="hidden" name="datasetIdx" value="' + $_idx + '" /><span class="btn4datasetRemove"> ×</span></span></p>';
		$($datasetListDiv).find('.dset-item').each(function(j, m) {
			if ($(m).find('input[name=datasetIdx]').val() == $_idx) $isSkip = true;
		});
		if (!$isSkip) $($datasetListDiv).append($tag);
		$($datasetListDiv).find('.dset-fst').remove();
	});
	<%-- 데이터셋 추가 등록 여부 확인 알림창 오픈--%>
	var $success = function() { return false; }, $dismiss = function() { $('#auth_dataset').modal('hide'); };
	fn_openSweetAlert( { title : '알림', msg : '추가로 등록하시겠습니까?', icon : 'warning', cancel : true, success : $success, dismiss : $dismiss } );
});
<%-- 데이터셋 접근 권한 - 항목 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4datasetRemove', function() {
	$(this).parents('p')[0].remove();
	if ($($datasetListDiv).children().length == 0) $($datasetListDiv).html('<span class="dset-item dset-fst"><input type="hidden" name="datasetIdx" value="${efn:encrypt(0)}" />모든 데이터셋</span>');
	$('input[name=datasetIsOpen]:checked').click();
});
<%-- 데이터셋 항목 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4item', function() {
	var $didx = $(this).data('data'), $cidx = $(this).data('cat');
	var $action = 'datasetItemListAjax.do?cmnx=${cmnx}';
	
	$(this).addClass('on').siblings('.btn4item').removeClass('on');
});
$(document).ready(function() {
	$jstree = $('#jstree').bind('ready.jstree', function() { $('a.jstree-anchor:eq(0)').click(); }).jstree({ 'core' : { 'check_callback' : true }, 'plugins' : [ 'types', 'dnd' ], 'types' : { 'default' : { 'icon' : 'fal fa-folder' } } });
	fn_ajax( { url : 'codeInfoAjax.do?cmnx=${cmnx}', dataType : 'json',  success : function(data) { try {$code = data} catch (e) {}}  } );
});

</script>
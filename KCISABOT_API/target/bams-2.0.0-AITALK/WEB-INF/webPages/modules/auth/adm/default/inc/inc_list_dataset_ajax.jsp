<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '', $didxArr = new Array();

$('.dataset-list input[name=datasetIdx]').each(function(i, e) {
	var $didx = $(this).val();
	if ($didx != 0) $didxArr.push(fn_endata($(this).val(), 1));
});
<c:set var="URL_PAGE_LIST" value="${fn:replace(URL_PAGE_LIST, 'list.do', 'datasetListAjax.do')}" />
<c:if test="${empty list}">
varSource += '<tr><td colspan="7">${_infoNoDataMsg_}</td></tr>';
</c:if>

<c:set var="listNo" value="${listStartNo}" />
<c:forEach items="${list}" var="model" varStatus="i">
	var $isChk = $.inArray('${model.datasetIdx}', $didxArr) > -1;
varSource += '<tr>';
varSource += '<th scope="row">';
varSource += '<div class="custom-control custom-checkbox">';
//varSource += '<input type="checkbox" name="datasetCboxs" id="datasetCboxs${model.datasetIdx}" value="${model.datasetIdx}" data-nav="${model.category} > ${model.datasetName}" class="custom-control-input">';
varSource += '<input type="checkbox" name="datasetCboxs" id="datasetCboxs${i.count}" value="${efn:encrypt(model.datasetIdx)}" data-nav="${model.category} > ${model.datasetName}"' + ($isChk ? ' checked disabled' : '') + ' class="custom-control-input">';
varSource += '<label class="custom-control-label" for="datasetCboxs${i.count}"></label>';
varSource += '</div>';
varSource += '</th>';
varSource += '<td>${listNo}</td>';
varSource += '<td>${model.category}</td>';
//varSource += '<td><a href="#" id="dataset${model.datasetIdx}" data-data="${model.datasetIdx}" data-dismiss="modal">${model.datasetName}</a></td>';
varSource += '<td><span class="text-point">${model.datasetName}</span></td>';
varSource += '<td>${model.regiName} (${fn:substring(model.regiDate, 0, 10)})</td>';
varSource += '<td><c:if test="${!empty model.modiDate}">${model.modiName} (${fn:substring(model.modiDate, 0, 10)})</c:if></td>';
varSource += '</tr>';
<c:set var="listNo" value="${listNo - 1}" />
</c:forEach>

$('#datasetCount').text('${count}건');
$('#datasetTbody').html(varSource);

// set paging
$isLoad = true;
var $pagingId = 'pagingDataset';
<c:if test="${empty param.page}">
if (!fn_isNull( $('#' + $pagingId).data("twbs-pagination") )) $('#' + $pagingId).twbsPagination('destroy');
var varOpt = {obj : $pagingId, action : '${URL_PAGE_LIST}', idx : '${param.idx}', total : ${pageTotalPage}, visible : ${blockSize}};
fn_setPaging(varOpt);
varOpt = null;
</c:if>
$isLoad = false;

varSource = null;
$pagingId = null;
$didxArr = null;
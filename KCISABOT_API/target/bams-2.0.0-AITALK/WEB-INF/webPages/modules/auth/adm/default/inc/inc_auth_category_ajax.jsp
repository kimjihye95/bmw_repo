<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '';

<c:set var="modelItem" value="categoryIdx" />
varSource += '<select class="select2 form-control" multiple="multiple" id="${modelItem}" name="${modelItem}">';
	<c:forEach items="${categoryList}" var="categoryModel" varStatus="i">
		<c:if test="${!i.first}">
	<c:set var="categoryTarget" value="|${categoryModel.categoryIdx}|" />
varSource += '<option value="${efn:encrypt(categoryModel.categoryIdx)}"<c:if test="${fn:indexOf(authCategory, categoryTarget) ge 0}"> selected</c:if>>${categoryModel.categoryName}</option>';
		</c:if>
	</c:forEach>
varSource += '</select>';

$('#regCatDiv').html(varSource);
$('.select2').select2();

varSource = null;
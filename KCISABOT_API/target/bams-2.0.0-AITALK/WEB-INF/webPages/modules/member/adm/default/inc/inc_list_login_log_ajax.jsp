<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var $source = '';

<c:set var="URL_PAGE_LIST" value="${fn:replace(URL_PAGE_LIST, 'list.do', 'memberLoglistAjax.do')}" />
<c:if test="${empty list}">
$source += '<tr><td colspan="3">${_infoNoDataMsg_}</td></tr>';
</c:if>

<c:set var="listNo" value="${listStartNo}" />
<c:forEach items="${list}" var="model" varStatus="i">
	<c:choose>
		<c:when test="${type eq 1}">
$source += '<tr>';
$source += '<td>${listNo}</td>';
$source += '<td>${model.loginDate}</td>';
$source += '<td>${model.statusName}</td>';
$source += '</tr>';
		</c:when>
		
		<c:otherwise>
$source += '<tr>';
$source += '<td>${listNo}</td>';
$source += '<td>( ID : ${efn:getMaskData(model.memberId, 1)} ) [${efn:getMaskData(model.memberName, 1)} ] ${model.act}</td>';
$source += '<td>${model.regiDate}</td>';
$source += '</tr>';
		</c:otherwise>
	</c:choose>
<c:set var="listNo" value="${listNo - 1}" />
</c:forEach>

$('#${type eq 1 ? 'loginLogCount' : 'userLogCount'}').text('${count}건');
$('#${type eq 1 ? 'loginLogTbody' : 'userLogTbody'}').html($source);

// set paging
$isLoad = true;
var $pagingId = '${type eq 1 ? 'loginLogPaging' : 'userLogPaging'}';
<c:if test="${empty param.lpage}">
if (!fn_isNull( $('#' + $pagingId).data("twbs-pagination") )) $('#' + $pagingId).twbsPagination('destroy');
var varOpt = {obj : $pagingId, action : '${URL_PAGE_LIST}', param : '&q=${param.q}', idx : '${param.idx}', pageName : 'lpage', total : ${pageTotalPage}, visible : ${blockSize}};
fn_setPaging(varOpt);
varOpt = null;
</c:if>
$isLoad = false;

$('#${type eq 1 ? 'memberLoginLogModal' : 'memberUserLogModal'}').modal();
try { $('.input-daterange').datepicker('destroy'); } catch(e) {}
$('.input-daterange').datepicker({ keyboardNavigation: false, forceParse: false, autoclose: true, format: 'yyyy-mm-dd', language: 'kr', endDate : '+0d' });
$('#skey2').datepicker('destroy');

$source = null;
$pagingId = null;

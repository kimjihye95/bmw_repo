<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
		<%-- 사용자 등록 모달 start --%>
		<div class="modal fade" id="memberRegModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">사용자 추가</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_inputFrm" method="post" action="memberActionProc.do?cmnx=${cmnx}">
							<table class="tbWriteA mgt20">
								<caption>사용자 추가 입력입니다.</caption>
								<colgroup>
									<col width="15%" />
									<col width="35%" />
									<col width="15%" />
									<col width="35%" />
								</colgroup>
	
								<tbody class="pd10 alignC">
									<tr>
										<c:set var="modelItem" value="memberId" />
										<th scope="row"><label for="${modelItem}">아이디</label> <span>*</span></th>
										<td colspan="3">
											<div class="input-group">
												<input type="text" name="${modelItem}" id="${modelItem}" class="form-control" required>
												<div class="input-group-append">
													<button type="button" data-role="1" class="btn4valid btn btn-dark text-white waves-effect waves-themed">중복확인</button>
												</div>
												<div class="invalid-feedback id-invalid"></div>
											</div>
										</td>
									<tr>
									<tr>
										<c:set var="modelItem" value="memberName" />
										<th scope="row"><label for="${modelItem}">이름</label> <span>*</span></th>
										<td colspan="3"><input type="text" name="${modelItem}" id="${modelItem}" class="form-control" required></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="memberPwd" />
										<th scope="row"><label for="${modelItem}">비밀번호</label> <span>*</span></th>
										<td><input type="password" name="${modelItem}" id="${modelItem}" class="form-control" placeholder="${_comUserRegistFrmPwdMsg_}" required></td>
										<c:set var="modelItem" value="memberRePwd" />
										<th scope="row"><label for="${modelItem}">비밀번호 확인</label> <span>*</span></th>
										<td><input type="password" name="${modelItem}" id="${modelItem}" class="form-control" placeholder="" required></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="authIdx" />
										<th scope="row"><label for="${modelItem}">권한</label> <span>*</span></th>
										<td>
											<select name="${modelItem}" id="${modelItem}" class="form-control" required>
												<option value="">권한 선택</option>
												<c:forEach items="${authList}" var="authModel" varStatus="i">
													<option value="${efn:encrypt(authModel.authIdx)}">${efn:strContentsFormat(authModel.authName, '')}</option>
												</c:forEach>
											</select>
										</td>
										<c:set var="modelItem" value="department" />
										<th scope="row"><label for="${modelItem}">소속</label> <span>*</span></th>
										<td><input type="text" name="${modelItem}" id="${modelItem}" class="form-control" required></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="mobile" />
										<th scope="row"><label for="${modelItem}">연락처</label> <span>*</span></th>
										<td><input type="tel" name="${modelItem}" id="${modelItem}" placeholder="010-1234-5678" pattern="[0-9]{3}-[0-9]{3,4}-[0-9]{4}" class="form-control" required></td>
										<c:set var="modelItem" value="email" />
										<th scope="row"><label for="${modelItem}">이메일</label> <span>*</span></th>
										<td><input type="email" name="${modelItem}" id="${modelItem}" class="form-control" required></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="etc" />
										<th scope="row"><label for="${modelItem}">비고 </label></th>
										<td colspan="3"><textarea name="${modelItem}" id="${modelItem}" class="form-control" rows="5"></textarea></td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="modal-footer inno pdt0">
						<button type="button" class="btn4submit btn btn-warning text-white">등록</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		<%-- //사용자 등록 모달 end --%>
<script>
var $memRegFrm = $('#fn_inputFrm');
<%-- 아이디 중복확인 & 초기화 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4valid', function() {
	var $role = $(this).data('role');
	var $id = $memRegFrm.find('#memberId').val();

	if ($role == 1) {		<%-- 중복확인 버튼 이벤트 --%>
		if (!fn_checkFill($memRegFrm, 'memberId')) return false;
		else if (/[^(0-9a-zA-Z]/gi.test($id)) {
			fn_openSweetAlert( { title : '알림', msg : '영문, 숫자 만 입력이 가능합니다.', icon : 'warning' } );
			return false;
		}
		else if (!/^(?=[0-9a-zA-Z]{4,12}$)/.test($id)) {
			fn_openSweetAlert( { title : '알림', msg : '아이디는 4 ~ 12자 입력이 가능합니다.', icon : 'warning' } );
			return false;
		}
		else {
			var $action = 'searchId.do?cmnx=${cmnx}';
			var $data = $memRegFrm.serialize();
			var varOpt = { url : $action, data : $data, type : 'POST' };
			fn_ajax(varOpt);
		}
	}
	else {		<%-- 초기화 버튼 이벤트 --%>
		$('#memberId').prop('readonly', false).css('background-color', '#fff').val('').focus();
		$(this).data('role', 1).text('중복확인');
		$('.id-invalid').text('').addClass('invalid-feedback').removeClass('valid-feedback').hide();
	}
	return false;
});
<%-- 회원 등록 --%>
$(document).on('click', '.btn4submit', function() {
	<%-- 아이디 중복확인 버튼 클릭 여부 체크 --%>
	if($('.btn4valid').data('role') == 1 || $('#memberId').css('background-color') == '#fff') {
		fn_openSweetAlert( { title : '알림', msg : '${_comChkDupMsg_}', icon : 'info' } );
		return false;
	}
	
	if ($memRegFrm.valid()) {
		var $success = function() {
		 	var $action = 'memberActionProc.do?cmnx=${cmnx}';
			var source = [ 'memberPwd', 'memberRePwd' ];
			fn_encrypt($memRegFrm, source);
		 	var $data = $memRegFrm.serialize();
		 	fn_ajax({ url : $action, data : $data, type : 'POST' });
		}
		var $opt = { title : '알림', msg : '${_commonRegistMsg_}', icon : 'info', cancel : true, success : $success };
		fn_openSweetAlert($opt);
	}
	
	return false;
});
<%-- 회원 등록 모달 닫기 이벤트 --%>
$(document).on('hidden.bs.modal', '#memberRegModal', function() {
	$memRegFrm[0].reset();
	$('.btn4valid').data('role', 2).click();
	$memRegFrm.removeClass('was-validated');
	$memRegFrm.find('label.error').remove();
});
<%-- 회원 추가 validator --%>
$memRegFrm.validate({
	rules: {
		memberId : { required: true, minlength : 4, maxlength : 12 },
		memberName: { required: true, korEng: true },
		memberPwd: { required: true, pwdChk : true, minlength : 8, maxlength : 12 },
		memberRePwd: { required: true, equalTo: '#memberPwd' },
		authIdx : { required: true },
		department : { required: true, numKorEngSpc : true },
		mobile : { required: true, mobile : true },
		email : { required: true, email : true },
	},
	messages: {
		memberPwd: { required: fn_validatorCheckFill($memRegFrm, 'memberPwd'), pwdChk : '${_comUserRegistFrmPwdMsg_}' },
		memberRePwd: { equalTo: "${_comUserRegistFrmRepwdMsg_}" },
	},
	invalidHandler: function (form, validator) {
		var errors = validator.numberOfInvalids();
		if (errors) validator.errorList[0].element.focus();
	}
});
</script>

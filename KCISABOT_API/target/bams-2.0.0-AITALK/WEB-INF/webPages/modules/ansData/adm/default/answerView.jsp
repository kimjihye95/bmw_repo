<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../cresources/jsp/commonTop.jsp" %>
<style>
.btnClose { top:12px !important; }
table.scroll { width: 100%; border-spacing: 0; }
table.scroll tbody,
table.scroll thead { width:100%; display: block; }
thead tr th { height: 30px; line-height: 30px; }
table.scroll tbody { overflow-y: auto; overflow-x: hidden; }
</style>
<%-- <jsp:include page="${resourcesPath}/jsp/top.jsp" /> --%>
						<div class="answer_view panel" style="display:block;margin-bottom:0px;">
							<div class="row">
								<div class="col-lg-12">
									<div class="headArea">
										<p class="mgb5">${path}</p>
										<h5 class="card-title">${title}</h5>
										<div class="btnClose text-right">
											<button type="button" class="btn btn-dark waves-effect waves-themed answer_hide"><i class="fal fa-times text-warning"></i> &nbsp; 닫기</button>
										</div>
									</div>
									<div class="alert alert-primary alert-dismissible">
										<form id="fn_ansSearchFrm" action="answerViewAjax.do" method="POST">
											<div style="padding-left:20px;" class="mgb5">
												<input type="hidden" name="cmnx" value="${cmnx }"/>
												<input type="hidden" name="slmt" value=""/>
												<%-- <input type="hidden" name="categoryIdx" value="${categoryIdx }"/>
												<input type="hidden" name="datasetIdx" value="${datasetIdx }"/> --%>
												<c:forEach items="${datasetItemList}" var="ansModel" varStatus="i">
													<div class="custom-control custom-checkbox custom-control-inline">
														<input type="checkbox" class="custom-control-input" id="${ansModel.itemColName}${i.count}" name="schk" value="${ansModel.itemColName }">
														<label class="custom-control-label" for="${ansModel.itemColName}${i.count}">${ansModel.itemName}</label>
													</div>
												</c:forEach>
											</div>
											<div class="row mgt10">
												<div class="col-lg-12">
													<div class="input-group">
														<input type="text" name="skey" class="form-control" placeholder="검색어를 입력하세요." aria-label="Recipient's username" aria-describedby="view-button-addon5">
														<div class="input-group-append">
															<button class="btn btn-warning text-white waves-effect waves-themed" type="button" id="view-button-addon5"><i class="fal fa-search"></i></button>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>

									<div class="row">
										<div class="col-lg-6 justify-content-start">
											<p class="mgb0" style="line-height:38px;">Total. <span class="text-warning" id="totalCount">${count}건</span></p>
										</div>
										<div class="col-lg-6 justify-content-end">
											<div class="dt-buttons" style="text-align:right;">
												<button type="button" class="btn btn-warning waves-effect waves-themed text-white direct-add" data-toggle="modal">직접등록</button>
												<button type="button" class="btn4excelIn btn btn-success waves-effect waves-themed" data-toggle="modal">엑셀등록</button>
												<button type="button" class="btn4excelOut btn btn-info waves-effect waves-themed">내보내기</button>
												<button type="button" class="btn4datasetDelete btn btn-dark waves-effect waves-themed">삭제</button>
												<div class="btn-group mgl10">
													<select class="form-control" id="example-select">
														<option value="10" selected>10건씩 조회</option>
														<option value="20">20건씩 조회</option>
														<option value="30">30건씩 조회</option>
													</select>
												</div> 
												<button class="btn btn-outline-default" id="select4count" type="button"><span>변경</span></button> 
											</div>
										</div>
									</div>

									<div class="mgt20" style="min-height:500px;">
										<table class="table table-striped border-bottom">
											<thead class="bg-warning-900 text-white col-head text-center">
												<tr>
													<th style="width:50px;"></th>
													<th style="width:100px;" id="rnum">No</th>
													
													<c:forEach items="${datasetItemList}" var="ansModel" varStatus="i">
														<th class="column-name" id="${ansModel.itemColName }">${ansModel.itemName}</th>
													</c:forEach>
													
													<!-- <th id="description">비고</th> -->
													<th style="width:100px;" id="regiDate">등록</th>

													<th style="width:100px;" id="modiDate">최종수정</th>
													<th style="width:100px;">관리</th>
												</tr>
											</thead>
											<tbody class="text-center col-tbody"></tbody>
										</table>
									</div>
									
									<div class="mgt30" style="text-align:center;">
										<div id="paging-view"></div>
									</div>
								</div>
							</div>
						</div>
						<!-- 등록 모달 -->
						<%@ include file = "./inc/inc_modal.jsp" %>
						
						<!-- 엑셀 등록 모달 -->
						<%@ include file = "./inc/inc_modal_excel.jsp" %>
				
<script>
var $modifyVal;
var $idx;
var $ansSearchFrm = $('#fn_ansSearchFrm');
//var $ansDataCnt = 0, $itemCnt = ${empty datasetItemList ? 0 : fn:length(datasetItemList)};
//const $categoryIdx = "${categoryIdx}";
//const $datasetIdx = "${datasetIdx}";
$(document).ready(function(){
	$('#view-button-addon5').click();
})

<%-- 검색 버튼 클릭 이벤트 --%>
$(document).off('click', '#view-button-addon5').on('click', '#view-button-addon5', function(){
	var $action = $ansSearchFrm.attr('action');
	var $data = $ansSearchFrm.serialize();
	fn_ajax({ url : $action, data : $data});
});

<%-- 조회 건수 변경 이벤트 --%>
$(document).off('click', '#select4count').on('click', '#select4count', function(){
	$('input[name=slmt]').val($('#example-select').val());
	$('#view-button-addon5').click();
})

<%-- 엑셀등록 --%>
$(document).on('click', '.btn4excelIn', function() {
	if ($itemCnt > 0) {
		$('#excelListTable').html('');
		$('.excel-count').html('');
		$('.danger-count').html('');
		$('#fileUpload').val('');
		$('#excel_make').css('zIndex', '2060');
		$('.modal-backdrop').css('z-index', '2051');
		$('#excel_make').modal();
	}
	else {
		var $opt = { title : '알림', msg : '${title} 데이터셋 항목을 생성해 주시기 바랍니다.', icon : 'warning' };
		fn_openSweetAlert($opt);
	}
});
<%-- 엑셀 등록 모달 - 닫기 버튼 클릭 이벤트 --%>
$(document).off('click', '.btn4addExcelClose').on('click', '.btn4addExcelClose', function() {
	$('.modal-backdrop').css('z-index', '2040');
});
<%-- 엑셀 내보내기 --%>
$(document).off('click', '.btn4excelOut').on('click', '.btn4excelOut', function() {
	if ($ansDataCnt > 0) {
		var $colArr = new Array();
		var $keyArr = new Array();
		var $list = new Array();
		var $head = $('.col-head').find('.column-name');
		var $body = $('.col-tbody').children('.column');
		
		$($head).each(function(i, e){
				$colArr.push($(e).text());
				$keyArr.push($(e).attr("id"));
		});
		
		//fn_ajax({url : 'exportExcel.do', data : {datasetIdx : $datasetIdx, categoryIdx : $categoryIdx, key : JSON.stringify($keyArr), column :  JSON.stringify($colArr), cmnx : 1, datasetName : '${title}'}});
		fn_ajax({url : 'exportExcel.do', data : {key : JSON.stringify($keyArr), column :  JSON.stringify($colArr), cmnx : 1, datasetName : '${title}'}});
	}
	else {
		var $opt = { title : '알림', msg : '${_infoNoDataMsg_}', icon : 'warning' };
		fn_openSweetAlert($opt);
	}
});

<%-- 삭제 --%>
$(document).off('click', '.btn4datasetDelete').on('click', '.btn4datasetDelete', function() {
	var idxArr = new Array();
	var idxStr;
	
	if (!fn_checkCboxChecked('idx-chk')) return false;

	var $success = function() {
		$('input[name=idx-chk]:checked').each(function(i, e){
			var idxObj = new Object();
			idxObj.idx = $(e).val();
			idxArr.push(idxObj);
		});
		idxStr = JSON.stringify(idxArr);
		
		//fn_ajax( { url : "delete.do?cmnx=${cmnx}", data : { datasetIdx : $datasetIdx, inputData : idxStr}, type : 'POST'} );
		fn_ajax( { url : "delete.do?cmnx=${cmnx}", data : { inputData : idxStr}, type : 'POST'} );
	}
	
	var $opt = { title : '알림', msg : '${_commonDBDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
});

<%-- 직접등록 모달 오픈 --%>
$(document).off('click', '.direct-add').on('click', '.direct-add', function(){
	if ($itemCnt > 0) {
		$('#answer_make').find('.column-input').children('input').val('');
		$('#answer_make').modal();
	}
	else {
		var $opt = { title : '알림', msg : '${title} 데이터셋 항목을 생성해 주시기 바랍니다.', icon : 'warning' };
		fn_openSweetAlert($opt);
	}
})

<%-- 수정 input 열기 --%>
$(document).off('click', '.set-modify').on('click', '.set-modify', function(){
	var $column = $(this).parent().siblings('.column');
	var $description = $(this).parent().siblings('.description');
	var $input = $('.modi-direct-input');
	$modifyVal = new Array();
	
	$($column).each(function(i ,e){
		$($input.eq(i)).val($(e).text());
		//$(id).val($(e).text());
	})
	$modifyFrm.find('#ansDescription').val($description.text());
	$modifyFrm.find('input[name=idx]').val($(this).data('idx'));
	
})
<%-- 파일첨부 버튼 클릭 이벤트 --%>
$(document).off('click', '#button-attach').on('click', '#button-attach', function(){
	$('#fileUpload').click();
})

<%-- 파일첨부 --%>
$(document).off('change', '#fileUpload').on('change', '#fileUpload', function() {
	
	var formData = new FormData($('#frm')[0]);
	var uploadFile = $("input[name=fileUpload]").val();
	
	if(uploadFile == null || uploadFile == "") return;
	
	if(uploadFile != ""){ 
		
		var reg = /(.*?)\.(xlsx|xls)$/;
		
		if(uploadFile.match(reg)) {
			//alert("엑셀 파일입니다.");
		} else {
			//alert("엑셀 파일이어야 합니다.");
			return;
		}
	}
	
	$.ajax({
		type: "POST"
		,url: "excelUpload.do?cmnx=${cmnx}"
		,dataType : "script"
		,enctype: 'multipart/form-data' // 필수
		,data: formData // 필수
		,processData: false // 필수 
		,contentType: false // 필수
		,success: function(data) {
			if(data.result){
				alert("인물 등록이 완료 되었습니다.\n 사업참가이력 등록을 위한 추가 페이지로 넘어갑니다.");
				//$('#hd_personId').val(data.personId);
				//location.href=CommonUtil.getContextRoot()+'/networkingmap/registerHistory.do?p_id='+$('#hd_personId').val();
			}
		}
		,error: function(request,status,error) {
			var option = new Object();
			option.title = '실패';
			option.msg = '엑셀 데이터 불러오기를 실패했습니다.';
			option.icon = 'error';
			option.okMsg = '확인';
			fn_openSweetAlert(option);
		}
	});
});

<%-- 검색어 엔터키 이벤트 --%>
$(document).off('keypress', 'input[name=skey]').on('keypress', 'input[name=skey]', function(e) {
	if (e.keyCode == 13) {
		$('#view-button-addon5').click();
		return false;
	}
});

</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
<%pageContext.setAttribute("replaceChar", "\r\n"); %>
var varTheadSource = '', varSource = '', varCBoxSource = '';
var $ansDataCnt = ${count}, $itemCnt = ${empty datasetItemList ? 0 : fn:length(datasetItemList)};

<%-- THEAD 셋팅 --%>
varTheadSource += '<th style="width:50px;"></th>';
varTheadSource += '<th style="width:100px;" id="rnum">No</th>';
<c:forEach items="${datasetItemList}" var="ansModel" varStatus="i">
varTheadSource += '<th class="column-name" id="${ansModel.itemColName}">${efn:strContentsFormat(ansModel.itemName, '')}</th>';
</c:forEach>
varTheadSource += '<th style="width:100px;" id="regiDate">등록</th>';
varTheadSource += '<th style="width:100px;" id="modiDate">최종수정</th>';
varTheadSource += '<th style="width:100px;">관리</th>';
$('.col-head').html(varTheadSource);

<%-- 리스트 셋팅 --%>
varSource += '<c:if test="${empty categoryDatasetTableList}">';
varSource += '<tr>';
varSource += '	<td colspan="${fn:length(datasetItemList) + 5}">${_infoNoDataMsg_}</td>';
varSource += '</tr>';
varSource += '</c:if>';
varSource += '<c:forEach items="${categoryDatasetTableList}" var="ansModel" varStatus="i">'
varSource += '								<tr>';
varSource += '									<td>';
varSource += '										<div class="custom-control custom-checkbox">';
varSource += '											<input type="checkbox" class="custom-control-input" name="idx-chk" id="ck${i.count }" value="${efn:encrypt(ansModel.idx)}">';
varSource += '											<label class="custom-control-label" for="ck${i.count }"></label>';
varSource += '										</div>';
varSource += '									</td>';
varSource += '									<td><fmt:parseNumber var="num" integerOnly="true" value="${count - ansModel.rnum + 1}"/>${num }</td>';
varSource += '									<c:forEach var="item" items="${datasetItemList }" varStatus="status">';
varSource += '								 	<td class="column">${efn:strContentsFormat(ansModel[item.itemColName], '<br/>')}</td>';
//varSource += '								 	<td class="column">${fn:replace(ansModel[item.itemColName], replaceChar, "<br/>")}</td>';
varSource += '									</c:forEach>';
varSource += '									<%-- <td class="description"><span class="d-block text-truncate text-truncate-md">${datasetItemList[i.count].description}</span></td> --%>';
varSource += '									<td><fmt:formatDate value="${ansModel.regiDate}" pattern="yyyy-MM-dd"/></td>';
varSource += '									<td><fmt:formatDate value="${ansModel.modiDate}" pattern="yyyy-MM-dd"/></td>';
varSource += '									<td><button type="button" class="btn btn-light waves-effect waves-themed set-modify" data-idx="${efn:encrypt(ansModel.idx)}" data-toggle="modal" data-target="#answer_modi">수정</button></td>';
varSource += '								</tr>';
varSource += '</c:forEach>'

$('.col-tbody').html(varSource);
$('#totalCount').html(${count}+"건");

<%-- 직접 등록 모달 항목 셋팅 --%>
varSource = '';
<c:forEach items="${datasetItemList}" var="ansModel" varStatus="i">
varSource += '<tr>';
varSource += '<th scope="row"><label for="input0201">${efn:strContentsFormat(ansModel.itemName, '')} <span>${ansModel.required eq 1 ? '*' : '' }</span></label></th>';
varSource += '<td class="column-input">';
	<c:choose>
<c:when test="${ansModel.itemType eq 6 }">
varSource += '<textarea data-req="${ansModel.required}" data-name="${efn:strContentsFormat(ansModel.itemName, '')}" data-type="${ansModel.itemType }" data-valid="${ansModel.itemValid }" id="${ansModel.itemColName}" name="${ansModel.itemColName}" class="form-control add-direct-input" placeholder=""></textarea>';
</c:when>
<c:otherwise>
varSource += '<input type="text" data-req="${ansModel.required}" data-name="${efn:strContentsFormat(ansModel.itemName, '')}" data-type="${ansModel.itemType }" data-valid="${ansModel.itemValid }" id="${ansModel.itemColName}" name="${ansModel.itemColName}" class="form-control add-direct-input" placeholder="">';
</c:otherwise>
</c:choose>
varSource += '</td>';
varSource += '</tr>';
</c:forEach>
$('#answer_make tbody').html(varSource);

<%-- 응답데이터 수정 모달 항목 셋팅 --%>
varSource = '';
<c:forEach items="${datasetItemList}" var="ansModel" varStatus="i">
varSource += '<tr>';
varSource += '<th scope="row"><label for="input0201">${efn:strContentsFormat(ansModel.itemName, '')} <span>${ansModel.required eq 1 ? '*' : ''}</span></label></th>';
varSource += '<td class="column-input">';
<c:choose>
<c:when test="${ansModel.itemType eq 6 }">
varSource += '<textarea data-req="${ansModel.required}" data-name="${efn:strContentsFormat(ansModel.itemName, '')}" data-type="${ansModel.itemType }" data-valid="${ansModel.itemValid }" id="${ansModel.itemColName}" name="${ansModel.itemColName}" class="form-control modi-direct-input" placeholder=""></textarea>';
</c:when>
<c:otherwise>
varSource += '<input type="text" data-req="${ansModel.required}" data-name="${efn:strContentsFormat(ansModel.itemName, '')}" data-type="${ansModel.itemType }" data-valid="${ansModel.itemValid }" id="${ansModel.itemColName}" name="${ansModel.itemColName}" class="form-control modi-direct-input" placeholder="">';
</c:otherwise>
</c:choose>
varSource += '</td>';
varSource += '</tr>';

<%-- 항목 검색 체크박스 셋팅 --%>
<c:if test="${i.first}">
varCBoxSource += '<label class="custom-control-label sr-only" for="schk">검색할 </label>';
</c:if>
<c:set var="isCheck" value="${empty schkList or efn:indexOfList(schkList, ansModel.itemColName) ge 0}" />
varCBoxSource += '<div class="custom-control custom-checkbox custom-control-inline">';
varCBoxSource += '<input type="checkbox" class="custom-control-input" id="${ansModel.itemColName}${i.count}" name="schk" value="${efn:encrypt(ansModel.itemColName)}"${isCheck ? ' checked' : ''}>';
varCBoxSource += '<label class="custom-control-label" for="${ansModel.itemColName}${i.count}">${efn:strContentsFormat(ansModel.itemName, '')}</label>';
varCBoxSource += '</div>';
</c:forEach>
$('#answer_modi tbody').html(varSource);
$('#dsetItemDiv').html(varCBoxSource);

// table offset
<%-- var $table = $('table.scroll'), $tbodyTr = $table.find('tbody tr:first').children(), $colWidth;
$(window).resize(function() {
  $table.find('tbody').css('height', ($(window).height() - 440) + 'px');
	$table.find('thead tr:eq(0)').children().each(function(i, e) {
		var $docWidth = $(document).width() - 86;
		var $css = (i > 1 && i < $tbodyTr.length - 3) ? (($docWidth * (60 / ${fn:length(datasetItemList)} / 100)) + 'px') : ($docWidth * 0.08) + 'px';
		$(e).css('width', $css);
		$table.find('tbody tr:eq(0)').find('td:eq(' + i + ')').css('width', $css);
	});
}).resize(); --%>

<c:set var="URL_PAGE_LIST" value="${fn:replace(URL_PAGE_LIST, 'list.do', 'answerViewAjax.do') }"/>
// set paging
$isLoad = true;
var $pagingId = 'paging-view';
<c:if test="${empty param.page}">
if (!fn_isNull( $('#' + $pagingId).data("twbs-pagination") )) $('#' + $pagingId).twbsPagination('destroy');
var varOpt = {obj : $pagingId, action : '${URL_PAGE_LIST}', param: '&'+$ansSearchFrm.serialize(), idx : '${param.idx}', total : ${pageTotalPage}, visible : ${blockSize}};
fn_setPaging(varOpt);
varOpt = null;
</c:if>
$isLoad = false;

varTheadSource = null;
varSource = null;
varCBoxSource = null;
$pagingId = null;
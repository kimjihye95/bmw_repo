<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
<%pageContext.setAttribute("replaceChar", "\r\n"); %>

var varSource = '';
var dataSize = ${fn:length(dataList[0]) };
var itemSize  = ${fn:length(datasetItemList) };
console.log(dataSize, itemSize);
var totalDataSize = ${fn:length(dataList)};

if (${excelCount} > 1000) {
	fn_openSweetAlert({ title : ' 알림', msg : '원활한 데이터 등록을 위해 데이터는 1,000건 이하로 등록해 주십시오.', icon : 'warning' });
	$('input[name=fileUpload]').val('')
}
else if (dataSize != itemSize) {
	fn_openSweetAlert({ title : '실패', msg : '엑셀 데이터 불러오기를 실패했습니다.', icon : 'error' });
} else {

	varSource += '<thead class="bg-warning-900 text-white">';
	varSource += '	<tr>';
	varSource += '		<th style="width:50px;"></th>';
	varSource += '<c:forEach var="item" items="${datasetItemList }" varStatus="status">';
	varSource += '		<th class="excel-column" id="${item.itemColName }">${efn:strContentsFormat(item.itemName, '')}</th>';
	varSource += '</c:forEach>';
	varSource += '		<th style="width:100px;">관리</th>';
	varSource += '	</tr>';
	varSource += '</thead>';
	varSource += '<tbody class="alignC excel-tbody">';
	varSource += '	<c:forEach items="${dataList}" var="excelModel" varStatus="i">';
	varSource += '		<tr>';
	varSource += '			<td>';
	varSource += '				<div class="custom-control custom-checkbox">';
	varSource += '					<input type="checkbox" class="custom-control-input excel-chk" name="excel_chk" id="ans${i.count }">';
	varSource += '					<label class="custom-control-label" for="ans${i.count }"></label>';
	varSource += '				</div>';
	varSource += '			</td>';
	varSource += '<c:forEach items="${datasetItemList}" var="item" varStatus="status">';
	varSource += '			<td data-type="${item.itemType}" data-req="${item.required}" data-valid="${item.itemValid}">${efn:strContentsFormat(excelModel[item.itemName], '<br>')}</td>';
	varSource += '</c:forEach>';
	varSource += '			<td><button type="button" class="btn btn-light waves-effect waves-themed btn4excelModi">수정</button></td>';
	varSource += '		</tr>';
	varSource += '	</c:forEach>';
	varSource += '</tbody>';
	
	$('#excelListTable').html(varSource);
	$('.excel-count').html(${excelCount } + "건");
	$('.div4excelBtn').show();
	fn_setChkData();
	
	// table offset
	var $etable = $('table.excelScroll'), $etbodyTr = $etable.find('tbody tr:first').children();
	$(window).resize(function() {
	  $etable.find('tbody').css('height', ($(window).height() - 440) + 'px');
		$etable.find('thead tr:eq(0)').children().each(function(i, e) {
			var $docWidth = $(document).width() - 86;
			var $css = (i > 0 && i < $etbodyTr.length - 1) ? (($docWidth * (84 / ${fn:length(dataList[0])} / 100)) + 'px') : ($docWidth * 0.08) + 'px';
			$(e).css('width', $css);
			$etable.find('tbody tr').find('td:eq(' + i + ')').css('width', $css);
		});
	}).resize();
	
}

varSource = null;
dataSize = null;
itemSize  = null;
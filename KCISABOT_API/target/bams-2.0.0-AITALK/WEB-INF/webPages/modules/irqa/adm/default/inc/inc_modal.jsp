<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../cresources/jsp/common.jsp" %>
		<%-- IRQA 등록 start --%>
		<div class="modal fade" id="irqa_make" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">질문답변셋 등록</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_inputFrm" method="post" action="irqaActionProc.do" onsubmit="return false;">
							<input type="hidden" name="cmnx" value="${cmnx}" />
							<input type="hidden" name="botId" value="${efn:encrypt(botId)}" />
							<input type="hidden" name="botIdx" value="${efn:encrypt(botIdx)}" />
							<table class="tbWriteA mgt20" summary="질문 답변셋 정보 입력입니다.">
								<caption>질문 답변셋 정보 입력입니다.</caption>
								<colgroup>
									<col width="20%" />
									<col width="80%" />
								</colgroup>
	
								<tbody class="pd10 alignC">
									<tr>
										<th scope="row"><label for="irqa_question">질문 <span>*</span></label></th>
										<td><textarea class="form-control" id="irqa_question" name="irqa_question"></textarea></td>
									</tr>
									<tr>
										<th scope="row"><label for="irqa_answer">응답 <span>*</span></label></th>
										<td><textarea class="form-control" id="irqa_answer" name="irqa_answer"></textarea></td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="modal-footer pdt0">
						<button type="button" data-role="r" class="btn4submit btn btn-warning text-white">저장</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // IRQA 등록 end --%>

		<%-- IRQA 수정 start --%>
		<div class="modal fade" id="irqa_modi" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">질문답셋 수정</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_modFrm" method="post" action="irqaActionProc.do" onsubmit="return false;">
							<input type="hidden" name="cmnx" value="${cmnx}" />
							<input type="hidden" name="botId" value="${efn:encrypt(botId)}" />
							<input type="hidden" name="botIdx" value="${efn:encrypt(botIdx)}" />
							<input type="hidden" name="representQuerySeq" value="" />
							<table class="tbWriteA mgt20" summary="질문 답셋 수정 정보 입력입니다.">
								<caption>질문 답셋 수정 정보 입력입니다.</caption>
								<colgroup>
									<col width="20%" />
									<col width="80%" />
								</colgroup>
	
								<tbody class="pd10 alignC">
									<tr>
										<c:set var="modelItem" value="irqa_question" />
										<th scope="row"><label for="${modelItem}">질문 <span>*</span></label></th>
										<td><textarea class="form-control modi-textarea" id="${modelItem}" name="${modelItem}"></textarea></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="irqa_answer" />
										<th scope="row"><label for="irqa_answer">응답 <span>*</span></label></th>
										<td><textarea class="form-control modi-textarea" id="${modelItem}" name="${modelItem}"></textarea></td>
									</tr>
								</tbody>
							</table>
							<div class="mgt20">
								<h5>유사질문</h5>
								<div class="input-group">
									<c:set var="modelItem" value="parap-input" />
									<label for="${modelItem}" class="sr-only">유사질문</label>
									<textarea class="form-control" name="${modelItem}" id="${modelItem}" placeholder="유사질문을 입력하세요." style="min-height: 37px; max-height: 4rem;"></textarea>
									<!-- <input type="text" id="parap-input" class="form-control" placeholder="유사질문을 입력하세요."> -->
									<div class="input-group-append">
										<button type="button" class="btn btn-warning text-white waves-effect waves-themed" id="parap-add"><i class="fas fa-plus"></i></button>
									</div>
	
									<div id="parapViewDiv" class="col-lg-12 mgt20" style="padding:0px; height:200px; overflow:auto;"></div>
	
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer pdt0">
						<button type="button" data-role="m" class="btn4submit btn btn-warning text-white">저장</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // IRQA 수정 end --%>

<script>
<%-- IRQA 확인 버튼 클릭 이벤트 --%>
var $modal, $irqaFrm = $('#fn_inputFrm'), $irqaModFrm = $('#fn_modFrm');

$(document).on('click', '.btn4submit', function() {
	var $role = $(this).data('role'), $frm = ($role == 'r') ? $irqaFrm : $irqaModFrm;
	var textareaId = { 'irqa_question' : { 'title' : '질문' }, 'irqa_answer' : { 'title' : '응답' } };
	// 유효성 검증
	var result = true;
 	$.each( textareaId, function( key, value ) {
		if(fn_isNull( $frm.find('[id=' + key + ']').val() ) ) {
			var $opt = { title : '알림', msg : value.title + _comInputTextReqMsg, icon : 'warning', success : function() { $frm.find('[id=' + key + ']').trigger('focus'); } };
			fn_openSweetAlert($opt);
			result = false;
		}
		return result;
	});
 	
 	if(result) {
	 	var $action = $frm.attr('action');
	 	var $data;
	 	// 등록
	 	if($role == 'r') {
	 		$data = $frm.serialize();
	 	}
	 	// 수정
	 	else {
	 		var prevChanged = $(document).find( '.parap-cancel' )[0];
	 		// 수정가능 입력창 한 개만 나타나도록 함
	 		if(prevChanged != undefined) {
	 			var $opt = { title : '알림', msg : '수정 중인 유사질문을 수정완료한 뒤 재시도 해주세요.', icon : 'warning', cancel : false };
	 			fn_openSweetAlert($opt);
	 			return false;
	 		}
	 		$data = { 'cmnx' : $frm.find('input[name="cmnx"]').val(),
	 				  'botId' : $frm.find('input[name="botId"]').val(),
	 				  'botIdx' : $frm.find('input[name="botIdx"]').val(),
	 				  'representQuerySeq' : $frm.find('input[name="representQuerySeq"]').val(),
	 				  'irqa_question' : $frm.find('textarea[name="irqa_question"]').val(),
	 				  'irqa_answer' : $frm.find('textarea[name="irqa_answer"]').val() };
	 		var parapArr = [];
	 		$.each( $('#parapViewDiv').children(), function( idx, data) {
	 			var parapObj = { "paraphraseSeq": $(this).find('button:eq(0)').data('data'),//$(data).prop('id').split('-')[1],
	 							 "question": $(data).find('div[class="d-flex position-relative"] span').html().replace( /<br>/g, '\n' ),
	 							 "deleteF": ( $(data).css('display') == 'none' ) ? true : false };
	 			parapArr.push( parapObj );
	 		});
	 		$data['paraphraseList'] = JSON.stringify(parapArr);
	 	}
	 	
	 	fn_ajax({ url : $action, data : $data, type : 'POST' });
 	}
	
	return false;
});

<%-- IRQA 등록 모달 오픈 이벤트 --%>
$(document).on('shown.bs.modal', '#irqa_make', function() {
	$(this).find('textarea[id="irqa_question"]').trigger('focus');
	$modal = $('#irqa_make');
});

<%-- IRQA 등록 모달 닫기 이벤트 --%>
$(document).on('hidden.bs.modal', '#irqa_make', function() {
	$irqaFrm[0].reset();
});

<%-- 유사질문 등록 버튼 클릭 이벤트 --%>
$(document).on('click', '#parap-add', function() {
	var $parapViewDiv = $irqaModFrm.find('div[id="parapViewDiv"]');
	//var paraphraseSeq = ( $('#parapViewDiv').children().length > 0 ) ? Number($('#parapViewDiv').children(':first').prop('id').split('-')[1]) + 1 : 1;
	var question = $('#parap-input').val();
	if (!fn_checkFill($irqaModFrm, 'parap-input')) return false;
	var paraphraseSeq = ++ $paraphraseSeq;
	var $paraSeq = fn_endata(paraphraseSeq);
	
	var str = '';
		str += '<div class="panel p-1">';
		str += '	<div class="panel-hdr" style="border-bottom:0px;">';
		str += '		<div class="d-flex position-relative"><span>' +  changeSpclStrToCnvr( question ).replace( /(\r|\n|\r\n|\n\r)/g, '<br>' ) + '</span></div>';
		str += '		<div class="d-flex position-relative ml-auto">';
		str += '			<button type="button" class="btn btn-warning  btn-icon rounded-circle waves-effect waves-themed text-white btn4smod" data-data="' + $paraSeq + '" title="수정"><i class="fal fa-pen"></i></button>';
		str += '			<button type="button" class="btn btn-danger  btn-icon rounded-circle waves-effect waves-themed text-white mgl10 btn4sdel" data-data="' + $paraSeq + '" title="삭제"><i class="fal fa-times"></i></button>';
		//str += '			<button type="button" class="btn btn-danger  btn-icon rounded-circle waves-effect waves-themed text-white mgl10" data-data="' + $paraSeq + '" data-toggle="tooltip" data-offset="0,10" ';
		//str += '					data-original-title="Close" title="삭제" onclick="fn_hidden(' + paraphraseSeq + ')"><i class="fal fa-times"></i></button>';
		//str += '			<button type="button" class="btn btn-danger  btn-icon rounded-circle waves-effect waves-themed text-white mgl10" data-data="' + $paraSeq + '" title="삭제" onclick="fn_hidden(' + paraphraseSeq + ')"><i class="fal fa-times"></i></button>';
		str += '		</div>';
		str += '	</div>';
		str += '</div>';
	$parapViewDiv.prepend(str);
	$('#parap-input').val('');
});
<%-- 유사질문 수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4smod', function() {
	fn_change($(this));
});
<%-- 유사질문 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4sdel', function() {
	$(this).closest('div.p-1').hide();
});
<%-- 유사질문 수정가능하게 변경 --%>
function fn_change($this) {
	var prevChanged = $(document).find( '.parap-cancel' )[0];
	// 수정가능 입력창 한 개만 나타나도록 함
	if(prevChanged != undefined) {
		$(prevChanged).trigger('click');
	}
	var question = changeCnvrToSpclStr( $($this).parent().prev().find( 'span' ).html().replace( /<br>/g, '\n' ) );
	var seq = $($this).data('data');
	var $div = $($this).parent('div').parent('div');
	$div.children("div").remove();
	
	$div.append( $('<div/>', { 'class' : 'd-flex position-relative', style : 'min-width: 37.5rem; max-width: 37.5rem; min-height: 37px; max-height: 6.25rem;'})
			.append( $('<textarea/>', {'class' : 'form-control', text : question, style : 'min-width: 37.5rem; max-width: 37.5rem; min-height : 37px; max-height : 5rem; margin : 10px 0px;'}) ) );
	$div.append( $('<div/>', { 'class' : 'd-flex position-relative ml-auto'})
			//.append( $('<button/>', {type : 'button', 'class' : 'btn btn-info  btn-icon rounded-circle waves-effect waves-themed text-white', 'title' : '변경', 'data-data' : seq, click : function() { fn_rollback(this, $('#' + seq + '_question').val() ); } })
			.append( $('<button/>', {type : 'button', 'class' : 'btn btn-info  btn-icon rounded-circle waves-effect waves-themed text-white btn4sconf', 'title' : '변경', 'data-data' : seq, click : function() { fn_rollback(this, $(this).parents('div:eq(1)').find('textarea').val() ); } })
			.append( $('<i/>', {'class' : 'fal fa-check'}) ))
			.append( $('<button/>', {type : 'button', 'class' : 'parap-cancel btn btn-dark  btn-icon rounded-circle waves-effect waves-themed text-white mgl10', 'title' : '취소', 'data-data' : seq, click : function() { fn_rollback(this, question); } })
			.append( $('<i/>', {'class' : 'fal fa-times'}) )));
}
<%-- 유사질문 수정불가능하게 변경 --%>
function fn_rollback($this, question) {
	var seq = $($this).data('data');
	var $div = $($this).parent('div').parent('div');
	$div.children("div").remove();
	question = changeSpclStrToCnvr( question ).replace( /(\r|\n|\r\n|\n\r)/g, '<br>' );

	$div.append( $('<div/>', { 'class' : 'd-flex position-relative', style : 'width:500px;'}).append( '<span>' + question + '</span>' ) );
	$div.append( $('<div/>', { 'class' : 'd-flex position-relative ml-auto'})
			.append( $('<button/>', {type : 'button', 'class' : 'btn btn-warning  btn-icon rounded-circle waves-effect waves-themed text-white btn4smod', 'title' : '수정', 'data-data' : seq })
			.append( $('<i/>', {'class' : 'fal fa-pen'}) ))
			.append( $('<button/>', {type : 'button', 'class' : 'btn btn-danger  btn-icon rounded-circle waves-effect waves-themed text-white mgl10', 'title' : '삭제' })
			.append( $('<i/>', {'class' : 'fal fa-times'}) )));
}
</script>
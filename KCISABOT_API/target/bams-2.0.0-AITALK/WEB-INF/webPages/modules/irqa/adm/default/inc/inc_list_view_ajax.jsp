<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
<%pageContext.setAttribute("replaceChar", "\r\n"); %>

<c:set var="URL_PAGE_LIST" value="${fn:replace(URL_PAGE_LIST, 'list.do', 'irqaViewAjax.do') }"/>

var varSource = '';
<c:if test="${empty list}">
varSource += '	<div id="panel-1" class="panel pd20 text-center">${_infoNoDataMsg_}</div>';
</c:if>			

<c:forEach items="${list}" var="model" varStatus="i">
	<c:set var="seq" value="${efn:encrypt(model.representQuerySeq)}" />
	varSource += '	<div id="panel-${i.count}" class="panel pd20">';
	varSource += '		<div class="panel-hdr" style="border-bottom:0px;">';
	varSource += '			<div class="d-flex position-relative">';
	varSource += '				<div class="custom-control custom-checkbox custom-control-inline">';
	varSource += '					<input type="checkbox" class="custom-control-input" id="irqaCboxs${i.count}" name="irqaCboxs" value="${seq}">';
	varSource += '					<label class="custom-control-label" for="irqaCboxs${i.count}"></label>';
	varSource += '				</div>';
	varSource += '			</div>';
	varSource += '			<div class="position-relative col-8">';
	varSource += '				<div class="qa" data-data="${model.question}"><span class="text-info"><strong>질문</strong></span> ${model.question}</div>';
	varSource += '				<div class="qa" data-data="${model.answer}"><span class="text-danger"><strong>답변</strong></span> ${model.answer}</div>';
	varSource += '			</div>';
	varSource += '			<div class="d-flex position-relative ml-auto" style="max-width: 15rem;">';
	varSource += '				<span style="line-height:50px;" class="mr-5">유사질문 : ${model.paraphraseList.size()}</span>';
	varSource += '				<button type="button" class="btn4mod set-modify btn btn-warning btn-lg btn-icon rounded-circle waves-effect waves-themed text-white" data-toggle="modal" data-target="#irqa_modi" data-data="${seq}"><i class="fal fa-pen"></i></button>';
	varSource += '				<button type="button" class="set-delete btn btn-danger btn-lg btn-icon rounded-circle waves-effect waves-themed text-white mgl10" data-toggle="tooltip" data-offset="0,10" data-original-title="Close" data-data="${seq}"><i class="fal fa-times"></i></button>';
	varSource += '			</div>';
	varSource += '		</div>';
	varSource += '	</div>';
	
	<%-- var $tempArr = [];
	<c:forEach items="${model.paraphraseList}" var="chdModel" varStatus="i">
		var $tempObj = { 'paraphraseSeq' : ${chdModel.paraphraseSeq}, 'question' : '${chdModel.question}', 'deleteF' : ${chdModel.deleteF} };
		$tempArr.push($tempObj);
	</c:forEach>
	$paraphraseList['${model.representQuerySeq}'] = $tempArr; --%>
</c:forEach>

$('div[id="viewDiv"]').html(varSource);
$('#totalCount').html(${empty totalCount ? 0 : totalCount}+"건");

// set paging
$isLoad = true;
<c:if test="${empty param.page}">
	var paramStr = '';
	var params = { 'cmnx' : '${cmnx}', 'botId' : '${botId}', 'botIdx' : '${botIdx}', 'skey' : '${param.skey}', 'slmt' : '${param.slmt}' };
	$.each( params, function( key, value) {
		paramStr += '&' + key + '=' + value;
	});
	
	var $pagingId = 'paging-view';
	if (!fn_isNull( $('#' + $pagingId).data("twbs-pagination") )) $('#' + $pagingId).twbsPagination('destroy');
	<c:if test="${not empty list}">
		var varOpt = {action : '${URL_PAGE_LIST}', obj : $pagingId, total : '${pageTotalPage}', visible : '${blockSize}', param : paramStr};
		fn_setPaging(varOpt);
		varOpt = null;
	</c:if>
$pagingId = null;
</c:if>
$isLoad = false;

varSource = null;
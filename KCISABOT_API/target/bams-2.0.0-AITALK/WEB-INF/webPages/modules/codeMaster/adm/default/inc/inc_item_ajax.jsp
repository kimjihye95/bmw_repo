<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '';

varSource += '<tr>';
	<c:set var="modelItem" value="itemName" />
varSource += '<th scope="row"><label for="${modelItem}">항목명</label></th>';
varSource += '<td><input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" required /></td>';
varSource += '</tr>';
varSource += '<tr>';
	<c:set var="modelItem" value="itemCode1" />
varSource += '<th scope="row"><label for="${modelItem}">항목값1</label></th>';
varSource += '<td><input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" required /></td>';
varSource += '</tr>';
varSource += '<tr>';
	<c:set var="modelItem" value="itemCode2" />
varSource += '<th scope="row"><label for="${modelItem}">항목값2</label></th>';
varSource += '<td><input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" /></td>';
varSource += '</tr>';
varSource += '<tr>';
	<c:set var="modelItem" value="itemCode3" />
varSource += '<th scope="row"><label for="${modelItem}">항목값3</label></th>';
varSource += '<td><input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" /></td>';
varSource += '</tr>';
	<c:set var="modelItem" value="isuse" />
varSource += '<tr>';
varSource += '<th scope="row"><label for="${modelItem}">사용여부</label></th>';
varSource += '<td><input type="checkbox" name="${modelItem}" id="${modelItem}" value="1"<c:if test="${model[modelItem] eq 1}"> checked</c:if> /></td>';
varSource += '</tr>';

$('#itemModifyTbody').html(varSource);

// set validation
$validatorOpt = { id : ['itemName', 'itemCode1'] };
fn_setValidator($itemModFrm, $validatorOpt);
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '';

varSource += '<tr>';
	<c:set var="modelItem" value="masterId" />
varSource += '<th scope="row"><label for="${modelItem}">마스터ID</label></th>';
varSource += '<td>${model[modelItem]}</td>';
varSource += '</tr>';
varSource += '<tr>';
	<c:set var="modelItem" value="masterName" />
varSource += '<th scope="row"><label for="${modelItem}">마스터명</label></th>';
varSource += '<td><input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control" required /></td>';
varSource += '</tr>';
	<c:set var="modelItem" value="isuse" />
varSource += '<tr>';
varSource += '<th scope="row"><label for="${modelItem}">사용여부</label></th>';
varSource += '<td>';
varSource += '<div class="custom-control custom-checkbox">';
varSource += '<input type="checkbox" name="${modelItem}" id="${modelItem}" value="1"<c:if test="${model[modelItem] eq 1}"> checked</c:if> class="custom-control-input">';
varSource += '<label class="custom-control-label" for="${modelItem}"></label>';
varSource += '</div>';
varSource += '</td>';
varSource += '</tr>';

$('#masterModifyTbody').html(varSource);

// set validation
$validatorOpt = { id : ['masterName'] };
fn_setValidator($masterModFrm, $validatorOpt);
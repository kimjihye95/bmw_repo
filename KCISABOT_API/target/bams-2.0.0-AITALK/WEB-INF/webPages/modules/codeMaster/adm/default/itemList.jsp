<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />

						<c:set var="URL_DEFAULT_LIST" value="${fn:replace(URL_DEFAULT_LIST, 'list.do', 'itemList.do')}" />
						<c:set var="URL_PAGE_LIST" value="${fn:replace(URL_PAGE_LIST, 'list.do', 'itemList.do')}" />
						<div class="row">
							<div class="col-sm-12">
								<h4><span class="text-warning"><i class="fas fa-dot-circle"></i></span> 코드</h4>
							</div>
							<div class="col-sm-6 justify-content-start">
								<p class="mgb0" style="line-height:38px;">Total. <span class="text-warning">${count}건</span></p>
							</div>
							<div class="col-sm-6 justify-content-end">
								<form id="fn_searchFrm" action="${URL_DEFAULT_LIST}">
									<input type="hidden" name="cmnx" value="${cmnx}" />
									<input type="hidden" name="idx" value="${param.idx}" />
									<div class="dt-buttons" style="text-align:right;">
										<button type="button" class="btn btn-warning waves-effect waves-themed text-white" data-toggle="modal" data-target="#codeItemRegModal">추가</button>
										<button type="button" class="btn4del btn btn-dark waves-effect waves-themed">삭제</button>
										<div class="btn-group mgl10">
											<select name="slmt" class="form-control">
												<c:forEach begin="1" end="3" var="no">
													<option value="${no * 10}"<c:if test="${param.slmt eq no * 10}"> selected</c:if>>${no * 10}건씩 조회</option>
												</c:forEach>
											</select>
										</div> 
										<button type="button" class="btn4search btn btn-outline-default"><span>변경</span></button> 
									</div>
								</form>
							</div>
						</div>

						<div class="mgt20">
							<form id="fn_delFrm" action="itemDelProc.do?cmnx=${cmnx}" class="col-sm-12">
								<table class="table table-striped m-0 tbListA">
									<colgroup>
										<col width="5%" />
										<col width="18.75%" />
										<col width="18.75%" />
										<col width="18.75%" />
										<col width="18.75%" />
										<col width="10%" />
									</colgroup>
									<thead class="bg-warning-900 text-white">
										<tr>
											<th></th>
											<th>항목명</th>
											<th>항목값1</th>
											<th>항목값2</th>
											<th>항목값3</th>
											<th>관리</th>
										</tr>
									</thead>
									<tbody class="alignC">
									
										<c:if test="${empty list}">
											<tr><td colspan="6">${_infoNoDataMsg_}</td></tr>
										</c:if>
										
										<c:forEach items="${list}" var="model" varStatus="i">
											<c:set var="encItemIdx" value="${efn:encrypt(model.itemIdx)}" />
											<tr>
												<th scope="row">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" name="codeCboxs" id="codeCboxs${i.count}" value="${encItemIdx}" class="custom-control-input">
														<label class="custom-control-label" for="codeCboxs${i.count}"></label>
													</div>
												</th>
												<td>${model.itemName}</td>
												<td>${model.itemCode1}</td>
												<td>${model.itemCode2}</td>
												<td>${model.itemCode3}</td>
												<td><button type="button" data-data="${encItemIdx}" class="btn4mod btn btn-light waves-effect waves-themed" data-toggle="modal" data-target="#codeItemModModal">수정</button></td>
											</tr>
										
										</c:forEach>
									</tbody>
								</table>
							</form>
						</div>
						
						<div class="row mgt30">
							<div class="col-lg-4">&nbsp;</div>
							<div class="col-lg-4">
								<div id="paging"></div>
							</div>
							<div class="col-lg-4" style="text-align:right;">
								<button type="button" class="btn4list btn btn-warning waves-effect waves-themed text-white">목록</button>
							</div>
						</div>
						
						<%@ include file = "./inc/inc_itemList_modal.jsp" %>
<script>
var varOpt = {url : '${URL_PAGE_LIST}', obj : 'paging', page : 'page', idx : '${param.idx}', total : '${pageTotalPage}', visible : '${blockSize}'};
fn_setPaging(varOpt);
<%-- 조회 카운트 변경 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4search', function() {
	var $frm = $('#fn_searchFrm');
	$frm.submit();
});
<%-- 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4del', function() {
	var $frm = $('#fn_delFrm');
	if (!fn_checkCboxChecked('codeCboxs')) return false;

	var $success = function() {
		var $action = $frm.attr('action');
		var $data = $frm.serialize();
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
	
	var $opt = { title : '알림', msg : '${_confirmDeleteMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
	
	return false;
});
<%-- 코드  항목 수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4mod', function() {
	var $midx = $(this).data('data');
	var $action = 'itemInfoAjax.do?cmnx=${cmnx}';
	fn_ajax({ url : $action, data : { idx : $midx } });
	$itemModFrm.find('input[name=idx]').val($midx);
});
<%-- 목록 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4list', function() {
	var $page = $.cookie(_comMasterPageCookieName);
	var $url = 'list.do?cmnx=${cmnx}' + (!fn_isNull($page) ? '&page=' + $page : '');
	location.href = $url;
});
</script>
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
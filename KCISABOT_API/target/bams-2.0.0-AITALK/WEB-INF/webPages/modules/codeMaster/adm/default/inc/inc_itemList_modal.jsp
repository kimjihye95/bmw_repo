<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
		<%-- 마스터 코드 항목 등록 start --%>
		<div class="modal fade" id="codeItemRegModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">마스터코드 항목 추가</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_inputFrm" method="post">
							<input type="hidden" name="cnmx" value="${cmnx}" />
							<input type="hidden" name="midx" value="${param.idx}" />
							<table class="tbWriteA mgt20" summary="마스터코드 추가 입력입니다.">
								<caption>마스터코드 항목 추가 입력입니다.</caption>
								<colgroup>
									<col width="20%" />
									<col width="80%" />
								</colgroup>
	
								<tbody class="pd10 alignC">
									<tr>
										<c:set var="modelItem" value="itemName" />
										<th scope="row"><label for="${modelItem}">항목명</label> <span>*</span></th>
										<td><input type="text" name="${modelItem}" id="${modelItem}" class="form-control" value="" required></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="itemCode1" />
										<th scope="row"><label for="${modelItem}">항목값 1</label> <span>*</span></th>
										<td><input type="text" name="${modelItem}" id="${modelItem}" class="form-control" value="" required></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="itemCode2" />
										<th scope="row"><label for="${modelItem}">항목값 2</label></th>
										<td><input type="text" name="${modelItem}" id="${modelItem}" class="form-control" value=""></td>
									</tr>
									<tr>
										<c:set var="modelItem" value="itemCode3" />
										<th scope="row"><label for="${modelItem}">항목값 3</label></th>
										<td><input type="text" name="${modelItem}" id="${modelItem}" class="form-control" value=""></td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="modal-footer inno pdt0">
						<button type="button" data-role="r" class="btn4submit btn btn-warning text-white">저장</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // 마스터 코드 항목 등록 end --%>
		
		<%-- 마스터 코드 항목 수정 start --%>
		<div class="modal fade" id="codeItemModModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header inno">
						<h5 class="modal-title">마스터코드 수정</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fal fa-times"></i></span>
						</button>
					</div>
					<div class="modal-body">
						<form id="fn_modFrm" method="post">
							<input type="hidden" name="idx" />
							<input type="hidden" name="midx" value="${param.idx}" />
							<input type="hidden" name="cnmx" value="${cmnx}" />
							<table class="tbWriteA mgt20" summary="마스터코드 수정 입력입니다.">
								<caption>마스터코드 수정 입력입니다.</caption>
								<colgroup>
									<col width="20%" />
									<col width="80%" />
								</colgroup>
	
								<tbody id="itemModifyTbody" class="pd10 alignC"></tbody>
							</table>
						</form>
					</div>
					<div class="modal-footer inno pdt0">
						<button type="button" data-role="m" class="btn4submit btn btn-warning text-white">저장</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					</div>
				</div>
			</div>
		</div>
		<%-- // 마스터 코드 항목 수정 end --%>
<script>
<%-- 마스터폼 저장 버튼 클릭 이벤트 --%>
var $itemFrm = $('#fn_inputFrm'), $itemModFrm = $('#fn_modFrm');
$(document).on('click', '.btn4submit', function() {
	var $role = $(this).data('role'), $frm = ($role == 'r') ? $itemFrm : $itemModFrm;

	if ($frm.valid()) {
	 	var $action = 'itemActionProc.do?cnmx=${cmnx}';
	 	var $data = $frm.serialize();
	 	fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
	
	return false;
});
<%-- set validation --%>
var $validatorOpt = { id : ['itemName', 'itemCode1'] };
fn_setValidator($itemFrm, $validatorOpt);
<%-- 코드 마스터 항목 등록 모달 오픈 이벤트 --%>
$(document).on('shown.bs.modal', '#codeItemRegModal', function() {
	$(this).find('input[name=itemName]').trigger('focus');
});
<%-- 코드 마스터 항목 등록 모달 닫기 이벤트 --%>
$(document).on('hidden.bs.modal', '#codeItemRegModal', function() {
	$itemFrm[0].reset();
});
</script>
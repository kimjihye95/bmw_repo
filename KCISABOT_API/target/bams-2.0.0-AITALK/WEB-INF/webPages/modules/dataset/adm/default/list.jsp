<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../admresources/jsp/commonTop.jsp" %>
<jsp:include page="${resourcesPath}/jsp/top.jsp" />

						<div class="row">
							<div class="col-3 bg-fusion-400 bg-fusion-gradient" style="padding-left:0px; padding-right:0px; min-height:80vh;">
								<div class="col-lg-12 pd20"  style="background:#111;">
									<div class="custom-control custom-checkbox">
										<c:set var="modelItem" value="ishide" />
										<input type="checkbox" id="${modelItem}" value="${efn:encrypt('1')}" class="custom-control-input" checked>
										<label class="custom-control-label" for="${modelItem}">하위 분류 내 데이터 조회 안함</label>
									</div>
								</div>
								<div class="col-lg-12 pd20">
									<div id="jstree">
										<ul>
											<c:forEach items="${categoryList1}" var="rootCatModel" varStatus="x">
												<li data-data="${efn:encrypt(rootCatModel.categoryIdx)}" data-lvl="${efn:encrypt(rootCatModel.categoryLevel)}" data-path="${rootCatModel.categoryFullName}" class="jstree-open">${rootCatModel.categoryName}
													<ul>
														<c:forEach items="${categoryList2}" var="catModel" varStatus="i">
															<li data-data="${efn:encrypt(catModel.categoryIdx)}" data-lvl="${efn:encrypt(catModel.categoryLevel)}" data-path="${catModel.categoryFullName}"<c:if test="${i.first}"> class="jstree-open"</c:if>>${catModel.categoryName}
															
															<c:set var="subCatCnt" value="1" />
															<c:forEach items="${categoryList3}" var="subCatModel" varStatus="j">
																<c:if test="${catModel.categoryIdx eq subCatModel.parentCategoryIdx}">
																	<c:if test="${subCatCnt eq 1}">
																	<ul>
																	</c:if>
																		<li data-data="${efn:encrypt(subCatModel.categoryIdx)}" data-lvl="${efn:encrypt(subCatModel.categoryLevel)}" data-path="${subCatModel.categoryFullName}">${subCatModel.categoryName}</li>
																	<c:if test="${subCatModel.categoryCnt eq subCatCnt}">
																	</ul>
																	</c:if>
																	<c:set var="subCatCnt" value="${subCatCnt + 1}" />
																</c:if>
															</c:forEach>
															
														</c:forEach>
													</ul>
												</li>
											</c:forEach>
										</ul>
									</div>

								</div>
							</div>
							<div class="col-9" style="padding:0 20px 20px 20px;">
								<div class="alert alert-primary alert-dismissible">
									<p id="searchTitle" class="ml-3">ROOT</p>
									<form id="fn_searchFrm" action="datasetListAjax.do?cmnx=${cmnx}" target="targetIfrm">
										<input type="hidden" name="cmnx" value="${cmnx}" />
										<input type="hidden" name="idx" value="" />
										<input type="hidden" name="slvl" value="" />
										<input type="hidden" name="slmt" value="" />
										<input type="hidden" name="q" value="" />
										<div class="row">
											<div class="col-sm-3">
												<c:set var="modelItem" value="stype" />
												<select name="${modelItem}" id="${modelItem}" class="form-control">
													<option value="">전체</option>
													<option value="${efn:encrypt('n')}">데이터셋 명</option>
													<option value="${efn:encrypt('r')}">등록일</option>
												</select>
											</div>
											<div class="col-sm-9">
												<div class="input-group">
													<c:set var="modelItem" value="skey" />
													<input type="text" name="${modelItem}" id="${modelItem}" class="form-control" placeholder="검색어를 입력하세요.">
													<div class="input-group-append">
														<button type="button" class="btn4search btn btn-warning text-white waves-effect waves-themed"><i class="fal fa-search"></i></button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>

								<div class="row">
									<div class="col-sm-6 justify-content-start">
										<p class="mgb0" style="line-height:38px;">Total. <span id="datasetCount" class="text-warning"></span></p>
									</div>
									<div class="col-sm-6 justify-content-end">
										<div class="dt-buttons" style="text-align:right;">
											<button type="button" class="btn4add btn btn-warning waves-effect waves-themed text-white" data-toggle="modal">생성</button>
											<button type="button" class="btn4del btn btn-dark waves-effect waves-themed">삭제</button>
											<div class="btn-group mgl10">
												<c:set var="modelItem" value="slmt" />
												<select name="${modelItem}" id="${modelItem}" class="form-control">
													<option value="${efn:encrypt(10)}" selected>10건씩 조회</option>
													<option value="${efn:encrypt(20)}">20건씩 조회</option>
													<option value="${efn:encrypt(30)}">30건씩 조회</option>
												</select>
											</div> 
											<button class="btn4list btn btn-outline-default" type="button"><span>변경</span></button> 
										</div>
									</div>
								</div>

								<div class="mgt20">
									<form id="fn_delFrm" action="datasetDelProc.do?cmnx=${cmnx}" class="col-sm-12">
										<table class="table table-striped m-0 tbListA">
											<colgroup>
												<col style="width:5%" />
												<col style="width:6%" />
												<col style="width:12%" />
												<col />
												<col style="width:14%" />
												<col style="width:14%" />
												<col style="width:12%" />
											</colgroup>
											<thead class="bg-warning-900 text-white">
												<tr>
													<th></th>
													<th>No</th>
													<th>분류</th>
													<th>데이터셋 명</th>
													<th>등록</th>
													<th>최종수정</th>
													<th>관리</th>
												</tr>
											</thead>
											<tbody id="datasetTbody" class="alignC">
												<tr><td colspan="7">${_infoNoDataMsg_}</td></tr>
											</tbody>
										</table>
									</form>
								</div>
								
								<div class="mgt30" style="text-align:center;">
									<div id="paging"></div>
								</div>

							</div>
						</div>

						<%-- 데이터셋 등록 모달 --%>						
						<%@ include file = "./inc/inc_list_modal.jsp" %>
						<%-- //데이터셋 등록 모달 --%>
						
						<%-- 데이터셋 항목 모달 --%>
						<%@ include file = "./inc/inc_list_item_modal.jsp" %>
						<%-- //데이터셋 항목 모달 --%>
<script>
var $jstree, $jstreeId, $searchFrm = $('#fn_searchFrm'), $code = new Object();
<%-- 하위 분류 내 데이터조회안함 클릭 이벤트 --%>
$(document).on('click', '#ishide', function() {
	$jstreeId.find('a')[0].click();
});
<%-- 검색 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4search', function() {
	if ($searchFrm[0].checkValidity() === false) {
		event.preventDefault();
		event.stopPropagation();
		$searchFrm.addClass('was-validated');
	}
	else {
		fn_ajax( { url : $searchFrm.attr('action'), data : $searchFrm.serialize() } );
	}
});
<%-- 검색어 엔터키 이벤트 --%>
$(document).on('keypress', '#skey', function(e) {
	if (e.keyCode == 13) {
		$('.btn4search').click();
		return false;
	}
});
<%-- 리스트 카운트 적용 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4list', function() {
	$searchFrm.find('[name=slmt]').val($('select[name=slmt]').val());
	$('.btn4search').click();
	return false;
});
<%-- 분류 선택 이벤트 --%>
$(document).on('click', 'a.jstree-anchor', function() {
	$jstreeId = $('#' + ($(this).attr('id')).replace('_anchor', ''));
	var $data = $($jstreeId).data('data'), $lvl = $($jstreeId).data('lvl'), $path = $($jstreeId).data('path'), $hide = $('#ishide:checked').val();
	$searchFrm.find('input[name=idx]').val($data);
	$searchFrm.find('input[name=slvl]').val($lvl);
	$searchFrm.find('input[name=q]').val($hide);
	//$datasetFrm.find('input[name=categoryIdx]').val($data);
	$datasetFrm.find('input[name=categoryIdxName]').val($path);
	$datasetFrm.find('#categoryIdxName').text($path);

	$('#searchTitle').text($path);
	fn_ajax( { url : $searchFrm.attr('action'), data : $searchFrm.serialize() } );
});
<%-- 삭제 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4del', function() {
	var $frm = $('#fn_delFrm');
	if (!fn_checkCboxChecked('datasetCboxs')) return false;

	var $success = function() {
		var $action = $frm.attr('action');
		var $data = $frm.serialize();
		fn_ajax({ url : $action, data : $data, type : 'POST' });
	}
	
	var $opt = { title : '알림', msg : '${_commonDatsetDelMsg_}', icon : 'warning', cancel : true, success : $success };
	fn_openSweetAlert($opt);
	
	return false;
});
<%-- 데이터셋 생성 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4add', function() {
	<c:choose>
		<c:when test="${fn:length(categoryList) gt 1}">
		var $data = $(this).data('data');
		$('#dataSetRegModal select[name=categoryIdx]').val($data);
		$('#dataSetRegModal').modal();
		</c:when>
		<c:otherwise>
		var $opt = { title : '알림', msg : '데이터 분류를 먼저 생생해 주십시오.', icon : 'warning' };
		fn_openSweetAlert($opt);
		</c:otherwise>
	</c:choose>
});
<%-- 데이터셋 수정 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4mod', function() {
	var $didx = $(this).data('data');
	var $action = 'datasetInfoAjax.do?cmnx=${cmnx}';
	fn_ajax({ url : $action, data : { idx : $didx } });
	$datasetModFrm.find('input[name=idx]').val($didx);
	$modal = $('#dataSetModiModal');
});
<%-- 데이터셋 항목 버튼 클릭 이벤트 --%>
$(document).on('click', '.btn4item', function() {
	var $didx = $(this).data('data'), $cidx = $(this).data('cat');
	var $action = 'datasetItemListAjax.do?cmnx=${cmnx}';
	fn_ajax({ url : $action, data : { idx : $didx, cidx : $cidx } });
	$datasetItemFrm.find('input[name=didx]').val($didx);
	$datasetItemFrm.find('input[name=cidx]').val($cidx);
	$datasetItemDelFrm.find('input[name=didx]').val($didx);
	$datasetItemDelFrm.find('input[name=cidx]').val($cidx);
	
	$(this).addClass('on').siblings('.btn4item').removeClass('on');
});
$(document).ready(function() {
	$jstree = $('#jstree').bind('ready.jstree', function() { $('a.jstree-anchor:eq(0)').click(); }).jstree({ 'core' : { 'check_callback' : true }, 'plugins' : [ 'types' ], 'types' : { 'default' : { 'icon' : 'fal fa-folder' } } });
	fn_ajax( { url : 'codeInfoAjax.do?cmnx=${cmnx}', dataType : 'json',  success : function(data) { try {$code = data} catch (e) {}}  } );
});
</script>
<jsp:include page="${resourcesPath}/jsp/bottom.jsp" />
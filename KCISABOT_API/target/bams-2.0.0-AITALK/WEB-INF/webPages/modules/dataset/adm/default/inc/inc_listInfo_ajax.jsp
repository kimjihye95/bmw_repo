<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '', $categoryName = $datasetFrm.find('#categoryIdxName').text();
varSource += '<tr>';
	<c:set var="modelItem" value="categoryIdx" />
varSource += '<th scope="row"><label for="${modelItem}">분류</label> <span>*</span></th>';
varSource += '<td>';
varSource += '<select class="form-control order-select" id="${modelItem}" name="${modelItem}">';
	<c:forEach var="category" items="${modalCategoryList}" varStatus="i">
		<c:if test="${!i.first}">
varSource += '<option value="${efn:encrypt(category.categoryIdx)}" ${category.categoryIdx eq model.categoryIdx ? 'selected' : '' }>${category.category }</option>';
		</c:if>						
</c:forEach>
varSource += '</select>';
varSource += '<input type="hidden" name="categoryIdxName" value="">';
varSource += '</td>';
varSource += '</tr>';
varSource += '<tr>';
	<c:set var="modelItem" value="datasetName" />
varSource += '<th scope="row"><label for="${modelItem}">데이터셋 명</label> <span>*</span></th>';
varSource += '<td><input type="text" name="${modelItem}" id="${modelItem}" class="form-control" value="${model[modelItem]}" minlength="2" maxlength="20" required></td>';
varSource += '</tr>';
varSource += '<tr>';
	<c:set var="modelItem" value="refUrl" />
varSource += '<th scope="row"><label for="${modelItem}">참조 URL</label></th>';
varSource += '<td>';
varSource += '<div class="input-group">';
varSource += '<input type="text" name="${modelItem}" id="${modelItem}" value="${model[modelItem]}" class="form-control">';
varSource += '<div class="input-group-append">';
varSource += '<button class="btn4url btn btn-warning text-white waves-effect waves-themed" type="button"><i class="fal fa-globe"></i> URL 열기</button>';
varSource += '</div>';
varSource += '</div>';
varSource += '</td>';
varSource += '</tr>';
varSource += '<tr>';
	<c:set var="modelItem" value="description" />
varSource += '<th scope="row"><label for="${modelItem}">설명</label></th>';
varSource += '<td><textarea class="form-control" name="${modelItem}" id="${modelItem}" rows="5">${efn:strContentsFormat(model[modelItem], crlf)}</textarea></td>';
varSource += '</tr>';

$('#datasetModTbody').html(varSource);

// set validation
$validatorOpt = { id : ['categoryIdx', 'datasetName'], min : [ 0, 2 ], max : [ 0, 20 ]  };
fn_setValidator($datasetModFrm, $validatorOpt);

varSource = null;
$categoryName = null;
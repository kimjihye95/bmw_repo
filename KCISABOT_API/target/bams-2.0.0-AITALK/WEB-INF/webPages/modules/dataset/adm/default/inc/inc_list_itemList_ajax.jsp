<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '', $itemNameObj = new Object();
$itemCnt = 0;

<c:if test="${empty list}">
varSource += '<tr id="itemTr0"><td colspan="7">' + _comNoDatasetItemMsg_ + '</td></tr>';
</c:if>

<c:set var="listNo" value="${listStartNo}" />
<c:forEach items="${list}" var="model" varStatus="i">
	<c:set var="encItemIdx" value="${efn:encrypt(model.itemIdx)}" />
	<c:set var="encOrderNo" value="${efn:encrypt(model.orderNo)}" />
$itemCnt ++;
varSource += '<tr>';
varSource += '<th scope="row">';
varSource += '<div class="custom-control custom-checkbox">';
varSource += '<input type="checkbox" name="datasetItemCboxs" id="datasetItemCboxs${i.count}" value="${encItemIdx}" data-ordr="${encOrderNo}" class="custom-control-input">';
varSource += '<label class="custom-control-label" for="datasetItemCboxs${i.count}"></label>';
varSource += '</div>';
varSource += '</th>';
	<c:set var="modelItem" value="itemName" />
	<c:set var="itemName" value="${efn:strContentsFormat(model[modelItem], '')}" />
$itemNameObj['${i.count}'] = '${itemName}';
varSource += '<td><label for="${modelItem}" class="sr-only">항목명</label><input type="text" name="${modelItem}" id="${modelItem}' + $itemCnt + '" value="${efn:strContentsFormat(model[modelItem], '')}" class="form-control" placeholder="항목명 입력"></td>';
	<c:set var="modelItem" value="itemType" />
varSource += '<td>${model.itemCodeName}<input type="hidden"name="itemType" value="${efn:encrypt(model[modelItem])}" /></td>';
varSource += '<td>${model.itemCode2 eq null or model.itemCode2 eq '' ? model.itemValidName : model.itemCode2}</td>';
varSource += '<td>';
	<c:set var="modelItem" value="required" />
varSource += '<div class="custom-control custom-checkbox">';
varSource += '<input type="checkbox" name="${modelItem}" id="${modelItem}' + $itemCnt + '" value="1"<c:if test="${model[modelItem] eq 1}"> checked</c:if> class="custom-control-input">';
varSource += '<label class="custom-control-label" for="${modelItem}' + $itemCnt + '"></label>';
varSource += '</div>';
varSource += '</td>';
	<c:set var="modelItem" value="description" />
varSource += '<td><input type="text" name="${modelItem}" id="${modelItem}' + $itemCnt + '" value="${efn:strContentsFormat(model[modelItem], '')}" class="form-control" placeholder="미입력시 항목명으로 표시"></td>';
varSource += '<td>';
varSource += '<button type="button" data-data="${encItemIdx}" class="btn4itemSubmit btn btn-info waves-effect waves-themed text-white mr-2">수정</button>';
varSource += '<div class="btn-group-vertical" role="group">';
<c:if test="${!i.first}">
varSource += '<button type="button" data-role="u" data-data="${encItemIdx}" data-order="${encOrderNo}" class="btn4order btn btn-xs btn-dark waves-effect waves-themed"><i class="fal fa-arrow-up"></i></button>';
</c:if>
<c:if test="${!i.last}">
varSource += '<button type="button" data-role="d" data-data="${encItemIdx}" data-order="${encOrderNo}" class="btn4order btn btn-xs btn-dark waves-effect waves-themed"><i class="fal fa-arrow-down"></i></button>';
</c:if>
varSource += '</div>';
varSource += '</td>';
varSource += '</tr>';
<c:set var="listNo" value="${listNo - 1}" />
</c:forEach>

$('#dataSetItemTbody').html(varSource);

varSource = null;
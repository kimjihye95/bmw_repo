<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '';

<c:set var="URL_PAGE_LIST" value="${fn:replace(URL_PAGE_LIST, 'list.do', 'datasetListAjax.do')}" />
<c:if test="${empty list}">
varSource += '<tr><td colspan="7">${_infoNoDataMsg_}</td></tr>';
</c:if>

<c:set var="listNo" value="${listStartNo}" />
<c:forEach items="${list}" var="model" varStatus="i">
	<c:set var="data" value="${model.categoryIdx}|${model.datasetIdx}" />
	<c:set var="encData" value="${efn:encrypt(data)}" />
	<c:set var="encDatasetIdx" value="${efn:encrypt(model.datasetIdx)}" />
	<c:set var="encCategoryIdx" value="${efn:encrypt(model.categoryIdx)}" />
varSource += '<tr>';
varSource += '<th scope="row">';
varSource += '<div class="custom-control custom-checkbox">';
varSource += '<input type="checkbox" name="datasetCboxs" id="datasetCboxs${i.count}" value="${encData}" class="custom-control-input">';
varSource += '<label class="custom-control-label" for="datasetCboxs${i.count}"></label>';
varSource += '</div>';
varSource += '</th>';
varSource += '<td>${listNo}</td>';
varSource += '<td>${model.category}</td>';
//varSource += '<td><a href="#" id="dataset${i.count}" data-data="${encDatasetIdx}" data-toggle="modal" data-target="#dataSet_modi">${efn:strContentsFormat(model.datasetName, '')}</a></td>';
varSource += '<td><span class="text-point">${efn:strContentsFormat(model.datasetName, '')}</span></td>';
varSource += '<td>${model.regiName} (${fn:substring(model.regiDate, 0, 10)})</td>';
varSource += '<td><c:if test="${!empty model.modiDate}">${model.modiName} (${fn:substring(model.modiDate, 0, 10)})</c:if></td>';
varSource += '<td>';
varSource += '<div class="btn-group" role="group">';
varSource += '<button type="button" data-data="${encDatasetIdx}" class="btn4mod btn btn-xs btn-info waves-effect waves-themed" data-toggle="modal" data-target="#dataSetModiModal">수정</button>';
varSource += '<button type="button" data-data="${encDatasetIdx}" data-cat="${encCategoryIdx}" class="btn4item btn btn-xs btn-success waves-effect waves-themed" data-toggle="modal" data-target="#dataSetItemModal">항목</button>';
varSource += '</div>';
varSource += '</td>';
varSource += '</tr>';
<c:set var="listNo" value="${listNo - 1}" />
</c:forEach>

$('.btn4add').data('data', '${categoryIdx }');
$('#datasetCount').text('${count}건');
$('#datasetTbody').html(varSource);

// set paging
$isLoad = true;
var $pagingId = 'paging';
<c:if test="${empty param.page}">
if (!fn_isNull( $('#' + $pagingId).data("twbs-pagination") )) $('#' + $pagingId).twbsPagination('destroy');
var varOpt = {obj : $pagingId, action : '${URL_PAGE_LIST}', idx : '${param.idx}', total : ${pageTotalPage}, visible : ${blockSize}};
fn_setPaging(varOpt);
varOpt = null;
</c:if>
$isLoad = false;

varSource = null;
$pagingId = null;
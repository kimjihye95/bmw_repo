<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var $source = '', $idx = 0;
fn_ajax( { url : '/decrypt.do', data : { source : $jstreeId.data('data') }, dataType : 'text', success : function(data) { $idx = data; } })
<%-- 위치 > 데이터 분류 리스트 --%>
<c:forEach items="${list}" var="category" varStatus="i">
	<c:set var="encCategoryIdx" value="${efn:encrypt(category.categoryIdx)}" />
	<c:if test="${!i.first}">
$source += '<option value="${encCategoryIdx}"' + ($idx != 1 && $idx == '${category.categoryIdx}' ? ' selected' : '') + '>${category.category}</option>';
	</c:if>
</c:forEach>
setTimeout(function() { $('.order-select').html($source); $source = null; }, 100);

$idx = null;
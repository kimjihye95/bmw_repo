<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
var varSource = '';

<%-- 데이터 분류 리스트 --%>
varSource = '';
<c:if test="${empty categoryList}">
varSource += '<div class="panel pd20 text-center">${_infoNoDataMsg_}</div>';
</c:if>
<c:forEach var="categoryModel" items="${categoryList }" varStatus="status">
	<c:set var="data" value="${categoryModel.orderNo }|${categoryModel.parentCategoryIdx}|${categoryModel.categoryLevel}" />
	<c:set var="delData" value="${categoryModel.categoryIdx}_${categoryModel.parentCategoryIdx}" />
	<c:set var="encData" value="${efn:encrypt(data)}" />
	<c:set var="encDelData" value="${efn:encrypt(delData)}" />
	<c:set var="encCategoryIdx" value="${efn:encrypt(categoryModel.categoryIdx)}" />
	<c:set var="categoryName" value="${efn:strContentsFormat(categoryModel.categoryName, '')}" />
varSource += '<div id="panel-${status.count}" class="catPanel panel pd20" data-data="${encCategoryIdx}" data-panel-collapsed data-panel-fullscreen data-panel-close data-panel-locked data-panel-refresh data-panel-reset data-panel-color>';
varSource += '<div class="panel-hdr" style="border-bottom:0px;">';
varSource += '<h2><i class="fal fa-check text-warning mr-2"></i> ${categoryName}';
varSource += '<div class="d-flex position-relative ml-auto" style="max-width: 15rem;">';									
varSource += '<button class="btn btn-warning btn-lg btn-icon rounded-circle waves-effect waves-themed text-white modi-open" data-data="${encCategoryIdx}" data-name="${categoryName}" data-ndata="${encData}" data-category="${categoryModel.category }" data-toggle="modal" data-target="#class_modi"><i class="fal fa-pen"></i></button>';
varSource += '<button data-data="${encDelData}" class="btn btn-danger btn-lg btn-icon rounded-circle waves-effect waves-themed text-white mgl10 btn4del"><i class="fal fa-times"></i></button>';
varSource += '<button class="panel-drag btn4drag btn btn-dark btn-lg btn-icon rounded-circle waves-effect waves-themed text-white mgl10 ui-sortable-handle"><i class="fal fa-arrows"></i></button>';
varSource += '</div>';
varSource += '</h2>';
varSource += '</div>';
varSource += '</div>';
</c:forEach>
$('#categoryWrap').html(varSource);
fn_setPanel();

<%-- varSource = '';
varSource += '<option value="">선택하세요.</option>';
varSource += '<c:forEach var="category" items="${modalCategoryList }">';
	<c:set var="data" value="${category.orderNo }|${category.parentCategoryIdx}|${category.categoryLevel}|${category.categoryIdx}" />
	<c:set var="encData" value="${efn:encrypt(data)}" />
varSource += '<option value="${encData}"<c:if test="${idx eq category.categoryIdx}"> selected</c:if> data-role="${category.categoryLevel}">${category.category }</option>';
varSource += '</c:forEach>';
$('.order-select').html(varSource); --%>

<%-- 정렬 순서 변경 뒤 - 메뉴 트리 재구성 --%>
$('#jstree').remove();
$('#jstreeWrap').html('');
varSource = '';
varSource += '<div id="jstree">';
varSource += '<ul>';
varSource += '<c:forEach items="${categoryList1}" var="rootCatModel" varStatus="x">';
varSource += '<li data-data="${efn:encrypt(rootCatModel.categoryIdx)}" data-lvl="${efn:encrypt(rootCatModel.categoryLevel)}" data-path="${rootCatModel.categoryFullName}" class="jstree-open">${rootCatModel.categoryName}';
varSource += '<ul>'
varSource += '<c:forEach items="${categoryList2}" var="catModel" varStatus="i">';
varSource += '<li data-data="${efn:encrypt(catModel.categoryIdx)}" data-lvl="${efn:encrypt(catModel.categoryLevel)}" data-path="${catModel.categoryFullName}"<c:if test="${i.first}"> class="jstree-open"</c:if>>${efn:strContentsFormat(catModel.categoryName, '')}';
varSource += '</c:forEach>';
varSource += '</ul>';
varSource += '</li>';
varSource += '</c:forEach>';
varSource += '</ul>';
varSource += '</div>';
$('#jstreeWrap').append(varSource);
$('#jstree').jstree({ 'core' : { 'check_callback' : true }, 'plugins' : [ 'types' ], 'types' : { 'default' : { 'icon' : 'fal fa-folder' }, } });
$("#jstree").jstree("open_all");

$currentIdx = "${idx }";
var treeItem = $('.jstree-node');
$(treeItem).children().removeClass('jstree-clicked');
//$(treeItem).eq(0).jstree("open_node", $(treeItem).eq(0).attr('id'));
$(treeItem).each(function(i ,e){
	if(fn_endata($(e).data('data'), 1) == $currentIdx) {
		$('#jstree1').jstree("open_node", $(e).attr('id'));
		$(e).children('.jstree-anchor').attr('aria-selected', true);
		$(e).children('.jstree-anchor').addClass('jstree-clicked');
	}else if($(e).data('data') != $currentIdx && $(e).data('lvl') == 2){
		//$('#jstree1').jstree("close_node", $(e).attr('id'));
	}
})

if("${msg }" != ""){
	toastr['success']("${msg }");
}
$('#categoryWrap').sortable({ containment: '#catWrap', scroll: false });

varSource = null;
treeItem = null;
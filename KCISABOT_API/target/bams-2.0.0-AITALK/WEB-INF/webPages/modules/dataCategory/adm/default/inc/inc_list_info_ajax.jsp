<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file = "../../../../../../../admresources/jsp/commonTop.jsp" %>
<c:set var="data" value="${model.orderNo}|${model.parentCategoryIdx}|${model.categoryLevel}" />
<c:set var="encData" value="${efn:encrypt(data)}" />
$dataCategoryModFrm.find('input[name=idx]').val('${efn:encrypt(model.categoryIdx)}');
$dataCategoryModFrm.find('input[name=categoryName]').val('${model.categoryName}');
$dataCategoryModFrm.find('input[name=ndata]').val('${encData}');

$($dataCategoryModFrm.find('.order-select option')).each(function(i, e){
	if($(this).text() == '${model.category}') {
		$(this).prop('selected', true);
		$('#orderChk11').click();
		$($dataCategoryModFrm.find('.order-select')).click();
	}
});

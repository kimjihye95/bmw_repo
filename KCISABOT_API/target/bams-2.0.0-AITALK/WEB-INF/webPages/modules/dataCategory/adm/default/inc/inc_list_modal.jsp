<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- 데이터 카테고리 등록 start --%>
<div class="modal fade modal4cat" id="class_make" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width:1000px;">
		<div class="modal-content">
			<div class="modal-header inno">
				<h5 class="modal-title">데이터 분류 등록</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<form id="fn_inputFrm" method="post" action="dataCategoryActionProc.do?cmnx=${cmnx}">
					<table class="tbWriteA mgt20" summary="분류 추가 정보 입력입니다.">
						<caption>분류 추가 정보 입력입니다.</caption>
						<colgroup>
							<col width="20%" />
							<col width="80%" />
						</colgroup>

						<tbody class="pd10 alignC">
						<tr>
							<c:set var="modelItem" value="categoryName" />
							<th scope="row"><label for="${modelItem}">분류명</label> <span>*</span></th>
							<td><input type="text" id="${modelItem}" name="${modelItem}" class="form-control make" placeholder="분류명을 입력하세요." value="" minlength="2" maxlength="20"></td>
						</tr>
						<tr>
							<c:set var="modelItem" value="order-select" />
							<th scope="row"><label for="${modelItem}">위치</label><span>*</span></th>
							<td>
								<div class="custom-radio custom-control-inline">
									<select class="form-control order-select order-select-insert" id="${modelItem}" name="${modelItem}" style="max-width:500px;">
										<option value="">선택하세요.</option>
										<c:forEach var="category" items="${modalCategoryList }">
											<option value="${category.orderNo }|${category.parentCategoryIdx}|${category.categoryLevel}|${category.categoryIdx}">${category.category }</option>
										</c:forEach>
									</select>
								</div>
								<c:set var="modelItem" value="orderChk" />
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="${modelItem}1" name="${modelItem}"  checked value="${efn:encrypt('P')}">
									<label class="custom-control-label" for="${modelItem}1">위</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="${modelItem}2" name="${modelItem}" value="${efn:encrypt('N')}">
									<label class="custom-control-label" for="${modelItem}2">아래</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="${modelItem}3" name="${modelItem}" value="${efn:encrypt('I')}">
									<label class="custom-control-label" for="${modelItem}3">하위</label>
								</div>
								<input type="hidden" id="idx" name="idx" value=""/>
								<input type="hidden" name="categoryLevel" value=""/>
								<input type="hidden" name="orderNo" value=""/>
								<input type="hidden" name="parentCategoryIdx" value=""/>
								<input type="hidden" name="cnmx" value="${cmnx}" />
							</td>
						</tr>
						</tbody>
					</table>
				</form>

			</div>
			<div class="modal-footer inno pdt0">
				<button type="button" data-role="r" class="btn4submit btn btn-lg btn-warning text-white">등록</button>
				<button type="button" class="btn btn-lg btn-secondary" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</div>
<%-- // 데이터 카테고리 등록 end --%>

<%-- 데이터 카테고리 수정 start --%>
<div class="modal fade modal4cat" id="class_modi" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width:1000px;">
		<div class="modal-content">
			<div class="modal-header inno">
				<h5 class="modal-title">데이터 분류 수정</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fal fa-times"></i></span>
				</button>
			</div>
			<div class="modal-body">
				<form id="fn_modFrm" method="post" action="dataCategoryActionProc.do?cmnx=${cmnx}">
					<table class="tbWriteA mgt20" summary="분류 추가 정보 입력입니다.">
						<caption>분류 추가 정보 입력입니다.</caption>
						<colgroup>
							<col width="20%" />
							<col width="80%" />
						</colgroup>

						<tbody class="pd10 alignC">
						<tr>
							<c:set var="modelItem" value="categoryName" />
							<th scope="row"><label for="${modelItem}1">분류명</label> <span>*</span></th>
							<td><input type="text" id="${modelItem}1" name="${modelItem}" class="form-control" placeholder="분류명을 입력하세요." value="" minlength="2" maxlength="20"></td>
						</tr>
						<tr>
							<c:set var="modelItem" value="order-select" />
							<th scope="row"><label for="${modelItem}">위치</label> <span>*</span></th>
							<td>
								<div class="custom-radio custom-control-inline">
									<select class="form-control order-select order-select-modify" id="${modelItem}1" name="${modelItem}" style="max-width:500px;"></select>
								</div>
								<c:set var="modelItem" value="orderChk" />
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="${modelItem}11" name="${modelItem}"  checked value="${efn:encrypt('P')}">
									<label class="custom-control-label" for="${modelItem}11">위</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="${modelItem}12" name="${modelItem}" value="${efn:encrypt('N')}">
									<label class="custom-control-label" for="${modelItem}12">아래</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="${modelItem}13" name="${modelItem}" value="${efn:encrypt('I')}">
									<label class="custom-control-label" for="${modelItem}13">하위</label>
								</div>
								<input type="hidden" name="cnmx" value="${cmnx}" />
								<input type="hidden" name="idx" value=""/>
								<input type="hidden" name="ndata" value=""/>
							</td>
						</tr>
						</tbody>
					</table>
				</form>

			</div>
			<div class="modal-footer inno pdt0">
				<button type="button" data-role="m" class="btn4submit btn btn-lg btn-warning text-white">수정</button>
				<button type="button" class="btn btn-lg btn-secondary" data-dismiss="modal">취소</button>
			</div>
		</div>
	</div>
</div>
<%-- // 데이터 카테고리 수정 end --%>

<script>
<%-- 분류명 엔터키 이벤트 --%>
$(document).on('keypress', 'input[name=categoryName]', function(e) {
	if (e.keyCode == 13) {
		var $isMake = $(this).hasClass('make');
		var $index = ($isMake ? 0 : 1);
		$('.btn4submit').eq($index).click();
		return false;
	}
});
	<%-- 데이터셋 확인 버튼 클릭 이벤트 --%>
	var $modal;
	const $dataCategoryFrm = $('#fn_inputFrm'), $dataCategoryModFrm = $('#fn_modFrm');
	$(document).on('click', '.btn4submit', function() {
		var $data, $role = $(this).data('role'), $frm = ($role == 'r') ? $dataCategoryFrm : $dataCategoryModFrm;
		var $categoryName = 'categoryName', $locSelect = 'order-select';
		var $action = $frm.attr('action');
		
		if (!fn_checkFill($frm, $categoryName)) return false;
		if (!fn_checkSelect($frm, $locSelect)) return false;
		if ($frm.valid()) {

			var $categoryNameData = $frm.find('input[name=categoryName]').val();
			if (/[^(0-9가-힣ㄱ-ㅎㅏ-ㅣa-z\&\.\(\))\/\_ ]/gi.test($categoryNameData)) {
				var $opt = { title : '알림', msg : '한글, 영문, 숫자, 특수문자 (.&_/) 만 입력이 가능합니다.', icon : 'warning' };
				fn_openSweetAlert($opt);
				return false;
			}

			$data = $frm.serialize();
			fn_ajax({ url : $action, data : $data, type : 'POST' });
		} 

		return false;
	});

	<%-- set validation --%>
	var $validatorOpt = { id : ['idx', 'categoryName'], min : [ 0, 2 ], max : [ 0, 20 ] };
	fn_setValidator($dataCategoryFrm, $validatorOpt);

	<%-- 데이터 분류 등록 모달 열기 이벤트 --%>
	$(document).on('show.bs.modal', '#class_make', function() {
		$dataCategoryFrm.find('.order-select').click();
	});
	<%-- 데이터 분류 등록 모달 닫기 이벤트 --%>
	$(document).on('hidden.bs.modal', '#class_make', function() {
		$dataCategoryFrm[0].reset();
		$(this).find('input[name=orderChk]').prop('disabled', false);
	});

	<%-- 데이터 분류 추가 / 수정 모달 > 위치 선택 이벤트 --%>
	$(document).on('click', '.order-select', function() {
		var $role = $(this).children('option:selected').data('role');
		var $isMod = $(this).hasClass('order-select-modify');
		var $allRadioBtn = ($isMod) ? $('#orderChk11, #orderChk12, #orderChk13') : $('#orderChk1, #orderChk2, #orderChk3');
		var $udRadioBtn = ($isMod) ? $('#orderChk11, #orderChk12') : $('#orderChk1, #orderChk2');
		var $radioBtn = ($isMod) ? $('#orderChk13') : $('#orderChk3');
		var $altRadioBtn = ($isMod) ? $('#orderChk12') : $('#orderChk2');
		$($allRadioBtn).prop('disabled', false);<%-- 라디오 버튼 disabled 전체 해제 --%>
		if ($role == 1) {
			$($udRadioBtn).prop('disabled', true);<%-- 레벨 1일 경우 라디오 버튼 '위, 아래' disable --%>
			$($radioBtn).prop('checked', true);
		}
		else if ($role == 3) {
			$($radioBtn).prop('disabled', true);<%-- 레벨 3일 경우 라디오 버튼 '하위' disable --%>
			if ($radioBtn.is(':checked')) $($altRadioBtn).prop('checked', true);<%-- 레벨 3 && 하위 체크일 경우 '위' 항목으로 체크 --%>
		}
	});
</script>
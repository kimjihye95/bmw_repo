<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../cresources/jsp/commonTop.jsp"%>
<%-- Admin Path --%>
<spring:eval var="_adm_path_" expression="@environment.getProperty('spring.Main.Root.Path')"/>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<title>Big Error - Page Views - SmartAdmin v4.5.1</title>
	<meta name="description" content="Big Error">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="msapplication-tap-highlight" content="no">
	<link id="vendorsbundle" rel="stylesheet" media="screen, print" href="/admresources/css/vendors.bundle.css">
	<link id="appbundle" rel="stylesheet" media="screen, print" href="/admresources/css/app.bundle.css">
	<link id="mytheme" rel="stylesheet" media="screen, print" href="#">
</head>
<body>
	<div class="page-wrapper alt">
		<div class="h-alt-f d-flex flex-column align-items-center justify-content-center text-center">
			<h1 class="page-error color-fusion-500">
				ERROR <span class="text-gradient">${requestScope['javax.servlet.error.status_code']}</span>
				<small class="fw-500">Ooops! Something went wrong!</small>
			</h1>
			<h4>error message : ${requestScope['javax.servlet.error.message']}</h4>
			<h4>error exception : ${requestScope['javax.servlet.error.exception']}</h4>
			<a href="${_adm_path_}" class="btn btn-lg btn-primary mt-3">메인으로 돌아가기</a>
		</div>
	</div>
</body>
</html>
